-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 04:10 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simple_solution`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('85optpigas2r2vbmolo29ehjlrnm85td', '::1', 1557378140, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535373337333533333b757365725f73657373696f6e7c4f3a383a22737464436c617373223a32383a7b733a373a22757365725f6964223b733a313a2231223b733a31303a2266697273745f6e616d65223b733a363a224c6f6d657368223b733a393a226c6173745f6e616d65223b733a393a224b656c7761646b6172223b733a31333a22656d61696c5f61646472657373223b733a32323a226c6f6d65736835333837353540676d61696c2e636f6d223b733a31333a22757365725f70617373776f7264223b733a33323a226531306164633339343962613539616262653536653035376632306638383365223b733a31373a227374616e646172645f636c6173735f6964223b733a313a2239223b733a31363a227375626a6563745f636c6173735f6964223b733a313a2232223b733a31323a226f7267616e697a6174696f6e223b733a343a2274657374223b733a32353a226f7267616e697a6174696f6e5f70686f6e655f6e756d626572223b733a303a22223b733a31303a22796f75725f7469746c65223b733a343a2274657374223b733a31303a22796f75725f70686f6e65223b733a383a223132333435363738223b733a373a2261646472657373223b733a303a22223b733a31363a22746561726d5f636f6e646974696f6e73223b733a313a2230223b733a31333a2270726f66696c655f70686f746f223b733a32313a222f6d656469612f70726f66696c652f61332e6a7067223b733a393a22757365725f74797065223b733a313a2231223b733a31343a227363686f6f6c5f636f6c6c616765223b733a31333a2243656e7472616c20506f696e74223b733a31373a2269735f70726f66696c655f737461747573223b733a313a2231223b733a31353a22726567697374657265645f64617465223b733a31393a22323031392d30342d30322031383a32393a3134223b733a31333a22617070726f7665645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31323a227061796d656e745f6d6f6465223b733a303a22223b733a32313a22766572696669636174696f6e5f62795f656d61696c223b733a33323a223966633662626663386263373532353030323061363761386531613639643634223b733a32383a22766572696669636174696f6e5f62795f656d61696c5f737461747573223b733a313a2231223b733a31383a2272657365745f70617373776f72645f737472223b733a32353a22306f464d7a64736f375666526d525162303578716e75523531223b733a31303a2269735f64656c65746564223b733a313a2230223b733a373a2263726561746564223b733a31393a22323031392d30342d30322031383a32393a3134223b733a383a226d6f646966696564223b733a31393a22323031392d30352d30382030383a34333a3334223b733a31303a22637265617465645f6279223b733a303a22223b733a31313a226d6f6469666965645f6279223b733a303a22223b7d6c6173745f73657373696f6e5f69647c693a3133383b),
('estu1bm3bo63o9rrlo940ktvbrnfd3un', '::1', 1557379571, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535373337393537303b);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_button_pallate`
--

CREATE TABLE `tbl_button_pallate` (
  `id` int(11) NOT NULL,
  `que_no` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `test_id` int(11) NOT NULL DEFAULT '0',
  `className` varchar(200) COLLATE utf16_unicode_ci NOT NULL,
  `subject` varchar(100) COLLATE utf16_unicode_ci NOT NULL,
  `save_time` varchar(100) COLLATE utf16_unicode_ci NOT NULL,
  `ans_time` varchar(100) COLLATE utf16_unicode_ci NOT NULL,
  `que_id` int(11) DEFAULT '0',
  `subjectname` varchar(255) COLLATE utf16_unicode_ci DEFAULT NULL,
  `fragment` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `tbl_button_pallate`
--

INSERT INTO `tbl_button_pallate` (`id`, `que_no`, `user_id`, `test_id`, `className`, `subject`, `save_time`, `ans_time`, `que_id`, `subjectname`, `fragment`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 26, 'btnanswered', '1', '19:41', '3', 22, NULL, 1, '2019-05-07 02:56:59', '2019-05-07 02:56:59'),
(2, 2, 5, 26, 'btnanswered', '1', '19:38', '3', 23, NULL, 2, '2019-05-07 02:57:04', '2019-05-07 02:57:04'),
(3, 3, 5, 26, 'btnanswered', '1', '19:36', '2', 24, NULL, 3, '2019-05-07 02:57:09', '2019-05-07 02:57:09'),
(4, 4, 5, 26, 'btnnotanswered', '1', '19:33', '3', 0, NULL, 4, '2019-05-07 02:57:12', '2019-05-07 02:57:12'),
(5, 5, 5, 26, 'btnmarked', '1', '19:0', '34', 26, NULL, 5, '2019-05-07 02:57:46', '2019-05-07 02:57:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_completed_test`
--

CREATE TABLE `tbl_completed_test` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `test_id` int(11) NOT NULL DEFAULT '0',
  `total_question` int(11) DEFAULT '0',
  `max_mark` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `total_mark` float DEFAULT NULL,
  `total_wrong_que` int(11) DEFAULT NULL,
  `total_right_que` int(11) DEFAULT '0',
  `total_right_mark` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `total_wrong_mark` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `test_duration` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `time_taken` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `attempt_question` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `not_attempt_question` varchar(100) COLLATE utf16_unicode_ci DEFAULT NULL,
  `created` varchar(100) COLLATE utf16_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `tbl_completed_test`
--

INSERT INTO `tbl_completed_test` (`id`, `user_id`, `test_id`, `total_question`, `max_mark`, `total_mark`, `total_wrong_que`, `total_right_que`, `total_right_mark`, `total_wrong_mark`, `test_duration`, `time_taken`, `attempt_question`, `not_attempt_question`, `created`, `created_at`, `updated_at`) VALUES
(1, 5, 26, 5, '10', 4, 2, 2, '4', '0', NULL, '1:10', '4', '1', '07/05/2019', '2019-05-07 02:58:01', '2019-05-07 02:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_question`
--

CREATE TABLE `tbl_exam_question` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_status` int(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invited_exam_requests`
--

CREATE TABLE `tbl_invited_exam_requests` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `request_status` int(11) NOT NULL COMMENT '1 - Accept, 2 - Decline, 3 - Start, 4 - Complete',
  `request_date_time` datetime NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invited_exam_requests`
--

INSERT INTO `tbl_invited_exam_requests` (`id`, `student_id`, `teacher_id`, `exam_id`, `request_status`, `request_date_time`, `is_deleted`, `created`) VALUES
(1, 5, 1, 26, 4, '2019-05-04 14:24:00', 0, '2019-05-05 02:48:05'),
(37, 1, 1, 45, 1, '2019-05-09 00:00:00', 0, '2019-05-09 04:53:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invite_new_student`
--

CREATE TABLE `tbl_invite_new_student` (
  `invite_new_id` bigint(20) NOT NULL,
  `student_name` varchar(200) NOT NULL,
  `student_email` varchar(100) NOT NULL,
  `teacher_id` bigint(20) NOT NULL,
  `exam_id` bigint(20) NOT NULL,
  `invite_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_manage_folder`
--

CREATE TABLE `tbl_manage_folder` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(100) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module`
--

CREATE TABLE `tbl_module` (
  `_id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `faIcon` varchar(255) NOT NULL,
  `Controller_name` varchar(255) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `is_all` int(11) NOT NULL,
  `module_type` int(11) NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_module`
--

INSERT INTO `tbl_module` (`_id`, `module_name`, `faIcon`, `Controller_name`, `method_name`, `status`, `is_all`, `module_type`, `section_id`) VALUES
(1, 'Standard', '', 'MasterController', 'StandardExecute', 1, 1, 1, 1),
(2, 'General', '', 'SystemSettingController', 'GeneralSettingExecute', 1, 1, 1, 9),
(3, 'All Student List', '', 'StudentsController', 'getAllStudents', 1, 1, 1, 2),
(4, 'All Approved', '', 'StudentsController', 'getAllApprovedStudents', 1, 1, 1, 2),
(5, 'All Nonapproved', '', 'StudentsController', 'getAllNonApprovedStudents', 1, 1, 1, 5555),
(6, 'Pending', '', 'StudentsController', 'getAllDisapprovedStudents', 1, 1, 1, 2),
(7, 'All Teachers List', '', 'TeachersController', 'getAllTeachers', 1, 1, 1, 3),
(8, 'All Approved', '', 'TeachersController', 'getAllApprovedTeachers', 1, 1, 1, 3),
(9, 'All Non Approved', '', 'TeachersController', 'getAllNonApprovedTeachers', 1, 1, 1, 3),
(10, 'All Disapproved', '', 'TeachersController', 'getAllDisapprovedTeachers', 1, 1, 1, 3),
(11, 'Subject', '', 'MasterController', 'SubjectExecute', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_anser`
--

CREATE TABLE `tbl_question_anser` (
  `que_ans_id` bigint(20) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_anser`
--

INSERT INTO `tbl_question_anser` (`que_ans_id`, `question_id`, `option_answer_id`) VALUES
(1, 1, 1),
(2, 2, 6),
(3, 12, 17),
(4, 12, 18),
(5, 17, 19),
(6, 21, 25),
(7, 22, 27),
(8, 23, 29),
(9, 24, 34),
(10, 24, 35),
(11, 25, 38),
(12, 27, 43),
(13, 28, 45),
(14, 30, 54),
(15, 31, 56),
(16, 33, 58),
(17, 35, 64),
(18, 36, 67),
(19, 37, 72),
(20, 39, 77),
(21, 41, 79),
(22, 42, 81),
(23, 51, 83),
(24, 53, 85),
(25, 61, 108),
(26, 62, 111),
(27, 62, 112),
(28, 65, 116),
(29, 68, 127),
(30, 71, 135),
(31, 87, 158),
(32, 89, 163),
(33, 96, 168),
(34, 97, 169);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_option_1`
--

CREATE TABLE `tbl_question_option_1` (
  `question_option_id` int(11) NOT NULL,
  `que_id` int(11) NOT NULL,
  `que_option` varchar(200) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_option_1`
--

INSERT INTO `tbl_question_option_1` (`question_option_id`, `que_id`, `que_option`, `is_status`, `is_deleted`, `created`, `modified`) VALUES
(1, 1, 'True', 0, 0, '2019-05-01 11:36:22', '2019-05-01 11:36:22'),
(2, 1, 'False', 0, 0, '2019-05-01 11:36:22', '2019-05-01 11:36:22'),
(3, 2, 'Nagpur', 0, 0, '2019-05-01 11:45:20', '2019-05-01 11:45:20'),
(4, 2, 'Pune', 0, 0, '2019-05-01 11:45:20', '2019-05-01 11:45:20'),
(5, 2, 'mumbai', 0, 0, '2019-05-01 11:45:20', '2019-05-01 11:45:20'),
(6, 2, 'Delhi', 0, 0, '2019-05-01 11:45:20', '2019-05-01 11:45:20'),
(7, 10, 'te', 0, 0, '2019-05-01 12:00:32', '2019-05-01 12:00:32'),
(8, 10, 't', 0, 0, '2019-05-01 12:00:32', '2019-05-01 12:00:32'),
(9, 10, 'h', 0, 0, '2019-05-01 12:00:32', '2019-05-01 12:00:32'),
(10, 10, 'f', 0, 0, '2019-05-01 12:00:32', '2019-05-01 12:00:32'),
(11, 11, 'te', 0, 0, '2019-05-01 12:00:43', '2019-05-01 12:00:43'),
(12, 11, 't', 0, 0, '2019-05-01 12:00:43', '2019-05-01 12:00:43'),
(13, 11, 'h', 0, 0, '2019-05-01 12:00:43', '2019-05-01 12:00:43'),
(14, 11, 'f', 0, 0, '2019-05-01 12:00:43', '2019-05-01 12:00:43'),
(15, 12, 'te', 0, 0, '2019-05-01 12:02:22', '2019-05-01 12:02:22'),
(16, 12, 't', 0, 0, '2019-05-01 12:02:22', '2019-05-01 12:02:22'),
(17, 12, 'fdf', 0, 0, '2019-05-01 12:02:22', '2019-05-01 12:02:22'),
(18, 12, 'fdf', 0, 0, '2019-05-01 12:02:22', '2019-05-01 12:02:22'),
(19, 17, 'short_ans short_ans\r\nshort_ans \r\nshort_ans short_ans', 0, 0, '2019-05-01 12:09:58', '2019-05-01 12:09:58'),
(20, 19, '1', 0, 0, '2019-05-01 13:19:33', '2019-05-01 13:19:33'),
(21, 20, '1', 0, 0, '2019-05-01 13:20:10', '2019-05-01 13:20:10'),
(22, 20, '3', 0, 0, '2019-05-01 13:20:10', '2019-05-01 13:20:10'),
(23, 20, '5', 0, 0, '2019-05-01 13:20:10', '2019-05-01 13:20:10'),
(24, 20, '7', 0, 0, '2019-05-01 13:20:10', '2019-05-01 13:20:10'),
(25, 21, 'True', 0, 0, '2019-05-01 14:34:05', '2019-05-01 14:34:05'),
(26, 21, 'False', 0, 0, '2019-05-01 14:34:05', '2019-05-01 14:34:05'),
(27, 22, 'True', 0, 0, '2019-05-02 02:55:04', '2019-05-02 02:55:04'),
(28, 22, 'False', 0, 0, '2019-05-02 02:55:04', '2019-05-02 02:55:04'),
(29, 23, '1', 0, 0, '2019-05-02 02:55:37', '2019-05-02 02:55:37'),
(30, 23, '2', 0, 0, '2019-05-02 02:55:37', '2019-05-02 02:55:37'),
(31, 23, '3', 0, 0, '2019-05-02 02:55:37', '2019-05-02 02:55:37'),
(32, 23, '4', 0, 0, '2019-05-02 02:55:37', '2019-05-02 02:55:37'),
(33, 23, '5', 0, 0, '2019-05-02 02:55:37', '2019-05-02 02:55:37'),
(34, 24, 'te', 0, 0, '2019-05-02 02:56:07', '2019-05-02 02:56:07'),
(35, 24, 't', 0, 0, '2019-05-02 02:56:07', '2019-05-02 02:56:07'),
(36, 24, 'fdf', 0, 0, '2019-05-02 02:56:07', '2019-05-02 02:56:07'),
(37, 24, 'fdf', 0, 0, '2019-05-02 02:56:07', '2019-05-02 02:56:07'),
(38, 25, 'ht', 0, 0, '2019-05-02 02:56:24', '2019-05-02 02:56:24'),
(39, 26, '5', 0, 0, '2019-05-02 02:56:50', '2019-05-02 02:56:50'),
(40, 26, '5', 0, 0, '2019-05-02 02:56:50', '2019-05-02 02:56:50'),
(41, 26, '8', 0, 0, '2019-05-02 02:56:50', '2019-05-02 02:56:50'),
(42, 26, '7', 0, 0, '2019-05-02 02:56:50', '2019-05-02 02:56:50'),
(43, 27, 'True', 0, 0, '2019-05-02 10:00:32', '2019-05-02 10:00:32'),
(44, 27, 'False', 0, 0, '2019-05-02 10:00:32', '2019-05-02 10:00:32'),
(45, 28, 'one', 0, 0, '2019-05-02 10:04:56', '2019-05-02 10:04:56'),
(46, 28, 'two', 0, 0, '2019-05-02 10:04:56', '2019-05-02 10:04:56'),
(47, 28, 'three', 0, 0, '2019-05-02 10:04:56', '2019-05-02 10:04:56'),
(48, 28, 'four', 0, 0, '2019-05-02 10:04:56', '2019-05-02 10:04:56'),
(49, 28, 'five', 0, 0, '2019-05-02 10:04:56', '2019-05-02 10:04:56'),
(50, 29, 'apple', 0, 0, '2019-05-02 10:08:34', '2019-05-02 10:08:34'),
(51, 29, 'ball', 0, 0, '2019-05-02 10:08:34', '2019-05-02 10:08:34'),
(52, 29, 'cat', 0, 0, '2019-05-02 10:08:34', '2019-05-02 10:08:34'),
(53, 29, 'dog', 0, 0, '2019-05-02 10:08:34', '2019-05-02 10:08:34'),
(54, 30, 'True', 0, 0, '2019-05-06 07:09:50', '2019-05-06 07:09:50'),
(55, 30, 'False', 0, 0, '2019-05-06 07:09:50', '2019-05-06 07:09:50'),
(56, 31, 'True', 0, 0, '2019-05-07 06:06:26', '2019-05-07 06:06:26'),
(57, 31, 'False', 0, 0, '2019-05-07 06:06:26', '2019-05-07 06:06:26'),
(58, 33, 'True', 0, 0, '2019-05-07 06:21:44', '2019-05-07 06:21:44'),
(59, 33, 'False', 0, 0, '2019-05-07 06:21:44', '2019-05-07 06:21:44'),
(60, 34, 'Test implementation and execution', 0, 0, '2019-05-07 06:23:46', '2019-05-07 06:23:46'),
(61, 34, 'Test planning and control', 0, 0, '2019-05-07 06:23:46', '2019-05-07 06:23:46'),
(62, 34, 'Test analysis and design', 0, 0, '2019-05-07 06:23:46', '2019-05-07 06:23:46'),
(63, 35, 'Performed by customers at their own site', 0, 0, '2019-05-07 06:26:33', '2019-05-07 06:26:33'),
(64, 35, 'Performed by customers at their software developer’s site', 0, 0, '2019-05-07 06:26:33', '2019-05-07 06:26:33'),
(65, 35, 'Performed by an independent test team', 0, 0, '2019-05-07 06:26:33', '2019-05-07 06:26:33'),
(66, 35, 'Useful to test bespoke software', 0, 0, '2019-05-07 06:26:33', '2019-05-07 06:26:33'),
(67, 36, 'True', 0, 0, '2019-05-07 06:29:31', '2019-05-07 06:29:31'),
(68, 36, 'False', 0, 0, '2019-05-07 06:29:31', '2019-05-07 06:29:31'),
(69, 37, 'error guessing', 0, 0, '2019-05-07 06:38:17', '2019-05-07 06:38:17'),
(70, 37, 'Walkthrough', 0, 0, '2019-05-07 06:38:17', '2019-05-07 06:38:17'),
(71, 37, 'Data flow analysis', 0, 0, '2019-05-07 06:38:17', '2019-05-07 06:38:17'),
(72, 37, 'Inspections', 0, 0, '2019-05-07 06:38:17', '2019-05-07 06:38:17'),
(73, 38, 'apple', 0, 0, '2019-05-07 06:40:17', '2019-05-07 06:40:17'),
(74, 38, 'ball', 0, 0, '2019-05-07 06:40:17', '2019-05-07 06:40:17'),
(75, 38, 'cat', 0, 0, '2019-05-07 06:40:17', '2019-05-07 06:40:17'),
(76, 38, 'dog', 0, 0, '2019-05-07 06:40:17', '2019-05-07 06:40:17'),
(77, 39, 'True', 0, 0, '2019-05-07 07:53:31', '2019-05-07 07:53:31'),
(78, 39, 'False', 0, 0, '2019-05-07 07:53:31', '2019-05-07 07:53:31'),
(79, 41, 'True', 0, 0, '2019-05-07 09:35:03', '2019-05-07 09:35:03'),
(80, 41, 'False', 0, 0, '2019-05-07 09:35:03', '2019-05-07 09:35:03'),
(81, 42, 'True', 0, 0, '2019-05-07 09:39:39', '2019-05-07 09:39:39'),
(82, 42, 'False', 0, 0, '2019-05-07 09:39:39', '2019-05-07 09:39:39'),
(83, 51, 'True', 0, 0, '2019-05-07 09:47:21', '2019-05-07 09:47:21'),
(84, 51, 'False', 0, 0, '2019-05-07 09:47:21', '2019-05-07 09:47:21'),
(85, 53, 'True', 0, 0, '2019-05-07 09:55:24', '2019-05-07 09:55:24'),
(86, 53, 'False', 0, 0, '2019-05-07 09:55:24', '2019-05-07 09:55:24'),
(87, 56, 'apple', 0, 0, '2019-05-07 10:02:51', '2019-05-07 10:02:51'),
(88, 56, 'ball', 0, 0, '2019-05-07 10:02:51', '2019-05-07 10:02:51'),
(89, 56, 'cat', 0, 0, '2019-05-07 10:02:51', '2019-05-07 10:02:51'),
(90, 56, 'dog', 0, 0, '2019-05-07 10:02:51', '2019-05-07 10:02:51'),
(91, 57, 'apple', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(92, 57, 'ball', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(93, 57, 'cat', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(94, 57, 'dog', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(95, 58, 'apple', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(96, 58, 'ball', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(97, 58, 'cat', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(98, 58, 'dog', 0, 0, '2019-05-07 10:02:56', '2019-05-07 10:02:56'),
(99, 59, 'apple', 0, 0, '2019-05-07 10:02:57', '2019-05-07 10:02:57'),
(100, 59, 'ball', 0, 0, '2019-05-07 10:02:57', '2019-05-07 10:02:57'),
(101, 59, 'cat', 0, 0, '2019-05-07 10:02:57', '2019-05-07 10:02:57'),
(102, 59, 'dog', 0, 0, '2019-05-07 10:02:57', '2019-05-07 10:02:57'),
(103, 60, 'apple', 0, 0, '2019-05-07 10:03:01', '2019-05-07 10:03:01'),
(104, 60, 'ball', 0, 0, '2019-05-07 10:03:01', '2019-05-07 10:03:01'),
(105, 60, 'cat', 0, 0, '2019-05-07 10:03:01', '2019-05-07 10:03:01'),
(106, 60, 'dog', 0, 0, '2019-05-07 10:03:01', '2019-05-07 10:03:01'),
(107, 61, 'apple', 0, 0, '2019-05-07 10:07:31', '2019-05-07 10:07:31'),
(108, 61, 'ball', 0, 0, '2019-05-07 10:07:31', '2019-05-07 10:07:31'),
(109, 61, 'cat', 0, 0, '2019-05-07 10:07:31', '2019-05-07 10:07:31'),
(110, 61, 'dog', 0, 0, '2019-05-07 10:07:31', '2019-05-07 10:07:31'),
(111, 62, 'Error guessing', 0, 0, '2019-05-07 10:21:01', '2019-05-07 10:21:01'),
(112, 62, 'Walkthrough', 0, 0, '2019-05-07 10:21:01', '2019-05-07 10:21:01'),
(113, 62, 'Data flow analysis', 0, 0, '2019-05-07 10:21:01', '2019-05-07 10:21:01'),
(114, 62, 'Inspections', 0, 0, '2019-05-07 10:21:01', '2019-05-07 10:21:01'),
(115, 64, 'Error guessing', 0, 0, '2019-05-07 10:22:23', '2019-05-07 10:22:23'),
(116, 65, 'Error guessing', 0, 0, '2019-05-07 10:22:59', '2019-05-07 10:22:59'),
(117, 66, 'Error guessing', 0, 0, '2019-05-07 10:26:03', '2019-05-07 10:26:03'),
(118, 66, 'Performed by customers at their software developer’s site', 0, 0, '2019-05-07 10:26:03', '2019-05-07 10:26:03'),
(119, 66, 'tes', 0, 0, '2019-05-07 10:26:03', '2019-05-07 10:26:03'),
(120, 66, 'Useful to test bespoke software', 0, 0, '2019-05-07 10:26:03', '2019-05-07 10:26:03'),
(121, 67, 'test1', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(122, 67, 'test2', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(123, 67, 'test3', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(124, 67, 'test3', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(125, 67, 'test4', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(126, 67, 'test5', 0, 0, '2019-05-07 10:26:56', '2019-05-07 10:26:56'),
(127, 68, 'True', 0, 0, '2019-05-07 11:40:02', '2019-05-07 11:40:02'),
(128, 68, 'False', 0, 0, '2019-05-07 11:40:02', '2019-05-07 11:40:02'),
(129, 69, 'a', 0, 0, '2019-05-07 11:41:21', '2019-05-07 11:41:21'),
(130, 70, 'a', 0, 0, '2019-05-07 11:41:39', '2019-05-07 11:41:39'),
(131, 70, 'b', 0, 0, '2019-05-07 11:41:39', '2019-05-07 11:41:39'),
(132, 70, 'c', 0, 0, '2019-05-07 11:41:39', '2019-05-07 11:41:39'),
(133, 70, 'd', 0, 0, '2019-05-07 11:41:39', '2019-05-07 11:41:39'),
(134, 71, 'a', 0, 0, '2019-05-07 11:41:49', '2019-05-07 11:41:49'),
(135, 71, 'b', 0, 0, '2019-05-07 11:41:49', '2019-05-07 11:41:49'),
(136, 71, 'c', 0, 0, '2019-05-07 11:41:49', '2019-05-07 11:41:49'),
(137, 71, 'd', 0, 0, '2019-05-07 11:41:49', '2019-05-07 11:41:49'),
(138, 72, 'a', 0, 0, '2019-05-07 11:42:55', '2019-05-07 11:42:55'),
(139, 73, 'a', 0, 0, '2019-05-07 11:43:27', '2019-05-07 11:43:27'),
(140, 74, 'a', 0, 0, '2019-05-07 11:43:49', '2019-05-07 11:43:49'),
(141, 75, 'a', 0, 0, '2019-05-07 11:43:56', '2019-05-07 11:43:56'),
(142, 76, 'a', 0, 0, '2019-05-07 11:44:10', '2019-05-07 11:44:10'),
(143, 77, 'a', 0, 0, '2019-05-07 11:44:20', '2019-05-07 11:44:20'),
(144, 78, 'a', 0, 0, '2019-05-07 11:44:30', '2019-05-07 11:44:30'),
(145, 79, 'a', 0, 0, '2019-05-07 11:44:39', '2019-05-07 11:44:39'),
(146, 80, 'a', 0, 0, '2019-05-07 11:44:54', '2019-05-07 11:44:54'),
(147, 81, 'a', 0, 0, '2019-05-07 11:45:13', '2019-05-07 11:45:13'),
(148, 82, 'a', 0, 0, '2019-05-07 11:45:27', '2019-05-07 11:45:27'),
(149, 83, 'a', 0, 0, '2019-05-07 11:45:38', '2019-05-07 11:45:38'),
(150, 85, 'apple', 0, 0, '2019-05-07 12:10:35', '2019-05-07 12:10:35'),
(151, 85, 'ball', 0, 0, '2019-05-07 12:10:35', '2019-05-07 12:10:35'),
(152, 85, 'cat', 0, 0, '2019-05-07 12:10:35', '2019-05-07 12:10:35'),
(153, 85, 'dog', 0, 0, '2019-05-07 12:10:35', '2019-05-07 12:10:35'),
(154, 86, 'apple', 0, 0, '2019-05-07 12:12:26', '2019-05-07 12:12:26'),
(155, 86, 'ball', 0, 0, '2019-05-07 12:12:26', '2019-05-07 12:12:26'),
(156, 86, 'cat', 0, 0, '2019-05-07 12:12:26', '2019-05-07 12:12:26'),
(157, 86, 'dog', 0, 0, '2019-05-07 12:12:26', '2019-05-07 12:12:26'),
(158, 87, 'testing testingtestingtestingtestingtestingtestingtestingtesting testing testingtestingtestingtestingtestingtestingtestingtesting testing testingtestingtestingtestingtestingtestingtestingtesting testi', 0, 0, '2019-05-07 12:13:29', '2019-05-07 12:13:29'),
(159, 88, 'a', 0, 0, '2019-05-07 12:19:45', '2019-05-07 12:19:45'),
(160, 88, 'b', 0, 0, '2019-05-07 12:19:45', '2019-05-07 12:19:45'),
(161, 88, 'c', 0, 0, '2019-05-07 12:19:45', '2019-05-07 12:19:45'),
(162, 88, 'd', 0, 0, '2019-05-07 12:19:45', '2019-05-07 12:19:45'),
(163, 89, 'True', 0, 0, '2019-05-07 16:10:31', '2019-05-07 16:10:31'),
(164, 89, 'False', 0, 0, '2019-05-07 16:10:32', '2019-05-07 16:10:32'),
(165, 96, 'Nagpur', 0, 0, '2019-05-09 04:26:13', '2019-05-09 04:26:13'),
(166, 96, 'Pune', 0, 0, '2019-05-09 04:26:13', '2019-05-09 04:26:13'),
(167, 96, '3', 0, 0, '2019-05-09 04:26:13', '2019-05-09 04:26:13'),
(168, 96, 'Delhi', 0, 0, '2019-05-09 04:26:13', '2019-05-09 04:26:13'),
(169, 97, 'True', 0, 0, '2019-05-09 04:26:20', '2019-05-09 04:26:20'),
(170, 97, 'False', 0, 0, '2019-05-09 04:26:20', '2019-05-09 04:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question_pair`
--

CREATE TABLE `tbl_question_pair` (
  `pair_id` bigint(20) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `pair_option` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_question_pair`
--

INSERT INTO `tbl_question_pair` (`pair_id`, `question_id`, `option_id`, `pair_option`) VALUES
(1, 20, 21, '2'),
(2, 20, 22, '4'),
(3, 20, 23, '6'),
(4, 20, 24, '8'),
(5, 26, 39, '5'),
(6, 26, 40, '5'),
(7, 26, 41, '9'),
(8, 26, 42, '8'),
(9, 29, 50, 'a'),
(10, 29, 51, 'b'),
(11, 29, 52, 'c'),
(12, 29, 53, 'd'),
(13, 38, 73, 'a'),
(14, 38, 74, 'b'),
(15, 38, 75, 'c'),
(16, 38, 76, 'd'),
(17, 88, 159, 'apple'),
(18, 88, 160, 'ball'),
(19, 88, 161, 'cat'),
(20, 88, 162, 'dog');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_que_creation`
--

CREATE TABLE `tbl_que_creation` (
  `que_id` int(11) NOT NULL,
  `que_name` text NOT NULL,
  `que_type_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `que_points` int(11) NOT NULL,
  `que_image` varchar(100) DEFAULT NULL,
  `que_option` tinyint(1) NOT NULL,
  `que_rows` int(11) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_que_creation`
--

INSERT INTO `tbl_que_creation` (`que_id`, `que_name`, `que_type_id`, `test_id`, `user_id`, `que_points`, `que_image`, `que_option`, `que_rows`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'Narendra modi is a pm...?', 1, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 'Indian capital is [] ..?', 2, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 'Indian capital is [] ..?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(9, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(10, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(11, 'Narendra modi is a pm...?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(12, 'Indian capital is [] ..?', 3, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(13, 'Narendra modi is a pm...?', 4, 24, NULL, 2, '/media/question/a1.jpg', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(14, 'Narendra modi is a pm...?', 4, 24, NULL, 2, '/media/question/a2.jpg', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(15, 'Narendra modi is a pm...?', 4, 24, NULL, 2, '/media/question/a3.jpg', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(16, 'Narendra modi is a pm...?', 4, 24, NULL, 2, '/media/question/a4.jpg', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(17, 'Indian capital is [] ..?', 4, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(18, 'Indian capital is [] ..?', 5, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(19, 'Indian capital is [] ..?', 5, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(20, 'Indian capital is [] ..?', 5, 24, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(21, 'test', 1, 6, NULL, 2, '', 0, 0, 0, 0, '2019-05-01 00:00:00', '0000-00-00 00:00:00', '', ''),
(22, 'Standard math Test', 1, 26, NULL, 2, '', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(23, 'Standard math Test[]', 2, 26, NULL, 2, '', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(24, 'Narendra modi is a pm...?', 3, 26, NULL, 2, '', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(25, 'Standard math Test', 4, 26, NULL, 2, '', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(26, 'Standard math Test', 5, 26, NULL, 2, '', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(27, 'testing is not possible', 1, 27, NULL, 2, '/media/question/maxresdefault.jpg', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(28, 'fill in the blank()', 2, 27, NULL, 2, '/media/question/Supplies.jpg', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(29, 'multiple select question', 5, 27, NULL, 2, '/media/question/1212121IMG_3220_2000x.jpg', 0, 0, 0, 0, '2019-05-02 00:00:00', '0000-00-00 00:00:00', '', ''),
(30, 'test question', 1, 19, NULL, 2, '/media/question/176_1556873612-kirayoung3.jpg', 0, 0, 0, 0, '2019-05-06 00:00:00', '0000-00-00 00:00:00', '', ''),
(31, 'testing is not possible', 1, 35, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(32, 'select multiple question', 3, 35, NULL, 2, '/media/question/7-craziest-restaurant-dishes-burger-1455274802.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(33, 'Which is NOT true -  The black box tester: A	should be ', 1, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(34, 'In which activity of the Fundamental Test Process is th', 3, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(35, 'Beta testing is:', 3, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(36, 'testing is not possible testing is not possible testing', 1, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(37, '[inspection]is not a static testing techniques', 2, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(38, 'match the pair', 5, 36, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(39, 'testing is not possible', 1, 38, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(40, 'explain the terms testing', 4, 38, NULL, 5, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(41, '100% testing is not possible', 1, 39, NULL, 4, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(42, 'testing is not possible', 1, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(43, 'test', 4, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(44, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(45, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM1.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(46, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM2.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(47, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM3.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(48, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM4.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(49, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM5.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(50, 'Which is NOT true -  The black box tester:', 3, 39, NULL, 8, '/media/question/081116_CSODance_RBJM6.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(51, 'testing is not possible', 1, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(52, 'testing is not possible', 4, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(53, '100%testing is not possible', 1, 39, NULL, 4, '/media/question/insurance_solution_large.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(54, 'Beta testing is:[]', 2, 39, NULL, 4, '/media/question/travelling-around-croatia.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(55, 'Beta testing is:[]', 2, 39, NULL, 4, '/media/question/travelling-around-croatia1.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(56, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(57, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy1.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(58, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy2.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(59, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy3.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(60, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy4.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(61, 'Beta testing is:', 2, 39, NULL, 5, '/media/question/1_-_Copy5.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(62, 'Which of the following is NOT a static testing techniqu', 3, 39, NULL, 6, '/media/question/Things-to-Know-about-Travelling_-_Copy.png', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(63, 'Which of the following is NOT a static testing techniqu', 3, 39, NULL, 6, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(64, 'testing is not possible', 3, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(65, 'test', 3, 39, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(66, 'testing is not possible', 3, 39, NULL, 2, '/media/question/7-craziest-restaurant-dishes-burger-14552748021.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(67, 'testing is not possible', 3, 39, NULL, 5, '/media/question/6afa122597958f25baaf72f9d4502752--hot-teenagers-boys-handsome-boys-teenagers.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(68, 'The Earth is mostly water.', 1, 40, NULL, 4, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(69, '100%{testing]not possibe', 2, 40, NULL, 4, '/media/question/travelling-to-canada-for-the-first-time-cover_-_Copy.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(70, '100%{testing]not possibe', 2, 40, NULL, 4, '/media/question/travelling-to-canada-for-the-first-time-cover_-_Copy1.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(71, '100%{testing]not possibe', 2, 40, NULL, 4, '/media/question/travelling-to-canada-for-the-first-time-cover_-_Copy2.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(72, '[test] is important', 2, 40, NULL, 2, '/media/question/9.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(73, '[test] is important', 2, 40, NULL, 2, '/media/question/91.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(74, '[test] is important', 2, 40, NULL, 2, '/media/question/92.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(75, '[test] is important', 2, 40, NULL, 2, '/media/question/93.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(76, '[test] is important', 2, 40, NULL, 2, '/media/question/94.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(77, '[test] is important', 2, 40, NULL, 2, '/media/question/95.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(78, '[test] is important', 2, 40, NULL, 2, '/media/question/96.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(79, '[test] is important', 2, 40, NULL, 2, '/media/question/97.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(80, '[test] is important', 2, 40, NULL, 2, '/media/question/98.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(81, '[test] is important', 2, 40, NULL, 2, '/media/question/99.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(82, '[test] is important', 2, 40, NULL, 2, '/media/question/910.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(83, '[test] is important', 2, 40, NULL, 2, '/media/question/911.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(84, '[]is', 2, 40, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(85, '[]fill it', 2, 41, NULL, 2, '/media/question/maxresdefault1.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(86, 'select one', 3, 41, NULL, 5, '/media/question/x800_1513869747_logo.png', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(87, 'enter the answer', 4, 41, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(88, 'match the pair', 5, 41, NULL, 10, '/media/question/x800_1513869747_logo1.png', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(89, 'For example, when from date is selected, then using this \"onSelect\" event we will set the \"minDate\" attribute of \"txtToDate\" textbox to the selected date in \"txtFromDate\". What it does is that, dates less than the selected from date will be disabled in \"txtToDate\" textbox. And same way set the \"MaxDate\" attribute for \"txtFromDate\" in onSelect event of \"txtToDate\" textbox. Below is the complete jQuery code.', 1, 42, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(90, 'Narendra modi is a pm...?', 2, 43, NULL, 2, '/media/question/a13.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(91, 'Narendra modi is a pm...?', 2, 43, NULL, 2, '/media/question/a14.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(92, 'Narendra modi is a pm...?', 2, 43, NULL, 2, '/media/question/a15.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(93, 'Narendra modi is a pm...?', 2, 43, NULL, 2, '/media/question/a16.jpg', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(94, 'Indian capital is [] ..?', 3, 43, NULL, 2, '', 0, 0, 0, 0, '2019-05-07 00:00:00', '0000-00-00 00:00:00', '', ''),
(95, 'Default value. The marker is a filled circle', 2, 44, NULL, 2, '', 0, 0, 0, 0, '2019-05-08 00:00:00', '0000-00-00 00:00:00', '', ''),
(96, 'Default value. The marker is a filled circle[].', 2, 45, NULL, 2, '', 0, 0, 0, 0, '2019-05-09 00:00:00', '0000-00-00 00:00:00', '', ''),
(97, 'test', 1, 45, NULL, 2, '', 0, 0, 0, 0, '2019-05-09 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_q_type_master`
--

CREATE TABLE `tbl_q_type_master` (
  `question_type_id` int(11) NOT NULL,
  `question_type_name` varchar(55) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_q_type_master`
--

INSERT INTO `tbl_q_type_master` (`question_type_id`, `question_type_name`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'True or False', 1, 0, '0000-00-00 00:00:00', '2019-04-26 02:33:32', '0', '0'),
(2, 'Fill in the blank', 1, 0, '0000-00-00 00:00:00', '2019-04-26 02:33:35', '0', '0'),
(3, 'Multiple Select/Choice', 1, 0, '0000-00-00 00:00:00', '2019-04-26 02:33:37', '0', '0'),
(4, 'Short Answer', 1, 0, '0000-00-00 00:00:00', '2019-04-26 02:33:39', '0', '0'),
(5, 'Matching', 1, 0, '0000-00-00 00:00:00', '2019-04-26 02:33:42', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section`
--

CREATE TABLE `tbl_section` (
  `_id` int(11) NOT NULL,
  `sectionName` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_section`
--

INSERT INTO `tbl_section` (`_id`, `sectionName`, `status`) VALUES
(1, 'Master', 1),
(2, 'Students', 1),
(3, 'Teachers', 1),
(4, 'Reports', 1),
(5, 'Exam', 1),
(6, 'Result', 1),
(7, 'Payments', 1),
(8, 'Blog', 1),
(9, 'System Setting', 1),
(10, 'Administrator', 1),
(11, 'CMS', 1),
(12, 'Promotions', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_standard_class`
--

CREATE TABLE `tbl_standard_class` (
  `standard_class_id` int(11) NOT NULL,
  `standard_class_name` varchar(60) NOT NULL,
  `is_status` int(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_standard_class`
--

INSERT INTO `tbl_standard_class` (`standard_class_id`, `standard_class_name`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'History', 0, 0, '0000-00-00 00:00:00', '2019-03-22 22:05:28', '', ''),
(2, 'test', 1, 1, '0000-00-00 00:00:00', '2019-05-02 02:52:28', '', ''),
(3, '10th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:52:14', '', ''),
(4, '9th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:52:01', '', ''),
(5, '8th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:51:43', '', ''),
(6, '7th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:51:25', '', ''),
(7, '6th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:51:12', '', ''),
(8, '5th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:50:57', '', ''),
(9, '4th', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:50:40', '', ''),
(10, '3rd', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:50:24', '', ''),
(11, '2nd', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:50:10', '', ''),
(12, '1st', 1, 0, '0000-00-00 00:00:00', '2019-05-02 02:49:56', '', ''),
(13, 'test new', 1, 1, '0000-00-00 00:00:00', '2019-03-23 14:35:24', '', ''),
(14, 'fsddf', 1, 1, '0000-00-00 00:00:00', '2019-03-23 14:35:03', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject_class`
--

CREATE TABLE `tbl_subject_class` (
  `subject_class_id` int(11) NOT NULL,
  `subject_class_name` varchar(60) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subject_class`
--

INSERT INTO `tbl_subject_class` (`subject_class_id`, `subject_class_name`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'English', 0, 0, '0000-00-00 00:00:00', '2019-04-02 18:09:40', '', ''),
(2, 'History', 1, 0, '0000-00-00 00:00:00', '2019-03-25 17:29:57', '', ''),
(3, 'Mathematics', 1, 1, '0000-00-00 00:00:00', '2019-03-25 19:31:38', '', ''),
(4, 'sdsd', 1, 1, '0000-00-00 00:00:00', '2019-03-25 19:22:46', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_system_config`
--

CREATE TABLE `tbl_system_config` (
  `sysytem_config_id` int(11) NOT NULL,
  `system_config_key` varchar(255) NOT NULL,
  `system_config_value` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_system_config`
--

INSERT INTO `tbl_system_config` (`sysytem_config_id`, `system_config_key`, `system_config_value`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'company_name', 'Test Solution', '0000-00-00 00:00:00', '2019-03-21 05:49:11', '', ''),
(2, 'company_address', 'Nagpur', '0000-00-00 00:00:00', '2019-03-16 15:46:17', '', ''),
(3, 'company_website', 'www.test.com', '0000-00-00 00:00:00', '2019-03-16 15:46:25', '', ''),
(4, 'company_email', 'test@gmail.com', '0000-00-00 00:00:00', '2019-03-16 15:46:31', '', ''),
(5, 'company_phone', '9970426205', '0000-00-00 00:00:00', '2019-03-16 15:46:34', '', ''),
(6, 'company_fax', 'fax1', '0000-00-00 00:00:00', '2019-03-16 15:46:37', '', ''),
(7, 'dateformat', 'd.m.Y', '0000-00-00 00:00:00', '2019-03-16 16:09:03', '', ''),
(8, 'protocol', 'smtp', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(9, 'mailpath', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(10, 'smtp_host', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(11, 'smtp_port', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(12, 'smtp_crypto', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(13, 'smtp_user', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(14, 'smtp_pass', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test_creation`
--

CREATE TABLE `tbl_test_creation` (
  `test_id` int(11) NOT NULL,
  `test_name` varchar(60) NOT NULL,
  `test_uniqe_code` varchar(60) NOT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `test_description` text,
  `test_instructions` text,
  `test_form_date` datetime NOT NULL,
  `test_to_date` datetime NOT NULL,
  `test_access_code` varchar(10) NOT NULL,
  `test_time_limit` varchar(20) NOT NULL,
  `test_question_random` int(1) NOT NULL DEFAULT '0',
  `test_status` int(3) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_test_creation`
--

INSERT INTO `tbl_test_creation` (`test_id`, `test_name`, `test_uniqe_code`, `folder_id`, `user_id`, `test_description`, `test_instructions`, `test_form_date`, `test_to_date`, `test_access_code`, `test_time_limit`, `test_question_random`, `test_status`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(6, 'Test', 'h9MAZ-Q9pLx-Fy1ei-EQR2x', NULL, 1, 'test', 'test', '2019-04-26 00:00:00', '2019-04-27 00:00:00', 'S2MQ5', '120', 1, 1, 1, 0, '2019-05-01 14:31:02', '2019-04-28 13:29:20', '', ''),
(7, '12th Standard math Test', 'W2fI9-XzmcK-VNDyE-be2GY', NULL, 12, '12th Standard math Test', '12th Standard math Test', '2019-04-28 00:00:00', '2019-04-30 00:00:00', 'UVW23', '20', 1, 1, 1, 0, '2019-04-28 14:58:22', '2019-04-28 14:58:22', '', ''),
(8, 'Test 2', 'z34wZ-d64vH-rNGL3-r9HJj', NULL, 12, 'test', 'test', '2019-04-28 00:00:00', '2019-04-30 00:00:00', '0WXYQ', '20', 1, 1, 1, 0, '2019-04-28 15:37:11', '2019-04-28 15:16:19', '', ''),
(9, 'Test 4', 'R1qDg-XtGBO-5OQL9-mED05', NULL, 12, 't', 't', '2019-04-28 00:00:00', '2019-04-28 00:00:00', '5TVGA', '130', 1, 1, 1, 0, '2019-04-28 15:16:29', '2019-04-28 15:16:29', '', ''),
(10, 'Test 4', 'PcW5R-tvMst-GGhvg-LI5bG', NULL, 12, 'd', 'd', '2019-04-28 00:00:00', '2019-04-28 00:00:00', '0MQJX', '20', 0, 1, 1, 0, '2019-04-28 15:16:40', '2019-04-28 15:16:40', '', ''),
(11, '12th Standard math Test', 'lzB9R-j1AfS-hMD23-Z4LWT', NULL, 12, 'test', 'test', '2019-04-28 00:00:00', '2019-04-28 00:00:00', 'Y5J0S', '40', 1, 1, 1, 0, '2019-04-28 16:12:02', '2019-04-28 16:12:02', '', ''),
(12, '12th Standard math Test', '0DvI7-OBTO8-5jD79-0V4zw', NULL, 12, 'test', 'test', '2019-04-28 00:00:00', '2019-04-30 00:00:00', '9C4PK', '40', 1, 1, 1, 0, '2019-04-28 16:24:36', '2019-04-28 16:24:36', '', ''),
(13, '12th Standard math Test', 'TPDzX-RnmJ2-hXMEk-wovLN', NULL, 12, 'Standard math Test', 'Standard math Test', '2019-04-28 00:00:00', '2019-04-28 00:00:00', '1OC8J', '20', 0, 1, 1, 0, '2019-04-28 16:37:38', '2019-04-28 16:37:38', '', ''),
(14, '12th Standard math Test', 'jtsYf-l7hhQ-GFYZl-2GgVq', NULL, 12, '1000', '1000', '2019-04-28 00:00:00', '2019-06-20 00:00:00', 'GOCZ8', '20', 0, 1, 1, 0, '2019-04-28 16:38:26', '2019-04-28 16:38:26', '', ''),
(15, '12th Standard math Test', 'Xsu4A-yy1gQ-zOaJD-ix488', NULL, 12, '1000', '1000', '2019-04-28 00:00:00', '2019-04-30 00:00:00', '8AS0B', '20', 0, 1, 1, 0, '2019-04-28 16:38:46', '2019-04-28 16:38:46', '', ''),
(16, '12th Standard math Test', 'PI2Ec-aoMhD-pmQBD-j98L6', NULL, 12, '1000', '1000', '2019-04-28 00:00:00', '2019-04-28 00:00:00', 'Y24PX', '20', 0, 1, 1, 0, '2019-04-28 16:39:54', '2019-04-28 16:39:54', '', ''),
(17, '12th Standard math Test', 'TJIgV-Q7E9X-BDcMJ-OTioO', NULL, 1, ' Standard math Test', ' Standard math Test', '2019-04-29 00:00:00', '2019-04-29 00:00:00', '92EB4', '20', 0, 1, 1, 0, '2019-04-29 08:19:55', '2019-04-29 08:19:55', '', ''),
(18, '12th Standard math Test', 'YN90X-CDUJQ-I4uf5-3JcFe', NULL, 1, 'create_question', 'create_question', '2019-04-30 00:00:00', '2019-04-30 00:00:00', 'T5163', '20', 1, 1, 1, 0, '2019-04-30 05:11:00', '2019-04-30 05:11:00', '', ''),
(19, '12th Standard math Test', 'Iz0dH-pX6Ms-xiI3O-Vh84x', NULL, 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-05-06 00:00:00', '2019-05-16 00:00:00', 'PS1QC', '40', 1, 1, 1, 0, '2019-05-06 03:57:14', '2019-05-01 07:45:37', '', ''),
(20, '12th Standard math Test', 'tNe23-K1WOR-sIpRH-k4x7m', NULL, 1, 'test', 'test', '2019-05-01 00:00:00', '2019-05-01 00:00:00', 'Q3ZEN', '20', 1, 1, 1, 0, '2019-05-01 07:52:18', '2019-05-01 07:52:18', '', ''),
(21, '12th Standard math Test', 'VRJNW-HXCva-lU61u-8hXOs', NULL, 1, 'test', 'test', '2019-05-01 00:00:00', '2019-05-01 00:00:00', 'MWUI1', '50', 1, 1, 1, 0, '2019-05-01 07:52:51', '2019-05-01 07:52:51', '', ''),
(22, '12th Standard math Test', 'us5ij-RUcpU-1kvF8-iEXaF', NULL, 1, 'test', 'test', '2019-05-01 00:00:00', '2019-05-01 00:00:00', 'W8UVX', '20', 1, 1, 1, 0, '2019-05-01 08:13:19', '2019-05-01 08:13:19', '', ''),
(23, '', '', NULL, 1, NULL, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', 'NSYGI', '', 0, 1, 1, 0, '2019-05-01 11:16:41', '2019-05-01 11:15:19', '', ''),
(24, '12th Standard math Test', 'Ps6Qi-RY8bY-5jDtw-o05fj', NULL, 1, 'test', 'test', '2019-05-01 00:00:00', '2019-05-01 00:00:00', 'D701I', '20', 1, 1, 1, 0, '2019-05-01 11:31:57', '2019-05-01 11:31:57', '', ''),
(25, '12th Standard math Test', 'Wryzd-YGjoE-aZrfa-YmU2h', NULL, 1, '12th Standard math Test', '12th Standard math Test', '2019-05-01 00:00:00', '2019-05-01 00:00:00', 'LZYY7', '20', 1, 1, 1, 0, '2019-05-01 13:49:57', '2019-05-01 13:49:57', '', ''),
(26, '12th Standard math Test', 'IoynV-Osttl-zLiMO-A2ryw', NULL, 1, 'Standard math Test', 'Standard math Test', '2019-05-02 00:00:00', '2019-05-11 00:00:00', 'D4XLF', '20', 0, 1, 1, 0, '2019-05-07 04:11:55', '2019-05-02 02:54:54', '', ''),
(27, 'test2', 'AlEnP-oYZ6l-ZSfEe-OsXYD', NULL, 19, 'testing testing', 'testing', '2019-05-02 00:00:00', '2019-05-17 00:00:00', 'ZHL7F', '40', 1, 1, 1, 0, '2019-05-02 10:30:39', '2019-05-02 09:59:45', '', ''),
(28, 'test2', 'inqti-O3VOA-VD0V5-v7pkT', NULL, 19, 'test', 'test', '2019-05-02 00:00:00', '2019-05-09 00:00:00', 'WD41D', '20', 1, 1, 1, 0, '2019-05-02 10:20:47', '2019-05-02 10:20:47', '', ''),
(29, 'test3', '1qAAb-LKao9-oPKoC-WQjs1', NULL, 19, '', '', '2019-05-02 00:00:00', '2019-10-09 00:00:00', '2EMQH', '90', 1, 1, 1, 0, '2019-05-02 10:44:36', '2019-05-02 10:44:36', '', ''),
(30, 'test', 'qxgFX-o15Zh-bFdi5-e87Sw', NULL, 19, 'dfgj', 'kfgj', '2019-05-02 00:00:00', '2019-05-02 00:00:00', '5X58X', '20', 0, 1, 1, 0, '2019-05-02 10:53:53', '2019-05-02 10:53:53', '', ''),
(31, 'test 4', 'OZsbz-sFRJW-sYXBM-2WOuU', NULL, 19, 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset s', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset s', '2019-05-02 00:00:00', '2019-05-06 00:00:00', 'JBBWB', '40', 1, 1, 1, 0, '2019-05-02 10:58:07', '2019-05-02 10:54:55', '', ''),
(32, 'test6', '2etWZ-Sm42d-KD6FT-jAC4D', NULL, 19, 'tewst', '', '2019-05-02 00:00:00', '2019-10-06 00:00:00', 'CQDXK', '20', 0, 1, 1, 0, '2019-05-02 11:05:07', '2019-05-02 11:03:15', '', ''),
(33, 'test7', 'm4lHw-C5SLG-XI0gW-fdIbZ', NULL, 19, 'test', 'test', '2019-05-07 00:00:00', '2019-07-06 00:00:00', 'Y9JQJ', '20', 0, 1, 1, 0, '2019-05-02 11:30:52', '2019-05-02 11:30:52', '', ''),
(34, 'hello', '6fBO2-7s4fi-ZgzJf-zRRpA', NULL, 1, 'test tre ', 'trtr etret', '2019-05-06 00:00:00', '2019-05-22 00:00:00', '9WJL2', '140', 1, 1, 1, 0, '2019-05-06 08:13:45', '2019-05-06 08:13:45', '', ''),
(35, 'test', 'vmcan-2qVJB-CBSa0-hwcaI', NULL, 21, 'test', 'test', '2019-05-07 00:00:00', '2019-08-07 00:00:00', 'YGTB1', '20', 1, 1, 1, 0, '2019-05-07 06:06:09', '2019-05-07 06:06:09', '', ''),
(36, 'ISTQB', 'AjUeA-nbGl2-ec5tE-Vy8aa', NULL, 22, 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.', 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.', '2019-05-07 00:00:00', '2019-05-09 00:00:00', 'QRQ21', '40', 1, 1, 1, 0, '2019-05-07 06:17:50', '2019-05-07 06:17:50', '', ''),
(37, 'test1', 'vEDBi-o1qTf-BjdiQ-Rn0pg', NULL, 24, 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.12We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.', 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.12We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.', '2019-05-07 00:00:00', '2019-08-06 00:00:00', 'TGC7J', '60', 0, 1, 1, 0, '2019-05-07 07:26:08', '2019-05-07 07:26:08', '', ''),
(38, 'test2', 'BUSF2-zBvQ5-2K1z0-WIvnr', NULL, 24, 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.12We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.', 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.12We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.', '2019-05-07 00:00:00', '2019-08-10 00:00:00', 'XQHD5', '20', 0, 1, 1, 0, '2019-05-07 07:29:42', '2019-05-07 07:29:42', '', ''),
(39, 'ISTQB', '2aaVQ-JGKQz-9O96T-CZ62t', NULL, 25, 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.\r\n\r\n\r\n\r\n\r\n\r\n', 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.\r\n\r\n\r\n', '2019-05-07 00:00:00', '2019-08-08 00:00:00', 'S2Q9Y', '60', 0, 1, 1, 0, '2019-05-07 09:33:53', '2019-05-07 09:33:53', '', ''),
(40, 'test', 'yFJ6n-0rlA0-oUmLx-ZvQ6a', NULL, 26, 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.1234', 'We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.5678\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.We have done ISTQB online tests previously. Now here is an attempt to test your software testing basic knowledge with a simple 20 question test. The following software testing mock test is designed to test your ability to meet software testing requirements.\r\n\r\nThis free online software testing quiz will help you for self-assessment and prepare for other certification exams as well as software testing interview.5678', '2019-05-07 00:00:00', '2019-07-09 00:00:00', 'LW24M', '80', 0, 1, 1, 0, '2019-05-07 11:18:37', '2019-05-07 11:10:59', '', ''),
(41, 'maths test', 'dSsLJ-Ccl2C-4tsos-sSLpO', NULL, 26, 'testing', 'testing', '2019-05-07 00:00:00', '2019-05-09 00:00:00', 'M05D8', '20', 0, 1, 1, 0, '2019-05-07 12:06:27', '2019-05-07 12:06:27', '', ''),
(42, '12th Standard math Test', '8pRqI-xcv7n-TIexG-tjoVP', NULL, 1, 'For example, when from date is selected, then using this \"onSelect\" event we will set the \"minDate\" attribute of \"txtToDate\" textbox to the selected date in \"txtFromDate\". What it does is that, dates less than the selected from date will be disabled in \"txtToDate\" textbox. And same way set the \"MaxDate\" attribute for \"txtFromDate\" in onSelect event of \"txtToDate\" textbox. Below is the complete jQuery code.', 'For example, when from date is selected, then using this \"onSelect\" event we will set the \"minDate\" attribute of \"txtToDate\" textbox to the selected date in \"txtFromDate\". What it does is that, dates less than the selected from date will be disabled in \"txtToDate\" textbox. And same way set the \"MaxDate\" attribute for \"txtFromDate\" in onSelect event of \"txtToDate\" textbox. Below is the complete jQuery code.', '2019-05-08 00:00:00', '2019-05-15 00:00:00', 'I622C', '20', 0, 1, 1, 0, '2019-05-07 16:10:06', '2019-05-07 16:10:06', '', ''),
(43, '12th Standard math Test', 'bCfaC-FNLka-Jh9c0-dVHYz', NULL, 1, 'test', 'test', '2019-05-07 00:00:00', '2019-05-15 00:00:00', 'TN52Z', '50', 1, 1, 1, 0, '2019-05-07 17:25:22', '2019-05-07 17:25:22', '', ''),
(44, 'test', 'W5Aky-1roo6-yTENS-rVZuX', NULL, 1, 'test', 'test', '2019-05-08 00:00:00', '2019-05-22 00:00:00', 'L6ZE0', '60', 1, 1, 1, 0, '2019-05-08 02:42:31', '2019-05-08 02:42:31', '', ''),
(45, '12th Standard math Test', 'NS1CR-o8fjL-qx3kW-khP9O', NULL, 1, 'test', 'test', '2019-05-09 00:00:00', '2019-05-15 00:00:00', 'LXFQK', '50', 1, 1, 1, 0, '2019-05-09 04:25:46', '2019-05-09 04:25:46', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_login_status`
--

CREATE TABLE `tbl_user_login_status` (
  `session_id` int(11) NOT NULL,
  `usermaster_id` int(11) DEFAULT NULL,
  `ipAddress` varchar(50) DEFAULT NULL,
  `loginIn` datetime(6) DEFAULT NULL,
  `logOut` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_login_status`
--

INSERT INTO `tbl_user_login_status` (`session_id`, `usermaster_id`, `ipAddress`, `loginIn`, `logOut`) VALUES
(1, 1, '::1', '2019-04-17 08:42:55.000000', '2019-04-17 08:42:59.000000'),
(2, 1, '::1', '2019-04-17 08:43:07.000000', '2019-04-17 08:46:30.000000'),
(3, 12, '::1', '2019-04-17 09:23:09.000000', '2019-04-17 09:25:51.000000'),
(4, 12, '::1', '2019-04-17 09:25:58.000000', '2019-04-17 09:26:17.000000'),
(5, 12, '::1', '2019-04-17 09:26:36.000000', '2019-04-17 09:26:44.000000'),
(6, 12, '::1', '2019-04-21 10:39:17.000000', '2019-04-21 11:16:19.000000'),
(7, 12, '::1', '2019-04-21 11:22:17.000000', '2019-04-21 11:54:11.000000'),
(8, 5, '::1', '2019-04-21 11:54:19.000000', '2019-04-21 12:01:30.000000'),
(9, 12, '::1', '2019-04-21 12:01:47.000000', '2019-04-21 12:02:13.000000'),
(10, 12, '::1', '2019-04-21 12:02:26.000000', '2019-04-21 12:02:53.000000'),
(11, 12, '::1', '2019-04-21 12:03:05.000000', '2019-04-21 12:03:46.000000'),
(12, 5, '::1', '2019-04-21 12:03:56.000000', '2019-04-21 12:04:10.000000'),
(13, 12, '::1', '2019-04-21 12:04:36.000000', '2019-04-21 12:18:11.000000'),
(14, 12, '::1', '2019-04-21 12:19:06.000000', '2019-04-21 12:19:24.000000'),
(15, 5, '::1', '2019-04-21 12:19:44.000000', '2019-04-21 12:20:01.000000'),
(16, 12, '::1', '2019-04-21 12:20:50.000000', '2019-04-21 12:25:51.000000'),
(17, 12, '::1', '2019-04-21 12:26:01.000000', '2019-04-21 12:37:35.000000'),
(18, 12, '::1', '2019-04-21 12:37:46.000000', '2019-04-21 13:17:22.000000'),
(19, 5, '::1', '2019-04-21 13:17:32.000000', '2019-04-21 14:01:22.000000'),
(20, 12, '::1', '2019-04-21 14:01:36.000000', '2019-04-21 14:03:16.000000'),
(21, 12, '::1', '2019-04-22 22:42:47.000000', '2019-04-22 23:23:01.000000'),
(22, 12, '::1', '2019-04-22 23:23:37.000000', '2019-04-22 23:23:37.000000'),
(23, 12, '::1', '2019-04-24 08:14:15.000000', '2019-04-24 08:37:27.000000'),
(24, 5, '::1', '2019-04-24 08:37:33.000000', '2019-04-24 08:37:45.000000'),
(25, 12, '::1', '2019-04-24 08:37:52.000000', '2019-04-24 08:55:11.000000'),
(26, 12, '::1', '2019-04-24 08:55:56.000000', '2019-04-24 09:31:21.000000'),
(27, 12, '::1', '2019-04-24 09:31:31.000000', '2019-04-24 09:31:42.000000'),
(28, 12, '::1', '2019-04-24 09:32:23.000000', '2019-04-24 09:34:46.000000'),
(29, 5, '::1', '2019-04-24 09:34:52.000000', '2019-04-24 09:35:24.000000'),
(30, 12, '::1', '2019-04-24 09:35:30.000000', '2019-04-24 11:00:25.000000'),
(31, 12, '::1', '2019-04-24 11:00:32.000000', '2019-04-24 11:00:33.000000'),
(32, 12, '::1', '2019-04-24 11:02:44.000000', '2019-04-24 11:21:55.000000'),
(33, 12, '::1', '2019-04-24 11:24:07.000000', '2019-04-24 11:44:25.000000'),
(34, 12, '::1', '2019-04-24 11:44:58.000000', '2019-04-24 11:45:37.000000'),
(35, 5, '::1', '2019-04-24 11:45:49.000000', '2019-04-24 11:46:09.000000'),
(36, 12, '::1', '2019-04-24 12:15:44.000000', '2019-04-24 12:20:48.000000'),
(37, 12, '::1', '2019-04-24 14:30:05.000000', '2019-04-24 14:53:04.000000'),
(38, 12, '::1', '2019-04-24 15:02:48.000000', '2019-04-24 15:56:42.000000'),
(39, 12, '::1', '2019-04-24 15:57:05.000000', '2019-04-24 16:33:55.000000'),
(40, 5, '::1', '2019-04-24 16:34:04.000000', '2019-04-24 16:34:20.000000'),
(41, 12, '::1', '2019-04-24 16:34:30.000000', '2019-04-24 16:34:30.000000'),
(42, 12, '::1', '2019-04-26 07:27:53.000000', '2019-04-26 07:27:53.000000'),
(43, 12, '::1', '2019-04-28 13:48:45.000000', '2019-04-28 14:50:41.000000'),
(44, 12, '::1', '2019-04-28 14:52:15.000000', '2019-04-28 14:52:15.000000'),
(45, 12, '::1', '2019-04-28 16:52:31.000000', '2019-04-28 18:05:46.000000'),
(46, 5, '::1', '2019-04-28 18:05:51.000000', '2019-04-28 18:13:08.000000'),
(47, 12, '::1', '2019-04-28 18:13:16.000000', '2019-04-28 18:21:38.000000'),
(48, 12, '::1', '2019-04-28 18:21:54.000000', '2019-04-28 18:59:30.000000'),
(49, 12, '::1', '2019-04-28 18:59:52.000000', '2019-04-28 22:17:51.000000'),
(50, 14, '::1', '2019-04-28 22:28:19.000000', '2019-04-28 22:28:29.000000'),
(51, 1, '::1', '2019-04-28 22:30:07.000000', '2019-04-28 22:38:58.000000'),
(52, 14, '::1', '2019-04-29 11:20:28.000000', '2019-04-29 11:22:01.000000'),
(53, 1, '::1', '2019-04-29 11:22:07.000000', '2019-04-29 11:22:07.000000'),
(54, 1, '::1', '2019-04-29 12:14:14.000000', '2019-04-29 12:37:45.000000'),
(55, 14, '::1', '2019-04-29 12:37:51.000000', '2019-04-29 12:40:31.000000'),
(56, 1, '::1', '2019-04-29 12:40:45.000000', '2019-04-29 12:40:45.000000'),
(57, 1, '::1', '2019-04-29 13:27:28.000000', '2019-04-29 13:42:02.000000'),
(58, 14, '::1', '2019-04-29 13:37:19.000000', '2019-04-29 13:37:19.000000'),
(59, 14, '::1', '2019-04-29 13:46:00.000000', '2019-04-29 13:49:02.000000'),
(60, 1, '::1', '2019-04-29 13:49:14.000000', '2019-04-29 14:01:04.000000'),
(61, 1, '::1', '2019-04-29 14:06:58.000000', '2019-04-29 14:33:07.000000'),
(62, 14, '::1', '2019-04-29 14:33:29.000000', '2019-04-29 15:10:32.000000'),
(63, 1, '::1', '2019-04-29 15:10:40.000000', '2019-04-29 15:13:14.000000'),
(64, 14, '::1', '2019-04-29 15:51:54.000000', '2019-04-29 15:52:03.000000'),
(65, 1, '::1', '2019-04-29 15:52:50.000000', '2019-04-29 16:17:36.000000'),
(66, 14, '::1', '2019-04-29 16:32:54.000000', '2019-04-29 17:11:25.000000'),
(67, 1, '::1', '2019-04-29 17:11:42.000000', '2019-04-29 17:21:47.000000'),
(68, 1, '::1', '2019-04-29 17:34:18.000000', '2019-04-29 17:52:34.000000'),
(69, 14, '::1', '2019-04-29 17:55:00.000000', '2019-04-29 17:56:36.000000'),
(70, 14, '::1', '2019-04-29 17:58:04.000000', '2019-04-29 17:58:04.000000'),
(71, 1, '::1', '2019-04-29 20:32:23.000000', '2019-04-29 20:32:23.000000'),
(72, 1, '::1', '2019-04-30 08:28:40.000000', '2019-04-30 10:14:19.000000'),
(73, 5, '::1', '2019-04-30 10:14:23.000000', '2019-04-30 10:17:32.000000'),
(74, 1, '::1', '2019-04-30 10:17:37.000000', '2019-04-30 10:18:17.000000'),
(75, 5, '::1', '2019-04-30 10:18:22.000000', '2019-04-30 10:26:56.000000'),
(76, 1, '::1', '2019-04-30 10:27:20.000000', '2019-04-30 10:27:20.000000'),
(77, 1, '::1', '2019-04-30 15:33:15.000000', '2019-04-30 15:47:03.000000'),
(78, 1, '::1', '2019-04-30 15:47:09.000000', '2019-04-30 15:47:09.000000'),
(79, 14, '::1', '2019-04-30 16:12:31.000000', '2019-04-30 16:26:43.000000'),
(80, 1, '::1', '2019-04-30 16:30:37.000000', '2019-04-30 16:30:37.000000'),
(81, 1, '::1', '2019-05-01 11:29:44.000000', '2019-05-01 11:29:44.000000'),
(82, 1, '::1', '2019-05-01 13:43:04.000000', '2019-05-01 13:43:04.000000'),
(83, 1, '49.35.30.122', '2019-05-02 08:17:12.000000', '2019-05-02 08:17:34.000000'),
(84, 14, '49.35.30.122', '2019-05-02 08:18:03.000000', '2019-05-02 08:22:55.000000'),
(85, 1, '49.35.30.122', '2019-05-02 08:23:03.000000', '2019-05-02 08:31:25.000000'),
(86, 16, '49.35.30.122', '2019-05-02 08:32:36.000000', '2019-05-02 08:33:12.000000'),
(87, 16, '49.35.30.122', '2019-05-02 08:33:20.000000', '2019-05-02 08:33:26.000000'),
(88, 17, '49.35.30.122', '2019-05-02 08:36:58.000000', '2019-05-02 08:36:58.000000'),
(89, 17, '49.35.30.122', '2019-05-02 08:38:52.000000', '2019-05-02 08:50:59.000000'),
(90, 19, '49.248.85.190', '2019-05-02 15:28:04.000000', '2019-05-02 15:28:04.000000'),
(91, 19, '49.248.85.190', '2019-05-02 16:23:30.000000', '2019-05-02 16:23:30.000000'),
(92, 1, '157.33.219.130', '2019-05-06 08:28:57.000000', '2019-05-06 08:32:30.000000'),
(93, 5, '157.33.219.130', '2019-05-06 08:32:36.000000', '2019-05-06 08:33:02.000000'),
(94, 1, '157.33.219.130', '2019-05-06 08:33:07.000000', '2019-05-06 09:32:21.000000'),
(95, 1, '103.73.214.116', '2019-05-06 12:38:26.000000', '2019-05-06 12:38:26.000000'),
(96, 1, '49.248.193.169', '2019-05-06 13:41:54.000000', '2019-05-06 13:41:54.000000'),
(97, 1, '157.33.216.118', '2019-05-06 14:54:20.000000', '2019-05-06 14:54:20.000000'),
(98, 1, '103.73.214.116', '2019-05-06 16:42:48.000000', '2019-05-06 16:42:48.000000'),
(99, 1, '157.33.220.151', '2019-05-07 03:26:11.000000', '2019-05-07 03:26:11.000000'),
(100, 5, '157.33.142.162', '2019-05-07 09:32:46.000000', '2019-05-07 11:06:15.000000'),
(101, 5, '157.33.167.121', '2019-05-07 11:07:16.000000', '2019-05-07 11:07:16.000000'),
(102, 5, '49.248.194.33', '2019-05-07 11:31:42.000000', '2019-05-07 11:32:25.000000'),
(103, 21, '49.248.194.33', '2019-05-07 11:34:37.000000', '2019-05-07 11:40:13.000000'),
(104, 22, '49.248.194.33', '2019-05-07 11:44:12.000000', '2019-05-07 12:45:31.000000'),
(105, 23, '49.248.194.33', '2019-05-07 12:24:02.000000', '2019-05-07 12:25:14.000000'),
(106, 5, '49.248.194.33', '2019-05-07 12:25:24.000000', '2019-05-07 12:25:24.000000'),
(107, 24, '49.248.194.33', '2019-05-07 12:50:43.000000', '2019-05-07 14:27:38.000000'),
(108, 25, '49.248.194.33', '2019-05-07 14:41:26.000000', '2019-05-07 16:10:32.000000'),
(109, 26, '49.248.194.33', '2019-05-07 16:30:32.000000', '2019-05-07 16:39:37.000000'),
(110, 26, '49.248.194.33', '2019-05-07 16:37:05.000000', '2019-05-07 17:53:15.000000'),
(111, 26, '49.248.194.33', '2019-05-07 16:39:52.000000', '2019-05-07 17:53:19.000000'),
(112, 26, '49.248.194.33', '2019-05-07 17:35:17.000000', '2019-05-07 17:57:19.000000'),
(113, 26, '49.248.194.33', '2019-05-07 17:57:35.000000', '2019-05-07 17:58:39.000000'),
(114, 26, '49.248.194.33', '2019-05-07 17:58:59.000000', '2019-05-07 17:58:59.000000'),
(115, 26, '49.248.194.33', '2019-05-07 18:05:01.000000', '2019-05-07 18:05:01.000000'),
(116, 1, '::1', '2019-05-07 21:16:23.000000', '2019-05-07 22:42:37.000000'),
(117, 1, '::1', '2019-05-07 22:42:55.000000', '2019-05-07 23:06:35.000000'),
(118, 1, '::1', '2019-05-07 23:08:58.000000', '2019-05-07 23:10:40.000000'),
(119, 1, '::1', '2019-05-07 23:15:56.000000', '2019-05-07 23:15:56.000000'),
(120, 1, '::1', '2019-05-08 07:50:29.000000', '2019-05-08 07:51:18.000000'),
(121, 5, '::1', '2019-05-08 07:51:25.000000', '2019-05-08 07:52:48.000000'),
(122, 1, '::1', '2019-05-08 07:52:53.000000', '2019-05-08 08:07:29.000000'),
(123, 1, '::1', '2019-05-08 08:11:49.000000', '2019-05-08 08:11:49.000000'),
(124, 14, '::1', '2019-05-08 08:18:17.000000', '2019-05-08 08:18:24.000000'),
(125, 1, '::1', '2019-05-08 08:18:29.000000', '2019-05-08 08:34:12.000000'),
(126, 5, '::1', '2019-05-08 08:34:17.000000', '2019-05-08 08:41:09.000000'),
(127, 1, '::1', '2019-05-08 08:42:59.000000', '2019-05-08 08:59:49.000000'),
(128, 1, '::1', '2019-05-08 09:00:30.000000', '2019-05-08 09:00:30.000000'),
(129, 1, '::1', '2019-05-08 23:52:32.000000', '2019-05-09 00:04:22.000000'),
(130, 5, '::1', '2019-05-09 00:04:28.000000', '2019-05-09 00:08:49.000000'),
(131, 1, '::1', '2019-05-09 00:08:56.000000', '2019-05-09 00:08:56.000000'),
(132, 1, '::1', '2019-05-09 03:59:55.000000', '2019-05-09 09:57:21.000000'),
(133, 14, '::1', '2019-05-09 06:48:04.000000', '2019-05-09 06:48:14.000000'),
(134, 14, '::1', '2019-05-09 08:24:50.000000', '2019-05-09 08:26:10.000000'),
(135, 14, '::1', '2019-05-09 08:47:58.000000', '2019-05-09 09:15:32.000000'),
(136, 5, '::1', '2019-05-09 09:57:31.000000', '2019-05-09 10:00:13.000000'),
(137, 5, '::1', '2019-05-09 10:00:20.000000', '2019-05-09 10:32:46.000000'),
(138, 1, '::1', '2019-05-09 10:18:38.000000', '2019-05-09 10:18:38.000000'),
(139, 1, '::1', '2019-05-09 10:32:51.000000', '2019-05-09 10:56:10.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_question_answered`
--

CREATE TABLE `tbl_user_question_answered` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `q_no` int(11) NOT NULL,
  `save_time` varchar(255) DEFAULT NULL,
  `answer_status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exam_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 for incomplete,1 for complete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_question_answered`
--

INSERT INTO `tbl_user_question_answered` (`id`, `user_id`, `test_id`, `question_id`, `answer_id`, `q_no`, `save_time`, `answer_status`, `created_at`, `updated_at`, `exam_status`) VALUES
(1, 5, 26, 22, 28, 0, '19:55', 'wrong', NULL, '2019-05-07 02:56:56', '0'),
(2, 5, 26, 23, 29, 0, '19:39', 'right', NULL, '2019-05-07 02:57:03', '0'),
(3, 5, 26, 24, 37, 0, '19:49', 'wrong', NULL, '2019-05-07 02:57:08', '0'),
(4, 5, 26, 26, 41, 0, '19:46', 'wrong', NULL, '2019-05-07 02:57:15', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_registration`
--

CREATE TABLE `tbl_user_registration` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `standard_class_id` int(11) DEFAULT NULL,
  `subject_class_id` int(11) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `organization_phone_number` varchar(100) NOT NULL,
  `your_title` varchar(100) NOT NULL,
  `your_phone` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `tearm_conditions` tinyint(1) NOT NULL,
  `profile_photo` text NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1-Teacher, 2-Student',
  `school_collage` text NOT NULL,
  `is_profile_status` int(11) NOT NULL COMMENT ' 0 - Non-Approve, 1 - Approve , 2 - Disapprove ',
  `registered_date` datetime NOT NULL,
  `approved_date` datetime NOT NULL,
  `payment_mode` varchar(30) NOT NULL,
  `verification_by_email` varchar(255) NOT NULL,
  `verification_by_email_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-false,1-true',
  `reset_password_str` varchar(40) DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `modified_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_registration`
--

INSERT INTO `tbl_user_registration` (`user_id`, `first_name`, `last_name`, `email_address`, `user_password`, `standard_class_id`, `subject_class_id`, `organization`, `organization_phone_number`, `your_title`, `your_phone`, `address`, `tearm_conditions`, `profile_photo`, `user_type`, `school_collage`, `is_profile_status`, `registered_date`, `approved_date`, `payment_mode`, `verification_by_email`, `verification_by_email_status`, `reset_password_str`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'Lomesh', 'Kelwadkar', 'lomesh538755@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, 2, 'test', '', 'test', '12345678', '', 0, '/media/profile/a3.jpg', 1, 'Central Point', 1, '2019-04-02 18:29:14', '0000-00-00 00:00:00', '', '9fc6bbfc8bc75250020a67a8e1a69d64', 1, '0oFMzdso7VfRmRQb05xqnuR51', 0, '2019-04-02 18:29:14', '2019-05-08 03:13:34', '', ''),
(5, 'Amol', 'Kelwadkar', 'amolkharate@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, 2, 'test', '', '', '12345674', '', 0, '/media/profile/16-dramatic-upsweep-with-faded-sides.jpg', 2, 'VMV', 1, '2019-04-02 18:29:14', '0000-00-00 00:00:00', '', '9fc6bbfc8bc75250020a67a8e1a69d64', 1, '6hwB0rAt9GKez8Mrlqz3E70kE', 0, '2019-04-02 18:29:14', '2019-05-07 07:07:57', '', ''),
(7, 'test', 'test', 'amol@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '12345666', 'test', '123456546', 'testt', 0, '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '0000-00-00 00:00:00', '2019-04-17 03:41:57', '', ''),
(8, 'Lomesh', 'Kelwadkar', 'lomesh53872@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur Railway Station, Sitabuldi, Nagpur, Maharashtra, India', 1, '', 1, '', 1, '2019-04-05 09:12:06', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-05 09:12:06', '2019-04-17 03:42:00', '', ''),
(9, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '7888888888', 'test', '9996663367', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:08:26', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:08:26', '2019-04-17 03:42:03', '', ''),
(10, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:12:40', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:12:40', '2019-04-17 03:43:42', '', ''),
(11, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:14:42', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:14:42', '2019-04-17 03:47:17', '', ''),
(12, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test 12366', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '/media/profile/pho_21.jpg', 1, '', 1, '2019-04-17 09:17:56', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:17:56', '2019-04-28 16:51:01', '', ''),
(13, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 0, '', 1, '', 0, '2019-04-24 14:30:21', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-24 14:30:21', '2019-04-28 16:51:05', '', ''),
(14, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, NULL, NULL, '', '', NULL, '', 0, '/media/profile/60333.jpg', 2, 'VMV', 1, '2019-04-28 22:21:14', '0000-00-00 00:00:00', '', '9fc6bbfc8bc75250020a67a8e1a69d64', 1, NULL, 0, '2019-04-28 22:21:14', '2019-04-29 11:03:20', '', ''),
(15, 'Lomesh', 'Kelwadkar', 'lomesh538744@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test 123', '9996666666', 'test', '9996663366', 'Nagoya, Aichi, Japan', 1, '', 1, '', 0, '2019-04-29 16:51:26', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-04-29 16:51:26', '2019-04-29 11:21:26', '', ''),
(16, 'Lomesh', 'Kelwadkar', 'lomesh53879@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 8, NULL, NULL, '', '', NULL, '', 0, '', 2, 'VMV', 1, '2019-04-29 16:53:19', '0000-00-00 00:00:00', '', 'b8c5430a26e66c04833ca6d5c6e5f7c2', 1, NULL, 0, '2019-04-29 16:53:19', '2019-04-29 11:24:29', '', ''),
(17, 'Rohit', 'Kelwadkar', 'rohit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'te', '8553652558', 'test', '7414252552', 'Nashville, TN, USA', 1, '', 1, '', 1, '2019-05-02 08:34:31', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-05-02 08:34:31', '2019-05-02 03:06:54', '', ''),
(18, 'teacher2', 'test', 'teacher2@yopmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, 2, 'test', '7587564534', 'testing', '7676767687', 'Karlsruhe, Germany', 1, '', 1, '', 1, '2019-05-02 15:24:10', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-05-02 15:24:10', '2019-05-02 09:55:56', '', ''),
(19, 'teacher2', 'test', 'teacher2@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '7987568756', 'test', '5687563465', 'Key West, FL, USA', 1, '', 1, '', 1, '2019-05-02 15:27:26', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-05-02 15:27:26', '2019-05-02 09:57:58', '', ''),
(20, 'test', 'student', 'durgesh.alkurn@gmail.com', '25f9e794323b453885f5181f1b624d0b', 12, NULL, NULL, '', '', NULL, '', 0, '', 2, 'Alkurn', 0, '2019-05-06 19:00:22', '0000-00-00 00:00:00', '', '882d38db0ee6c2ad82f84576d2c4944a', 0, NULL, 0, '2019-05-06 19:00:22', '2019-05-06 13:30:22', '', ''),
(21, 'teacher7', 'test', 'teacher7@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '763762767', 'test', '768767675', 'Saratoga Springs, NY, USA', 1, '', 1, '', 1, '2019-05-07 11:33:18', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-05-07 11:33:18', '2019-05-07 06:09:29', '', ''),
(22, 'teacher7', 'test', 'teacher7@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '8087458756', 'test', '9975301429', 'Raleigh, NC, USA', 1, '', 1, '', 1, '2019-05-07 11:41:41', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-05-07 11:41:41', '2019-05-07 07:15:44', '', ''),
(23, 'student7', 'test', 'student7@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, NULL, NULL, '', '', NULL, '', 0, '', 2, 'jit', 1, '2019-05-07 12:23:19', '0000-00-00 00:00:00', '', 'f8db02fd9c97fad699662d46d04b8bca', 0, NULL, 1, '2019-05-07 12:23:19', '2019-05-07 07:15:55', '', ''),
(24, 'teacher7', 'test', 'teacher7@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '8087203010', 'test', '6756576576', 'Saraf market, nazai bazar, lalitpur U.P., Talabpura Road, Talabpura, Lakshmipura, Lalitpur, Uttar Pradesh, India', 1, '', 1, '', 1, '2019-05-07 12:49:37', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-05-07 12:49:37', '2019-05-07 08:57:25', '', ''),
(25, 'teacher7', 'test', 'teacher7@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '9975301429', 'test', '8087568756', 'Tulum, Quintana Roo, Mexico', 1, '', 1, '', 1, '2019-05-07 14:40:54', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-05-07 14:40:54', '2019-05-07 09:11:11', '', ''),
(26, 'teacher7a', 'test', 'teacher7a@yopmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '', '', '575474574', '', 1, '/media/profile/2044604-content-pretty-indian-woman-making-frame-gesture.jpg', 1, '', 1, '2019-05-07 16:29:48', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-05-07 16:29:48', '2019-05-07 12:44:12', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_date` datetime NOT NULL,
  `admin_section` varchar(5000) DEFAULT NULL,
  `admin_module` varchar(5000) DEFAULT NULL,
  `user_modified` datetime NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_permission` text NOT NULL,
  `userType` int(4) NOT NULL,
  `userStatus` tinyint(4) NOT NULL,
  `module_permission` text NOT NULL,
  `subModule_permission` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `gender` text NOT NULL,
  `mobile` text NOT NULL,
  `user_Type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_pass`, `user_date`, `admin_section`, `admin_module`, `user_modified`, `user_last_login`, `user_permission`, `userType`, `userStatus`, `module_permission`, `subModule_permission`, `username`, `password`, `is_deleted`, `name`, `gender`, `mobile`, `user_Type`) VALUES
(1, 'admin@gmail.com', '$2a$08$l7uwxaOK2UbKOX8t.1yh.eKEfZ4TOaREBWamr16m2G0Y5lB/51u6m', '2016-11-05 17:36:38', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46', '2017-06-17 11:44:17', '2019-05-07 11:34:09', 'dashboard,', 1, 0, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36', '1,2,3,4,5,6,7,8,14,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62', 'test@gmail.com', '7cd9d2e3065cef4756b2b7132dc40042', 1, 'Asfak', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_button_pallate`
--
ALTER TABLE `tbl_button_pallate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_completed_test`
--
ALTER TABLE `tbl_completed_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_invited_exam_requests`
--
ALTER TABLE `tbl_invited_exam_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_invite_new_student`
--
ALTER TABLE `tbl_invite_new_student`
  ADD PRIMARY KEY (`invite_new_id`);

--
-- Indexes for table `tbl_manage_folder`
--
ALTER TABLE `tbl_manage_folder`
  ADD PRIMARY KEY (`folder_id`);

--
-- Indexes for table `tbl_module`
--
ALTER TABLE `tbl_module`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `tbl_question_anser`
--
ALTER TABLE `tbl_question_anser`
  ADD PRIMARY KEY (`que_ans_id`);

--
-- Indexes for table `tbl_question_option_1`
--
ALTER TABLE `tbl_question_option_1`
  ADD PRIMARY KEY (`question_option_id`);

--
-- Indexes for table `tbl_question_pair`
--
ALTER TABLE `tbl_question_pair`
  ADD PRIMARY KEY (`pair_id`);

--
-- Indexes for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  ADD PRIMARY KEY (`que_id`),
  ADD KEY `que_type_id` (`que_type_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `tbl_q_type_master`
--
ALTER TABLE `tbl_q_type_master`
  ADD PRIMARY KEY (`question_type_id`);

--
-- Indexes for table `tbl_section`
--
ALTER TABLE `tbl_section`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `tbl_standard_class`
--
ALTER TABLE `tbl_standard_class`
  ADD PRIMARY KEY (`standard_class_id`);

--
-- Indexes for table `tbl_subject_class`
--
ALTER TABLE `tbl_subject_class`
  ADD PRIMARY KEY (`subject_class_id`);

--
-- Indexes for table `tbl_system_config`
--
ALTER TABLE `tbl_system_config`
  ADD PRIMARY KEY (`sysytem_config_id`);

--
-- Indexes for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `folder_id` (`folder_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `folder_id_2` (`folder_id`);

--
-- Indexes for table `tbl_user_login_status`
--
ALTER TABLE `tbl_user_login_status`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tbl_user_question_answered`
--
ALTER TABLE `tbl_user_question_answered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `standard_class_id` (`standard_class_id`),
  ADD KEY `subject_class_id` (`subject_class_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_button_pallate`
--
ALTER TABLE `tbl_button_pallate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_completed_test`
--
ALTER TABLE `tbl_completed_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_invited_exam_requests`
--
ALTER TABLE `tbl_invited_exam_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_invite_new_student`
--
ALTER TABLE `tbl_invite_new_student`
  MODIFY `invite_new_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_manage_folder`
--
ALTER TABLE `tbl_manage_folder`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_module`
--
ALTER TABLE `tbl_module`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_question_anser`
--
ALTER TABLE `tbl_question_anser`
  MODIFY `que_ans_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tbl_question_option_1`
--
ALTER TABLE `tbl_question_option_1`
  MODIFY `question_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `tbl_question_pair`
--
ALTER TABLE `tbl_question_pair`
  MODIFY `pair_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  MODIFY `que_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tbl_q_type_master`
--
ALTER TABLE `tbl_q_type_master`
  MODIFY `question_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_section`
--
ALTER TABLE `tbl_section`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_standard_class`
--
ALTER TABLE `tbl_standard_class`
  MODIFY `standard_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_subject_class`
--
ALTER TABLE `tbl_subject_class`
  MODIFY `subject_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_system_config`
--
ALTER TABLE `tbl_system_config`
  MODIFY `sysytem_config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tbl_user_login_status`
--
ALTER TABLE `tbl_user_login_status`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `tbl_user_question_answered`
--
ALTER TABLE `tbl_user_question_answered`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  ADD CONSTRAINT `tbl_que_creation_ibfk_1` FOREIGN KEY (`que_type_id`) REFERENCES `tbl_q_type_master` (`question_type_id`),
  ADD CONSTRAINT `tbl_que_creation_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tbl_test_creation` (`test_id`);

--
-- Constraints for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  ADD CONSTRAINT `tbl_test_creation_ibfk_1` FOREIGN KEY (`folder_id`) REFERENCES `tbl_manage_folder` (`folder_id`),
  ADD CONSTRAINT `tbl_test_creation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user_registration` (`user_id`);

--
-- Constraints for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD CONSTRAINT `tbl_user_registration_ibfk_1` FOREIGN KEY (`standard_class_id`) REFERENCES `tbl_standard_class` (`standard_class_id`),
  ADD CONSTRAINT `tbl_user_registration_ibfk_2` FOREIGN KEY (`subject_class_id`) REFERENCES `tbl_subject_class` (`subject_class_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
