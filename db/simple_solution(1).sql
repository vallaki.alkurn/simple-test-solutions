-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2019 at 02:02 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simple_solution`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('n1ti4r3m8vn6295nr53ldk52kc88g3ku', '::1', 1556095409, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535363038383634393b),
('34fqng9noimq084lqgdi0usmol6rap45', '::1', 1556106433, 0x5f5f63695f6c6173745f726567656e65726174657c693a313535363130333836313b757365725f73657373696f6e7c4f3a383a22737464436c617373223a32383a7b733a373a22757365725f6964223b733a323a223132223b733a31303a2266697273745f6e616d65223b733a363a224c6f6d657368223b733a393a226c6173745f6e616d65223b733a393a224b656c7761646b6172223b733a31333a22656d61696c5f61646472657373223b733a32303a226c6f6d6573683533383740676d61696c2e636f6d223b733a31333a22757365725f70617373776f7264223b733a33323a226531306164633339343962613539616262653536653035376632306638383365223b733a31373a227374616e646172645f636c6173735f6964223b4e3b733a31363a227375626a6563745f636c6173735f6964223b733a313a2232223b733a31323a226f7267616e697a6174696f6e223b733a31303a2254657374203132333636223b733a32353a226f7267616e697a6174696f6e5f70686f6e655f6e756d626572223b733a31303a2239393936363636363636223b733a31303a22796f75725f7469746c65223b733a343a2274657374223b733a31303a22796f75725f70686f6e65223b733a31303a2239393936363633333636223b733a373a2261646472657373223b733a32363a224e61677075722c204d616861726173687472612c20496e646961223b733a31363a22746561726d5f636f6e646974696f6e73223b733a313a2231223b733a31333a2270726f66696c655f70686f746f223b733a32353a222f6d656469612f70726f66696c652f70686f5f32312e6a7067223b733a393a22757365725f74797065223b733a313a2231223b733a31343a227363686f6f6c5f636f6c6c616765223b733a303a22223b733a31373a2269735f70726f66696c655f737461747573223b733a313a2231223b733a31353a22726567697374657265645f64617465223b733a31393a22323031392d30342d31372030393a31373a3536223b733a31333a22617070726f7665645f64617465223b733a31393a22303030302d30302d30302030303a30303a3030223b733a31323a227061796d656e745f6d6f6465223b733a303a22223b733a32313a22766572696669636174696f6e5f62795f656d61696c223b733a303a22223b733a32383a22766572696669636174696f6e5f62795f656d61696c5f737461747573223b733a313a2230223b733a31383a2272657365745f70617373776f72645f737472223b4e3b733a31303a2269735f64656c65746564223b733a313a2230223b733a373a2263726561746564223b733a31393a22323031392d30342d31372030393a31373a3536223b733a383a226d6f646966696564223b733a31393a22323031392d30342d32342031363a32383a3236223b733a31303a22637265617465645f6279223b733a303a22223b733a31313a226d6f6469666965645f6279223b733a303a22223b7d6c6173745f73657373696f6e5f69647c693a34313b);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_manage_folder`
--

CREATE TABLE `tbl_manage_folder` (
  `folder_id` int(11) NOT NULL,
  `folder_name` varchar(100) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module`
--

CREATE TABLE `tbl_module` (
  `_id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `faIcon` varchar(255) NOT NULL,
  `Controller_name` varchar(255) NOT NULL,
  `method_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `is_all` int(11) NOT NULL,
  `module_type` int(11) NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_module`
--

INSERT INTO `tbl_module` (`_id`, `module_name`, `faIcon`, `Controller_name`, `method_name`, `status`, `is_all`, `module_type`, `section_id`) VALUES
(1, 'Standard', '', 'MasterController', 'StandardExecute', 1, 1, 1, 1),
(2, 'General', '', 'SystemSettingController', 'GeneralSettingExecute', 1, 1, 1, 9),
(3, 'All Student List', '', 'StudentsController', 'getAllStudents', 1, 1, 1, 2),
(4, 'All Approved', '', 'StudentsController', 'getAllApprovedStudents', 1, 1, 1, 2),
(5, 'All Nonapproved', '', 'StudentsController', 'getAllNonApprovedStudents', 1, 1, 1, 5555),
(6, 'Pending', '', 'StudentsController', 'getAllDisapprovedStudents', 1, 1, 1, 2),
(7, 'All Teachers List', '', 'TeachersController', 'getAllTeachers', 1, 1, 1, 3),
(8, 'All Approved', '', 'TeachersController', 'getAllApprovedTeachers', 1, 1, 1, 3),
(9, 'All Non Approved', '', 'TeachersController', 'getAllNonApprovedTeachers', 1, 1, 1, 3),
(10, 'All Disapproved', '', 'TeachersController', 'getAllDisapprovedTeachers', 1, 1, 1, 3),
(11, 'Subject', '', 'MasterController', 'SubjectExecute', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_option_creation`
--

CREATE TABLE `tbl_option_creation` (
  `option_id` int(11) NOT NULL,
  `option_list` varchar(255) NOT NULL,
  `option_ans` varchar(255) DEFAULT NULL,
  `que_id` int(11) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_que_creation`
--

CREATE TABLE `tbl_que_creation` (
  `que_id` int(11) NOT NULL,
  `que_name` varchar(55) NOT NULL,
  `que_type_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `que_option` tinyint(1) NOT NULL,
  `que_rows` int(11) NOT NULL,
  `que_points` int(11) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_q_type_master`
--

CREATE TABLE `tbl_q_type_master` (
  `question_type_id` int(11) NOT NULL,
  `question_type_name` varchar(55) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section`
--

CREATE TABLE `tbl_section` (
  `_id` int(11) NOT NULL,
  `sectionName` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_section`
--

INSERT INTO `tbl_section` (`_id`, `sectionName`, `status`) VALUES
(1, 'Master', 1),
(2, 'Students', 1),
(3, 'Teachers', 1),
(4, 'Reports', 1),
(5, 'Exam', 1),
(6, 'Result', 1),
(7, 'Payments', 1),
(8, 'Blog', 1),
(9, 'System Setting', 1),
(10, 'Administrator', 1),
(11, 'CMS', 1),
(12, 'Promotions', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_standard_class`
--

CREATE TABLE `tbl_standard_class` (
  `standard_class_id` int(11) NOT NULL,
  `standard_class_name` varchar(60) NOT NULL,
  `is_status` int(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_standard_class`
--

INSERT INTO `tbl_standard_class` (`standard_class_id`, `standard_class_name`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'History', 0, 0, '0000-00-00 00:00:00', '2019-03-22 22:05:28', '', ''),
(2, 'test', 1, 0, '0000-00-00 00:00:00', '2019-03-23 13:58:33', '', ''),
(3, 'fsf', 1, 0, '0000-00-00 00:00:00', '2019-03-23 13:58:44', '', ''),
(4, 'trsy', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:23:27', '', ''),
(5, 'a', 0, 0, '0000-00-00 00:00:00', '2019-03-23 14:23:54', '', ''),
(6, 'eadfaf', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:24:28', '', ''),
(7, 'sd', 0, 0, '0000-00-00 00:00:00', '2019-03-23 14:24:52', '', ''),
(8, 'saddsd', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:27:22', '', ''),
(9, 'sdsd', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:27:30', '', ''),
(10, 'a', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:29:16', '', ''),
(11, 'a', 1, 0, '0000-00-00 00:00:00', '2019-03-23 14:29:21', '', ''),
(12, 'test', 0, 0, '0000-00-00 00:00:00', '2019-03-25 19:26:57', '', ''),
(13, 'test new', 1, 1, '0000-00-00 00:00:00', '2019-03-23 14:35:24', '', ''),
(14, 'fsddf', 1, 1, '0000-00-00 00:00:00', '2019-03-23 14:35:03', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject_class`
--

CREATE TABLE `tbl_subject_class` (
  `subject_class_id` int(11) NOT NULL,
  `subject_class_name` varchar(60) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subject_class`
--

INSERT INTO `tbl_subject_class` (`subject_class_id`, `subject_class_name`, `is_status`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'English', 0, 0, '0000-00-00 00:00:00', '2019-04-02 18:09:40', '', ''),
(2, 'History', 1, 0, '0000-00-00 00:00:00', '2019-03-25 17:29:57', '', ''),
(3, 'Mathematics', 1, 1, '0000-00-00 00:00:00', '2019-03-25 19:31:38', '', ''),
(4, 'sdsd', 1, 1, '0000-00-00 00:00:00', '2019-03-25 19:22:46', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_system_config`
--

CREATE TABLE `tbl_system_config` (
  `sysytem_config_id` int(11) NOT NULL,
  `system_config_key` varchar(255) NOT NULL,
  `system_config_value` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(60) NOT NULL,
  `modified_by` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_system_config`
--

INSERT INTO `tbl_system_config` (`sysytem_config_id`, `system_config_key`, `system_config_value`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'company_name', 'Test Solution', '0000-00-00 00:00:00', '2019-03-21 05:49:11', '', ''),
(2, 'company_address', 'Nagpur', '0000-00-00 00:00:00', '2019-03-16 15:46:17', '', ''),
(3, 'company_website', 'www.test.com', '0000-00-00 00:00:00', '2019-03-16 15:46:25', '', ''),
(4, 'company_email', 'test@gmail.com', '0000-00-00 00:00:00', '2019-03-16 15:46:31', '', ''),
(5, 'company_phone', '9970426205', '0000-00-00 00:00:00', '2019-03-16 15:46:34', '', ''),
(6, 'company_fax', 'fax1', '0000-00-00 00:00:00', '2019-03-16 15:46:37', '', ''),
(7, 'dateformat', 'd.m.Y', '0000-00-00 00:00:00', '2019-03-16 16:09:03', '', ''),
(8, 'protocol', 'smtp', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(9, 'mailpath', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(10, 'smtp_host', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(11, 'smtp_port', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(12, 'smtp_crypto', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(13, 'smtp_user', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', ''),
(14, 'smtp_pass', '', '0000-00-00 00:00:00', '2019-03-16 16:41:44', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test_creation`
--

CREATE TABLE `tbl_test_creation` (
  `test_id` int(11) NOT NULL,
  `test_name` varchar(60) NOT NULL,
  `test_uniqe_code` varchar(60) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_form_date` datetime NOT NULL,
  `test_to_date` datetime NOT NULL,
  `test_access_code` varchar(10) NOT NULL,
  `test_time_limit` varchar(20) NOT NULL,
  `test_status` int(3) NOT NULL,
  `is_status` tinyint(1) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_login_status`
--

CREATE TABLE `tbl_user_login_status` (
  `session_id` int(11) NOT NULL,
  `usermaster_id` int(11) DEFAULT NULL,
  `ipAddress` varchar(50) DEFAULT NULL,
  `loginIn` datetime(6) DEFAULT NULL,
  `logOut` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_login_status`
--

INSERT INTO `tbl_user_login_status` (`session_id`, `usermaster_id`, `ipAddress`, `loginIn`, `logOut`) VALUES
(1, 1, '::1', '2019-04-17 08:42:55.000000', '2019-04-17 08:42:59.000000'),
(2, 1, '::1', '2019-04-17 08:43:07.000000', '2019-04-17 08:46:30.000000'),
(3, 12, '::1', '2019-04-17 09:23:09.000000', '2019-04-17 09:25:51.000000'),
(4, 12, '::1', '2019-04-17 09:25:58.000000', '2019-04-17 09:26:17.000000'),
(5, 12, '::1', '2019-04-17 09:26:36.000000', '2019-04-17 09:26:44.000000'),
(6, 12, '::1', '2019-04-21 10:39:17.000000', '2019-04-21 11:16:19.000000'),
(7, 12, '::1', '2019-04-21 11:22:17.000000', '2019-04-21 11:54:11.000000'),
(8, 5, '::1', '2019-04-21 11:54:19.000000', '2019-04-21 12:01:30.000000'),
(9, 12, '::1', '2019-04-21 12:01:47.000000', '2019-04-21 12:02:13.000000'),
(10, 12, '::1', '2019-04-21 12:02:26.000000', '2019-04-21 12:02:53.000000'),
(11, 12, '::1', '2019-04-21 12:03:05.000000', '2019-04-21 12:03:46.000000'),
(12, 5, '::1', '2019-04-21 12:03:56.000000', '2019-04-21 12:04:10.000000'),
(13, 12, '::1', '2019-04-21 12:04:36.000000', '2019-04-21 12:18:11.000000'),
(14, 12, '::1', '2019-04-21 12:19:06.000000', '2019-04-21 12:19:24.000000'),
(15, 5, '::1', '2019-04-21 12:19:44.000000', '2019-04-21 12:20:01.000000'),
(16, 12, '::1', '2019-04-21 12:20:50.000000', '2019-04-21 12:25:51.000000'),
(17, 12, '::1', '2019-04-21 12:26:01.000000', '2019-04-21 12:37:35.000000'),
(18, 12, '::1', '2019-04-21 12:37:46.000000', '2019-04-21 13:17:22.000000'),
(19, 5, '::1', '2019-04-21 13:17:32.000000', '2019-04-21 14:01:22.000000'),
(20, 12, '::1', '2019-04-21 14:01:36.000000', '2019-04-21 14:03:16.000000'),
(21, 12, '::1', '2019-04-22 22:42:47.000000', '2019-04-22 23:23:01.000000'),
(22, 12, '::1', '2019-04-22 23:23:37.000000', '2019-04-22 23:23:37.000000'),
(23, 12, '::1', '2019-04-24 08:14:15.000000', '2019-04-24 08:37:27.000000'),
(24, 5, '::1', '2019-04-24 08:37:33.000000', '2019-04-24 08:37:45.000000'),
(25, 12, '::1', '2019-04-24 08:37:52.000000', '2019-04-24 08:55:11.000000'),
(26, 12, '::1', '2019-04-24 08:55:56.000000', '2019-04-24 09:31:21.000000'),
(27, 12, '::1', '2019-04-24 09:31:31.000000', '2019-04-24 09:31:42.000000'),
(28, 12, '::1', '2019-04-24 09:32:23.000000', '2019-04-24 09:34:46.000000'),
(29, 5, '::1', '2019-04-24 09:34:52.000000', '2019-04-24 09:35:24.000000'),
(30, 12, '::1', '2019-04-24 09:35:30.000000', '2019-04-24 11:00:25.000000'),
(31, 12, '::1', '2019-04-24 11:00:32.000000', '2019-04-24 11:00:33.000000'),
(32, 12, '::1', '2019-04-24 11:02:44.000000', '2019-04-24 11:21:55.000000'),
(33, 12, '::1', '2019-04-24 11:24:07.000000', '2019-04-24 11:44:25.000000'),
(34, 12, '::1', '2019-04-24 11:44:58.000000', '2019-04-24 11:45:37.000000'),
(35, 5, '::1', '2019-04-24 11:45:49.000000', '2019-04-24 11:46:09.000000'),
(36, 12, '::1', '2019-04-24 12:15:44.000000', '2019-04-24 12:20:48.000000'),
(37, 12, '::1', '2019-04-24 14:30:05.000000', '2019-04-24 14:53:04.000000'),
(38, 12, '::1', '2019-04-24 15:02:48.000000', '2019-04-24 15:56:42.000000'),
(39, 12, '::1', '2019-04-24 15:57:05.000000', '2019-04-24 16:33:55.000000'),
(40, 5, '::1', '2019-04-24 16:34:04.000000', '2019-04-24 16:34:20.000000'),
(41, 12, '::1', '2019-04-24 16:34:30.000000', '2019-04-24 16:34:30.000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_registration`
--

CREATE TABLE `tbl_user_registration` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `standard_class_id` int(11) DEFAULT NULL,
  `subject_class_id` int(11) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `organization_phone_number` varchar(100) NOT NULL,
  `your_title` varchar(100) NOT NULL,
  `your_phone` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `tearm_conditions` tinyint(1) NOT NULL,
  `profile_photo` text NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1-Teacher, 2-Student',
  `school_collage` text NOT NULL,
  `is_profile_status` int(11) NOT NULL COMMENT ' 0 - Non-Approve, 1 - Approve , 2 - Disapprove ',
  `registered_date` datetime NOT NULL,
  `approved_date` datetime NOT NULL,
  `payment_mode` varchar(30) NOT NULL,
  `verification_by_email` varchar(255) NOT NULL,
  `verification_by_email_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-false,1-true',
  `reset_password_str` varchar(40) DEFAULT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL,
  `modified_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_registration`
--

INSERT INTO `tbl_user_registration` (`user_id`, `first_name`, `last_name`, `email_address`, `user_password`, `standard_class_id`, `subject_class_id`, `organization`, `organization_phone_number`, `your_title`, `your_phone`, `address`, `tearm_conditions`, `profile_photo`, `user_type`, `school_collage`, `is_profile_status`, `registered_date`, `approved_date`, `payment_mode`, `verification_by_email`, `verification_by_email_status`, `reset_password_str`, `is_deleted`, `created`, `modified`, `created_by`, `modified_by`) VALUES
(1, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, 2, 'test', '', '', '12345678', '', 0, '', 1, 'Central Point', 1, '2019-04-02 18:29:14', '0000-00-00 00:00:00', '', '9fc6bbfc8bc75250020a67a8e1a69d64', 1, '0oFMzdso7VfRmRQb05xqnuR51', 1, '2019-04-02 18:29:14', '2019-04-17 03:30:02', '', ''),
(5, 'Amol', 'K', 'amolkharate@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 9, 2, 'test', '', '', '12345674', '', 0, '', 2, 'Central Point', 1, '2019-04-02 18:29:14', '0000-00-00 00:00:00', '', '9fc6bbfc8bc75250020a67a8e1a69d64', 1, '6hwB0rAt9GKez8Mrlqz3E70kE', 0, '2019-04-02 18:29:14', '2019-04-21 06:24:01', '', ''),
(7, 'test', 'test', 'amol@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'test', '12345666', 'test', '123456546', 'testt', 0, '', 1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '0000-00-00 00:00:00', '2019-04-17 03:41:57', '', ''),
(8, 'Lomesh', 'Kelwadkar', 'lomesh53872@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur Railway Station, Sitabuldi, Nagpur, Maharashtra, India', 1, '', 1, '', 1, '2019-04-05 09:12:06', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-05 09:12:06', '2019-04-17 03:42:00', '', ''),
(9, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '7888888888', 'test', '9996663367', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:08:26', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:08:26', '2019-04-17 03:42:03', '', ''),
(10, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:12:40', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:12:40', '2019-04-17 03:43:42', '', ''),
(11, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '', 1, '', 0, '2019-04-17 09:14:42', '0000-00-00 00:00:00', '', '', 0, NULL, 1, '2019-04-17 09:14:42', '2019-04-17 03:47:17', '', ''),
(12, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', NULL, 2, 'Test 12366', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 1, '/media/profile/pho_21.jpg', 1, '', 1, '2019-04-17 09:17:56', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-04-17 09:17:56', '2019-04-24 10:58:26', '', ''),
(13, 'Lomesh', 'Kelwadkar', 'lomesh5387@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, 2, 'Test', '9996666666', 'test', '9996663366', 'Nagpur, Maharashtra, India', 0, '', 1, '', 0, '2019-04-24 14:30:21', '0000-00-00 00:00:00', '', '', 0, NULL, 0, '2019-04-24 14:30:21', '2019-04-24 09:00:21', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_date` datetime NOT NULL,
  `admin_section` varchar(5000) DEFAULT NULL,
  `admin_module` varchar(5000) DEFAULT NULL,
  `user_modified` datetime NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_permission` text NOT NULL,
  `userType` int(4) NOT NULL,
  `userStatus` tinyint(4) NOT NULL,
  `module_permission` text NOT NULL,
  `subModule_permission` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `gender` text NOT NULL,
  `mobile` text NOT NULL,
  `user_Type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_pass`, `user_date`, `admin_section`, `admin_module`, `user_modified`, `user_last_login`, `user_permission`, `userType`, `userStatus`, `module_permission`, `subModule_permission`, `username`, `password`, `is_deleted`, `name`, `gender`, `mobile`, `user_Type`) VALUES
(1, 'exam@gmail.com', '$2a$08$l7uwxaOK2UbKOX8t.1yh.eKEfZ4TOaREBWamr16m2G0Y5lB/51u6m', '2016-11-05 17:36:38', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46', '2017-06-17 11:44:17', '2019-04-18 22:52:09', 'dashboard,', 1, 0, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36', '1,2,3,4,5,6,7,8,14,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62', 'test@gmail.com', '7cd9d2e3065cef4756b2b7132dc40042', 1, 'Asfak', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_manage_folder`
--
ALTER TABLE `tbl_manage_folder`
  ADD PRIMARY KEY (`folder_id`);

--
-- Indexes for table `tbl_module`
--
ALTER TABLE `tbl_module`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `tbl_option_creation`
--
ALTER TABLE `tbl_option_creation`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `que_id` (`que_id`);

--
-- Indexes for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  ADD PRIMARY KEY (`que_id`),
  ADD KEY `que_type_id` (`que_type_id`),
  ADD KEY `test_id` (`test_id`);

--
-- Indexes for table `tbl_q_type_master`
--
ALTER TABLE `tbl_q_type_master`
  ADD PRIMARY KEY (`question_type_id`);

--
-- Indexes for table `tbl_section`
--
ALTER TABLE `tbl_section`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `tbl_standard_class`
--
ALTER TABLE `tbl_standard_class`
  ADD PRIMARY KEY (`standard_class_id`);

--
-- Indexes for table `tbl_subject_class`
--
ALTER TABLE `tbl_subject_class`
  ADD PRIMARY KEY (`subject_class_id`);

--
-- Indexes for table `tbl_system_config`
--
ALTER TABLE `tbl_system_config`
  ADD PRIMARY KEY (`sysytem_config_id`);

--
-- Indexes for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  ADD PRIMARY KEY (`test_id`),
  ADD KEY `folder_id` (`folder_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_user_login_status`
--
ALTER TABLE `tbl_user_login_status`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `standard_class_id` (`standard_class_id`),
  ADD KEY `subject_class_id` (`subject_class_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_manage_folder`
--
ALTER TABLE `tbl_manage_folder`
  MODIFY `folder_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_module`
--
ALTER TABLE `tbl_module`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_option_creation`
--
ALTER TABLE `tbl_option_creation`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  MODIFY `que_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_q_type_master`
--
ALTER TABLE `tbl_q_type_master`
  MODIFY `question_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_section`
--
ALTER TABLE `tbl_section`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_standard_class`
--
ALTER TABLE `tbl_standard_class`
  MODIFY `standard_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_subject_class`
--
ALTER TABLE `tbl_subject_class`
  MODIFY `subject_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_system_config`
--
ALTER TABLE `tbl_system_config`
  MODIFY `sysytem_config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user_login_status`
--
ALTER TABLE `tbl_user_login_status`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_option_creation`
--
ALTER TABLE `tbl_option_creation`
  ADD CONSTRAINT `tbl_option_creation_ibfk_1` FOREIGN KEY (`que_id`) REFERENCES `tbl_que_creation` (`que_id`);

--
-- Constraints for table `tbl_que_creation`
--
ALTER TABLE `tbl_que_creation`
  ADD CONSTRAINT `tbl_que_creation_ibfk_1` FOREIGN KEY (`que_type_id`) REFERENCES `tbl_q_type_master` (`question_type_id`),
  ADD CONSTRAINT `tbl_que_creation_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `tbl_test_creation` (`test_id`);

--
-- Constraints for table `tbl_test_creation`
--
ALTER TABLE `tbl_test_creation`
  ADD CONSTRAINT `tbl_test_creation_ibfk_1` FOREIGN KEY (`folder_id`) REFERENCES `tbl_manage_folder` (`folder_id`),
  ADD CONSTRAINT `tbl_test_creation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user_registration` (`user_id`);

--
-- Constraints for table `tbl_user_registration`
--
ALTER TABLE `tbl_user_registration`
  ADD CONSTRAINT `tbl_user_registration_ibfk_1` FOREIGN KEY (`standard_class_id`) REFERENCES `tbl_standard_class` (`standard_class_id`),
  ADD CONSTRAINT `tbl_user_registration_ibfk_2` FOREIGN KEY (`subject_class_id`) REFERENCES `tbl_subject_class` (`subject_class_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
