/*---------- Jquery Form Validation with PHP Form Data -----*/
/*---------------------------------------------------------
/*----------------| Lomesh Kelwadkar  |--------------------
/*----------------| Version : 1.0     |--------------------
/*----------------| Date : 23/09/2016 |--------------------
/*----------------| Nagpur    		  |--------------------
/*----------------| LK Group		  |--------------------
/*------------------------------------------------------*/
/*$(document).ready(function(){
		loaderIn();
		loaderOut();
	});*/
	//Start Loader
	
	
	var $loading = $('.loader,.loader-inner').hide();
                   //Attach the event handler to any element
                   $(document)
                     .ajaxStart(function () {
                        //ajax request went so show the loading image
                         $loading.show();
                     })
                   .ajaxStop(function () {
                       //got response so hide the loading image
                        $loading.hide();
                    });
	//loaderIn();
	function loaderIn()
	{
		$('.loader,.loader-inner').show();
	}
	//Hide Loader with check status
	function loaderOut(test)
	{
		$('.loader').fadeOut(1000,function(){
			//console.log(test);
			if(typeof(test) != "undefined"){
				if(test.status == 1){
					get_success(test.msg,test.url);
				}else{
					get_error(test.msg,test.url);
				}
			}
		});
	}
	//Get Success msg
	function get_success(msg,url){
		if(msg){
			$('.globel-msg').removeClass('g-error');
			$('.globel-msg').fadeIn('slow').html(msg);
			$('.globel-msg').addClass('g-success');
			$('.globel-msg').fadeOut(5000,function(){
				if(url){
					window.location.assign(url);
				}	
			});
		}
	}
	//Get Success msg
	function get_error(msg,url){
		$('.globel-msg').removeClass('g-success');
		$('.globel-msg').fadeIn('slow').html(msg);
	    $('.globel-msg').addClass('g-error');
	    $('.globel-msg').fadeOut(5000,function(){
			if(url){
				window.location.assign(url);
			}	
		});
	}


//Only Number
$(function() {
  $('.number').on('keydown',  function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
  
	$('.textName11').on('keydown',  function(e){
		 var regex = new RegExp(/^[a-zA-Z\s]+$/);
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else
			{
			e.preventDefault();
			//alert('Please Enter Alphabate');
			return false;
			}
	});
  
})
/* --------------- Lk Submit forms ---------------------*/
function lkForms(ID){
	var url = $('#'+ID).attr('url');
	var data = new FormData($('#'+ID)[0]);
	$.ajax({
		type:'POST',
		url:url,
		data:data,
		dataType:'json',
		async:false,
		processData: false,
		contentType: false,
		beforeSend: function(data){
			//loaderIn();
		},
		success: function (data) {
			if(data.status == false){
				$('.msg-gloabal').html(data.response.msg);
				//loaderOut({status:0,msg:data.response.msg,url:data.url});
			}else{
				$('.msg-gloabal').html(data.response.msg);
				$('#'+ID)[0].reset();
				if(data.url){
					$(":submit").attr("disabled", true);
					window.location.assign(data.url);
				}
				//loaderOut({status:1,msg:data.response.msg,url:data.url});
			}
			
		}
	});	
	return false;
}
//----------------- Validation of registration form ----------------//
$(function(){
	//Registration Form
	 $('#registration_student').validate({
		rules: {
				first_name: {
				   required : true,
		    	   minlength   : 3,
				},
				last_name: {
					required: true,
					minlength: 3,
				},
				email_address: {
					required: true,
					email: true
				},
				standard_class_id: {
					required: true,
				},
				user_password: {
					required: true,
					minlength: 5,
				},
				user_cpassword: {
					required: true,
					equalTo: "#user_password"
				},
				school_collage: {
					required: true,
				},
				tearm_conditions: {
					required: true,
				}
			},
		messages: {
				 first_name:{
					required :"First Name field is required",
				 }, 
				 last_name:{
					required :"Last Name field is required",
				 },
				 email_address:{
					required :"Email Address field is required",
				 },
				 standard_class_id:{
					required :"Standard/Class field is required",
				 },
				 user_password:{
					required :"Password field is required",
				 },
				  user_cpassword:{
					required :"Confirm Password field is required",
					equalTo :"Password and confirmation password do not match",
				 },
				 school_collage:{
					required :"School/College field is required",
				 },
				 tearm_conditions: {
					required: 'Please check terms of use & privacy policy',
				},
			},
		submitHandler: function(form) {
			if($('#tearm_conditions').is(":checked")){
				$('#tearm').html(''); 
				lkForms('registration_student');
				$(window).scrollTop(0);
			}else{
				$('#tearm').show().html('Please check terms of use & privacy policy'); 
			}
			//form.submit();
		  }
	 });
});

$(function(){
	//Registration Form
	 $('#registration_teacher').validate({
		rules: {
				first_name: {
				   required : true,
		    	   minlength   : 3,
				},
				last_name: {
					required: true,
					minlength: 3,
				},
				email_address: {
					required: true,
					email: true
				},
				subject_class_id: {
					required: true,
				},
				user_password: {
					required: true,
					minlength: 5,
				},
				user_cpassword: {
					required: true,
					equalTo: "#user_password"
				},
				organization: {
					required: true,
				},
				your_phone: {
					required: true,
					digits: true,
					minlength: 8,
					maxlength: 10
				},
				organization_phone_number: {
					digits: true,
					minlength: 8,
					maxlength: 10
				},
				tearm_conditions: {
					required: true,
				}
			},
		messages: {
				 first_name:{
					required :"First Name field is required",
				 }, 
				 last_name:{
					required :"Last Name field is required",
				 },
				 email_address:{
					required :"Email Address field is required",
				 },
				 subject_class_id:{
					required :"Subject field is required",
				 },
				 user_password:{
					required :"Password field is required",
				 },
				  user_cpassword:{
					required :"Confirm Password field is required",
					equalTo :"Password and confirmation password do not match",
				 },
				 organization:{
					required :"Organization field is required",
				 },
				 your_phone:{
					required :"Your Phone field is required",
					minlength: "phone no should be between 8 to 10 character",
					maxlength: "phone no should be between 8 to 10 character",
				 },
				 organization_phone_number: {
					minlength: "phone no should be between 8 to 10 character",
					maxlength: "phone no should be between 8 to 10 character",
				},
				tearm_conditions: {
					required: 'Please check terms of use & privacy policy',
				}
			},
		submitHandler: function(form) {
			if($('#tearm_conditions').is(":checked")){
				$('#tearm').html(''); 
				lkForms('registration_teacher');
				$(window).scrollTop(0);
			}else{
				$('#tearm').show().html('Please check terms of use & privacy policy');
				//$(window).scrollTop(0); 
			}
			//form.submit();
		  }
	 });

	 //User Login
	 $('#auth_with_login').validate({
		rules: {
				user_email: {
				   required : true,
				},
				user_password: {
					required: true,
				},
			},
		messages: {
				 user_email:{
					required :"Email Address field is required",
				 }, 
				 user_password:{
					required :"Password field is required",
				 }
			},
		submitHandler: function(form) {			
			 if($('#user_remember').is(":checked")){
			 	var testObject = { 'user_email': $('#email').val(), 'user_password': $('#pwd').val()};
				localStorage.setItem('remember_user', JSON.stringify(testObject));
			 }else{
				var testObject = { 'user_email': '', 'user_password': ''};
				localStorage.setItem('remember_user', JSON.stringify(testObject));
			 }
			 loaderIn();
			 lkForms('auth_with_login');
			//form.submit();
		  }
	 });
	 
	 var retrievedObject = localStorage.getItem('remember_user');
	 if(retrievedObject){
		 var user_details = JSON.parse(retrievedObject);
		 $('#email').val(user_details.user_email);
		 $('#pwd').val(user_details.user_password);
		 if(user_details.user_email != ""){  $('#user_remember').attr('checked',true); }
	 }
    // console.log('remember_user: ', JSON.parse(retrievedObject));
	
	
	//User Login
	 $('#reset_password').validate({
		rules: {
				user_email: {
				   required : true,
				}
			},
		messages: {
				 user_email:{
					required :"Email Address field is required",
				 }
			},
		submitHandler: function(form) {	
			 loaderIn();		
			 form.submit();
		  }
	 });
	 
	 //User Login
	 $('#change_password').validate({
		rules: {
				new_user_password: {
				   required : true,
				},
				new_user_password_confirm: {
				   required : true,
				   equalTo: "#new_user_password"
				}
			},
		messages: {
				 new_user_password:{
					required :"Password field is required",
				 },
				 new_user_password_confirm:{
					required :"Confirm Password field is required",
					equalTo :"Password and confirmation password do not match",
				 }
			},
		submitHandler: function(form) {	
			 loaderIn();		
			 form.submit();
		  }
	 });
	 	//Registration Form
	 $('#card_details').validate({
		rules: {
				card_number: {
				   required : true,
				   digits: true,
				   minlength   : 16,
					maxlength   : 20,
				  
				},
				cardholder_name: {
				   required : true,
				},
				cvv_number: {
					minlength   : 3,
					maxlength   : 3,
				    required : true,
					digits: true,
				},
				expiry_date: {
				   required : true,
				   
				},
			},
		messages: {
				 card_number:{
					required :"Card Number field is required",
					digits:'Card Number should be numeric',
					minlength   :'Card Number at least 16 digit.',
					maxlength   :'Card Number at least 20 digit.',
				
				 },
				 cardholder_name:{
					required :"Cardholder Name field is required",
				 },
				 cvv_number:{
					required : "CVV field is required",
					minlength   :'CVV at least 3 digit.',
					maxlength   :'CVV at least 4 digit.',
					digits:'only digits'
				 },
				 expiry_date:{
					required :"Ex Date field is required",
				 }, 
			},
		submitHandler: function(form) {
				lkForms('card_details');
				$(window).scrollTop(0);
			
		  }
	 });
	 
});


