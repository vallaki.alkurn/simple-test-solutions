/* --------------- Lk Submit forms ---------------------*/
function lkForms(ID){
	
	var url = $('#'+ID).attr('url');
	var data = new FormData($('#'+ID)[0]);
	
	$.ajax({
		type:'POST',
		url:url,
		data:data,
		dataType:'json',
		processData: false,
		contentType: false,
		beforeSend: function(data){
			$('#cload').show();
		},
		success: function (data) {
			$('#cload').hide();
			if(data.error){
				$('#lk-erorr').show(300).html(data.error);
				closeMsg();
			}else{
				$('#lk-success').show(300).html(data.msg);
				if(data.edit != 'yes'){
					$('#'+ID)[0].reset();
				}
				$('#'+ID)[0].reset();	
				closeMsg();
				if(data.url){
					window.location.assign(data.url)
				}
				
			}
			//$('#reg_submit').val('Submit');
		}

	});	
	return false;
}

function closeMsg()
{
	$('#example').DataTable().ajax.reload();
	$('#lk-success').delay(5000).fadeOut('slow');
	$('#lk-erorr').delay(5000).fadeOut('slow');
}

function rowDelete(rowID)
{
	var url = $('#'+rowID).attr('url');
	var uid = $('#'+rowID).attr('uid');
	var utable = $('#'+rowID).attr('utable');
	var ucolumn = $('#'+rowID).attr('ucolumn');
	//alert(url+uid+utable+ucolumn);
	var data = 'row_id='+uid+'&row_table='+utable+'&row_column='+ucolumn;
	//alert(data);
	//return false;
	if(confirm('Are you sure you want to delete this record?')){
		$.ajax({
		  method: 'POST',
		  url: url,
		  data: data,
		  dataType:'json',
		  processData: false,
		  success: function(data){
			   $('#rowid_'+uid).hide();
			   $('#lk-success').show(200).html(data.msg);
			   closeMsg();
			  
			}
		})
	}else{
		return false;
	}	
}

/* ------------------------------ Change Status of Record ---------------*/
function rowStatus(rowID)
{
	var url = $('#'+rowID).attr('url');
	var uid = $('#'+rowID).attr('uid');
	var utable = $('#'+rowID).attr('utable');
	var ucolumn = $('#'+rowID).attr('ucolumn');
	var scolumn = $('#'+rowID).attr('scolumn');
	var status = $('#'+rowID).attr('status');
	//alert(url+uid+utable+ucolumn);
	var data = 'row_id='+uid+'&row_table='+utable+'&row_column='+ucolumn+'&scolumn='+scolumn+'&status='+status;
	//alert(data);
	//return false;
	if(confirm('Are you sure you want to change status?')){
		$.ajax({
		  method: 'POST',
		  url: url,
		  data: data,
		  dataType:'json',
		  processData: false,
		  success: function(data){
			  // $('#rowid_'+uid).hide();
			  $('#example').DataTable().ajax.reload();
			   $('#lk-success').show(200).html(data.msg);
			   closeMsg();
			  
			}
		})
	}else{
		return false;
	}	
}





function imageDelete(rowID)
{
	var url = $('#'+rowID).attr('url');
	var cID = rowID.split('_');
	var data = 'img_id='+cID[1];
	/*alert(cID[1]);
	return false;*/
	if(confirm('Are you sure you want to delete this record?')){
		$.ajax({
		  method: 'POST',
		  url: url,
		  data: data,
		  dataType:'json',
		  processData: false,
		  success: function(data){
			   $('#imgp_'+cID[1]).hide();
			}
		})
	}else{
		return false;
	}
}

/* ------------------------ Get Images --------------------*/
function setImage(pID,url)
{
	
	var url = url;
	var data = "product_id="+pID;
	
	$.ajax({
		type:'POST',
		url:url,
		data:data,
		dataType:'json',
		processData: false,
		beforeSend: function(data){
			$('#cload').show();
		},
		success: function (data) {
			$('#cload').hide();
			$('#pro-name').html(data.title);
			$('.product-list').html(data.html);
				
		}

	});	
}

function setmainImage(thisID)
{
	var url = $('#'+thisID).attr('url');
	var productID = $('#'+thisID).attr('productID');
	var cID = thisID.split('_');
	var data = "product_id="+productID+"&img_id="+cID[1];
	$.ajax({
		type:'POST',
		url:url,
		data:data,
		dataType:'json',
		processData: false,
		beforeSend: function(data){
			$('#cload').show();
		},
		success: function (data) {
			$('#cload').hide();
			alert(data.html);
		}

	});	
}