
$(document).ready(function () {
	
	//disable typing in date picker input
	$('#test_form_date,#test_to_date').keydown(function(e) {
		 e.preventDefault();
		 return false;
	});
	//disable typing in date picker input end

	//main date picker
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var checkin = $('#test_form_date,#test_to_date').datepicker({
		format: 'dd-mm-yyyy',
		startDate: now,
		autoclose: true,
		orientation: 'top left'
	})
	.on('changeDate', function(e){	
		selStartDate = e.date;
		var nextDay = new Date(e.date);
		nextDay.setDate(nextDay.getDate() );
		$('#test_form_date,#test_to_date').datepicker('setStartDate', nextDay);
		if(checkout.val() == '') checkout.focus();	

		if (checkout.datepicker('getDate') == 'Invalid Date') {
			var newDate = new Date(e.date)
			newDate.setDate(newDate.getDate());
			checkout.datepicker('update',newDate);
			checkout.focus();	
		}
		
		/*------------ Time Fucntion -------*/
		pickupTime($(this).val());
		   
	});	
	var checkout = $('#test_form_date,#test_to_date').datepicker({
		format: 'dd-mm-yyyy',
		startDate: now,
		autoclose: true,
		orientation: 'top'
	})
	.on('changeDate', function(e){					
	});	
	//main date picker end

});

function pickupTime(iddate)
{
	//salert(iddate);
	/*------------ Time Fucntion -------*/
	var data = 'caldate='+iddate;
	var url = $('#dpd1').attr('url');
	$.ajax({
		type:'POST',
		url:url,
		data:data,
		/*dataType:'json',*/
		success: function (data) {
			if(data){
				$('#tripTime').html(data);
			}
		}
	});	
}