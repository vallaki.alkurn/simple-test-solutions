function getdatatableRecord(Id,Url)
{
  table = $(Id).DataTable({
        dom: 'lBfrtip',
        buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
        ],
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                        "url": Url,
                        "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                {
                        "targets": [ -1 ], //last column
                        "orderable": false, //set not orderable
                },
                ],
        });
}


$(document).ready(function(){
    $('.btnShowAssets').click(function(){
        $('#assetsList').html('');
        // =============== Modified by @gieart_dotcom ===========
        $.ajax({
                type : 'GET',
                url : SERVER + 'Masteradmin/assets/browse_assets',
                success : function (images){
                    $('#assetsList').html(images);
                }
             });
        // =======================================================
    })
});


function setFeaturedImage(path){
    var asset_path = path.replace(BASE_URI,"");
    $('#featured_image').val(asset_path);
    
    $('.preview_featured_image').html('<img src="'+path+'" class="img-responsive thumbnail" onclick="removeFeaturedImage()" style="width:150px;height:150px;cursor:pointer"/>');
}

function removeFeaturedImage(){
    $('#featured_image').val('');
    $('.preview_featured_image').html('');
}

browseAsset = function(page){
    $('#assetsList').html('');
    // =============== Modified by @gieart_dotcom ===========
    $.ajax({
            type : 'GET',
            url : SERVER + 'Masteradmin/assets/browse_assets?page='+page,
            success : function (images){
                $('#assetsList').html(images);
            }
    });
    // =======================================================
}