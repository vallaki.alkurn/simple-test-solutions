
$(document).ready(function()
{
        
    });
function getQuestion1(optionArray,essayDetail,questionText,queType,essayexist){
   
    if(queType==8){
        return getMCQTrueFalse();
    }
    else if(queType==7){
        if(essayexist==0){
            
            return getMCQSingleChoice(optionArray);
        }
        else{
             hideEssay();
            return getMCQEssay(optionArray,essayDetail,questionText,queType);
        }

    }
    else if(queType==5){
        if(essayexist==0){
            return getMCQMutipleChoice(optionArray);
        }
        else{
            hideEssay();
            return  getMCQEssay(optionArray,essayDetail,questionText,queType);
        }
    }
    else if(queType==6){
        return getMCQSequence(optionArray);
    }
    else if(queType==3){
        return getMCQMatchMatrix(optionArray);
    }
    else if(queType==4){
        return  getMCQMatchtheFollowing(optionArray);
    }
    else if(queType==2){
        return  getMCQFillInTheBlanks(optionArray);
    }
}

function getMCQTrueFalse() {
    var html="";
    html = "<div style='width:auto; margin:10px 0 0 10px;'>";
    html = html + "<ul class='optionOver' style='width:auto; overflow:hidden;padding:5px;'>";/*here class optionOver and padding given by navneet*/
    html = html + "<li style='float:left; margin:0px 0 0 10px;'>";
    html = html + "<input type='radio' id='mcqTF' onclick='unCheckRadio();colorChange();'  name='mcqTF' value='true'>";
    html = html + "</li>";
    html = html + "<li style='float:left; margin:0px 0 0 10px; cursor:pointer;' onclick='trueFalseCheck(1)'>True</li>";
    html = html + "</li>";
    html = html + "</div>";

    html = html + "<div  id='mcqTF' style='width:auto; margin:10px 0 0 10px;'>";
    html = html + "<ul class='optionOver' style='width:auto; overflow:hidden;padding:5px;'>";/*here class optionOver and padding given by navneet*/
    html = html + "<li style='float:left; margin:0px 0 0 10px;'>";
    html = html + "<input type='radio'  id='mcqTF' onclick='unCheckRadio();colorChange();'  name='mcqTF' value='false'>";
    html = html + "</li>";
    html = html + "<li style='float:left; margin:0px 0 0 10px; cursor:pointer;' onclick='trueFalseCheck(0)'>False</li>";
    html = html + "</li>";
    html = html + "</div>";

    return html;
}

function getMCQSingleChoice(optionArray) {
    var html="";
    var htmlSequence="";
    //added by ankit garg for  suffle option value
    var suffle=(document.getElementById("testSetting").value).substring(2,3);
    var tempArray=new Array();
    var valueArray = getValueArray();
    var countArray=optionArray.length; 
    for (var i = 1; i < countArray; i++) {  
        tempArray[i-1]=optionArray[i];
    }
    if(suffle==1)
        suffleArray=arrayShuffle(tempArray);
    else
        suffleArray=tempArray;
    countArray=suffleArray.length; 
    for ( i = 0; i < countArray; i++) {
        //print_r($valueArray);
        var j = $.inArray( suffleArray[i],optionArray );
        html = "<div style='width:auto; margin:10px 0 0 10px; '>";
        html = html + "<ul class='optionOver' style='width:auto; overflow:hidden;padding:5px;'><label>";/*here class optionOver and padding given by navneet*/
        html = html + "<li style='float:left; margin:0px 10px 0 0px;'>";
        html = html + "<input type='radio' id='mcqSingle' name='mcqSingle' onclick='unCheckRadio();colorChange();' value='" + valueArray[j] + "' >";
        html = html + "</li>";
        html = html + "<li style='float:left; overflow:hidden;  margin:0px 0 0 10px; cursor:pointer;'>" + suffleArray[i] + "</li>";
        html = html + "</label>";
        html = html +"<li id='rightWrong_"+valueArray[j]+"' style='display:none;' class='wrong-question'>&nbsp;</li></ul>";
        html = html + "</div>";

        htmlSequence = htmlSequence + html;
    }
    // }
    return htmlSequence;
}


function getValueArray() {
    var valueArray = new Array();
    valueArray[1]="a";
    valueArray[2]="b";
    valueArray[3]="c";
    valueArray[4]="d";
    valueArray[5]="e";
    valueArray[6]="f";
    valueArray[7]="g";
    valueArray[8]="h";

    return valueArray;
}
function getValueArrayPQ() {
    var valueArray = new Array();
    valueArray[1]="p";
    valueArray[2]="q";
    valueArray[3]="r";
    valueArray[4]="s";
    valueArray[5]="t";
    valueArray[6]="u";
    valueArray[7]="v";
    valueArray[8]="w";

    return valueArray;
}


function getMCQSequence(optionArray) {
    var html="";
    var optionValue="";
    var htmlSequence="";
    var valueArray =getValueArray();
    //print_r($value);
    html = "<div style='width:auto; border:1px #d8d9db solid;'>";

    html = html + "<table width='100%' align='center' border='0px' bgcolor='#eff0f0' cellpadding='10px' cellspacing='0px' style='border-bottom:1px #d8d9db solid;'>";
    html = html + "  <tr style=''>";
    html = html + "<td style='width:80%;border-right:#d8d9db solid 1px; padding-left:10px; font-weight:bold;'>Sequence Options</td>";
    html = html + "<td style=' padding-left:10px; font-weight:bold; color:#345895;'>Correct Sequence</td>";
    html = html + " </tr>";
    html = html + " </table>";


    html = html + " <table width='100%' align='center' border='0px' cellspacing='0px' cellpadding='10px'>";
    var j = optionArray.length;
    var k = 1;
    for (var i = 1; i <optionArray.length; i++) {
        html = html + " <tr style='border-bottom:1px #C66 solid;'>";
        html = html + " <td style='width:4%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid; background-color:#eff0f0;'>" + valueArray[i] + "</td>";
        html = html + " <td style='width:76%;border-right:#d8d9db solid 1px; padding-left:10px;border-bottom:1px #d8d9db solid;'>" + optionArray[i] + "</td>";
        html = html + " <td style='width:4%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid;background-color:#eff0f0;'>" + i + "</td>";
        html = html + " <td style='padding-left:10px; border-bottom:1px #d8d9db solid;'>";
        html = html + " <select id='mcqSequence" + i + "' class='test-text'>";
        html = html + " <option value='-1'></option>";
        while (j > 0) {

            optionValue = optionValue + " <option value='" + valueArray[k] + "' onclick='colorChange();'>$valueArray[$k]</option>";
            k++;
            j--;
        }
        html = html + optionValue;
        html = html + " </select></td>";
        html = html + " </tr>";
    }
    html = html + " </table>";

    html = html + " </div>";

    htmlSequence = htmlSequence + html;

    return htmlSequence;
}

function getMCQEssay(optionArray, essayDetails, questionText, questionType) {
  
    var html="";
    html = "<div style='width:auto; border:1px #d8d9db solid; margin:20px 0 0 0px;cursor:pointer;cursor:hand;'>";

    html = html + "<table width='100%' align='center' border='0px' cellpadding='10px' cellspacing='0px' >";
    html = html + "<tr>";
    html = html + "<td style=' width:50%; border-right:#d8d9db solid 1px; padding-left:17px; background-color:#eff0f0;border-bottom:1px #d8d9db solid; font-weight:bold;'>Essay Paragraph</td>";
    html = html + "<td style=' width:50%; padding-left:17px; font-weight:bold; background-color:#eff0f0;border-bottom:1px #d8d9db solid;'>Question</td>";
    html = html + "</tr>";
    html = html + "<tr>";
    html = html + "<td style=' border-right:#d8d9db solid 0px; width:60%; '>";

    //html = html + $essayDetails;

    html = html + "<div style='width:auto; text-align:justify; line-height:16pt; padding:5px 5px 5px 5px;   height:360px; overflow:auto;'>";
    html = html + essayDetails;
    html = html + "</div>";

    html = html + "</td>";
    html = html + "<td valign='top'>";
    html = html + "<div style='width:auto;  height:380px; overflow:auto; padding:5px;'>";
    html = html + "<table width='99%' border='0' cellpadding='0px' cellspacing='0px'>";
    html = html + "<tr>";
    html = html + "<td align='left' valign='top' style='color:#000000; text-align:justify;padding:0 0 12px 5px;'>" + questionText + "</td>";
    html = html + "</tr>";
    html = html + "<tr>";
    html = html + "<td align='left' valign='top'>";
    html = html + "<table width='100%' border='0' cellpadding='0px' cellspacing='0px'>";

    if (questionType == "7") {       
        html = html + getMCQSingleChoiceForEssay(optionArray);
    } else {       
        html = html + getMCQMulitpleChoiceForEssay(optionArray);
    }
    html = html + "</table>";
    html = html + "</td>";
    html = html + "</tr>";
    html = html + "</table>";
    html = html + "</div>";
    html = html + "</td>";
    html = html + "</tr>";
    html = html + "</table>";
    html = html + "</div>";
    return html;
}

function getMCQMatchMatrix(optionArray) {

    var html="";
    var htmlSequence="";
    var valueArray =getValueArray();
    var valueArrayPQ =getValueArrayPQ();
    var optionCount = ((optionArray.length - 1)/2);
    html = "<div style='width:auto; border:1px #d8d9db solid;'>";
    html = html + "<table width='100%' align='center' border='0px' bgcolor='#eff0f0' cellpadding='10px' cellspacing='0px' style='border-bottom:1px #d8d9db solid;'>";
    html = html + "<tr style='border-bottom:1px #C66 solid;'>";
    html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;'>&nbsp;</td>";
    html = html + "<td style='width:43.5%;border-right:#d8d9db solid 1px; padding-left:10px;'>Choice</td>";
    html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;'>&nbsp;</td>";
    html = html + "<td style=' padding-left:10px;' >Match</td>";
    html = html + "</tr>";
    html = html + "</table>";

    html = html + "<table width='100%' align='center' border='0px' cellpadding='10px' cellspacing='0px'>";
     
    for (var i = 1; i <= optionCount; i++) {       
        var j=i+optionCount;
        html = html + "<tr style='border-bottom:1px #C66 solid;'>";
        html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid;'>" + valueArray[i].toUpperCase() + "</td>";
        html = html + "<td style='width:43.5%;border-right:#d8d9db solid 1px; padding-left:10px;border-bottom:1px #d8d9db solid;'>" + optionArray[i] + "</td>";
        html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid;'>" + valueArrayPQ[i].toUpperCase() + "</td>";
        html = html + "<td style=' padding-left:10px; border-bottom:1px #d8d9db solid;'>"
        + optionArray[j] + "</td>";
        html = html + "<td style='border-bottom:1px #d8d9db solid;'>&nbsp;</td>";
        html = html + "</tr>";
    }
    html = html + "</table>";
    html = html + "</div>";

    html = html + "<div style='width:auto;'>";
    html = html + "<div style='margin:20px 0 20px 0px; float:left;'>";

    html = html + "<div style='width:auto;'>";
    html = html + "<ul style='width:auto; background-color:#eff0f0; border:1px #d8d9db solid;'>";
    html = html + "<li class='optionOver' style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid; '>&nbsp;</li>";/*here class optionOver given by navneet*/
    for ( i = 1; i <= optionCount; i++) {
        html = html + "<li style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center;border-right:1px #d8d9db solid; '>" + valueArrayPQ[i].toUpperCase() + "</li>";
    }
    html = html + "<div style='clear:both;'></div>";
    html = html + "</ul>";
    html = html + "</div>";

    for ( i = 1; i <= optionCount; i++) {

        html = html + "<div style='width:auto;'>";
        html = html + "<ul style='width:auto; border-right:1px #d8d9db solid; border-left:1px #d8d9db solid; border-bottom:1px #d8d9db solid;'>";
        html = html + "<li class='optionOver' style='float:left; padding:5px 0px 0px 0px; background-color:#f6f6f6; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid;'>" + valueArray[i].toUpperCase() + "</li>";/*here class optionOver given by navneet*/
         j = optionCount;
        var k = 1;
        while (j > 0) {

            html = html + "<li class='optionOver' style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid;'><input name='mcqMatchMatrix' id='mcqMatchMatrix' type='checkbox'  onclick='colorChange();' value='" + valueArray[i] + "-" + valueArrayPQ[k] + "' /></li>";            
            k++;
            j--;
        }
        html = html + "<div style='clear:both;'></div>";
        html = html + "</ul>";
        html = html + "</div>";
    }

    html = html + "</div>";
    html = html + "<div style='clear:both'></div>";
    html = html + "</div>";

    htmlSequence = htmlSequence + html;

    return htmlSequence;
}

function getMCQMatchtheFollowing(optionArray) {

       
    var html="";
    var htmlSequence="";
    var valueArray = getValueArray();
    var valueArrayPQ = getValueArrayPQ();

    html = "<div style='width:auto; border:1px #d8d9db solid;'>";
    html = html + "<table width='100%' align='center' border='0px' bgcolor='#eff0f0' cellpadding='10px' cellspacing='0px' style='border-bottom:1px #d8d9db solid;'>";
    html = html + "<tr style='border-bottom:1px #C66 solid;'>";
    html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;'>&nbsp;</td>";
    html = html + "<td style='width:43.5%;border-right:#d8d9db solid 1px; padding-left:10px;'>Choice</td>";
    html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;'>&nbsp;</td>";
    html = html + "<td style=' padding-left:10px;' >Match</td>";
    html = html + "</tr>";
    html = html + "</table>";

    html = html + "<table width='100%' align='center' border='0px' cellpadding='10px' cellspacing='0px'>";
    var optionCount = ((optionArray.length - 1)/2);
    for (var i = 1; i <= optionCount; i++) {

        html = html + "<tr style='border-bottom:1px #C66 solid;'>";
        html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid;'>" + valueArray[i].toUpperCase() + "</td>";
        html = html + "<td style='width:43.5%;border-right:#d8d9db solid 1px; padding-left:10px;border-bottom:1px #d8d9db solid;'>" + optionArray[i] + "</td>";
        html = html + "<td style=' width:3.5%; text-align:center; border-right:#d8d9db solid 1px;border-bottom:1px #d8d9db solid;'>" + valueArrayPQ[i].toUpperCase() + "</td>";
        html = html + "<td style=' padding-left:10px; border-bottom:1px #d8d9db solid;'>" + optionArray[(i + optionCount)] + "</td>";
        html = html + "<td style='border-bottom:1px #d8d9db solid;'>&nbsp;</td>";
        html = html + "</tr>";
    }
    html = html + "</table>";
    html = html + "</div>";

    html = html + "<div style='width:auto;'>";
    html = html + "<div style='margin:20px 0 20px 0px; float:left;'>";

    html = html + "<div style='width:auto;'>";
    html = html + "<ul style='width:auto; background-color:#eff0f0; border:1px #d8d9db solid;'>";
    html = html + "<li  class='optionOver' style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid; '>&nbsp;</li>";/*here class optionOver given by navneet*/
    for ( i = 1; i <= optionCount; i++) {
        html = html + "<li  class='optionOver' style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center;border-right:1px #d8d9db solid; '>" + valueArrayPQ[i].toUpperCase() + "</li>";/*here class optionOver given by navneet*/
    }
    html = html + "<div style='clear:both;'></div>";
    html = html + "</ul>";
    html = html + "</div>";

    for ( i = 1; i <= optionCount; i++) {

        html = html + "<div style='width:auto;'>";
        html = html + "<ul style='width:auto; border-right:1px #d8d9db solid; border-left:1px #d8d9db solid; border-bottom:1px #d8d9db solid;'>";
        html = html + "<li class='optionOver' style='float:left; padding:5px 0px 0px 0px; background-color:#f6f6f6; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid;'>" + valueArray[i].toUpperCase() + "</li>";/*here class optionOver given by navneet*/
        var j = optionCount;
        var k = 1;
        while (j > 0) {

            html = html + "<li class='optionOver' style='float:left; padding:5px 0px 0px 0px; vertical-align:middle; height:25px; width:40px; text-align:center; border-right:1px #d8d9db solid;'><input name='mcqMatchtheFollowing_"+ valueArray[i] +"' type='radio' onclick='unCheckRadio();colorChange();'  value='" + valueArray[i] + "-" + valueArrayPQ[k] + "' /></li>";/*here class optionOver given by navneet*/
               
            k++;
            j--;
        }
        html = html + "<div style='clear:both;'></div>";
        html = html + "</ul>";
        html = html + "</div>";
    }

    html = html + "</div>";
    html = html + "<div style='clear:both'></div>";
    html = html + "</div>";

    htmlSequence = htmlSequence + html;

    return htmlSequence;
}
function getMCQFillInTheBlanks(optionArray) {
    var html="";
    var htmlSequence="";
    var valueArray = getValueArray();
    for (var i = 1; i <optionArray.length; i++) {
        html = html + "<div style='width:auto; margin:0px 0 0 0px; background-color:#fdfdf8; border-top:#f0f0ef 1px solid; border-bottom:#f0f0ef 1px solid; padding:5px 0px;'>";
        html = html + "<input name='mcqFB_" + i + "' id='mcqFB_" + i + "'  onkeyup='checkbutton();' onblur='checkbutton();colorChange();' autocomplete='off'  type='text' class='text-field' />";
        html = html + "</div>";
    }


    htmlSequence = htmlSequence + html;

    return htmlSequence;
}
function getMCQMutipleChoice(optionArray) {

    var html="";
    var htmlSequence="";
    var suffle=(document.getElementById("testSetting").value).substring(2,3);
    //alert(suffle);
    var tempArray=new Array();
    var valueArray = getValueArray();
    var countArray=optionArray.length; 
    for (var i = 1; i < countArray; i++) {
        tempArray[i-1]=optionArray[i];
    }
    
    if(suffle==1)
        suffleArray=arrayShuffle(tempArray);
    else
        suffleArray=tempArray;
    countArray=suffleArray.length; 
    for ( i = 0; i < countArray; i++) {
        //print_r($valueArray);
        var j = $.inArray( suffleArray[i],optionArray );
        html = "<div style='width:auto; margin:10px 0 0 10px;'>";
        html = html + "<ul class='optionOver' style='width:auto; overflow:hidden;padding:5px;'><label>";/*here class optionOver and padding given by navneet*/
        html = html + "<li style='float:left; margin:0px 0 0 10px;'>";
        html = html + "<input type='checkBox' onclick='colorChange()' id='mcqMultiple' name='mcqMultiple' value='" + valueArray[j] + "' >";
        html = html + "</li>";
        html = html + "<li style='float:left; margin:0px 0 0 10px; cursor:pointer;'>" + suffleArray[i] + "</li>";
        html = html + "</label></ul>";
        html = html + "</div>";

        htmlSequence = htmlSequence + html;
    }
    return htmlSequence;
}

function getMCQSingleChoiceForEssay(optionArray) {

    var html="";
    var op="";
    var suffle=(document.getElementById("testSetting").value).substring(2,3);
    var tempArray=new Array();
    var valueArray = getValueArray();
    var countArray=optionArray.length;
    for (var i = 1; i < countArray; i++) {
        tempArray[i-1]=optionArray[i];
    }
    if(suffle==1)
        suffleArray=arrayShuffle(tempArray);
    else
        suffleArray=tempArray;
    countArray=suffleArray.length;
    for ( i = 0; i < countArray; i++) {
        var j = $.inArray(suffleArray[i],optionArray );
        op="'"+valueArray[j]+"'";
        html = html + "<tr class='optionOver' style='padding: 20px;cursor:pointer;cursor:hand;'>";/*here class optionOver given by navneet*/
        html = html + "<td width='10%' align='left' valign='top' style='padding:5px;'>";/*here class padding given by navneet*/
        html = html + '<input type="radio" id="mcqSingle" name="mcqSingle" onclick="unCheckRadio();colorChange();" value="' + valueArray[j] + '" >';
        html = html + '</td>';
        html = html + '<td style="padding:5px;" width="90%" align="left" valign="top" style="padding-top:0px; cursor:pointer;" onclick="checkessaysingle('+op+');colorChange()">';/*here class padding given by navneet*/
        html = html + suffleArray[i];
        html = html + '</td>';
        html = html + '</tr>';
        html = html + '<tr><td>&nbsp;</td></tr>';

    }
    return html;
}
function getMCQMulitpleChoiceForEssay(optionArray) {
    var html="";
     var suffle=(document.getElementById("testSetting").value).substring(2,3);
    //alert(suffle);
    var tempArray=new Array();
    var valueArray = getValueArray();
    var countArray=optionArray.length; 
    for (var i = 1; i < countArray; i++) {
        tempArray[i-1]=optionArray[i];
    }
    
    if(suffle==1)
        suffleArray=arrayShuffle(tempArray);
    else
        suffleArray=tempArray;
    countArray=suffleArray.length; 
    for ( i = 0; i < countArray; i++) {
        //print_r($valueArray);
        var j = $.inArray(suffleArray[i],optionArray );
        html = html + "<tr class='optionOver' style='padding: 20px;'>";/*here class optionOver and padding given by navneet*/
        html = html + "<td width='10%' align='left' valign='top'>";
        html = html + "<input type='checkBox' onclick='colorChange()' id='mcqMultiple' name='mcqMultiple' value='" + valueArray[j] + "' >";
        html = html + "</td>";
        html = html + "<td width='90%' align='left' valign='top' style='padding-top:0px;'>";
        html = html + suffleArray[i] ;
        html = html + "</td>";
        html = html + "</tr>";
        html = html + "<tr><td>&nbsp;</td></tr>";
    }
    return html;
}
//for changing color on the basis of responce by ankit garg
function colorChange(){
   var value=document.getElementById("serialNo").value;
    rgvalue=value;
 if($("#testSetting").val().substring(10,11)==1){
        if(document.getElementById("questionType").value==7){           
            if(answerOfQue!=getResponse()){              
                $("#rightWrong_"+answerOfQue).removeClass();
                $("#rightWrong_"+answerOfQue).addClass("right-question");
                $("#rightWrong_"+answerOfQue).html("<a href='#' onclick='showSolution();'>View Solution</a>");
                //document.getElementById("rightWrong_"+answerOfQue)
                document.getElementById("rightWrong_"+answerOfQue).style.display="block";
                document.getElementById("rightWrong_"+getResponse()).style.display="block";
               
            }
        }
 }
}
    
//added by ankit garg for option shuffling
function arrayShuffle(oldArray) {
    var newArray = oldArray.slice();
    var len = newArray.length;
    var i = len;
    while (i--) {
        var p = parseInt(Math.random()*len);
        var t = newArray[i];
        newArray[i] = newArray[p];
        newArray[p] = t;
    }
    return newArray; 

}
// this is for display attempte ,unattempted subject wise button
function showsubjectWise(serialNo,pre,time,res){  

    var maxSerialNo=document.getElementById("maxSerialNoArray").value;
    var maxSerialNoArray=maxSerialNo.split(",");    
    for(var t=0;t<maxSerialNoArray.length;t++){
        if(serialNo!=maxSerialNoArray[t]){
           $("#sujectwiseDetail_"+maxSerialNoArray[t]).hide();
        }
    }
    var check=0;
    var var1=0;
    var var2=0;
    var j=parseInt($("#totalQuestion").val())+1;
    maxSerialNoArray[maxSerialNoArray.length]=j;
    for(var i=0;i<maxSerialNoArray.length-1;i++){
        if(parseInt(maxSerialNoArray[i])<=parseInt(serialNo) && parseInt(serialNo)<=parseInt(maxSerialNoArray[i+1])){
            check=1;
            var1=parseInt(maxSerialNoArray[i]);
            var2=parseInt(maxSerialNoArray[i+1]);
        }
    }
    var totalQuestion=var2-var1;
    var attempted=0;
    var review=0;
    var reviewattempted=0;
    for(var temp=var1;temp<var2;temp++){
        if(attemptQuesResponseArray[temp]!="" && arrvar[temp]==0){
            attempted++;
        }
        if(arrvar[temp]==1 && attemptQuesResponseArray[temp]==""){
            review++;
        }
        if(arrvar[temp]==1 && attemptQuesResponseArray[temp]!=""){
            reviewattempted++;
        }

    }
    var unattempt=totalQuestion-(attempted + reviewattempted + review);
    var html="<li>Attempted<strong class='attempt' id='attemptedQue'>"+attempted+"</strong></li>"+
    "<li>Unattempted<strong class='unattempt' id='unattemptedQue'>"+unattempt+"</strong></li>";
 if(time==0 && pre==1 && res==0){html=html + "<li>Marked For Review<strong class='mark' id='reviewQue'>"+review+"</strong></li>"+
    "<li>Attempted+Review<strong class='attemptreview' id='reviewQue'>"+reviewattempted+"</strong></li>";
 }
  html=html + "<li>Total Questions<strong class='total'  id='totalQue'>"+totalQuestion+"</strong></li>";
 
    $("#subjectData_"+serialNo).html(html);
    $("#sujectwiseDetail_"+serialNo).show();
}
function hidesubjectWise(id){
    $("#sujectwiseDetail_"+id).hide();
}
// this is for display subject wise button
function hiddensubjectwise(serialNo){
    if($("#testSetting").val().substring(0,1)==1){
    var lower=0;
    var upper=0;
    var subidArray=$("#maxSerialNoArray").val().split(",");
   
    for(var i=0;i<subidArray.length-1;i++){
        if(subidArray[i]<=serialNo && serialNo<subidArray[i+1]){
            lower=subidArray[i];
            upper=subidArray[i+1];
            i=subidArray.length+10;
        }


    }
    if(serialNo >= subidArray[subidArray.length-1]){
        lower=subidArray[subidArray.length-1];
        upper=parseInt($("#totalQuestion").val());
    }
    var totalQuestion=parseInt($("#totalQuestion").val());
    if(totalQuestion==upper && serialNo >=subidArray[subidArray.length-1]){
        upper=upper+1;
    }
    for(var  j=1;j<=totalQuestion;j++){
        if(lower<=j && j<upper){
            $("#wrightImage_"+j).show();
        }else{
            $("#wrightImage_"+j).hide();
        }

    }
    }

}

function hideSolution(){
    $("#solutiondiv").hide();
}
function showSolution(){
 document.getElementById("solutiondiv").style.display="block";
}
function redgreen(rgvalue){ 
    if(arrvar[rgvalue]==1 && attemptQuesResponseArray[rgvalue]!=''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("greenred");
    }
   else  if(arrvar[rgvalue]==1 && attemptQuesResponseArray[rgvalue]==''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("blue");
    }
     else  if(attemptQuesResponseArray[rgvalue]!=''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("green");
    }
    else{
        $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("red");
    }
}
    function redgreenrandom(rgvalue){ 
    if(arrvar[randomvarArray[rgvalue]]==1 && attemptQuesResponseArray[randomvarArray[rgvalue]]!=''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("greenred");
    }
   else  if(arrvar[randomvarArray[rgvalue]]==1 && attemptQuesResponseArray[randomvarArray[rgvalue]]==''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("blue");
    }
     else  if(attemptQuesResponseArray[randomvarArray[rgvalue]]!=''){
          $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("green");
    }
    else{
        $("#wrightImage_"+rgvalue).removeClass();
          $("#wrightImage_"+rgvalue).addClass("white");
    }
    }
        
