<?php

class Student_model extends CI_Model {

    function __construct() {
        parent::__construct();
		//$this->get_user = get_user();
		//print_r($this->db->last_query());
    }
	 
	public function updateStatus($request_status,$request_id)
    {
		$data=array('request_status'=>$request_status);
        $this->db->where('id',$request_id);
        return $this->db->update('tbl_invited_exam_requests',$data);
    }


    public function getExamquestion($exam_id)
    {
         $this->db->select('tbl_que_creation.*,tbl_q_type_master.question_type_name');

        $this->db->from('tbl_test_question');
		  $this->db->join('tbl_que_creation', 'tbl_que_creation.que_id = tbl_test_question.que_id' );
      $this->db->join('tbl_q_type_master', 'tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id' );
    
        $this->db->where('tbl_test_question.test_id', $exam_id);   
       // $this->db->order_by('tbl_test_question.que_id','desc');   
        $q = $this->db->get();
    //   print_r($q->result());exit;
        return $res = $q->result();
    }
     public function getExamoption($que_id)
    {
		$q = $this->db->query(' select tbl_question_option_1.*,tbl_question_pair.pair_id,tbl_question_pair.pair_option from tbl_question_option_1 left join 
		   tbl_question_pair ON tbl_question_pair.option_id=tbl_question_option_1.question_option_id where tbl_question_option_1.que_id ="'.$que_id.'" order by "'.rand().'"
		');
       /* $this->db->select('tbl_question_option_1.*,tbl_question_pair.pair_id,tbl_question_pair.pair_option');
        $this->db->from('tbl_question_option_1');
		 $this->db->leftjoin('tbl_question_pair', 'tbl_question_pair.option_id = tbl_question_option_1.question_option_id' );
        $this->db->where('que_id', $que_id);   
        $this->db->order_by('rand()');*/
      // $q = $this->db->get();
       // print_r($q->result());exit;
        return $res = $q->result();
    }
     public function getTestDetailsById($exam_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_test_creation');
        $this->db->where('test_id', $exam_id);   
       
        $q = $this->db->get();
        
        return $res = $q->row();
    }
	 
	  public function getCompletedTest()
	{
		
		$this->db->select('tbl_test_creation.*,tbl_completed_test.total_mark,tbl_completed_test.max_mark,tbl_invited_exam_requests.request_status');
		$this->db->from('tbl_invited_exam_requests');
		$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_invited_exam_requests.exam_id');
		$this->db->join('tbl_completed_test','tbl_completed_test.request_id = tbl_invited_exam_requests.id');
		$this->db->where(['tbl_invited_exam_requests.student_id' => get_user_id(),'request_status'=>'4']);
		$q = $this->db->get();
		$res = $q->result();
		return $res;
	}
	 public function getInvitedBy($exam_id)
	 {
		  $this->db->select('tbl_user_registration.first_name,tbl_user_registration.last_name');
        $this->db->from('tbl_invited_exam_requests');
		$this->db->join('tbl_user_registration','tbl_user_registration.user_id = tbl_invited_exam_requests.teacher_id');
        $this->db->where('exam_id', $exam_id);   
        $this->db->where('student_id', get_user_id());   
       
        $q = $this->db->get();
        $res = $q->row();
        return $res->first_name.' '.$res->last_name;
	 }
	 public function getCompletedTestDetails()
	  {
		  
	  }
	  function getJsonTestResultData($data)
    {
		
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_completed_test');	
	//	$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_completed_test.test_id');
		if($data['search'])
		$this->db->where($data['search']);
		
		$this->db->where(array('tbl_completed_test.user_id' =>get_user_id()));
		/*if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}*/
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();

		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_completed_test');
	//	$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_completed_test.test_id');
		if($data['search'])
		$this->db->where($data['search']);
		
		$this->db->where(array('tbl_completed_test.user_id' =>get_user_id()));
		/*if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}*/
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	 public function getStudentInviteData()
	 {
		  $this->db->select('tbl_invited_exam_requests.*');
        $this->db->from('tbl_invited_exam_requests');
		
        $this->db->where('request_status','!=','4');   
        $this->db->where('student_id', get_user_id());   
       $q = $this->db->get();
        return $res = $q->result();
	 }
}
?>
