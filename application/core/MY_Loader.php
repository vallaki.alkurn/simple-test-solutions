<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

		class MY_Loader extends MX_Loader {
		function __construct(){
		parent::__construct();
		
		}

		function admin_view($template_name, $data = array(), $return = FALSE){
		$data['adminModule']="Masteradmin/";
		if($this->session->userdata('user_Type')==1) {
		$this->load->model('Masteradmin/Global_model');
		$data['section'] = $this->Global_model->GetmultiData('tbl_section','*');
		$userss= $this->Global_model->getUserModulePermission();
		$module_permission=  $userss->module_permission;
		$data['module'] = explode(',', $module_permission);
		$subModule_permission=  $userss->subModule_permission;
		$data['subModule'] = explode(',', $subModule_permission);


		$content = $this->view('header', $data, $return);
		$content = $this->view($template_name, $data, $return);
		$content = $this->view('footer', $data, $return);
		}else if($this->session->userdata('user_Type')==2) {
		$this->load->model('Admin/Global_model');
		
		$content = $this->view('header', $data, $return);
		$content = $this->view($template_name, $data, $return);
		$content = $this->view('footer', $data, $return);
		}else{
		redirect(base_url().'Cadmin/Welcome/login');
		}
		if($return){
		return $content;
		}

		}

		
		function getModuleDetails($moduleId,$sectionId){
		$this->load->model('Masteradmin/Global_model');
		return $data=$this->Global_model->get_moduleDetails($moduleId,$sectionId);
		
		}
		function  getSubModuleDetails($moduleId){
		$this->load->model('Masteradmin/Global_model');
		return $data=$this->Global_model->get_SubmoduleDetails($moduleId);
		}
		
		function front_view($template_name, $data = array(), $return = FALSE)
		{
			//print_r($data);
			//$header_temp =  APPPATH."views\header";
			//$footer_temp =  APPPATH."views\footer";
			/******get category name******/
			/*if($template_name == 'template/404'){
				$seo_data = array('title' => '404',
							  'keyword' => 'Sorry, we can not find the page you were looking for.',
							  'description' => 'Sorry, we can not find the page you were looking for.');
			}*/
			$content = $this->view('template/header' , $data, $return);
			$content = $this->view($template_name, $data, $return);
			$content = $this->view('template/footer', $data, $return);
			if($return)
			{
				return $content;
			}
		}
		
		

}