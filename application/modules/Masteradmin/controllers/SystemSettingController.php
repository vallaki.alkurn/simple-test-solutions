<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SystemSettingController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->load->model('System_config_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->lang->load('system_config_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}
	}

	public function GeneralSettingExecute()
	{ 
     $this->load->admin_view('GeneralSettingExecute/config_listing');
	}

  public function updateInformation()
  {
      $data = array(
                   'company_name' => $this->input->post('company_name'),
                   'company_address' => $this->input->post('company_address'),
                   'company_website' => $this->input->post('company_website'),
                   'company_email' => $this->input->post('company_email'),
                   'company_phone' => $this->input->post('company_phone'),
                   'company_fax' => $this->input->post('company_fax')
                   );
      $saveRecord = $this->System_config_model->batch_save($data);
      if($saveRecord)
      {
         $this->session->set_flashdata('message', 'Record Updated Successfully.');
      }
      redirect('Masteradmin/SystemSettingController/GeneralSettingExecute');     
  }

  public function updateGeneral()
  {
      $data = array(
                   'dateformat' => $this->input->post('dateformat')
                   );
      $saveRecord = $this->System_config_model->batch_save($data);
      if($saveRecord)
      {
         $this->session->set_flashdata('message', 'Record Updated Successfully.');
      }
      redirect('Masteradmin/SystemSettingController/GeneralSettingExecute');     
  }

  

  public function updateEmail()
  {
      $data = array(
                   'protocol' => $this->input->post('protocol'),
                   'mailpath' => $this->input->post('mailpath'),
                   'smtp_host' => $this->input->post('smtp_host'),
                   'smtp_port' => $this->input->post('smtp_port'),
                   'smtp_crypto' => $this->input->post('smtp_crypto'),
                   'smtp_user' => $this->input->post('smtp_user'),
                   'smtp_pass' => $this->input->post('smtp_pass')
                   );
      $saveRecord = $this->System_config_model->batch_save($data);
      if($saveRecord)
      {
         $this->session->set_flashdata('message', 'Record Updated Successfully.');
      }
      redirect('Masteradmin/SystemSettingController/GeneralSettingExecute');     
  }


}