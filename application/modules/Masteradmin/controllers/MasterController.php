<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}

	}

  public function SubjectExecute()
  { 
     $data['subject'] = $this->Global_model->GetmultiData('tbl_subject_class','subject_class_id,subject_class_name',array('is_deleted'=>0));

      $subject = array('' => $this->lang->line('select_subject'));
      foreach($data['subject'] as $row)
      {
        $subject[$row->subject_class_name] = $row->subject_class_name;
      }
      $data['subject'] = $subject;

     $this->load->admin_view('SubjectExecute/subject_class', $data);
  }

  public function getSubjectList()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('subject_class_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";
              if($standName[1] != 'clear') {
              $search = "(subject_class_name LIKE '" . $standName [1] . "%' && is_status LIKE '" . $statusName [1] . "%')";
              }
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(subject_class_name like '%" . $_POST['search']['value'] . "%' || is_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.subject_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('subject_class_id','subject_class_name','is_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       } 
      $list = $this->Global_model->getSingleListAjax('tbl_subject_class s',$get,'subject_class_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $missing->subject_class_name;
              $row[] = ($missing->is_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'MasterController/AddSubject/'.$missing->subject_class_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->subject_class_id.','."'tbl_subject_class'".','."'subject_class_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
          }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }

  public function AddSubject($id = NULL)
  {
      $data['subject_edit'] = $this->Global_model->GetsingleData('tbl_subject_class','*',array('subject_class_id'=>$id));
      $this->load->admin_view('SubjectExecute/add_subject',$data);
  }


  public function SaveSubject()
  { 
        $this->form_validation->set_rules($this->lang->line('subject_class_name_error'), $this->lang->line('subject_class_name_erro_name'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
           $data['subject_edit'] = array();
           $this->load->admin_view('SubjectExecute/add_subject',$data);
        }
        else
        {   
            $data = array(
                         'subject_class_name' => $this->input->post('subject_class_name'),
                         'is_status' => $this->input->post('is_status')
                         );
            $saveRecord = $this->Global_model->InsertUpdate('tbl_subject_class',$data,'subject_class_id');
            if($saveRecord)
            { 
               $adminModule = 'Masteradmin/';
                //echo $adminModule.'StandardExecute/standard_class';
                if($this->input->post('id') == '')
                {
                  $this->session->set_flashdata('message', 'Record Saved Successfully.');
                } else {
                  $this->session->set_flashdata('message', 'Record Updated Successfully.');
                }
                if($this->input->post('save')) {
                  redirect($adminModule.'MasterController/SubjectExecute');
                } else {
                  redirect($adminModule.'MasterController/AddSubject');
                }
            }
        }
  }




  //Standard Master Code Start
	public function StandardExecute()
	{ 
     $data['standard'] = $this->Global_model->GetmultiData('tbl_standard_class','standard_class_id,standard_class_name',array('is_deleted'=>0));

      $standard = array('' => $this->lang->line('select_standard'));
      foreach($data['standard'] as $row)
      {
        $standard[$row->standard_class_name] = $row->standard_class_name;
      }
      $data['standard'] = $standard;

     $this->load->admin_view('StandardExecute/standard_class', $data);
	}


  public function AddStandard($id = NULL)
  {
      $data['standard_edit'] = $this->Global_model->GetsingleData('tbl_standard_class','*',array('standard_class_id'=>$id));
      

      $this->load->admin_view('StandardExecute/add_standard',$data);
  }

  public function SaveStandard()
  {
   
        $this->form_validation->set_rules($this->lang->line('standard_class_name_error'), $this->lang->line('standard_class_name_erro_name'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
           $data['standard_edit'] = array();
           $this->load->admin_view('StandardExecute/add_standard',$data);
        }
        else
        {   
            $data = array(
                         'standard_class_name' => $this->input->post('standard_class_name'),
                         'is_status' => $this->input->post('is_status')
                         );
            $saveRecord = $this->Global_model->InsertUpdate('tbl_standard_class',$data,'standard_class_id');
            if($saveRecord)
            { 
               $adminModule = 'Masteradmin/';
                //echo $adminModule.'StandardExecute/standard_class';
                if($this->input->post('id') == '')
                {
                  $this->session->set_flashdata('message', 'Record Saved Successfully.');
                } else {
                  $this->session->set_flashdata('message', 'Record Updated Successfully.');
                }
                if($this->input->post('save')) {
                  redirect($adminModule.'MasterController/StandardExecute');
                } else {
                  redirect($adminModule.'MasterController/AddStandard');
                }
            }
        }
  }

  public function getStandardList()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('city_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";
              if($standName[1] != 'clear') {
              $search = "(standard_class_name LIKE '" . $standName [1] . "%' && is_status LIKE '" . $statusName [1] . "%' )";
              }
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(standard_class_name like '%" . $_POST['search']['value'] . "%'  || is_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.standard_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('standard_class_id','standard_class_name','is_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       } 
      $list = $this->Global_model->getSingleListAjax('tbl_standard_class s',$get,'standard_class_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $missing->standard_class_name;
              $row[] = ($missing->is_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'MasterController/AddStandard/'.$missing->standard_class_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->standard_class_id.','."'tbl_standard_class'".','."'standard_class_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
          }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }

  function DeteteRecord()
  {
    $deleteRecord = $this->db->delete($_POST['table'], array('user_id' => $_POST['id'])); 

    //$deleteRecord = $this->Global_model->UpdateRecord($_POST['table'],array('is_deleted'=>1),array($_POST['dbid']=>$_POST['id']));
    if($deleteRecord)
    {
        echo '{"code":"200"}';
    }
    else
    {
        echo '{"code":"100"}';
    }
  }
}