<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {
   const  moduled='Masteradmin';
	 public function __construct() 
   {
       parent::__construct();
       $this->config->set_item('base_url', $this->config->config['secure_url']);
	     $this->load->model('Global_model');
       $this->load->library('SimpleLoginSecure');
	}
	
  public function index()
	{  
      if ($this->Global_model->isLoggedIn() === TRUE) 
      {
  			$data['session']="NoLogin";
        redirect(base_url() . 'Masteradmin/Dashboard');
	    }
	   $this->load->view('login');
	}

  public function logout()
  {
		$this->simpleloginsecure->logout();
    redirect(base_url() . 'Masteradmin/Auth');
	}

  public function login()
  {
     if($this->simpleloginsecure->login($this->input->post('username'), $this->input->post('password'), 1)) 
     {
       echo '{"status":"success","code":"200","msg":"Login success"}';
     }
     else
     {
      echo '{"status":"failed","code":"100","msg":"wrong credential"}';
     }
  }
}
