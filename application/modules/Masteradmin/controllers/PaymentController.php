<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->load->model('Payment_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}

	}
 
  //Exam List
	public function transaction_list()
	{ 
     $this->load->admin_view('Payments/get_all_transaction');
	}

  public function getAllTranasction()
  {
       $adminModule = 'Masteradmin/';
       $sort = array('test_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "test_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
              $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || card_number like '%" . $_POST['search']['value'] . "%' || payment_method like '%" . $_POST['search']['value'] . "%'  || tbl_transaction.amt like '%" . $_POST['search']['value'] . "%' || payment_status like '%" . $_POST['search']['value'] . "%' || payment_status like '%" . $_POST['search']['value'] . "%' || tbl_transaction.payment_date like '%" . $_POST['search']['value'] . "%')";

             } elseif (isset($_POST['unn'])){
            $search = "tbl_user_registration.user_id  ='".$_POST['unn']."'";
             }
          }
      } 

      $get['fields'] = array('first_name','last_name','card_number','payment_method','payer_status','tbl_transaction.amt','payment_status','payment_status','tbl_transaction.payment_date');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Payment_model->getAllTranasction($get,'tbl_transaction.id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name.' '.$missing->last_name;
              $row[] = $missing->card_number;
              $row[] = $missing->payment_method;
              $row[] = $missing->payer_status;
              $row[] = $missing->amt;
              $row[] = $missing->payment_status;
              $row[] = date('M d Y',strtotime($missing->payment_date));
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }

  //user card details list
  public function teacher_account_details_list()
  {
    $this->load->admin_view('Payments/get_all_user_account_details');
  }


  public function getAllUserCardDetails()
  {
       $adminModule = 'Masteradmin/';
       $sort = array('test_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "test_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
              $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || card_number like '%" . $_POST['search']['value'] . "%' || cardholder_name like '%" . $_POST['search']['value'] . "%'  || cvv_number like '%" . $_POST['search']['value'] . "%' || expiry_date like '%" . $_POST['search']['value'] . "%' || payment_date like '%" . $_POST['search']['value'] . "%' || amt like '%" . $_POST['search']['value'] . "%')";

             } elseif (isset($_POST['unn'])){
            $search = "tbl_user_registration.user_id  ='".$_POST['unn']."'";
             }
          }
      } 

      $get['fields'] = array('first_name','last_name','card_number','cardholder_name','cvv_number','expiry_date','payment_date','amt','status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Payment_model->getAllUserAccountDetails($get,'tbl_user_card_details.id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name.' '.$missing->last_name;
              $row[] = $missing->card_number;
              $row[] = $missing->cardholder_name;
              $row[] = $missing->cvv_number;
              $row[] = $missing->expiry_date;
              $row[] = $missing->amt;
              $row[] = $this->paymentStatus($missing->status);
              $row[] = ($missing->payment_date == 'NULL') ? '' : $missing->payment_date;
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }


  public function paymentStatus($status)
  {
    if($status == 1)
    {
      $payment_status = 'Pending';
    } else if($status == 2) {
      $payment_status = 'Success';
    }else if($status == 3) {
      $payment_status = 'Cancelled';
    }
    return $payment_status;
  }
}