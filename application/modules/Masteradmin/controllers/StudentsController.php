<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentsController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->load->model('Student_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}

	}

 
  //Standard Master Code Start
	public function getAllStudents()
	{ 
     $this->load->admin_view('StudentExecute/get_all_student');
	}

  public function getAllApprovedStudents()
  { 
     $this->load->admin_view('StudentExecute/get_all_approved_student');
  }

  

  public function getAllApprovedStudentsAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
             $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || school_collage like '%" . $_POST['search']['value'] . "%' || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.user_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','school_collage','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Student_model->getAllApprovedStudent($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->school_collage;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'StudentsController/StudentProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'StudentsController/AddStudent/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }

  public function getAllNonApprovedStudents()
  { 
     $this->load->admin_view('StudentExecute/get_all_non_approved_student');
  }




  public function getAllNonApprovedStudentsAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || school_collage like '%" . $_POST['search']['value'] . "%' || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.standard_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','school_collage','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Student_model->getAllNonApprovedStudent($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->school_collage;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'StudentsController/StudentProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'StudentsController/AddStudent/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }
  

  public function getAllDisapprovedStudents()
  { 
     $this->load->admin_view('StudentExecute/get_all_disapproved_student');
  }


   public function getAllDisApprovedStudentsAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
              $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || school_collage like '%" . $_POST['search']['value'] . "%' || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.standard_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','school_collage','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Student_model->getAllDisApprovedStudent($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->school_collage;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'StudentsController/StudentProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'StudentsController/AddStudent/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }


  public function getAllStudentsAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || school_collage like '%" . $_POST['search']['value'] . "%' || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.user_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','school_collage','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Student_model->getAllStudent($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->school_collage;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'StudentsController/StudentProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'StudentsController/AddStudent/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }

      public function StudentProfile($userId)
      {
         $data['student'] = getStudentProfile($userId);
         $data['user_id'] = $userId;
         $this->load->admin_view('StudentExecute/student_profile',$data);  
      }


      public function AddStudent($id = NULL)
      {

        $data['student_edit'] = $this->Global_model->GetsingleData('tbl_user_registration','*',array('user_id'=>$id)); 
        if($id) {
          $data['formtitle'] = $this->lang->line('student_update_title');
        } else {
           $data['formtitle'] = $this->lang->line('student_add_title');
        }      
        $data['standard'] = $this->getStadard();
        $this->load->admin_view('StudentExecute/new_student',$data);  
      }

      public function getStadard()
      {
        $dataArr = array();
        $dataArr = array(' '=>'Select Standard / Class');
        $standard = $this->Global_model->GetmultiData('tbl_standard_class','*',array('is_deleted'=>0,'is_status'=>1));
        foreach($standard as $key => $value)
        {
          $dataArr[$value->standard_class_id] = $value->standard_class_name;
        }
        return $dataArr;
      }

      public function SaveStudent()
      {
       $adminModule = 'Masteradmin/';
       $chechExist = $this->Global_model->GetsingleData('tbl_user_registration','*',array('email_address'=>$this->input->post('email_address'),'is_deleted'=>0));
       $data['formtitle'] = $this->lang->line('student_add_title');


       if($chechExist && $this->input->post('id') == '')
       {
         $this->session->set_flashdata('message_name', 'Email address already registered');
         $data['student_edit'] = array();
         $data['standard'] = $this->getStadard();
         $this->load->admin_view('StudentExecute/new_student',$data);
       } else {
        
        $this->form_validation->set_rules($this->lang->line('studetn_class_first_name_error')
, $this->lang->line('standard_class_first_name_erro_name'), 'required');
       $this->form_validation->set_rules($this->lang->line('studetn_class_last_name_error')
, $this->lang->line('standard_class_last_name_erro_name'), 'required');
        $this->form_validation->set_rules($this->lang->line('studetn_class_email_address_error')
, $this->lang->line('standard_class_email_address_erro_name'), 'required|valid_email');

        $this->form_validation->set_rules($this->lang->line('studetn_class_school_collage_error')
, $this->lang->line('standard_class_school_collageerro_name'), 'required');
        $this->form_validation->set_rules($this->lang->line('studetn_class_standard_error')
, $this->lang->line('standard_class_name_erro_name'), 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
           $data['student_edit'] = array();
           $data['standard'] = $this->getStadard();
           $this->load->admin_view('StudentExecute/new_student',$data);
        }
        else
        {   
            if($this->input->post('id') == '')
            {
            $data = array(
                         'first_name' => $this->input->post('first_name'),
                         'last_name' => $this->input->post('last_name'),
                          'email_address' => $this->input->post('email_address'),
                          'school_collage' => $this->input->post('school_collage'),
                          'user_type' => 2,
                          'registered_date' => date('Y-m-d H:i:s'),
                          'is_profile_status' => $this->input->post('is_profile_status'),
                          'standard_class_id' => $this->input->post('standard_class_id')
                         );
           } else {
               $data = array(
                         'first_name' => $this->input->post('first_name'),
                         'last_name' => $this->input->post('last_name'),
                          'email_address' => $this->input->post('email_address'),
                          'school_collage' => $this->input->post('school_collage'),
                          'user_type' => 2,
                          'is_profile_status' => $this->input->post('is_profile_status'),
                          'standard_class_id' => $this->input->post('standard_class_id')
                         );
           }
            $saveRecord = $this->Global_model->InsertUpdate('tbl_user_registration',$data,'user_id');
            if($saveRecord)
            { 

                if($this->input->post('id') == '')
                {
                  $this->session->set_flashdata('message', 'Record Saved Successfully.');
                } else {
                  $this->session->set_flashdata('message', 'Record Updated Successfully.');
                }
                //echo $adminModule.'StandardExecute/standard_class';
                if($this->input->post('save')) {
                  redirect($adminModule.'StudentsController/getAllStudents');
                } else {
                  redirect($adminModule.'StudentsController/AddStudent');
                }
            }
        }
        }
      }
}