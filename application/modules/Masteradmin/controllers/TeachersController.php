<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeachersController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->load->model('Teacher_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}

	}




 
 
  //Standard Master Code Start
	public function getAllTeachers()
	{ 
     $this->load->admin_view('TeacherExecute/get_all_teacher');
	}

  public function getAllApprovedTeachers()
  { 
     $this->load->admin_view('TeacherExecute/get_all_approved_teacher');
  }

  public function getAllApprovedTeachersAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || subject_class_name like '%" . $_POST['search']['value'] . "%'|| is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.subject_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','subject_class_name','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Teacher_model->getAllApprovedTeacher($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->subject_class_name;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'TeachersController/TeacherProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'TeachersController/AddTeacher/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }

  public function getAllNonApprovedTeachers()
  { 
     $this->load->admin_view('TeacherExecute/get_all_non_approved_teacher');
  }


  public function getAllNonApprovedTeachersAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || subject_class_name like '%" . $_POST['search']['value'] . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.subject_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','subject_class_name','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Teacher_model->getAllNonApprovedTeacher($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->subject_class_name;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');  
              $row[] ='<a href="'.base_url().''.$adminModule.'TeachersController/TeacherProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'TeachersController/AddTeacher/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }
  

  public function getAllDisapprovedTeachers()
  { 
     $this->load->admin_view('TeacherExecute/get_all_disapproved_teacher');
  }


   public function getAllDisApprovedTeachersAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || subject_class_name like '%" . $_POST['search']['value'] . "%' || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.subject_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','subject_class_name','is_profile_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Teacher_model->getAllDisApprovedTeacher($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->subject_class_name;
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
              $row[] ='<a href="'.base_url().''.$adminModule.'TeachersController/TeacherProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'TeachersController/AddTeacher/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }


  public function getAllTeachersAjax()
  { 
       $adminModule = 'Masteradmin/';
       $sort = array('first_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "first_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
            $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || email_address like '%" . $_POST['search']['value'] . "%' || subject_class_name like '%" . $_POST['search']['value'] . "%'  || is_profile_status like '%" . getTeacherStatus($_POST['search']['value']) . "%')";
             } elseif (isset($_POST['unn'])){
            $search = "s.subject_class_id  ='".$_POST['unn']."'";
             }
          }
        } 

      $get['fields'] = array('user_id','first_name','last_name','email_address','subject_class_name','is_profile_status','paid_membership');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Teacher_model->getAllTeacher($get,'user_id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->first_name;
              $row[] = $missing->last_name;
              $row[] = $missing->email_address;
              $row[] = $missing->subject_class_name;
             
              $row[] = ($missing->is_profile_status == 1) ? $this->lang->line('dropdown_status_label_1') : $this->lang->line('dropdown_status_label_0');
			   $row[] = $missing->paid_membership;
              $row[] ='<a href="'.base_url().''.$adminModule.'TeachersController/TeacherProfile/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a> <a href="'.base_url().''.$adminModule.'TeachersController/AddTeacher/'.$missing->user_id.'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>  <button type="button" data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete" onclick="DeleteRecord('.$missing->user_id.','."'tbl_user_registration'".','."'user_id'".');"><i class="fa fa-trash-o"></i></button>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
 
      }

     

      public function AddTeacher($id = NULL)
      {
        $data['teacher_edit'] = $this->Global_model->GetsingleData('tbl_user_registration','*',array('user_id'=>$id));
        if($id) {
          $data['formtitle'] = $this->lang->line('teacher_update_title');
        } else {
           $data['formtitle'] = $this->lang->line('teacher_add_title');
        }
         $data['subject'] = $this->getSubject();
        $this->load->admin_view('TeacherExecute/new_teacher',$data);  
      }

       public function TeacherProfile($userId)
      {
         $data['teacher'] = getStudentProfile($userId);
         $data['user_id'] = $userId;
         $this->load->admin_view('TeacherExecute/teacher_profile',$data);  
      }


      public function SaveTeacher()
      {

       $adminModule = 'Masteradmin/';
       $chechExist = $this->Global_model->GetsingleData('tbl_user_registration','*',array('email_address'=>$this->input->post('email_address'),'is_deleted'=>0));
       $data['formtitle'] = $this->lang->line('teacher_add_title');

       if($chechExist && $this->input->post('id') == '')
       {
         $this->session->set_flashdata('message_name', 'Email address already registered');
         $data['teacher_edit'] = array();
         $data['subject'] = $this->getSubject();
         $this->load->admin_view('TeacherExecute/new_teacher',$data);
       } else {
        $this->form_validation->set_rules($this->lang->line('studetn_class_first_name_error')
, $this->lang->line('standard_class_first_name_erro_name'), 'required');
       $this->form_validation->set_rules($this->lang->line('studetn_class_last_name_error')
, $this->lang->line('standard_class_last_name_erro_name'), 'required');
        $this->form_validation->set_rules($this->lang->line('studetn_class_email_address_error')
, $this->lang->line('standard_class_email_address_erro_name'), 'required|valid_email');
        $this->form_validation->set_rules($this->lang->line('teacher_organization_name_error')
, $this->lang->line('teacher_organization_name_erro_name'), 'required');
          $this->form_validation->set_rules($this->lang->line('teacher_your_phone_name_error')
, $this->lang->line('teacher_your_phone_name_erro_name'), 'required');
          $this->form_validation->set_rules($this->lang->line('teacher_subject_name_error')
, $this->lang->line('teacher_subject_name_erro_name'), 'required');

           $this->form_validation->set_rules($this->lang->line('teacher_subject_name_error')
, $this->lang->line('teacher_subject_name_erro_name'), 'required');

        
        if ($this->form_validation->run() == FALSE)
        {
           $data['teacher_edit'] = array();
           $data['subject'] = $this->getSubject();
           $this->load->admin_view('TeacherExecute/new_teacher',$data);
        }
        else
        {   
            if($this->input->post('id') == '')
            {
            $data = array(
                         'first_name' => $this->input->post('first_name'),
                         'last_name' => $this->input->post('last_name'),
                          'email_address' => $this->input->post('email_address'),
                          'organization' => $this->input->post('organization'),
                          'organization_phone_number' => $this->input->post('organization_phone_number'),
                          'your_title' => $this->input->post('your_title'),
                          'your_phone' => $this->input->post('your_phone'),
                          'address' => $this->input->post('address'),
                           'registered_date' => date('Y-m-d H:i:s'),
                          'user_type' => 1,
                          'is_profile_status' => $this->input->post('is_profile_status'),
                          'subject_class_id' => $this->input->post('subject_class_id')
                         );
            } else {
                $data = array(
                         'first_name' => $this->input->post('first_name'),
                         'last_name' => $this->input->post('last_name'),
                          'email_address' => $this->input->post('email_address'),
                          'organization' => $this->input->post('organization'),
                          'organization_phone_number' => $this->input->post('organization_phone_number'),
                          'your_title' => $this->input->post('your_title'),
                          'your_phone' => $this->input->post('your_phone'),
                          'address' => $this->input->post('address'),
                          'user_type' => 1,
                          'is_profile_status' => $this->input->post('is_profile_status'),
                          'subject_class_id' => $this->input->post('subject_class_id')
                         );
            }
            $saveRecord = $this->Global_model->InsertUpdate('tbl_user_registration',$data,'user_id');
            if($saveRecord)
            { 
                if($this->input->post('id') == '')
                {
                  $this->session->set_flashdata('message', 'Record Saved Successfully.');
                } else {
                  $this->session->set_flashdata('message', 'Record Updated Successfully.');
                }

                //echo $adminModule.'StandardExecute/standard_class';
                if($this->input->post('save')) {
                  redirect($adminModule.'TeachersController/getAllTeachers');
                } else {
                  redirect($adminModule.'TeachersController/AddTeacher');
                }
            }
        }
        }
      }

      public function getSubject()
      {
        $dataArr = array();
        $dataArr = array(' '=>'Select Subject');
        $subject = $this->Global_model->GetmultiData('tbl_subject_class','*',array('is_deleted'=>0,'is_status'=>1));
        foreach($subject as $key => $value)
        {
          $dataArr[$value->subject_class_id] = $value->subject_class_name;
        }
        return $dataArr;
      }

}