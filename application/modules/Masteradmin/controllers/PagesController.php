<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagesController extends MX_Controller {

     public function __construct() {
        parent::__construct();
        $this->config->set_item('base_url', $this->config->config['secure_url']);
        parent::__construct();
        $this->load->model('Post');
        $this->load->model('Category');
        $this->load->model('Tag');
         $this->load->model('Global_model');
         $this->load->library('form_validation');
        //$this->load->model('Tag');

        //$this->allow_group_access(array('admin','members'));
        
        if ($this->Global_model->isLoggedIn() === TRUE) {
            $data['session']="NoLogin";
        } else {
            redirect(base_url() . 'Masteradmin/Auth');
        }
    }

    public function addPages(){
         $data['post_status'] = array(
            1 => 'Active',
            2 => 'Deactive'
        );
        $this->load->admin_view('Pages/addpages',$data);
    }
	

	public function index(){
		$config['base_url'] = site_url('admin/posts/index/');
		$config['total_rows'] = count($this->Post->find_pages());
		$config['per_page'] = 100;
		$config["uri_segment"] = 4;
        $user_id = null;
        if ($this->input->get('q')):
            $q = $this->input->get('q');
            $data['posts'] = $this->Post->find_pages($config['per_page'], $this->uri->segment(4),$user_id, $q);
            if (empty($data['posts'])) {
                $this->session->set_flashdata('message', message_box('Data tidak ditemukan','danger'));
                redirect('admin/posts/index');
            }
            $config['total_rows'] = count($data['posts']);
        else:
            $data['posts'] = $this->Post->find_pages($config['per_page'], $this->uri->segment(4),$user_id);
        endif;


        //$data['pagination'] = $this->bootstrap_pagination($config);
        
		$this->load->admin_view('Pages/index',$data);
	}

    public function delete($id = null){
        if(!empty($id)){
            $this->Post->delete_pages($id);
            $this->session->set_flashdata('message','Page has been deleted','success');
            redirect('Masteradmin/PagesController/index');
        }else{
            $this->session->set_flashdata('message','Invalid id','danger');
            redirect('Masteradmin/PagesController/index');
        }
    }
 
	public function add(){
		$this->form_validation->set_rules('title', 'title', 'required|is_unique[posts.title]');
        $this->form_validation->set_rules('body', 'body', 'required');
        $data = array(); 
        if ($this->form_validation->run() == TRUE) {
            $data = $_POST;
            // print_data($data);exit;
            $data['type'] = 'pages';
            $data['published_at'] = date('Y-m-d');
            $data['created'] = date("Y-m-d H:i:s");
            $data['modified'] = date("Y-m-d H:i:s");

            $this->Post->create_pages($data);
            $post_id = $this->db->insert_id();
            $this->session->set_flashdata('message', 'New page has been saved','success');
            redirect('Masteradmin/PagesController/addPages');
        }
         $data['post_status'] = array(
            1 => 'Active',
            2 => 'Deactive'
        );
        redirect('Masteradmin/PagesController');
	}

	public function edit($id = null){

        if($id == null){
            $id = $this->input->post('id');
        }

        $data['post_status'] = array(
            1 => 'Active',
            2 => 'Deactive'
        );
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('body', 'body', 'required');
       
        $this->form_validation->set_error_delimiters('', '<br/>');
        if ($this->form_validation->run() == TRUE) {
    
            $data = $_POST;
            $data['modified'] = date("Y-m-d H:i:s");
            
            // $data['user_id'] = $this->session->userdata('user_id');
            $this->Post->update_pages($data,$id);
            $post_id = $id;
            $this->session->set_flashdata('message', 'Page has been saved','success');
            redirect('Masteradmin/PagesController');
        }
        $data['post'] = $this->Post->find_by_id_pages($id);
        $this->load->admin_view('Pages/edit',$data);
    }
}
