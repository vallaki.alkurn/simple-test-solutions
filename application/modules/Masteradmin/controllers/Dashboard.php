<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
 public function __construct() {
        parent::__construct();
        $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
		if ($this->Global_model->isLoggedIn() === TRUE) {
			$data['session']="NoLogin";
		} else {
			redirect(base_url() . 'Masteradmin/Auth');
		}
	}

	public function index()
	{
     $total_teacher = $this->Global_model->GetsingleData('tbl_user_registration','count(*) as teachercount',array('user_type'=>1,'is_deleted'=>0));
     $total_student = $this->Global_model->GetsingleData('tbl_user_registration','count(*) as studentcount',array('user_type'=>2,'is_deleted'=>0));
     $total_attempt_exam = $this->Global_model->GetsingleData('tbl_completed_test','count(*) as totalcount',NULL);
     $total_completed_exam = $this->Global_model->GetsingleData('tbl_completed_test','count(*) as totalcount',array('test_status'=>'Complete'));
     $total_incompleted_exam = $this->Global_model->GetsingleData('tbl_completed_test','count(*) as totalcount',array('test_status'=>'Pending'));
      $total_exam_list = $this->Global_model->GetsingleData('tbl_test_creation','count(*) as totalcount',array('is_status'=>1,'is_deleted'=>0));
    


      $data = array(
                    'total_teacher'=>$total_teacher->teachercount,
                    'total_student'=>$total_student->studentcount,
                    'total_attempt_exam'=>$total_attempt_exam->totalcount,
                    'total_completed_exam'=>$total_completed_exam->totalcount,
                    'total_incompleted_exam'=>$total_incompleted_exam->totalcount,
                    'total_exam_list'=>$total_exam_list->totalcount);
    
      $this->load->admin_view('Dashboard/DashboardHome', $data);
	}

  public function addMaster()
  {
      $this->load->admin_view('page/addMasterForm');
  }

  

	public function ViewMenuAjax()
	{
		 $sort = array('city_name');
     if($_POST['search']['value']) {
     $search="city_name like '%" . $_POST['search']['value'] . "%'";
     }elseif(isset($_POST['unn'])){
     $search = " a._id ='".$_POST['unn']."'";
     }
    $get['fields'] = array('city_name');
    if(isset($search)){
        $get['search']=$search;
        }
        $get['myll']=$_POST['start'];
        $get['offset'] = $_POST['length'];
        if(isset($_POST['order'][0])){
          $orrd= $_POST['order'][0];
          $get['title']=$orrd['column'];
          $get['order']=$orrd['dir'];
     }
    $list = $this->Global_model->getSingleListAjax('city a',$get);
    $cc=$list['count'];
        $data = array();
        $no = $_POST['start'];
        $total_rec = array_pop($list);
        foreach ($list as $missing) {
            $no++;
            $row = array();
            $row[] = $missing->city_name;
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" =>$cc,
                        "recordsFiltered" => $total_rec,
                        "data" => $data,
                );
        echo json_encode($output);
	}
}