<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MX_Controller {

     public function __construct() {
        parent::__construct();
        $this->config->set_item('base_url', $this->config->config['secure_url']);
        parent::__construct();
        $this->load->model('Post');
        $this->load->model('Category');
        $this->load->model('Tag');
         $this->load->model('Global_model');
         $this->load->library('form_validation');
        //$this->load->model('Tag');

        //$this->allow_group_access(array('admin','members'));
        
        if ($this->Global_model->isLoggedIn() === TRUE) {
            $data['session']="NoLogin";
        } else {
            redirect(base_url() . 'Masteradmin/Auth');
        }
    }

    public function addblog(){
        $this->load->admin_view('posts/add');
    }
	

	public function index(){
		$config['base_url'] = site_url('admin/posts/index/');
		$config['total_rows'] = count($this->Post->find());
		$config['per_page'] = 100;
		$config["uri_segment"] = 4;
        $user_id = null;
        if ($this->input->get('q')):
            $q = $this->input->get('q');
            $data['posts'] = $this->Post->find($config['per_page'], $this->uri->segment(4),$user_id, $q);
            if (empty($data['posts'])) {
                $this->session->set_flashdata('message', message_box('Data tidak ditemukan','danger'));
                redirect('admin/posts/index');
            }
            $config['total_rows'] = count($data['posts']);
        else:
            $data['posts'] = $this->Post->find($config['per_page'], $this->uri->segment(4),$user_id);
        endif;
        //$data['pagination'] = $this->bootstrap_pagination($config);
        
		$this->load->admin_view('Posts/index',$data);
	}
 
	public function add(){
        
		$this->form_validation->set_rules('title', 'title', 'required|is_unique[posts.title]');
        $this->form_validation->set_rules('body', 'body', 'required');
        //$this->form_validation->set_rules('featured_image', 'Featured Image', 'required');
        //$this->form_validation->set_rules('published_at', 'date', '');
        if (empty($_FILES['featured_image']['name']))
        {
            $this->form_validation->set_rules('featured_image', 'Featured Image', 'required');
        }

        $data = array(); 
        if ($this->form_validation->run() == TRUE) {

            if ($_FILES['featured_image']['error'] != 4) {
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '*';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload("featured_image")) {
                        $image = $this->upload->data();
                        $url = $config['upload_path'] . $image['orig_name'];
                        $asset = array(
                            'type' => 'post',
                            'mime' => $image['file_type'],
                            'extension' => $image['file_ext'],
                            'size' => $image['file_size'],
                            'description' => $image['raw_name'],
                            'path' => $url,
                            'user_id' => $this->session->userdata('user_id'),
                            'created' => date("Y-m-d H:i:s"),
                            'modified' => date("Y-m-d H:i:s")
                        );
                        //$this->Asset->create($asset);
                        $data1 = array(
                            'link' => $url,
                            'name' => $image["raw_name"]
                        );
                    } else {
                       echo $errors = $this->upload->display_errors();
                       $this->session->set_flashdata('error_message', $errors,'danger');
                       redirect('Masteradmin/posts/add');
                       exit;
                    }
            }

            $data = $_POST;

            $data['featured_image'] = base_url($data1['link']);
            // print_data($data);exit;
            $data['type'] = 'post';
            $data['published_at'] = date('Y-m-d');
            $data['created'] = date("Y-m-d H:i:s");
            $data['modified'] = date("Y-m-d H:i:s");


            $this->Post->create($data);
            $post_id = $this->db->insert_id();
            $this->session->set_flashdata('message', 'New blog has been saved','success');
            redirect('Masteradmin/posts');
        }
         $data['post_status'] = array(
            1 => 'Active',
            2 => 'Deactive'
        );
       	$this->load->admin_view('Posts/add', $data);
	}

	public function edit($id = null){
        if($id == null){
            $id = $this->input->post('id');
        }

        $data['post_status'] = array(
            1 => 'Active',
            2 => 'Deactive'
        );
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('body', 'body', 'required');
       
        $this->form_validation->set_error_delimiters('', '<br/>');
        if ($this->form_validation->run() == TRUE) {



            if ($_FILES['featured_image_new']['error'] != 4) {
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '*';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload("featured_image_new")) {
                        $image = $this->upload->data();
                        $url = $config['upload_path'] . $image['orig_name'];
                        $asset = array(
                            'type' => 'post',
                            'mime' => $image['file_type'],
                            'extension' => $image['file_ext'],
                            'size' => $image['file_size'],
                            'description' => $image['raw_name'],
                            'path' => $url,
                            'user_id' => $this->session->userdata('user_id'),
                            'created' => date("Y-m-d H:i:s"),
                            'modified' => date("Y-m-d H:i:s")
                        );
                        //$this->Asset->create($asset);
                        $data1 = array(
                            'link' => $url,
                            'name' => $image["raw_name"]
                        );
                     } else {
                       echo $errors = $this->upload->display_errors();
                       $this->session->set_flashdata('error_message', $errors,'danger');
                       redirect('Masteradmin/posts/edit/'.$id.'');
                       exit;
                    }
            }


            
            
            $data = $_POST;
            $data['modified'] = date("Y-m-d H:i:s");
            if(!empty($_FILES['featured_image_new']['name'])) {
            $data['featured_image'] = base_url($data1['link']);
            } else {
             $data['featured_image'] = $_POST['featured_image'];    
            }  

          
            
            // $data['user_id'] = $this->session->userdata('user_id');
            $this->Post->update($data,$id);
            $post_id = $id;
            $this->session->set_flashdata('message', 'Post has been saved','success');
            redirect('Masteradmin/posts');
        }
        $data['post'] = $this->Post->find_by_id($id);
        $this->load->admin_view('Posts/edit',$data);
	}

	public function delete($id = null){
		if(!empty($id)){
            $this->Post->delete($id);
            $this->session->set_flashdata('message','Post has been deleted','success');
            redirect('Masteradmin/posts/index');
        }else{
            $this->session->set_flashdata('message','Invalid id','danger');
            redirect('Masteradmin/posts/index');
        }
	}
}
