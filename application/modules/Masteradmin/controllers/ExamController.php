<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExamController extends MX_Controller {
 public function __construct() {
      parent::__construct();
      $this->config->set_item('base_url', $this->config->config['secure_url']);
	    $this->load->model('Global_model');
      $this->load->model('Exam_model');
      $this->lang->load('standard_controller_lang', 'english');
      $this->lang->load('global_controller_lang', 'english');
      $this->load->library('form_validation');
  		if ($this->Global_model->isLoggedIn() === TRUE) {
  			$data['session']="NoLogin";
  		} else {
  			redirect(base_url() . 'Masteradmin/Auth');
  		}

	}
 
  //Exam List
	public function completed_exam_list()
	{ 
     $this->load->admin_view('Exam/completed_exam_list');
	}

  public function getAllCompletedExam()
  {
       $adminModule = 'Masteradmin/';
       $sort = array('test_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "test_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
             $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || test_name like '%" . $_POST['search']['value'] . "%' || test_description like '%" . $_POST['search']['value'] . "%'  || test_instructions like '%" . $_POST['search']['value'] . "%'  || test_form_date like '%" . $_POST['search']['value'] . "%'  || test_to_date like '%" . $_POST['search']['value'] . "%'  || test_access_code like '%" . $_POST['search']['value'] . "%'  || test_time_limit like '%" . $_POST['search']['value'] . "%' )";

             } elseif (isset($_POST['unn'])){
            $search = "tbl_user_registration.user_id  ='".$_POST['unn']."'";
             }
          }
      } 

      $get['fields'] = array('id','test_name','test_description','test_instructions','test_form_date','test_to_date','test_access_code','test_time_limit','first_name','last_name');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Exam_model->getAllCompletedExams($get,'id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->test_name;
              $row[] = $missing->first_name.' '.$missing->last_name;
              $row[] = $missing->test_description;
              $row[] = $missing->test_instructions;
              $row[] = date('M d Y',strtotime($missing->test_form_date));
              $row[] = date('M d Y',strtotime($missing->test_to_date));
              $row[] = $missing->test_access_code;
              $row[] = $missing->test_time_limit.' Minute';
              $row[] ='<a href="'.base_url().''.$adminModule.'ExamController/CompletedExamDetails/'.$missing->id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }


  //Exam List
  public function incomplete_exam_list()
  { 
     $this->load->admin_view('Exam/incomplete_exam_list');
  }

  public function getAllInCompletedExam()
  {
       $adminModule = 'Masteradmin/';
       $sort = array('test_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "test_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
              $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || test_name like '%" . $_POST['search']['value'] . "%' || test_description like '%" . $_POST['search']['value'] . "%'  || test_instructions like '%" . $_POST['search']['value'] . "%'  || test_form_date like '%" . $_POST['search']['value'] . "%'  || test_to_date like '%" . $_POST['search']['value'] . "%'  || test_access_code like '%" . $_POST['search']['value'] . "%'  || test_time_limit like '%" . $_POST['search']['value'] . "%' )";

             } elseif (isset($_POST['unn'])){
            $search = "tbl_user_registration.user_id  ='".$_POST['unn']."'";
             }
          }
      } 

      $get['fields'] = array('id','test_name','test_description','test_instructions','test_form_date','test_to_date','test_access_code','test_time_limit','first_name','last_name');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Exam_model->getAllInCompletedExam($get,'id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->test_name;
              $row[] = $missing->first_name.' '.$missing->last_name;
              $row[] = $missing->test_description;
              $row[] = $missing->test_instructions;
              $row[] = date('M d Y',strtotime($missing->test_form_date));
              $row[] = date('M d Y',strtotime($missing->test_to_date));
              $row[] = $missing->test_access_code;
              $row[] = $missing->test_time_limit.' Minute';
              $row[] ='<a href="'.base_url().''.$adminModule.'ExamController/CompletedExamDetails/'.$missing->id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }


  //Exam List
  public function all_attempt_exam_list()
  { 
     $this->load->admin_view('Exam/all_attempt_exam_list');
  }

  public function getAllAttemptExam()
  {
       $adminModule = 'Masteradmin/';
       $sort = array('test_name');
        if($_POST['search']['value']){
          if(strpos($_POST['search']['value'],':')){
            list($f,$v) = explode(':',$_POST['search']['value']);
            {
              $spl = explode('&',$_POST['search']['value']);
              $standName = explode(':',$spl[0]);
              $statusName = explode(':',$spl[1]);

              //echo $search = "standard_class_name = ".$standName [1]." & is_status = ".$standName [1]."";

              $search = "test_name LIKE '" . $standName [1] . "%'";
              //$search = "$f LIKE '" . $v . "%'";
              //print_r($search);

             //$search = "$f LIKE '" . $v . "%'";
            }
          }else{
             if($_POST['search']['value']) {
              $search = "(first_name like '%" . $_POST['search']['value'] . "%' || last_name like '%" . $_POST['search']['value'] . "%' || test_name like '%" . $_POST['search']['value'] . "%' || test_description like '%" . $_POST['search']['value'] . "%'  || test_instructions like '%" . $_POST['search']['value'] . "%'  || test_form_date like '%" . $_POST['search']['value'] . "%'  || test_to_date like '%" . $_POST['search']['value'] . "%'  || test_access_code like '%" . $_POST['search']['value'] . "%'  || test_time_limit like '%" . $_POST['search']['value'] . "%' )";

             } elseif (isset($_POST['unn'])){
            $search = "tbl_user_registration.user_id  ='".$_POST['unn']."'";
             }
          }
      } 

      $get['fields'] = array('id','test_name','test_description','test_instructions','test_form_date','test_to_date','test_access_code','test_time_limit','first_name','last_name',' tbl_completed_test.test_status');
      if(isset($search))  {
          $get['search']=$search;
          }
          $get['myll']=$_POST['start'];
          $get['offset'] = $_POST['length'];
          if(isset($_POST['order'][0])){
            $orrd= $_POST['order'][0];
            $get['title']=$orrd['column'];
            $get['order']=$orrd['dir'];
       }
      $list = $this->Exam_model->getAllAttemptExam($get,'id');
      $cc=$list['count'];
          $data = array();
          $no = $_POST['start'];
          $total_rec = array_pop($list);
          $sr = 1;
          foreach ($list as $missing) {
              $no++;
              $row = array();
              $row[] = $sr++;
              $row[] = $missing->test_name;
              $row[] = $missing->first_name.' '.$missing->last_name;
              $row[] = $missing->test_description;
              $row[] = $missing->test_instructions;
              $row[] = date('M d Y',strtotime($missing->test_form_date));
              $row[] = date('M d Y',strtotime($missing->test_to_date));
              $row[] = $missing->test_access_code;
              $row[] = $missing->test_time_limit.' Minute';
              $row[] = $missing->test_status;
              $row[] ='<a href="'.base_url().''.$adminModule.'ExamController/CompletedExamDetails/'.$missing->id.'" data-toggle="tooltip" title="" class="btn btn-success" data-original-title="View Profile"><i class="fa fa-eye"></i></a>';
              $data[] = $row;
            }
          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" =>$cc,
                          "recordsFiltered" => $total_rec,
                          "data" => $data,
                  );
          echo json_encode($output);
  }

  public function CompletedExamDetails($id) {
     $data['data_completed_exam'] = $this->Exam_model->completedexam_details($id);
     $this->load->admin_view('Exam/attempt_exam_details',$data);

  }
}