<?php
class Student_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getAllStudent($data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('user_type',2);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllApprovedStudent($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',1);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',1);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function getAllNonApprovedStudent($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllDisApprovedStudent($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('is_deleted',0);
        $this->db->where('user_type',2);
        $this->db->where('is_profile_status',2);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
}
?>
