<?php

class Manage_admin_model extends CI_Model {

    function __construct() {
        parent::__construct();



    }

    function isLoggedIn() {
        if (!empty($this->session->userdata('user_id')) && $this->session->userdata('userType')==1)
            return TRUE;
        else
            return FALSE;
    }


public function getAllmodules(){
$modulequery=$this->db->query("SELECT * FROM `tbl_module` ");
$result= $modulequery->result();
foreach($result as $singleModule){
     $dataas[]=array('moduleId'=>$singleModule->_id, 'module_name'=>$singleModule->module_name, 'Module_permission'=>$this->getAllmodulleLevel($singleModule->_id));
     //$allmodule[]=
     //$singleModule->id

}
  return  $dataas;

}

public function getAllmodulleLevel($moduleId){
       $modulequery=$this->db->query("SELECT * FROM `tbl_permission` where module_id='".$moduleId."' ");
    return   $result= $modulequery->result();


}
















public function getUserModulePermission(){
$user_id=$this->session->userdata('user_id');
$this->db->select('*');
$this->db->where('user_id', $user_id);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
return $query->row();



}

public function get_moduleDetails($moduleId){
$this->db->select('*');
$this->db->where('_id', $moduleId);
$this->db->from('tbl_module');
$query = $this->db->get();
$rowCount=$query->num_rows();
return $query->row();

}

 public function get_SubmoduleDetails($moduleId){
 $user_id=$this->session->userdata('user_id');
 $users=$this->getUsersDetailsById($user_id);
 $subModule_permission=  $users->subModule_permission;
 $data['subModule'] = explode(',', $subModule_permission);
 $this->db->select('*');
$this->db->where('module_id', $moduleId);
$this->db->where_in('_id', $data['subModule']);
$this->db->from('tbl_permission');
$query = $this->db->get();
$rowCount=$query->num_rows();
if($rowCount){
   return $query->result();

}else{
   return $empty= array();

}

 }


















public function getUsersDetailsById($id){
$this->db->select('*');
$this->db->where('user_id', $id);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
 return $query->row();
 }



   public function forgetPassword($email){
   if(!empty($email)){
$this->db->select('*');
$this->db->where('user_email', $email);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
if($rowCount){
  return TRUE;
}else{
 return FALSE;
}
}else{
     return FALSE;
   }

   }

    public function getUserDataBYEmail($email){
   if(!empty($email)){
$this->db->select('*');
$this->db->where('user_email', $email);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
if($rowCount){
  return $row=$query->row();
}else{
 return FALSE;
}
}else{
     return FALSE;
   }

   }


	function checkUserMobile($Mobile) {
        $query = $this->db->query("SELECT * from tbl_users where mobileNo='".$Mobile."'");
       if ($query->num_rows() > 0) {
            return TRUE ;
        } else {
            return FALSE;
        }

    }



	public function randomNumber(){
      return md5(rand('46484664841',4455));
     }

   public function setUserSession($id){

	   $this->session->set_userdata(array(
                            'userId'       => $id,
                            'status'        => TRUE
                    ));
	     if ($this->session->userdata('userId'))
            return $this->session->userdata('userId');
        else
            return FALSE;
	   }

	 function get_user_by_email($table, $email){

		$query = $this->db->query("SELECT * from ".$table." where email_id='".$email."'");

			if ($query->num_rows()  > 0) {
		  		return $query->row();
			}
	}







 /*function PaymentRequestList($data){
	    $this->db->select($data['fields']);
		$this->db->from('tbl_users as b');
		$this->db->join('tbl_requested_payment as a', 'b._id=a.user_id','inner');
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		if(isset($data['order'])){
		$this->db->order_by($data['title'], $data['order']);
		}else{
					$this->db->order_by('a.modified', 'asc');


			}
		$this->db->limit($data['offset'],$data['myll']);
                $query = $this->db->get();
                $row = $query->result();

		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_users as b');
		$this->db->join('tbl_requested_payment as a', 'b._id=a.user_id','inner');
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		$count = $this->db->count_all_results();
		$row['count'] = $count;
		return $row;


	 }



	function getIMWAyUserList($data){
	 $this->db->select($data['fields']);
		$this->db->from('tbl_users');

		$this->db->where("usertype=2");
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		if(isset($data['order']))
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['offset'],$data['myll']);
                $query = $this->db->get();
                $row = $query->result();
		
		//For total number of records
		$this->db->select($data['fields']);

		$this->db->from('tbl_users');

		$this->db->where("usertype=2");
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		$count = $this->db->count_all_results();
		$row['count'] = $count;
		return $row;
	
	
	}

    
    
    function getRegisUserList($data){

   $this->db->select($data['fields']);
		$this->db->from('tbl_users as a');
	
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		if(isset($data['order']))
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['offset'],$data['myll']);
                $query = $this->db->get();
                $row = $query->result();
		
		//For total number of records
		$this->db->select($data['fields']);

		$this->db->from('tbl_users as a');
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		$count = $this->db->count_all_results();
		$row['count'] = $count;
		return $row;
    
    
    
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      */
	  
	    public function getMenuList($data){
        $this->db->select($data['fields']);
        $this->db->from('tbl_footer_sub_menu tfsm');
        $this->db->join('tbl_footer_menu tfm','tfsm.menu_head = tfm._id','left');
		$this->db->where('tfsm.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }

        if(isset($data['order'])){
        $this->db->order_by($data['title'], $data['order']);
        }
        else{
        $this->db->order_by('tfsm._id', 'asc');
         }
        $this->db->limit($data['offset'],$data['myll']);
        $query = $this->db->get();
        $row = $query->result();

        $this->db->select($data['fields']);
         $this->db->from('tbl_footer_sub_menu tfsm');
        $this->db->join('tbl_footer_menu tfm','tfsm.menu_head = tfm._id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
    
    
    

	/*************get apply job*******/
	 public function getApplyJobs($data)
	{
        $this->db->select('*,taj._id as tajid');
        $this->db->from('tbl_apply_jobs taj');
		$this->db->join('career_jobs cj','taj.job_id = cj._id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by('taj._id', 'asc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        $row = $query->result();
		$this->db->select('*');
        $this->db->from('tbl_apply_jobs taj');
		$this->db->join('career_jobs cj','taj.job_id = cj._id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
    
    




}
?>
