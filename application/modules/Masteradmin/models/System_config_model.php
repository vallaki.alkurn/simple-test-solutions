<?php

class System_config_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

 
    
    public function batch_save($data)
    {   
        $success = TRUE;
        foreach($data as $key=>$value)
        {
            $success &= $this->save($key, $value);
        }
        return $success;
    }


    public function exists($key)
    {
        $this->db->from('tbl_system_config');
        $this->db->where('tbl_system_config.system_config_key', $key);
        return ($this->db->get()->num_rows() == 1);
    }


    public function save($key, $value)
    {
        $config_data = array(
            'system_config_key'   => $key,
            'system_config_value' => $value
        );
        if(!$this->exists($key))
        {
            return $this->db->insert('tbl_system_config', $config_data);
        }
        $this->db->where('system_config_key', $key);
        return $this->db->update('tbl_system_config', $config_data);
    }
}
?>
