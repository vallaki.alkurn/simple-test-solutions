<?php
class Payment_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getAllTranasction($data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from('tbl_transaction');
        $this->db->join('tbl_user_registration','tbl_transaction.user_id = tbl_user_registration.user_id','left');
        $this->db->join('tbl_user_card_details','tbl_user_card_details.id = tbl_transaction.card_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_transaction');
         $this->db->join('tbl_user_registration','tbl_transaction.user_id = tbl_user_registration.user_id','left');
        $this->db->join('tbl_user_card_details','tbl_user_card_details.id = tbl_transaction.card_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }



    public function getAllUserAccountDetails($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_card_details');
        $this->db->join('tbl_user_registration','tbl_user_card_details.user_id = tbl_user_registration.user_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_card_details');
        $this->db->join('tbl_user_registration','tbl_user_card_details.user_id = tbl_user_registration.user_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

}
?>
