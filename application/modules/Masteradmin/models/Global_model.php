<?php

class Global_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function isLoggedIn() {
        if ($this->session->userdata('user_Type')==1)
            return TRUE;
        else
            return FALSE;
    }


	public function getAllmodules()
	{
	$modulequery=$this->db->query("SELECT * FROM `tbl_module`");
	$result= $modulequery->result();
	foreach($result as $singleModule){
		 $dataas[]=array('moduleId'=>$singleModule->_id, 'module_name'=>$singleModule->module_name, 'Module_permission'=>$this->getAllmodulleLevel($singleModule->_id));
		 //$allmodule[]=
		 //$singleModule->id
	}
	return  $dataas;
	}

	public function getAllmodulleLevel($moduleId)
	{
		$modulequery=$this->db->query("SELECT * FROM `tbl_permission` where module_id='".$moduleId."'");
		return $result= $modulequery->result();
	}



public function getUserModulePermission(){
$user_id=$this->session->userdata('user_id');
$this->db->select('*');
$this->db->where('user_id', $user_id);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
return $query->row();
}

public function get_moduleDetails($moduleId,$sectionId){
$this->db->select('*');
$this->db->where('_id', $moduleId);
$this->db->where('section_id', $sectionId);
$this->db->from('tbl_module');
$query = $this->db->get();
//print_r($this->db->last_query());
$rowCount=$query->num_rows();
return $query->row();

}

 public function get_SubmoduleDetails($moduleId){
 $user_id=$this->session->userdata('user_id');
 $users=$this->getUsersDetailsById($user_id);
 $subModule_permission=  $users->subModule_permission;
 $data['subModule'] = explode(',', $subModule_permission);
 $this->db->select('*');
$this->db->where('module_id', $moduleId);
$this->db->where('is_left', 1);
$this->db->where_in('_id', $data['subModule']);
$this->db->from('tbl_permission');
$query = $this->db->get();
$rowCount=$query->num_rows();
if($rowCount){
   return $query->result();

}else{
   return $empty= array();

}

 }







public function getUsersDetailsById($id){
$this->db->select('*');
$this->db->where('user_id', $id);
$this->db->from('users');
$query = $this->db->get();
$rowCount=$query->num_rows();
 return $query->row();
 }



   public function forgetPassword($email){
  		if(!empty($email)){
		$this->db->select('*');
		$this->db->where('user_email', $email);
		$this->db->from('users');
		$query = $this->db->get();
		$rowCount=$query->num_rows();
		if($rowCount){
		  return TRUE;
		}else{
		 return FALSE;
		}
		}else{
			 return FALSE;
		   }

   }

    public function getUserDataBYEmail($email){
   if(!empty($email)){
					$this->db->select('*');
					$this->db->where('user_email', $email);
					$this->db->from('users');
					$query = $this->db->get();
					$rowCount=$query->num_rows();
					if($rowCount){
					  return $row=$query->row();
					}else{
					 return FALSE;
					}
					}else{
						 return FALSE;
					   }
  			           }


	function checkUserMobile($Mobile) {
        $query = $this->db->query("SELECT * from tbl_users where mobileNo='".$Mobile."'");
       if ($query->num_rows() > 0) {
            return TRUE ;
        } else {
            return FALSE;
        }

    }



	public function randomNumber(){
      return md5(rand('46484664841',4455));
     }

   public function setUserSession($id){

	   $this->session->set_userdata(array(
                            'userId'       => $id,
                            'status'        => TRUE
                    ));
	     if ($this->session->userdata('userId'))
            return $this->session->userdata('userId');
        else
            return FALSE;
	   }

	 function get_user_by_email($table, $email){
		$query = $this->db->query("SELECT * from ".$table." where email_id='".$email."'");
		if ($query->num_rows()  > 0) {
			return $query->row();
		}
	}




	
    /*********start comman function for all*******************/
    public function GetmultiData($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }

    public function GetsingleData($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
        $query = $this->db->get();
		
        $rowCount= $query->row();
		//print_r($this->db->last_query());
		//exit;
        return $rowCount;
    }

    public function SelectData($table = NULL, $field = NULL, $cond = NULL, $limit = NULL, $groupby = NULL, $order = NULL)
    {
    $this->db->select($field);
    $this->db->from($table);
    if($cond){
    $this->db->where($cond);
    }
    if($groupby){
         $this->db->group_by($groupby);
    }
    if($order){
    $this->db->order_by($order['col'],$order['order']);
    }
    $q = $this->db->get();
    $res = $q->result();
    //print_r($this->db->last_query());
    if($limit){
    return $res[0];
    }else{
    return $res;
    }
    }

    public function UpdateRecord($table, $data, $cond = NULL)
    {
	if($cond)
	{
    $this->db->where($cond);
	}
    $res = $this->db->update($table,  $data);
    //print_r($this->db->last_query());
    return $res;
    }

    public function InsertRecord($table, $data)
    {
    $res = $this->db->insert($table,$data);
	$insert_id = $this->db->insert_id();
    //print_r($this->db->last_query());
    return $insert_id;
    }


    public function InsertUpdate($table, $data, $tbid = NULL)
    {
        if(!empty($_POST['id']))
        {
            $this->db->where($tbid,$_POST['id']);
            $res = $this->db->update($table,$data);
            return $_POST['id'];
        }
		else
        {
            $res = $this->db->insert($table,$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    public function InsertUpdateByval($table, $data, $wherdata = NULL)
    {
		$res = $this->db->insert($table,$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	    //print_r($this->db->last_query());
    }

    public function InsertUpdatebywhere($table, $data, $wherdata=NULL)
    {   
    	//print_r($data);
 	    $ressdata= $this->GetsingleData($table, '*', $wherdata);
        if(count($ressdata)>0){
            $this->db->where($wherdata);
            $res = $this->db->update($table,  $data);
			//print_r($this->db->last_query());
            if(isset($ressdata->_id)){
            return  $ressdata->_id;  
            exit; 
            }
            if(isset($ressdata->id)){
            return  $ressdata->id;   
            exit; 
            }
        }else{
            $res = $this->db->insert($table,$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
		//exit;
   }
	
    public function DeleteRecord($table,$cond = NULL)
    {
        $this->db->where($cond);
        $res = $this->db->delete($table);
        return $res;
    }

    public function GetSingleDataRecord()
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by('a._id', 'asc');
        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();

        //For total number of records
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function getCreatedByName($id)
    {
        $this->db->select('name');
        $this->db->where('user_id',$id);
        $this->db->from('users');
        $query = $this->db->get();
        $rowCount = $query->row();
        return $rowCount;
    }

    public function getSingleListAjax($table,$data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
	
	
    /*********end comman function for all*******************/
	public function getUserBankDetail($table,$data)
	{
        $this->db->select($data['fields']);
        $this->db->from($table);
		$this->db->join('tbl_users tu','tu._id = tub.userId','left');
        $this->db->where('tub.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by('tub._id', 'asc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('tub.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
    /*********end comman function for all*******************/
	
	public function getallStore()
    {
        $this->db->select('store_name');
        $this->db->from('tbl_all_offer_details');
		$this->db->where('store_name !=','');
		$this->db->group_by('store_name');
        $query = $this->db->get();
        $rowCount = $query->result();
        return $rowCount;
    }


    public function getNotificationUserId()
    {
      $userId = explode(',',$_POST['user_ids']);
      $result = "'" . implode ( "', '", $userId ) . "'";
      $SQL = 'SELECT GROUP_CONCAT(_id) as groupId FROM `tbl_users` WHERE `tracking_id` IN('.$result.')';

        $query = $this->db->query($SQL);
        $rowCount = $query->row();
        return $rowCount;
    }
	
	public function Check_Cash_back_data_panel($unique_click_id, $unique_transaction_id)
    {
        $find_cashBack = $this->GetsingleData('tbl_cashback_users_by_panel', '_id', array('uniq_click_id' => $unique_click_id, 'uniq_transaction_id'=>$unique_transaction_id));
        if(count($find_cashBack)>0)
        {
            return $find_cashBack->_id;
        }
        else
        {
            return '';  
        }
    }
}
?>
