<?php
class Exam_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getAllCompletedExams($data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_completed_test.test_status','Complete');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_completed_test.test_status','Complete');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


     public function getAllInCompletedExam($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_completed_test.test_status','Pending');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_completed_test.test_status','Pending');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllAttemptExam($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function completedexam_details($id)
    {
        $this->db->select('*,(select first_name from tbl_user_registration where tbl_user_registration.user_id = tbl_test_creation.user_id) as teacher_first_name,(select last_name from tbl_user_registration where tbl_user_registration.user_id = tbl_test_creation.user_id) as teacher_last_name');
        $this->db->from('tbl_completed_test');
        $this->db->join('tbl_test_creation','tbl_completed_test.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_completed_test.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_completed_test.id',$id);
        $query = $this->db->get();
        return $row = $query->row();
    }

}
?>
