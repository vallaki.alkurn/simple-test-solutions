<?php
class Teacher_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getAllTeacher($data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('user_type',1);
        $this->db->where('tbl_user_registration.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('user_type',1);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllApprovedTeacher($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',1);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',1);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function getAllNonApprovedTeacher($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllDisApprovedTeacher($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
         $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_user_registration');
        $this->db->join('tbl_subject_class','tbl_subject_class.subject_class_id = tbl_user_registration.subject_class_id','left');
        $this->db->where('tbl_user_registration.is_deleted',0);
        $this->db->where('user_type',1);
        $this->db->where('is_profile_status',2);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
}
?>
