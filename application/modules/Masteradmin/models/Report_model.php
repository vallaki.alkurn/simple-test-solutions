<?php
class Report_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function getAllExams($data,$sortid)
	{
        $this->db->select($data['fields']);
        $this->db->from('tbl_test_creation');
        $this->db->join('tbl_user_registration','tbl_test_creation.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_test_creation.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by($sortid, 'desc');

        }
		if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
		}
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_test_creation');
        $this->db->join('tbl_user_registration','tbl_test_creation.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_test_creation.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }


    public function getAllQuestions($data,$sortid)
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_que_creation');
        $this->db->join('tbl_q_type_master','tbl_que_creation.que_type_id = tbl_q_type_master.question_type_id','left');
        $this->db->join('tbl_test_creation','tbl_que_creation.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_que_creation.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_que_creation.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by($sortid, 'desc');

        }
        if($data['offset'] != -1) {
        $this->db->limit($data['offset'],$data['myll']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from('tbl_que_creation');
        $this->db->join('tbl_q_type_master','tbl_que_creation.que_type_id = tbl_q_type_master.question_type_id','left');
        $this->db->join('tbl_test_creation','tbl_que_creation.test_id = tbl_test_creation.test_id','left');
        $this->db->join('tbl_user_registration','tbl_que_creation.user_id = tbl_user_registration.user_id','left');
        $this->db->where('tbl_que_creation.is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

}
?>
