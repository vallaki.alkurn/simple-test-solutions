
  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right"><a href="<?php echo site_url('Masteradmin/posts/add')?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add New"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $this->lang->line('standard_list_main_title'); ?></h1>
    </div>
  </div>
  
  <div class="container-fluid">
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $this->lang->line('standard_add_title'); ?></h3>
      </div> 
      <div class="panel-body">

        <div class="row">
    <div class="col-md-12">
        <div class="box"> 
            <div class="box-header">
                <h3 class="box-title">Posts</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php if($this->session->flashdata('message')) { ?>
                <div class="form-group">
                <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                </div>
                <?php } ?>
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Title</th>
                        <!-- <th>Slug</th> -->
                        <th style="width: 100px">Action</th>
                    </tr>
                    <?php if(!empty($posts)):?>
                        <?php foreach($posts as $post):?>
                            <tr>
                                <td><?php echo $post['id']?></td>
                                <td><?php echo $post['title']?></td>
                                <!-- <td><?php echo $post['slug']?></td> -->
                                <td>
                                    <a class="btn btn-primary"   href="<?php echo site_url('Masteradmin/posts/edit/'.$post['id'])?>"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-danger" href="<?php echo site_url('Masteradmin/posts/delete/'.$post['id'])?>" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    <?php else:?>
                        <tr><td colspan="5">No record found</td></tr>
                    <?php endif;?>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <?php //echo $pagination ?>
            </div>
        </div><!-- /.box -->
    </div>
</div>
      </div>
    </div>
</div>
