    <?php echo form_open(base_url().$adminModule.'SystemSettingController'."/".'updateInformation', 'class="form-horizontal" id="add_standard"'); ?>


		  <div class="form-group">
		    <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_company_name_label'); ?></label>
		    <div class="col-sm-10">
		     	 <?php
		    	 echo form_input(
                              array(
                                    'name'          => 'company_name',
                                    'id'            => 'id_company_name',
                                    'value'         => getsystemConfig('company_name'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_name_label')
                                     )
                               );

		    	?>
		    </div>
		  </div>
                      
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $this->lang->line('system_company_address_label'); ?></label>
            <div class="col-sm-10">
            	<?php
		    	 echo form_textarea(
                              array(
                                    'name'          => 'company_address',
                                    'id'            => 'id_company_address',
                                    'value'         => getsystemConfig('company_address'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_address_label')
                                     )
                               );

		    	?>
            </div>
			</div>

			<div class="form-group">
		    <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_company_website_label'); ?></label>
		    <div class="col-sm-10">
		     	 <?php
		    	 echo form_input(
                              array(
                                    'name'          => 'company_website',
                                    'id'            => 'id_company_websiet',
                                    'value'         => getsystemConfig('company_website'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_website_label')
                                     )
                               );

		    	?>
		    </div>
		  </div>

		  <div class="form-group">
		    <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_company_email_label'); ?></label>
		    <div class="col-sm-10">
		     	 <?php
		    	 echo form_input(
                              array(
                                    'name'          => 'company_email',
                                    'id'            => 'id_company_email',
                                    'value'         => getsystemConfig('company_email'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_email_label')
                                     )
                               );

		    	?>
		    </div>
		  </div>


		  <div class="form-group">
		    <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_company_phone_label'); ?></label>
		    <div class="col-sm-10">
		     	 <?php
		    	 echo form_input(
                              array(
                                    'name'          => 'company_phone',
                                    'id'            => 'id_company_phone',
                                    'value'         => getsystemConfig('company_phone'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_phone_label')
                                     )
                               );

		    	?>
		    </div>
		  </div>


		  <div class="form-group">
		    <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_company_fax_label'); ?></label>
		    <div class="col-sm-10">
		     	 <?php
		    	 echo form_input(
                              array(
                                    'name'          => 'company_fax',
                                    'id'            => 'id_company_fax',
                                    'value'         => getsystemConfig('company_fax'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_company_fax_label')
                                     )
                               );

		    	?>
		    </div>
		  </div>

		<?php if($this->session->flashdata('message')) { ?>
		   <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
				<div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
				</div>
		    </div>
		  </div>
		<?php } ?>


         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <div class="pull-left">
               <?php 
                echo form_submit(array(
                'name'=> 'save',
                'type' => 'submit',
                'value'=> $this->lang->line('save_button'),
                'class'=> 'btn btn-success',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_button')
                ));
                ?>
          </div>
   		</div>
   	 </div>
<?php echo form_close(); ?>