    <?php echo form_open(base_url().$adminModule.'SystemSettingController'."/".'updateEmail', 'class="form-horizontal"'); ?>


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_protocol_label'); ?></label>
        <div class="col-sm-10">
                <?php
                 echo form_dropdown('protocol',array(
                                      'smtp'         => 'smtp'
                                    ),getsystemConfig('protocol'),'class="form-control"');
                ?>
        </div>
      </div>



      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_path_to_sendmail_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'mailpath',
                                    'id'            => 'id_mailpath',
                                    'value'         => getsystemConfig('mailpath'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_path_to_sendmail_label')
                                     )
                               );

          ?>
        </div>
      </div>


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_server_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_host',
                                    'id'            => 'id_smtp_host',
                                    'value'         => getsystemConfig('smtp_host'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_server_label')
                                     )
                               );

          ?>
        </div>
      </div>


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_port_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_port',
                                    'id'            => 'id_smtp_port',
                                    'value'         => getsystemConfig('smtp_port'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_port_label')
                                     )
                               );

          ?>
        </div>
      </div>  



      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_encryption_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_crypto',
                                    'id'            => 'id_smtp_crypto',
                                    'value'         => getsystemConfig('smtp_crypto'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_encryption_label')
                                     )
                               );

          ?>
        </div>
      </div>  



       <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_timeout_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_timeout',
                                    'id'            => 'id_smtp_timeout',
                                    'value'         => getsystemConfig('smtp_timeout'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_timeout_label')
                                     )
                               );

          ?>
        </div>
      </div>  



      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_username_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_user',
                                    'id'            => 'id_smtp_user',
                                    'value'         => getsystemConfig('smtp_user'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_username_label')
                                     )
                               );

          ?>
        </div>
      </div>
             


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_smtp_password_label'); ?></label>
        <div class="col-sm-10">
           <?php
           echo form_input(
                              array(
                                    'name'          => 'smtp_pass',
                                    'id'            => 'id_smtp_pass',
                                    'value'         => getsystemConfig('smtp_pass'),
                                    'maxlength'     => '100',
                                    'size'          => '50',
                                    'class'         => 'form-control',
                                    'placeholder'   => $this->lang->line('system_smtp_password_label')
                                     )
                               );

          ?>
        </div>
      </div>
                               


     

        <?php if($this->session->flashdata('message')) { ?>
           <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            </div>
          </div>
        <?php } ?>


         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <div class="pull-left">
               <?php 
                echo form_submit(array(
                'name'=> 'save',
                'type' => 'submit',
                'value'=> $this->lang->line('save_button'),
                'class'=> 'btn btn-success',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_button')
                ));
                ?>
          </div>
      </div>
     </div>
<?php echo form_close(); ?>