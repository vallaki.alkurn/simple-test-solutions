<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $this->lang->line('system_config_page_title'); ?></h1>
    </div>
  </div>
  <div class="container-fluid">
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?php echo $this->lang->line('system_config_form_title'); ?></h3>
      </div>
      <div class="panel-body">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-information" data-toggle="tab">Information</a></li>
            <li><a href="#tab-general" data-toggle="tab">General</a></li>  
            <li><a href="#tab-email" data-toggle="tab">Email</a></li>       
          </ul>
          <div class="tab-content">
              <div class="tab-pane active"  id="tab-information">
              <?php $this->load->view('information_config'); ?>
              </div>
        		  <div class="tab-pane" id="tab-general">
              <?php $this->load->view('general_config'); ?>
        			</div>	
              <div class="tab-pane" id="tab-email">
              <?php $this->load->view('email_config'); ?>
              </div>
          </div>
    </div>
</div>
</div>