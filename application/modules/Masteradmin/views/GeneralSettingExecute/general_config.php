    <?php echo form_open(base_url().$adminModule.'SystemSettingController'."/".'updateGeneral', 'class="form-horizontal"'); ?>


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_dateformat_label'); ?></label>
        <div class="col-sm-10">
                <?php
                 echo form_dropdown('dateformat',array(
                                      'd/m/Y'         => 'dd/mm/yyyy',
                                      'd.m.Y'         => 'd.m.Y',
                                      'm/d/Y'         => 'm/d/Y',
                                      'Y/m/d'         => 'Y/m/d',
                                      'd/m/y'         => 'd/m/y',
                                      'm/d/y'         => 'm/d/y',
                                      'y/m/d'         => 'y/m/d'
                                      ),getsystemConfig('dateformat'),'class="form-control"');
                ?>
        </div>
      </div>


      <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('system_timeformat_label'); ?></label>
        <div class="col-sm-10">
                <?php
                 echo form_dropdown('timeformat',array(
                                      'H:i:s'         => 'H:i:s',
                                      'h:i:s a'         => 'h:i:s a',
                                      'h:i:s A'         => 'h:i:s A'
                                      ),getsystemConfig('timeformat'),'class="form-control"');
                ?>
        </div>
      </div>


    <?php if($this->session->flashdata('message')) { ?>
       <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
        <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        </div>
      </div>
    <?php } ?>


         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <div class="pull-left">
               <?php 
                echo form_submit(array(
                'name'=> 'save',
                'type' => 'submit',
                'value'=> $this->lang->line('save_button'),
                'class'=> 'btn btn-success',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_button')
                ));
                ?>
          </div>
      </div>
     </div>
<?php echo form_close(); ?>