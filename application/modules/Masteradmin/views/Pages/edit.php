  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <!--   <button type="submit" form="form-option" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i></button> -->
        <a href="<?php echo base_url().$adminModule.'MasterController'."/".'StandardExecute'; ?>" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a></div>
        <h1>Pages</h1>
      
    </div>
  </div>
  <div class="container-fluid">
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $this->lang->line('standard_add_title'); ?></h3>
      </div> 
      <div class="panel-body">

         <?php if($this->session->flashdata('message')) { ?>
           <div class="form-group">
            <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
          </div>
         <?php } ?>

         <?php if($this->session->flashdata('error_message')) { ?>
           <div class="form-group">
            <div class="alert alert-danger" style="display: block;"><?php echo $this->session->flashdata('error_message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
          </div>
         <?php } ?>

       <form role="form" action="<?php echo site_url('Masteradmin/PagesController/edit')?>"  enctype="multipart/form-data" method="post">
                <input type="hidden" name="id" value="<?php echo $post['id']?>">
                <div class="box-body">
                    <div class="form-group">
                        <label for="post_name">Title</label>
                        <input type="text" name="title" class="form-control" id="post_name" placeholder="Title" value="<?php echo set_value('title', isset($post['title']) ? $post['title'] : '') ?>">
                    </div>
                    <div class="form-group">
                        <label for="post_body">Body</label>
                        <textarea name="body" class="form-control txteditor" id="post_body" placeholder="Body" rows="10"><?php echo set_value('body', isset($post['body']) ? $post['body'] : '') ?></textarea>
                    </div>

                     <div class="form-group">
                        <label for="post_status">Status</label>
                        <?php
                            echo form_dropdown('status',$post_status, set_value('status', isset($post['status']) ? $post['status'] : ''),array('class' => 'form-control'));
                        ?>
                    </div>
                   
                    
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button> 
                    <button type="button" class="btn btn-default" onclick="javascript:history.back()">Back</button>
                </div>
            </form>
      </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="assetsModal" tabindex="-1" role="dialog" aria-labelledby="assetsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="assetsModalLabel">Assets Manager</h4>
      </div>
        <div class="modal-body">
            <div class="row">
            <ul class="thumbnails padding-top list-unstyled" id="assetsList">

            </ul>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
  </div>
</div>

<!-- Line Control WYSIWYG -->
<script src="<?php echo base_url();?>assets/Admin/plugins/line_control_editor/editor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("button:submit").click(function(){
        $('.txteditor').text($('.txteditor').Editor("getText"));
    });
    
    var editor = $(".txteditor").Editor();
    $('.txteditor').Editor("setText", "<?php echo !empty($post['body']) ? addslashes($post['body']) :'';?>");        
})


function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.preview_featured_image_new').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#id_featured_image").change(function() {
  readURL(this);
});
</script>
