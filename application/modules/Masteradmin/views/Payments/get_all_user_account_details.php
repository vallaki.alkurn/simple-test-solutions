  <div class="page-header">
    <div class="container-fluid">
      <h1>User Card Details</h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>
         User Card Details List</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="example" class="table table-striped">
            <thead>
              <tr>
                <td>Sr No</td>
                <td>Teacher Name</td>
                <td>Card No</td>
                <td>Card Holder Name</td>
                <td>CVV Number</td>
                <td>Expiry Date</td>
                <td>Amount</td>
                <td>Status</td>
                <td>Payment Date</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>PaymentController/getAllUserCardDetails');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

