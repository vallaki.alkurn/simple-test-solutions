  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <!--   <button type="submit" form="form-option" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i></button> -->
     <!--    <a href="<?php echo base_url().$adminModule.'StudentsController'."/".'SaveStudent'; ?>" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Cancel"><i class="fa fa-reply"></i></a> --></div>
        <h1><?php echo $this->lang->line('student_update_profile_main_title'); ?></h1>
      
    </div>
  </div>
  <div class="container-fluid">
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $formtitle; ?></h3>
      </div>
      <div class="panel-body">
          <?php echo form_open(base_url().$adminModule.'StudentsController'."/".'SaveStudent', 'class="form-horizontal" id="add_standard"'); ?>
         

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-type"><?php echo $this->lang->line('add_standard_name_title'); ?></label>
            <div class="col-sm-10">
               <?php
                  echo form_input(
                                  array(
                                        'name'          => 'first_name',
                                        'id'            => 'id_first_name',
                                        'value'         =>  ($student_edit && $student_edit->first_name) ? $student_edit->first_name : $this->input->post('first_name'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'placeholder'   => $this->lang->line('add_standard_name_title')
                                         )
                                  );
                    echo form_input(
                                  array(
                                        'name'          => 'id',
                                        'id'            => 'id_user_id',
                                        'value'         => ($student_edit && $student_edit->user_id) ? $student_edit->user_id : $this->input->post('id'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'type'=>'hidden'
                                         )
                                  );

                ?>

                <?php if(form_error($this->lang->line('studetn_class_first_name_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('studetn_class_first_name_error')); ?></div>
                <?php } ?>
            </div>

          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-type"><?php echo $this->lang->line('add_standard_last_name_title'); ?></label>
            <div class="col-sm-10">
               <?php
                  echo form_input(
                                  array(
                                        'name'          => 'last_name',
                                        'id'            => 'id_last_name',
                                        'value'         =>  ($student_edit && $student_edit->last_name) ? $student_edit->last_name : $this->input->post('last_name'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'placeholder'   => $this->lang->line('add_standard_last_name_title')
                                         )
                                  );
                ?>
                <?php if(form_error($this->lang->line('studetn_class_last_name_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('studetn_class_last_name_error')); ?></div>
                <?php } ?>
            </div>

          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-type"><?php echo $this->lang->line('add_standard_email_title'); ?></label>
            <div class="col-sm-10">
               <?php
                  echo form_input(
                                  array(
                                        'name'          => 'email_address',
                                        'id'            => 'id_email_address',
                                        'value'         =>  ($student_edit && $student_edit->email_address) ? $student_edit->email_address : $this->input->post('email_address'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'placeholder'   => $this->lang->line('add_standard_email_title')
                                         )
                                  );
                ?>

                <?php if($this->session->flashdata('message_name')) { ?>
                   <div class="text-danger"><?php echo $this->session->flashdata('message_name'); ?></div>
                <?php } ?>
                <?php if(form_error($this->lang->line('studetn_class_email_address_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('studetn_class_email_address_error')); ?></div>
                <?php } ?>
            </div>

          </div>





          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-type"><?php echo $this->lang->line('add_standard_school_title'); ?></label>
            <div class="col-sm-10">
               <?php
                  echo form_input(
                                  array(
                                        'name'          => 'school_collage',
                                        'id'            => 'id_school_collage',
                                        'value'         =>  ($student_edit && $student_edit->school_collage) ? $student_edit->school_collage : $this->input->post('school_collage'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'placeholder'   => $this->lang->line('add_standard_school_title')
                                         )
                                  );
                ?>
                <?php if(form_error($this->lang->line('studetn_class_school_collage_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('studetn_class_school_collage_error')); ?></div>
                <?php } ?>
            </div>

          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $this->lang->line('add_standard_input_title'); ?></label>
            <div class="col-sm-10">
              <?php


              echo form_dropdown('standard_class_id',$standard,($student_edit) ? $student_edit->standard_class_id : $this->input->post('standard_class_id'),'class="form-control"');
              ?>  

               <?php if(form_error($this->lang->line('studetn_class_standard_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('studetn_class_standard_error')); ?></div>
                <?php } ?>

            </div>
          </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $this->lang->line('add_standard_input_status'); ?></label>
            <div class="col-sm-10">
              <?php
              echo form_dropdown('is_profile_status',array(
                              '1'         => $this->lang->line('dropdown_status_label_1'),
                              '0'        => $this->lang->line('dropdown_status_label_0')
                      ),($student_edit) ? $student_edit->is_profile_status : $this->input->post('is_profile_status'),'class="form-control"');
              ?>  
            </div>
          </div>


          

          <?php if($this->session->flashdata('message')) { ?>
           <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            </div>
          </div>
         <?php } ?>

         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <div class="pull-left">
               <?php 
                echo form_submit(array(
                'name'=> 'save',
                'type' => 'submit',
                'value'=> $this->lang->line('save_button'),
                'class'=> 'btn btn-success',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_button')
                ));
                ?>

                <?php 
                echo form_submit(array(
                'name'=> 'save_continue',
                'type' => 'submit',
                'value'=> $this->lang->line('save_and_continue_button'),
                'class'=> 'btn btn-primary',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_and_continue_button')
                ));
                ?>

                <?php 
                // echo form_submit(array(
                // 'name'=> 'reset',
                // 'type' => 'reset',
                // 'value'=> $this->lang->line('reset_button'),
                // 'class'=> 'btn btn-danger',
                // 'data-toggle'=> 'tooltip',
                // 'data-original-title'=> $this->lang->line('reset_button')
                // ));
                ?>
        
          </div>
          </div>
          </div>
          <?php echo  form_close(); ?>
      </div>
    </div>
</div>