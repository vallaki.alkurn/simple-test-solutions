  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right">
      </div>
      <h1><?php echo $this->lang->line('student_profile_main_title'); ?></h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
     
 
      <div class="panel-body">
      
        <div class="col-lg-12 col-xs-12 admin-order-list"> 
          <div class="col-lg-4 col-xs-12 order-left pull-left">
            <div class="row">
              <div class="admin-order-left clearfix">
                <ul class="list-unstyled clearfix">
                  <li><span class="order-placed"><?php //echo $this->lang->line('add_registration_date_title'); ?> </span></li>
                  <li class="order-date-time">
                    <span class="date"><b><?php //echo $student->registered_date; ?></b></span>
                  </li>
                  <li>
                    <span class="admin-order-id"><?php echo $this->lang->line('add_standard_name_title'); ?> :</span>
                    <span class="admin-student"><?php echo $student->first_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method"><?php echo $this->lang->line('add_standard_last_name_title'); ?> : </span>
                    <span class="admin-student"><?php echo $student->last_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method"><?php echo $this->lang->line('add_standard_email_title'); ?> : </span>
                   <span class="admin-student"><?php echo $student->email_address; ?></span>
                  </li>
  
                                <li class="admin-last-subtotal">
                    <span class="admin-subtotal"><?php echo $this->lang->line('add_standard_school_title'); ?> :</span>
                    <span class="admin-student"><?php echo $student->school_collage; ?></span>
                  </li>
                                     
                </ul>
              </div>
            </div>
          </div><!-- order-left -->
      
          <div class="col-lg-8 col-xs-12 order-right pull-right">  
               
              <div class="admin-order-right clearfix">
                <div class="col-lg-12 list-unstyled">
                  
                
                <div class="col-lg-3 marginbottom">
                   <?php echo anchor('Masteradmin/StudentsController/AddStudent/'.$user_id.'', 'Update Profile', 'class="btn btn-success profilebutton"') ?> 
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Exam History',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Result History',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Upcoming Test',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Send Invitation',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Add Block List',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'Manage Status',
                        'class'=> 'btn btn-primary profilebutton disabledbutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'Under Construction'
                        ));
                    ?>
                </div>  



               



                </div>   

           </div> 
                  
              
              
          </div><!-- order-right -->
        </div>
      </div>
    </div>
  </div>

<style type="text/css">

.disabledbutton
{
  background: #CCC;
  border: 1px solid #CCC;
}
.disabledbutton:hover
{
  background: #CCC;
  border: 1px solid #CCC;
  cursor: context-menu;
}

</style>

<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>StudentsController/getAllStudentsAjax');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

