  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <!--   <button type="submit" form="form-option" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Save"><i class="fa fa-save"></i></button> -->
        <a href="<?php echo base_url().$adminModule.'MasterController'."/".'SubjectExecute'; ?>" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa  fa-reply"></i></a></div>
        <h1><?php echo $this->lang->line('subject_list_main_title'); ?></h1>
      
    </div>
  </div>
  <div class="container-fluid">
        <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $this->lang->line('subject_add_title'); ?></h3>
      </div>
      <div class="panel-body">
          <?php echo form_open(base_url().$adminModule.'MasterController'."/".'SaveSubject', 'class="form-horizontal" id="add_standard"'); ?>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-type"><?php echo $this->lang->line('add_subject_input_title'); ?></label>
            <div class="col-sm-10">
               <?php
                  echo form_input(
                                  array(
                                        'name'          => 'subject_class_name',
                                        'id'            => 'id_subject_class_name',
                                        'value'         => ($subject_edit && $subject_edit->subject_class_name) ? $subject_edit->subject_class_name : $this->input->post('subject_class_name'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'placeholder'   => $this->lang->line('add_subject_input_title')
                                         )
                                  );

                   echo form_input(
                                  array(
                                        'name'          => 'id',
                                        'id'            => 'id_subject_class_id',
                                        'value'         => ($subject_edit && $subject_edit->subject_class_id) ? $subject_edit->subject_class_id : $this->input->post('subject_class_id'),
                                        'maxlength'     => '100',
                                        'size'          => '50',
                                        'class'         => 'form-control',
                                        'type'=>'hidden'
                                         )
                                  );
                ?>
                <?php if(form_error($this->lang->line('subject_class_name_error'))) { ?>
                <div class="text-danger"><?php echo form_error($this->lang->line('subject_class_name_error')); ?></div>
                <?php } ?>
            </div>
           </div>


          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $this->lang->line('add_subject_input_status'); ?></label>
            <div class="col-sm-10">
              <?php
              echo form_dropdown('is_status',array(
                              '1'         => $this->lang->line('dropdown_status_label_1'),
                              '0'        => $this->lang->line('dropdown_status_label_0')
                      ),($subject_edit) ? $subject_edit->is_status : $this->input->post('is_status'),'class="form-control"');
              ?>  
            </div>
          </div>

          <?php if($this->session->flashdata('message')) { ?>
           <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?> <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            </div>
          </div>
         <?php } ?>

         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <div class="pull-left">
               <?php 
                echo form_submit(array(
                'name'=> 'save',
                'type' => 'submit',
                'value'=> $this->lang->line('save_button'),
                'class'=> 'btn btn-success',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_button')
                ));
                ?>

                <?php 
                echo form_submit(array(
                'name'=> 'save_continue',
                'type' => 'submit',
                'value'=> $this->lang->line('save_and_continue_button'),
                'class'=> 'btn btn-primary',
                'data-toggle'=> 'tooltip',
                'data-original-title'=> $this->lang->line('save_and_continue_button')
                ));
                ?>

                <?php 
                // echo form_submit(array(
                // 'name'=> 'reset',
                // 'type' => 'reset',
                // 'value'=> $this->lang->line('reset_button'),
                // 'class'=> 'btn btn-danger',
                // 'data-toggle'=> 'tooltip',
                // 'data-original-title'=> $this->lang->line('reset_button')
                // ));
                ?>
        
          </div>
          </div>
          </div>
          <?php echo  form_close(); ?>
      </div>
    </div>
</div>