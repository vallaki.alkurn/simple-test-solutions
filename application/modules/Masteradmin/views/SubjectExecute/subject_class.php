  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right"><a href="<?php echo base_url().$adminModule.'MasterController'."/".'AddSubject'; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add New"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $this->lang->line('subject_list_main_title'); ?></h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>
        <?php echo $this->lang->line('subject_list_title'); ?></h3>
      </div>
      
          <?php if($this->session->flashdata('message')) { ?>
          <br>
          <div class="form-group">
            <div class="col-sm-12">
            <div class="alert alert-success" style="display: block;"><?php echo $this->session->flashdata('message'); ?><button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            </div>
          </div><br>
           <?php } ?>
      <div class="panel-body">
        <div class="well">
         <?php echo form_open('#', 'class="form-horizontal" id="subject_form_filter"'); ?>  
         <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start"><?php echo $this->lang->line('add_subject_input_title'); ?></label>
                  <?php 
                   echo form_dropdown('subject_class_name',$subject,'a','class="form-control" id="id_subject_class_name"');
                   ?>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-customer"><?php echo $this->lang->line('add_standard_input_status'); ?></label>
                <?php 
                  echo form_dropdown('is_status',array(
                              '1'         => $this->lang->line('dropdown_status_label_1'),
                              '0'        => $this->lang->line('dropdown_status_label_0')
                      ),1 ,'class="form-control" id="id_is_status"');
                ?>
              </div>
              

              <button type="button" id="button-filter" class="btn btn-primary pull-right marginleft"><i class="fa fa-search"></i> Filter</button>

              <button type="button" id="button-filter-clear" class="btn btn-success pull-right">Clear</button>
            </div>
          </div>
          <?php echo form_close(); ?>


        </div>
        <div class="table-responsive">
          <table id="example" class="table table-striped">
            <thead>
              <tr>
                <td><?php echo $this->lang->line('add_subject_input_title'); ?></td>
                <td><?php echo $this->lang->line('add_subject_input_status'); ?></td>
                <td><?php echo $this->lang->line('action'); ?></td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>MasterController/getSubjectList');
});
$(document).ready(function() {
$('#button-filter').click(function(){
    var oTable = $('#example').DataTable(); 
    oTable.search('s.subject_class_id:'+$('#id_subject_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
    $('#example_filter input').val('');
});

$('#button-filter-clear').click(function(){
    $('#subject_form_filter')[0].reset();
    var oTable = $('#example').DataTable(); 
    oTable.search('clear:clear&reset:reset').draw();
    $('#example_filter input').val('');
});
});
</script> 

