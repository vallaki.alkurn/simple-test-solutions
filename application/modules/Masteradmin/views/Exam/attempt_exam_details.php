  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right">
      </div>
      <h1>Attemp Exam Details</h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
     
 
      <div class="panel-body">
      
        <div class="col-lg-12 col-xs-12 admin-order-list"> 
          

          <div class="col-lg-6 col-xs-12 order-left pull-left">
            <div class="row">
              <div class="admin-order-left clearfix">
                <ul class="list-unstyled clearfix">
                  
                  <li>
                    <span class="admin-pay-method">Teacher Name :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->teacher_first_name.' '.$data_completed_exam->teacher_last_name; ?></span>
                  </li>

                  <li>
                    <span class="admin-pay-method">Exam Name :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->test_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method">Student Name : </span>
                    <span class="admin-student"><?php echo $data_completed_exam->first_name.' '.$data_completed_exam->last_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method">Exam Description : </span>
                   <span class="admin-student"><?php echo $data_completed_exam->test_description; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Exam Instructions :</span>
                    <span class="admin-student"><?php echo $student->test_instructions; ?></span>
                  </li>
  
                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">From Date :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->test_form_date; ?></span>
                  </li>

                   <li class="admin-last-subtotal">
                    <span class="admin-pay-method">To Date :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->test_to_date; ?></span>
                  </li>

                   <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Exam Access Code :</span>
                    <span class="admin-student"><?php echo $student->test_access_code; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Exam Time Limit :</span>
                    <span class="admin-student"><?php echo $student->test_time_limit; ?></span>
                  </li>


                                     
                </ul>
              </div>
            </div>
          </div>


          <div class="col-lg-6 col-xs-12 order-left pull-left">
            <div class="row">
              <div class="admin-order-left clearfix">
                <ul class="list-unstyled clearfix">
                  <li><span class="order-placed"><?php //echo $this->lang->line('add_registration_date_title'); ?> </span></li>
                  <li class="order-date-time">
                    <span class="date"><b><?php //echo $student->registered_date; ?></b></span>
                  </li>
                  <li>
                    <span class="admin-pay-method">Total Question :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->total_question; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method">Max Mark : </span>
                    <span class="admin-student"><?php echo $data_completed_exam->max_mark; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method">Total Mark : </span>
                   <span class="admin-student"><?php echo $data_completed_exam->total_mark; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Total Wrong Que :</span>
                    <span class="admin-student"><?php echo $student->total_wrong_que; ?></span>
                  </li>
  
                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Total Right Que  :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->total_right_que; ?></span>
                  </li>

                   <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Total Right Mark:</span>
                    <span class="admin-student"><?php echo $data_completed_exam->total_right_mark; ?></span>
                  </li>

                   <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Total Wrong Mark :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->total_wrong_mark; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Time Taken :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->time_taken; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Attempt Question :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->attempt_question; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Non Attempt Question :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->not_attempt_question; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-pay-method">Test Status :</span>
                    <span class="admin-student"><?php echo $data_completed_exam->test_status; ?></span>
                  </li>
                                     
                </ul>
              </div>
            </div>
          </div>

             
              
              
          </div><!-- order-right -->
        </div>
      </div>
    </div>
  </div>

<style type="text/css">

.disabledbutton
{
  background: #CCC;
  border: 1px solid #CCC;
}
.disabledbutton:hover
{
  background: #CCC;
  border: 1px solid #CCC;
  cursor: context-menu;
}
.admin-pay-method
{
  font-weight: bold;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>StudentsController/getAllStudentsAjax');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

