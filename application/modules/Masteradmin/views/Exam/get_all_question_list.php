  <div class="page-header">
    <div class="container-fluid">
      <h1>Question List</h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>
         Question List</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="example" class="table table-striped">
            <thead>
              <tr>
                <td>Sr No</td>
                <td>Question Name</td>
                <td>Question Type</td>
                <td>Test Name</td>
                <td>Question Points</td>
                <td>Teacher Name</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>ReportController/getAllQuestion');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

