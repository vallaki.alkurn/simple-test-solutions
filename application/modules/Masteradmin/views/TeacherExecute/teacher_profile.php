  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right"><a href="<?php echo base_url().$adminModule.'MasterController'."/".'AddStandard'; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add New"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $this->lang->line('teacher_profile_main_title'); ?></h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
     
 
      <div class="panel-body">
      
        <div class="col-lg-12 col-xs-12 admin-order-list"> 
          <div class="col-lg-4 col-xs-12 order-left pull-left">
            <div class="row">
              <div class="admin-order-left clearfix">
                <ul class="list-unstyled clearfix">
                  <li><span class="order-placed"><?php echo $this->lang->line('add_registration_date_title'); ?> :</span></li>
                  <li class="order-date-time">
                    <span class="date"><b><?php echo date('m/d/Y',strtotime($teacher->registered_date)); ?></b></span>
                  </li>
                  <li>
                    <span class="admin-order-id"><?php echo $this->lang->line('add_standard_name_title'); ?> :</span>
                    <span class="admin-student"><?php echo $teacher->first_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method"><?php echo $this->lang->line('add_standard_last_name_title'); ?> : </span>
                    <span class="admin-student"><?php echo $teacher->last_name; ?></span>
                  </li>
                  <li>
                    <span class="admin-pay-method"><?php echo $this->lang->line('add_standard_email_title'); ?> : </span>
                   <span class="admin-student"><?php echo $teacher->email_address; ?></span>
                  </li>
  
                 

                  <li class="admin-last-subtotal">
                    <span class="admin-subtotal"><?php echo $this->lang->line('add_organization_teacher_title'); ?> :</span>
                    <span class="admin-student"><?php echo $teacher->organization; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-subtotal"><?php echo $this->lang->line('add_organization_phone_teacher_title'); ?> :</span>
                    <span class="admin-student"><?php echo $teacher->organization_phone_number; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-subtotal"><?php echo $this->lang->line('add_your_title_teacher_title'); ?> :</span>
                    <span class="admin-student"><?php echo $teacher->your_title; ?></span>
                  </li>

                  <li class="admin-last-subtotal">
                    <span class="admin-subtotal"><?php echo $this->lang->line('add_your_phone_teacher_title'); ?> :</span>
                    <span class="admin-student"><?php echo $teacher->your_phone; ?></span>
                  </li>


                                     
                </ul>
              </div>
            </div>
          </div><!-- order-left -->
      
          <div class="col-lg-8 col-xs-12 order-right pull-right">  
               
              <div class="admin-order-right clearfix">
                <div class="col-lg-12 list-unstyled">
                  
                
                <div class="col-lg-3 marginbottom">
                   <?php echo anchor('Masteradmin/TeachersController/AddTeacher/'.$user_id.'', 'Update Profile', 'class="btn btn-success profilebutton"') ?> 
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  


                   <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  


                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  

                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  


                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  


                 <div class="col-lg-3 marginbottom">
                    <?php 
                        echo form_submit(array(
                        'name'=> 'save_continue',
                        'type' => 'submit',
                        'value'=> 'New Button',
                        'class'=> 'btn btn-primary profilebutton',
                        'data-toggle'=> 'tooltip',
                        'data-original-title'=> 'New Button'
                        ));
                    ?>
                </div>  




                </div>   

           </div> 
                  
              
              
          </div><!-- order-right -->
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>StudentsController/getAllStudentsAjax');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

