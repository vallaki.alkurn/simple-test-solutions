  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right">

       <a href="<?php echo base_url().$adminModule.'TeachersController'."/".'AddTeacher'; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add New"><i class="fa fa-plus"></i></a>
      </div>
      <h1><?php echo $this->lang->line('teacher_list_dis_approved_student_main_title'); ?></h1>
    </div>
  </div>
  <div class="container-fluid">
            <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i>
        <?php echo $this->lang->line('teacher_list_main_title'); ?></h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="example" class="table table-striped">
            <thead>
              <tr>
                <td><?php echo $this->lang->line('add_standard_sr_no'); ?></td>
                <td><?php echo $this->lang->line('add_standard_name_title'); ?></td>
                <td><?php echo $this->lang->line('add_standard_last_name_title'); ?></td>
                <td><?php echo $this->lang->line('add_standard_email_title'); ?></td>
                <td><?php echo $this->lang->line('add_subject_teacher_title'); ?></td>
                 <td><?php echo $this->lang->line('add_standard_input_status'); ?></td>
                 <td><?php echo $this->lang->line('action'); ?></td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
$(document).ready(function() {
getdatatableRecord('#example','<?php echo base_url().$adminModule; ?>TeachersController/getAllDisApprovedTeachersAjax');
});
// $(document).ready(function() {
// $('#button-filter').click(function(){
//     var oTable = $('#example').DataTable(); 
//     oTable.search('s.standard_class_id:'+$('#id_standard_class_name').val()+'&'+'s.is_status:'+$('#id_is_status').val()).draw();
// });
// });
</script> 

