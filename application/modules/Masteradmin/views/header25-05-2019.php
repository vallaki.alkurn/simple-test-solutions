<!DOCTYPE html>
<html dir="ltr" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<title><?php echo getsystemConfig('company_name'); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="#" rel="icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Admin/js/jquery-2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Admin/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Admin/js/highcharts.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Admin/js/exporting.js"></script>
<link href="<?php echo base_url(); ?>assets/Admin/css/opencart.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/Admin/css/font-awesome.css" type="text/css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/Admin/css/summernote.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/Adminassets/js/summernote.js"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/moment.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>assets/Admin/css/bootstrap-datetimepicker.css"  rel="stylesheet">

<link type="text/css" href="<?php echo base_url(); ?>assets/Admin/css/stylesheet.css" rel="stylesheet" media="screen">
<script src="<?php echo base_url(); ?>assets/Admin/js/common.js" type="text/javascript"></script>
<!--datatable css and js -->
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/buttons.flash.min.js " type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/jszip.min.js " type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/pdfmake.min.js " type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/vfs_fonts.js " type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/buttons.html5.min.js " type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/Admin/js/datatable/buttons.print.min.js " type="text/javascript"></script>

<link href="<?php echo base_url(); ?>assets/Admin/css/datatable/jquery.dataTables.min.css"  rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/Admin/css/datatable/buttons.dataTables.min.css"  rel="stylesheet">

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2oRAljHGZArBeQc5OXY0MI5BBoQproWY&libraries=places"></script>
 
<!---->

<script src="<?php echo base_url(); ?>assets/Admin/js/my-script.js" type="text/javascript"></script>

<script>
var Admin_module='Masteradmin';
var baseurl='<?php echo base_url(); ?>';
</script>



</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
  <div class="navbar-header">
   <div class="logopanel">
    <h1 class="logotitle"><a href="#" class=""><?php echo getsystemConfig('company_name'); ?></a></h1>
   </div> 
     <a type="button" id="button-menu" class="pull-left"></a>
    
   </div>

  <div class="headerright">
  <ul class="nav pull-right navadjust">
    <li class="dropdown liadjust">

<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="label label-danger pull-left">15</span> <i class="fa fa-bell fa-lg"></i></a>


      <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
        <li class="dropdown-header">Exam</li>
        <li><a href="#" style="display: block; overflow: auto;"><span class="label label-warning pull-right">1</span>Processing</a></li>
        <li><a href="#"><span class="label label-success pull-right">4</span>Completed</a></li>
        
      </ul>
    </li>    
    <li><a href="<?php echo base_url('Masteradmin/auth/logout') ?>"><span class="hidden-xs hidden-sm hidden-md">Logout</span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
</div> 
  
</header>
<nav id="column-left" class="active"><div id="profile">
  <div><a class="dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-user" style="font-size:40px;float:left;"></i> <img src="Products%20Viewed%20Report_files/store-logo-2-100x100-45x45.jpg" alt="John Doe" title="admin" class="img-circle"></a></div>
  <div>
    <h4>Admin</h4>
    <small>Administrator</small></div>
</div>
<ul id="menu">
<li id="dashboard"><a href="<?php echo base_url('Masteradmin/Dashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a></li>

<?php foreach($section as $section_list) { ?>
<li><a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php  echo $section_list->sectionName; ?></span></a>
<ul class="collapse"> 
<?php  if(count($module) >0 ) {   ?>
<?php
foreach($module as $Assigned_module){
// print_r($Assigned_module);
$moduleData= $this->getModuleDetails($Assigned_module,$section_list->_id);
if($moduleData){  ?>

  <li><a href="<?php echo base_url().$adminModule.$moduleData->Controller_name."/".$moduleData->method_name; ?>"><?php echo $moduleData->module_name; ?></a></li>                 

<?php } } } ?>
</li></ul>
<?php } ?>
</nav>
<div id="content">
