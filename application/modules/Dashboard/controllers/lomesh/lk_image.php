<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lk_image extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper("file");
	}
	
	public function uploadImages($fileData)
	{
		/*---- Image Data --------------*/
		$fileD = $fileData['file'];
		$fileName = $fileData['fileName'];
		$Folder = $fileData['Folder'];
		$thumFolder = $fileData['thumFolder'];
		$width = $fileData['width'];
		$height = $fileData['height'];
		/*---- Image Count --------------*/
		$ci=get_instance();
		$number_of_files = sizeof($fileD[$fileName]['tmp_name']);
		$files = $fileD[$fileName];
		$errors = array();
		/*---- Image Error --------------*/
		for($i=0;$i<$number_of_files;$i++)
		{
		  if($fileD[$fileName]['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$fileD[$fileName]['name'][$i];
		}
		/*---- Image upload --------------*/
		$img_name = array();
		if(sizeof($errors)==0)
		{
		  $ci->load->library('upload');
		 
		  //$config['upload_path'] = './media/'.$Folder.'/';
		  $config['upload_path'] = $Folder;
		  $config['allowed_types'] = '*';
		  $source_path ='';
		  $target_path ='';
			  for ($i = 0; $i < $number_of_files; $i++) {
				  
				$_FILES[$fileName]['name'] = str_replace(" ", "_", time().'_'.$files['name'][$i]);
				$_FILES[$fileName]['type'] = $files['type'][$i];
				$_FILES[$fileName]['tmp_name'] = $files['tmp_name'][$i];
				$_FILES[$fileName]['error'] = $files['error'][$i];
				$_FILES[$fileName]['size'] = $files['size'][$i];
				
				$ci->upload->initialize($config);
				/*---- Image upload --------------*/
					if ($ci->upload->do_upload($fileName))
					{
						//echo 'done';
						/************************** Insert Record *************************/
						$img_name['names'][] = str_replace(" ", "_", time().'_'.$files['name'][$i]);
						/************************ Thumb **************************/
						  $data['uploads'][$i] = $ci->upload->data();
						 /* $source_path = './media/'.$Folder.'/'.$_FILES[$fileName]['name'];
						  $target_path =  './media/'.$thumFolder;*/
						  $source_path = $Folder.'/'.$_FILES[$fileName]['name'];
						  $target_path = $thumFolder;
						  $res7 = $this->do_resize($source_path,$target_path,$width,$height);
						/**************************************************/
						  
					}
					else
					{
						/*---- Image Error --------------*/
					  $data['upload_errors'][$i] = $ci->upload->display_errors();
					  //print_r($data['upload_errors'][$i]);
					}
			  }
			 
		}
		/*---- Image Return name --------------*/
		//print_r($img_name['names']);
		return $img_name;		
		exit;
	}
	/******************************************************************************/
	public function do_resize($source_path,$target_path,$width,$height)
	{
		/*echo $source_path;
		exit;*/
		$c = get_instance();
		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => FALSE,
			'quality' => 60,
			//'create_thumb' => TRUE,
			//'thumb_marker' => '_thumb',
			'width' => $width,
			'height' => $height
		);
		$c->load->library('image_lib');
		$c->image_lib->initialize($config_manip);
		if (!$c->image_lib->resize()) {
			echo $c->image_lib->display_errors();
		}
		// clear //
		return $c->image_lib->clear();
	}
	/* -------------------------- Single image upload -------------------------*/
	public function singleUploadImages($fileData)
	{
		//print_r($fileData);
		/*---- Image Data --------------*/
		$fileD = $fileData['file'];
		$fileName = $fileData['fileName'];
		$Folder = $fileData['Folder'];
		$thumFolder = $fileData['thumFolder'];
		$width = $fileData['width'];
		$height = $fileData['height'];
		
	/*	print_r($fileD[$fileName]['name']);
		exit;*/
		/*---- Image Count --------------*/
		$ci=get_instance();
		$ci->load->library('upload');
		$config['upload_path'] = $Folder;
		$config['allowed_types'] = '*';
		$source_path ='';
		$target_path ='';
		
		/*print_r($config);
		exit;*/
		
        $ci->load->library('upload');
		$ci->upload->initialize($config);
		
        if ($ci->upload->do_upload($fileName))
		{
			//echo 'done';
			/************************** Insert Record *************************/
			//$img_name = str_replace(" ", "_", time().'_'.$_FILES[$fileName]['name']);
			//$img_name = $_FILES[$fileName]['name'];
			//exit;
			/************************ Thumb **************************/
			//print_r($ci->upload->data());
			  $data['uploads'] = $ci->upload->data();
			  $img_name = $data['uploads']['file_name'];
			 /* $source_path = './media/'.$Folder.'/'.$_FILES[$fileName]['name'];
			  $target_path =  './media/'.$thumFolder;*/
			  $source_path = $Folder.'/'.$img_name;//str_replace(" ", "_",$_FILES[$fileName]['name']);
			  $target_path = $thumFolder;
			  $res7 = $this->do_resize($source_path,$target_path,$width,$height);
			/**************************************************/
			  
		}
		else
		{
			/*---- Image Error --------------*/
		  $data['upload_errors'] = $ci->upload->display_errors();
		  //print_r($data['upload_errors'][$i]);
		}
		/*---- Image Return name --------------*/
		//print_r($img_name['names']);
		/*echo $img_name;
		exit;*/
		return $img_name;		
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */