<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Dashboard extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
        parent::__construct();
		check_user_session() ? '' : redirect('login');
        $this->load->model('Dashboard_model');
		$this->get_user = get_user();
	}
	
	public function index()
	{
		
		if($this->get_user->user_type == 1){ 
			redirect('Dashboard/Teacher'); 
		}else{
			redirect('Dashboard/Student'); 
		}
		$data = '';
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Users Login',
							  'keyword' => 'Users Login',
							  'description' => 'Users Login');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('login',$header_array);
	}

	public function logout()
	{
		UpdateRow('tbl_user_login_status', array('logOut' => date('Y-m-d H:i:s')), 
			array('session_id'=>$this->session->userdata('last_session_id')));
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
	
	public function reset_password()
	{
		//print_r($_POST);
		
		if(empty($_POST['current_password']) || empty($_POST['new_password']) || empty($_POST['confirm_password']))
		{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('user_change_pass_error')),'url' =>'');
		}else{
			
			$user_details = SelectData('tbl_user_registration','user_id',
			array('user_id'=>$this->get_user->user_id,'user_password' => get_md5($_POST['current_password'])), 1, NULL);
			
			if($user_details){
				
				$res = UpdateRow('tbl_user_registration', array('user_password' => get_md5($_POST['new_password'])), 
						array('user_id'=>$this->get_user->user_id));
						
				$this->session->set_flashdata('msg', $this->lang->line('user_change_pass_success'));
				
				$dashboard_url = $this->get_user->user_type == 1 ? 'Dashboard/Teacher' : 'Dashboard/Student';
				
				$data = array('status' => true,
							 'response' => array('msg' =>''),'url' => base_url($dashboard_url));
			}else{
				$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('user_current_pass_error')),'url' =>'');
			}				
		}
		echo json_encode($data);
		exit;
	}	
	
	
	public function update_notification()
	{
		//print_r($_POST);
		if($_POST){
			$res = tbl_notification_update($this->get_user->user_id);
			if($res){
				$data = array('status' => true,
						  'response' => '','url' =>'');
				echo json_encode($data);
				exit;
			}	
		}
	}
	 
	 
  
}