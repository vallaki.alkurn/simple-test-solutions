<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  require_once(APPPATH.'views/template/sidebar-student.php'); ?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
							<div class="msg-gloabal"></div>
                                <div class="form-heading">Profile</div>
                                <form url="<?php echo base_url('Dashboard/Student/update_profie');?>" method="post" id="update_profile" name="update_profile" enctype="multipart/form-data" >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="profile-pic-div">
                                                <div class="pro-pic">
												<?php 
												$profile_img = $user_details->profile_photo  ?   base_url($user_details->profile_photo) : base_url('assets/front-design/images/icon-teacher.jpg');
												?>
												
                                                    <img width="125" accept="" src="<?php echo $profile_img; ?>" id="profileImg">
                                                </div>
                                                <div class="pic-button">
                                                    <div class="upload-file">
                                                        <input id="file-upload" onchange="readURL(this);" name='profile_photo' type="file" style="display:block;"> 
                                                        <span class="upload-btn">
														<i class="fa fa-camera-retro" aria-hidden="true"></i> 
														Change Profile Photo
														</span>
                                                    </div>
													<input type="hidden" name="profile_photo_edit" value="<?php echo $user_details->profile_photo;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $user_details->first_name;?>" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>Last Name</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $user_details->last_name;?>" placeholder="Last Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>Email Address</label>
                                                <input type="email" class="form-control" name="email_address" id="email_address" value="<?php echo $user_details->email_address;?>" placeholder="Email Address*" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Standard/Class</label>
											  <select name="standard_class_id" id="standard_class_id" class="select-color">
												<option value="">Standard/Class*</option>
												<?php foreach(get_standard_class() as $key => $val){
													?>
														 <option value="<?php echo $val->standard_class_id; ?>"  <?php if( $user_details->standard_class_id ==  $val->standard_class_id) echo 'selected'; ?>>
															<?php echo $val->standard_class_name; ?>
														 </option>
													<?php
												}?>
											  </select>
                                            </div>
                                        </div>
										
										<div class="col-md-6">
                                            <div class="form-group">
                                                    <label>School/College</label>
                                                <input type="text" class="form-control" name="school_collage" id="school_collage" value="<?php echo $user_details->school_collage;?>" placeholder="School/College*">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6"></div>
										<div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
</div>
	<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profileImg')
                    .attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
		
	$(function(){
		
		/*$('.upload-file').on('click',function(){
			
			alert();	
		});*/
		
	//Update Profile
	 $('#update_profile').validate({
		rules: {
				first_name: {
				   required : true,
		    	   minlength   : 3,
				},
				last_name: {
					required: true,
					minlength: 3,
				},
				email_address: {
					required: true,
					email: true
				},
				standard_class_id: {
					required: true,
				}
			},
		messages: {
				 first_name:{
					required :"First Name field is required",
				 }, 
				 last_name:{
					required :"Last Name field is required",
				 },
				 email_address:{
					required :"Email Address field is required",
				 },
				 subject_class_id:{
					required :"Standard/Class field is required",
				 }
			},
		submitHandler: function(form) {
				lkForms('update_profile');
				$(window).scrollTop(0);
			
		  }
	 });
	 });
	</script>