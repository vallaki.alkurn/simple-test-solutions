  <section class="middle_section inner-page update-profile loginpage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">  
                    <div class="form-heading">invite requests</div>
                    <div class="row">

                        <?php 
						if($results){
						foreach($results as $exam) { ?>
                        <div class="col-md-6">
                            <div class="request-outer">
                                <div class="request-left">
                                    <div class="request-content-left">
                                        <div class="req-row">
                                            <div class="req-heading">Subject</div>
                                            <div class="req-cont"><?php echo ($exam->subject_class_name) ? $exam->subject_class_name : ''; ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">Test Title</div>
                                            <div class="req-cont"><?php echo ($exam->test_name) ? $exam->test_name : ''; ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">randomize questions</div>
                                            <div class="req-cont"><?php echo ($exam->test_question_random == 1) ? 'Yes' : 'No'; ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">date</div>
                                            <div class="req-cont"><?php echo DateFormat($exam->test_form_date); ?> - <?php echo DateFormat($exam->test_to_date); ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">Test timing</div>
                                            <div class="req-cont"><?php echo ($exam->test_time_limit) ? $exam->test_time_limit : ''; ?> minutes</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="request-right">
                                    <div class="req-btn-section">
                                        <?php 
                                        //check status for not accept and not decline
                                        if($exam->request_status == 0) { ?>
                                        <div class="rqe-dkl"><a href="javascript:void();" onclick="statusExam(<?php echo $exam->request_id; ?>,2);">decline</a></div>
                                        <div class="req-acpt"><a href="javascript:void();" onclick="statusExam(<?php echo $exam->request_id; ?>,1);">accept</a></div>
                                        <?php } else if($exam->request_status == 1) { ?>
                                         <div class="req-acpt"><a href="<?php echo base_url('Dashboard/Student/start_exam/'.$exam->request_id.''); ?>">Details</a></div>    
                                        <?php } ?>
                                        <div class="req-row">
                                            <div class="req-heading">invited by</div>
                                            <div class="req-cont"><?php echo $exam->first_name .' '. $exam->last_name; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } 
						}else{
							echo '<div class="col-sm-12">'.$this->lang->line('invited_test_info').'</div>';
						}
					?>
                       


                        <div class="col-sm-12">
                            <div class="pagination">
                                <?php echo (isset($links)) ? $links : ''; ?>
                                <!-- <ul>
                                    <li><a href="#">Previous</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#">next</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<script>


function statusExam(request_id,request_status)
{
    var msg = (request_status == 1) ? 'accept' : 'decline';
    var data = 'request_id='+request_id+'&request_status='+request_status;
    if(confirm('Are you sure you want to '+msg+'?')){
     $.ajax({
       method: 'POST',
       url: '<?php echo base_url('Dashboard/Student/updateExamStatus') ?>',
       data: data,
       dataType:'json',
       processData: false,
       success: function(data){
            if(data.code == 200) {
                alert(data.message);
                location.reload();
            } else {
               alert(data.message);
            }
           // $('#rowid_'+uid).hide();
           // $('#example').DataTable().ajax.reload();
           //  $('#lk-success').show(200).html(data.msg);
           //  closeMsg();
              
         }
     })
    }else{
     return false;
    }
}

</script>



<!-- Middle section End