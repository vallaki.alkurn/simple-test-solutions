
<section class="middle_section inner-page">
        <div class="mws-panel-header" style="text-align:center;">
                    	<h4 style="color:#ccc;verticle-align:middle;"> Test Summery</h4><!--<i class="icon-table"></i>-->
                    </div>

                    <div class="container" >
                    <div>
                    <h3 class="testsummery-font">Your Test Submitted Successfully</h3>
                    <h3 class="testsummery-font" style="color: #686868;">Thank you for submitting your test.The summery of the test given below.</h3>

                    <h4 class="testnameset" style="color: #686868;"><span class="test-summery-testname">Test Name:</span> <?php echo $results->test_name; ?></h4>
                     <!--<h4  style="color: #686868;"><span style="font-size: 15px; font-weight: bold; color: #3076A0;">Subject Name:</span> SSC   Maths  </h4>  -->
                    </div>
                    <div class="mws-panel-body no-padding" style="overflow-x: auto">
                        <table class="table" ">
                            <thead>
                                <tr>
                                    <th style="width: 100px;">Total Questions</th>
                                    <th style="width: 100px;">Maximum Marks</th>
                                    <th style="width: 100px;">Total Attempted</th>
                                    <th style="width: 100px;"> Left Questions</th>
                                      <th style="width: 100px;">Correct Ques.</th>
                                        <th style="width: 100px;">InCorrect Ques.</th>
                                        <th style="width: 100px;">Total Time(in min.)</th>
                                          <th style="width: 100px;">Total Taken(in min.)</th>
                                            <th style="width: 100px;">Right Marks</th>
                                              <th style="width: 100px;"> Negative Marks</th>
                                              <th style="width: 100px;"> Total Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                     $total_per =  ($results->total_mark * 100)/ $results->max_mark;
                                  
                     ?>
                                <tr>
                                    <td><?php echo $results->total_question;?><?php //echo "select ans_time  from tbl_button_pallate where test_id=".$testCode." and test_type='".$testType."' and user_id='".$this->id."'"?></td>
                                    <td><?php echo $results->max_mark; ?></td>
                                    <td><?php echo $results->attempt_question;  ?></td>
                                    <td><?php echo $results->not_attempt_question; ?></td>
                                    <td><?php echo $results->total_right_que; ?></td>
                                    <td><?php echo $results->total_wrong_que; ?></td>
                                    <td><?php echo $results->duration; ?></td>
									 <td><?php echo $time_taken ?></td>
									 <td><?php echo $results->total_right_mark; ?></td>
                                    <td><?php echo $results->total_wrong_mark; ?></td>
                                    <td><?php echo $results->total_mark; ?></td>
                                </tr>


                            </tbody>
                        </table>
                    </div>


                   <div style="padding-top: 2%;" >
                   <h3 class="testsummery-score"><span style="color: #1C1C1C; font-weight:  bold;">  <i class="icol-emoticon-smile" style=" padding-top: 0%;  "></i> &nbsp;&nbsp;Your Score is <?php echo round($total_per,2); ?>% </span></h3>


                 </div>

                  </div>
	</section>
<script>
    
</script>