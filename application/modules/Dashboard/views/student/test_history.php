<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co" >
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-student.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Completed Test</div>
						</div>
						<!--<div class="col-lg-12">
								<div class="right-drop-down float-lg-right float-md-right">
									<div class="create-test"> <a href="<?php echo base_url('Dashboard/Question/create_question') ?>">Create Question</a> </div>
										<div class="drop-text"> Sort by Question Type </div>
										<div class="form-group">
										<select>
												<?php /*foreach(question_type() as $qtype) { ?>
												<option value="<?php echo $qtype->question_type_id; ?>"><?php echo $qtype->question_type_name; ?></option>
												<?php }*/ ?>
										</select>
										</div>
								</div>
						</div>-->
						<div class="col-lg-12" >
						<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							<div class="bank-list-outer">
								<div class="list-header">
									<div class="list-row">
										  <div class="list-column text-center sn">Sr No</div>
                                <div class="list-column ">Test Title</div>
                                <div class="list-column">Date</div>
                                <div class="list-column">Timing</div>
                                <div class="list-column">Total Question</div>
                                <div class="list-column">Total Marks</div>
                                <div class="list-column">Marks Obtained</div>
                                <div class="list-column text-center">Status</div>                                
                                <div class="list-column text-center">Action</div>
									</div>
								</div>
								<div class="list-boby">
								 <?php 
								 if($results){
								 $i = $srno == 0 ? 1 : $srno+1;
								 foreach ($results as $data) { ?>
									<div class="list-row">
											<div class="list-column text-center"><?php echo $i;//$data->test_id; ?></div>
											<div class="list-column text-center"><?php echo $data->test_name; ?></div>
											<div class="list-column text-center"><?php echo DateFormat($data->test_form_date).' to '.DateFormat($data->test_to_date); ?></div>
										
											<div class="list-column text-center"><?php echo $data->test_time_limit; ?> Minutes </div>
											<div class="list-column text-center">
											<?php echo totalQues('tbl_que_creation',array('test_id'=>$data->test_id)); ?>
											</div>
											<div class="list-column text-center"><?php echo $data->max_mark; ?></div>
											<div class="list-column text-center"><?php echo $data->total_mark; ?></div>
											<div class="list-column text-center"><?php echo 'Complete'; ?></div>
										<div class="list-column text-center action">
											
										<!--	<a href="<?php echo base_url('Dashboard/Student/test_history_details/'.$data->test_uniqe_code);?>"><i class="fa fa-eye" aria-hidden="true"></i></a>-->
										<a href="#" title="Download certificate"><i class="fa fa-download" aria-hidden="true"></i></a>
										</div>
									</div>
									 <?php $i++; } }else{ echo 'Test not found.';} ?>
								</div>
							</div>
                </div>
						<div class="col-lg-12" >
						<?php if (isset($links)) { ?>
                <?php echo $links ?>
            <?php } ?>
						<!--<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
							<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1">Previous</a> </li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"> <a class="page-link" href="#">Next</a> </li>
							</ul>
						</nav>-->
						<?php i/*f(isset($links)) { ?>
                <?php echo $links ?>
            <? }*/ ?>
			
	<!--<table id="example" class="display table table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Sr.No</th>
              	<th>Test Title</th>
				<th>Date</th>
              	<th>Randomize Questions</th>
				<th>Timing</th>
              	<th>Total Question</th>
				<th>Sent Invitations</th>
              	<th>Action</th>
            </tr>
        </thead>
    </table>-->
						</div>
				</div>
		</div>
</div>
<script>
/*$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "desc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php //echo base_url('Dashboard/Teacher/getUpcomingTestJson');?>",
		 "columns": [
            { "data": "test_id" },
			{ "data": "test_name" },
			{ "data": "date" },
			{ "data": "test_question_random" },
			{ "data": "test_time_limit" },
			{ "data": "total_question" },
			{ "data": "sent_invitation" },
			{ "data": "action" }
        ]
    });
	
});*/
</script>