<?php //print_r($user_details); ?>
<!-- The Modal -->
    <div class="modal" id="questionModal">
        <div class="modal-dialog">
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="qType"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="queview">
				</div>
            </div>
            </div>
        </div>
    </div>
	
	<div class="modal" id="testModal">
        <div class="modal-dialog">
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="qType">Add question on test</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="testList">
					<div id="err_msg"></div>
					<form  url="" method="post" id="set_question_for_test" name="set_question_for_test">
						<div class="" id="testList_c"></div>
						<button type="button" id="setTests"  class="btn btn-primary">Submit</button>
					</form>
				</div>
            </div>
            
            </div>
        </div>
    </div>
	
	<div class="modal" id="testModalDelete">
        <div class="modal-dialog">
            <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="deleteQuesFrom">Delete question</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="testListD">
					<div id="err_msg_"></div>
					<form  url="" method="post" id="delete_question_for_test" name="delete_question_for_test">
						<input type="hidden" name="que_id" id="que_id_to_delete" value=>
						<div class="" id="testList_delete"></div>
						<button type="button" id="delTests"  class="btn btn-primary" style="float:right;">Delete</button>
					</form>
				</div>
            </div>
            
            </div>
        </div>
    </div>
	
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">QUESTION BANK</div>
						</div>
						<div class="col-lg-12">
							<div class="msg-gloabal">
							<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="create-test">
								<a href="<?php echo base_url('Dashboard/Question/create-question/0'); ?>">Create Question</a>
							</div>
						</div>
						<div class="col-lg-12">
						<table id="example" class="display table table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>Sr no</th>
								<th>Question Type</th>
								<th>Questions</th>
								<!--<th>Test</th>-->
								<th>Set Point</th>
								<th>Add in Test</th>
								<th>Action</th>
							</tr>
							</thead>
						</table>
						</div>
				</div>
		</div>
</div>
<?php
// echo "<pre>";
// print_r($_SERVER);
// echo "</pre>";
$_SESSION['REDIRECT_URL'] = $_SERVER['REDIRECT_QUERY_STRING'];
?>
<script>
$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getQuestionBankJson');?>",
		 "columns": [
            { "data": "id" },
			{ "data": "question_type_name" },
			{ "data": "que_name" },
		/*	{ "data": "test_name" },*/
			{ "data": "que_points" },
			{ "data": "add_test" },
			/*{ "data": "total_question" },
			{ "data": "sent_invitation" },*/
			{ "data": "action" }
        ]
    });
	
});
</script>


<script>
function getQuestionDetails(qID)
{
	$('#queview').html('');
	$('#qType').html('');
	var data = 'que_id='+qID;
	$.ajax({
	  method: 'POST',
	  url: '<?php echo base_url('Dashboard/Teacher/question_details');?>',
	  data: data,
	  dataType:'json',
	  //processData: false,
	  success: function(data){
		  var rdata = data.response;
		  if(data.status == true){
			  $('#qType').html(rdata.q_type);
			  $('#queview').html(rdata.q_option);
		  }
		
	  }
	});
	
}

function getTestList(qID)
{
	//$('#testList').html('');
	$('#testList_c').html('');
	$('#testList').hide();
	var data = 'que_id='+qID;
	$.ajax({
	  method: 'POST',
	  url: '<?php echo base_url('Dashboard/Teacher/test_details_list');?>',
	  data: data,
	  dataType:'json',
	  //processData: false,
	  success: function(data){
		  $('#testList').show();
		  var rdata = data.response;
		  if(data.status == true){
			  $('#testList_c').html(rdata.form_detail);
		  }else{
			  $('#testList').html(rdata.form_detail);
		  }
	  }
	});
	
 }
 
 function getTestListDelete(qID)
{
	//$('#testList').html('');
	 $('#delTests').hide();
	$('#testList_c').html('');
	$('#testList').hide();
	var data = 'que_id='+qID;
	$.ajax({
	  method: 'POST',
	  url: '<?php echo base_url('Dashboard/Teacher/test_details_list_delete');?>',
	  data: data,
	  dataType:'json',
	  //processData: false,
	  success: function(data){
	  	$("#que_id_to_delete").val(qID);
		  $('#testListD').show();
		  var rdata = data.response;
		  if(data.status == true){
		  	$("#deleteQuesFrom").html('Delete Question from tests');
			$('#testList_delete').html(rdata.form_detail);
			$('#delTests').show();
		  }else{
		  	$("#deleteQuesFrom").html('Delete Question from Question bank');
			$('#delTests').show();
		  }
	  }
	});
	
 }
 
 
 //Invite Send to user
   $('#setTests').on('click',function(){
	   //alert();
		//var url = $('#student_sent_invite').attr('url');
		if($('[type="checkbox"]').is(":checked")){
			var data = new FormData($('#set_question_for_test')[0]);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('Dashboard/Teacher/set_question_for_test');?>',
				data:data,
				dataType:'json',
				async:false,
				processData: false,
				contentType: false,
				beforeSend: function(data){
					loaderIn();
				},
				success: function (data) {
					//console.log(data);
					if(data.status == false){
						window.location.assign(data.url);
						//$('.msg-gloabal').html(data.response.msg);
						//loaderOut({status:0,msg:data.response.msg,url:data.url});
					}else{
						//$('.msg-gloabal').html(data.response.msg);
						$('#set_question_for_test')[0].reset();
						if(data.url){
							//$(":submit").attr("disabled", true);
							window.location.assign(data.url);
						}
					}
				}
			});	
			return false;
        }else{
			$('#err_msg').html('<?php echo $this->lang->line('set_test_q_error'); ?>');
			return false;
		}
	});
	
	
	 //Delete Test
   $('#delTests').on('click',function(){
	   //alert();
		//var url = $('#student_sent_invite').attr('url');
		if ( $('[type="checkbox"]').length > 0 ) {
			if($('[type="checkbox"]').is(":checked")){
				var data = new FormData($('#delete_question_for_test')[0]);
				$.ajax({
					type:'POST',
					url:'<?php echo base_url('Dashboard/Teacher/delete_question_on_test');?>',
					data:data,
					dataType:'json',
					async:false,
					processData: false,
					contentType: false,
					beforeSend: function(data){
						loaderIn();
					},
					success: function (data) {
						//console.log(data);
						if(data.status == false){
							window.location.assign(data.url);
							//$('.msg-gloabal').html(data.response.msg);
							//loaderOut({status:0,msg:data.response.msg,url:data.url});
						}else{
							//$('.msg-gloabal').html(data.response.msg);
							$('#delete_question_for_test')[0].reset();
							if(data.url){
								//$(":submit").attr("disabled", true);
								window.location.assign(data.url);
							}
						}
					}
				});	
				return false;
	        }else{
				$('#err_msg_').html('<?php echo $this->lang->line('set_test_q_error'); ?>');
				return false;
			}
		}
		else
		{
			var data = new FormData($('#delete_question_for_test')[0]);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('Dashboard/Teacher/delete_question');?>',
				data:data,
				dataType:'json',
				async:false,
				processData: false,
				contentType: false,
				beforeSend: function(data){
					loaderIn();
				},
				success: function (data) {
					//console.log(data);
					if(data.status == false){
						window.location.assign(data.url);
						//$('.msg-gloabal').html(data.response.msg);
						//loaderOut({status:0,msg:data.response.msg,url:data.url});
					}else{
						//$('.msg-gloabal').html(data.response.msg);
						$('#delete_question_for_test')[0].reset();
						if(data.url){
							//$(":submit").attr("disabled", true);
							window.location.assign(data.url);
						}
					}
				}
			});	
			return false;
		}
		
	});
</script>