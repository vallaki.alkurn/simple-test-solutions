<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
							<div class="msg-gloabal"></div>
                                <div class="form-heading">Profile</div>
                                <form url="<?php echo base_url('Dashboard/Teacher/update_profie');?>" method="post" id="update_profile" name="update_profile" enctype="multipart/form-data" >
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="profile-pic-div">
                                                <div class="pro-pic">
												<?php 
												$profile_img = $user_details->profile_photo  ?   base_url($user_details->profile_photo) : base_url('assets/front-design/images/icon-teacher.jpg');
												?>
												
                                                    <img width="125" accept="" src="<?php echo $profile_img; ?>" id="profileImg">
                                                </div>
                                                <div class="pic-button">
                                                    <div class="upload-file">
                                                        <input id="file-upload" onchange="readURL(this);" name='profile_photo' type="file" style="display:block;"> 
                                                        <span class="upload-btn">
														<i class="fa fa-camera-retro" aria-hidden="true"></i> 
														Change Profile Photo
														</span>
                                                    </div>
													<input type="hidden" name="profile_photo_edit" value="<?php echo $user_details->profile_photo;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $user_details->first_name;?>" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>Last Name</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $user_details->last_name;?>" placeholder="Last Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label>Email Address</label>
                                                <input type="email" class="form-control" name="email_address" id="email_address" value="<?php echo $user_details->email_address;?>" placeholder="Email Address*" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <select name="subject_class_id" class="select-color" id="subject_class_id">
											 	<option value="">Subject*</option>
												<?php foreach(get_subject() as $key => $val){
													?>
														 <option value="<?php echo $val->subject_class_id; ?>"
														 <?php if( $user_details->subject_class_id ==  $val->subject_class_id) echo 'selected'; ?>>
															<?php echo $val->subject_class_name; ?>
														 </option>
													<?php
												}?>
                                              </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Organization</label>
                                                <input type="text" class="form-control" id="organization" value="<?php echo $user_details->organization;?>" placeholder="Organization*" name="organization">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Organization Phone</label>
                                                <input type="text" class="form-control" id="organization_phone_number" value="<?php echo $user_details->organization_phone_number;?>" placeholder="Organization Phone Number" name="organization_phone_number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Your Title</label>
                                                <input type="text" class="form-control" value="<?php echo $user_details->your_title;?>" id="your_title" placeholder="" name="your_title">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Your Phone</label>
                                                <input type="tel" class="form-control" id="your_phone" value="<?php echo $user_details->your_phone;?>" placeholder="Your Phone*"  name="your_phone">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="tel" class="form-control" id="address" value="<?php echo $user_details->address;?>" placeholder="Address" name="address">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
</div>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2oRAljHGZArBeQc5OXY0MI5BBoQproWY&libraries=places"></script>
	<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profileImg')
                    .attr('src', e.target.result)
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	
		function initialize() {
			var input = document.getElementById('address');
			var options = '';
			new google.maps.places.Autocomplete(input, options);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
		
	$(function(){
		
		/*$('.upload-file').on('click',function(){
			
			alert();	
		});*/
		
	//Update Profile
	 $('#update_profile').validate({
		rules: {
				first_name: {
				   required : true,
		    	   minlength   : 3,
				},
				last_name: {
					required: true,
					minlength: 3,
				},
				email_address: {
					required: true,
					email: true
				},
				subject_class_id: {
					required: true,
				},
				organization: {
					required: true,
				},
				your_phone: {
					required: true,
					digits: true,
					minlength: 8,
					maxlength: 10
				},
				organization_phone_number: {
					digits: true,
					minlength: 8,
					maxlength: 10
				},
			},
		messages: {
				 first_name:{
					required :"First Name field is required",
				 }, 
				 last_name:{
					required :"Last Name field is required",
				 },
				 email_address:{
					required :"Email Address field is required",
				 },
				 subject_class_id:{
					required :"Subject field is required",
				 },
				 organization:{
					required :"Organization field is required",
				 },
				 your_phone:{
					required :"Your Phone field is required",
					minlength: "phone no should be between 8 to 10 character",
					maxlength: "phone no should be between 8 to 10 character",
				 },
				 organization_phone_number: {
					minlength: "phone no should be between 8 to 10 character",
					maxlength: "phone no should be between 8 to 10 character",
				}
			},
		submitHandler: function(form) {
				lkForms('update_profile');
				$(window).scrollTop(0);
			
		  }
	 });
	 });
	</script>