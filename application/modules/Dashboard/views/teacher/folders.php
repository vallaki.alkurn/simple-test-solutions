<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<section class="middle_section inner-page update-profile loginpage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="login-container">
					<div class="form-div">   
						<div class="">
							<div class="form-heading">Manage Folders</div>
							<div class="row justify-content-center">
								<div class="col-md-4 text-center">
									<?php if ($this->session->flashdata('msg')): ?>
									<div class="alert alert-danger">
										<?php echo $this->session->flashdata('msg'); ?>
									</div>
									<?php endif; ?>
									<?php if ($this->session->flashdata('info')): ?>
									<div class="alert alert-info">
										<?php echo $this->session->flashdata('info'); ?>
									</div>
									<?php endif; ?>
								</div>
							</div>
							
							<form action="<?php echo base_url('Dashboard/Teacher/store_folder');?>" method="post" id="add_new_folder" name="add_new_folder" enctype="multipart/form-data">
							
								<div class="row justify-content-center">
									<div class="col-md-4 col-12">
										<div class="input-group">
											<label></label>
											<input type="text" class="form-control" id="folder_name" name="folder_name" value="<?php echo $folder->name ?>" placeholder="Enter Folder Name">
											<div class="input-group-append">
										      <button style="margin-top: 20px" type="submit" class="btn btn-primary">Add Folder</button>	
										    </div>
										</div>
										
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-9 col-12">
				<table class="table" id="myTable">
						<thead>
							<th>No:</th>
							<th>Folder name</th>
							<th>Created at</th>
							<th>Action</th>
						</thead>
						<?php $seq = 1; ?>
				<?php foreach ($folders as $folder) :?>
						<tr>
							<td><?php echo $seq++ ?></td>
							<td><?php echo $folder->name; ?></td>
							<td><?php echo DateFormat($folder->created_at); ?></td>
							<td>
								<a href="<?php echo base_url('Dashboard/Teacher/store-folder/') . $folder->id; ?>">
									<i class="fa fa-pencil fa-lg text-primary"></i>
								</a>
								<a href="<?php echo base_url('Dashboard/Teacher/delete-folder/'). $folder->id; ?>">
									<i class="fa fa-trash fa-lg text-danger"></i>
								</a>
								<a href="<?php echo base_url('Dashboard/Teacher/completed-test/'.$folder->id); ?>">View completed tests</a>
								<a href="<?php echo base_url('Dashboard/Teacher/upcoming-test/'.$folder->id); ?>">View upcoming tests</a>
							</td>
						</tr>
				<?php endforeach; ?>
				</table>
			</div>
		</div>

	</div>
</section>
<style type="text/css">
	.dataTables_wrapper .dataTables_paginate .paginate_button {
		padding: 0px !important;
	}

	.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
		border: none !important;
	}
</style>

<script type="text/javascript">
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>