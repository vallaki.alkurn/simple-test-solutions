<section class="middle_section inner-page update-profile loginpage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
							<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							<?php 
								if($test_details){
									echo 'Test :'. $test_details->test_name;
									echo '<br>';
									echo '<a href="'.base_url('Dashboard/Teacher/test-view/'.$test_details->test_uniqe_code).'">View Test</a>';
								  }
							?>
      <form url="<?php echo base_url('Dashboard/Question/add_new_test_questions');?>" method="post" id="add_new_test_questions" name="add_new_test_questions" enctype="multipart/form-data">
	  <input type="hidden" name="test_uniqe_code" id="test_uniqe_code" value="<?php echo $test_details->test_uniqe_code ;?>" >
								<input type="hidden" name="test_id" id="test_id" value="<?php echo $test_details->test_id ;?>" >
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <div class="add-que-heading">Add Questions</div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Select Question type</label>
                                                <div class="button dropdown"> 
                                                    <select class="select-color" id="QuestionTypeSelector" name="que_type_id" id="que_type_id">
                                                        <option value="">Select Option</option>
														<?php foreach(question_type() as $qtype) { ?>
                                                        <option value="<?php echo $qtype->question_type_id; ?>" <?php if($question_type==$qtype->question_type_id) echo 'selected'; ?> ><?php echo $qtype->question_type_name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Enter Question</label>
                                                <input type="text" class="form-control" id="que_name" value="" placeholder="Enter Question*" name="que_name">
                                               <!-- <span class="typ-warning">Mention blank space in []. ex. [capital]</span>
                                             -->
											 <span class="typ-warning">Mention blank space in []. ex. []</span>
											</div>
                                            
                                        </div>
                                        
										
										  <div class="col-md-6">
												<div class="form-group ">
													<label>Upload Files</label>
													<div class="upload-file create">
														<label for="file-upload">No file select</label>
														<input id="file-upload" name='que_image' type="file" style="display:block;"> 
														<span class="upload-btn"><i class="fa fa-folder-open" aria-hidden="true"></i> Browse</span>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group ">
													<label>Set Point</label>
													<input type="text" class="form-control" id="que_points" value="2" placeholder="Set Point*" name="que_points">
												</div>
                                                
											</div>
                                            <div id="validate_errors"></div>
											
                                        <div class="col-md-12">
                                            <div class="output">
                                                <div id="queTyp1" class="queTypeOuter">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>True or False</label>
                                                            <div class="options-section">
                                                                <div class="option"><input type="radio" name="option_que_true_false" value="1" checked><span>True</span></div>
                                                                <div class="option"><input type="radio" name="option_que_true_false" value="0"><span>False</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
													<br>
                                                </div>
                                                <div id="queTyp2" class="queTypeOuter">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Add options <span style="font-size:10px;">(Select options required for correct answer)</span></label>
                                                            <div id="dynamicInput">
                                                                <div id="fill_ans_validate"></div>
                                                                <div class="input-options">
																	<label><input type="radio" value="1" name="fill_ans"><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">1.</span>
                                                                    <input type="text" name="fill_que[1]" class="fill_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																	<label><input type="radio" value="2" name="fill_ans" ><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">2.</span>
                                                                    <input type="text" name="fill_que[2]" class="fill_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																	<label><input type="radio" value="3" name="fill_ans" ><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">3.</span>
                                                                    <input type="text" name="fill_que[3]" class="fill_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																<label><input type="radio" value="4" name="fill_ans" ><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">4.</span>
                                                                    <input type="text" name="fill_que[4]" class="fill_que">
																	</span>
                                                                </div>
                                                            </div>
                                                            <div class="add_option">
                                           <input type="button" value="Add more option" onClick="addInput('dynamicInput');">
                                                            </div>
                                                        </div>

                                                      

                                                    </div>
                                                </div>
                                                <div id="queTyp3" class="queTypeOuter">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Add options <span style="font-size:10px;">(Select options required for correct answer)</span></label>
                                                            <div id="dynamicInput2">
                                                                <div id="mul_ans_validate"></div>
                                                                <div class="input-options">
																	<label><input type="checkbox" value="1" name="mul_ans[]"><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">1.</span>
                                                                    <input type="text" name="mul_que[1]" class="mul_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																	<label><input type="checkbox" value="2" name="mul_ans[]"><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">2.</span>
                                                                    <input type="text" name="mul_que[2]" class="mul_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																	<label><input type="checkbox" value="3" name="mul_ans[]"><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">3.</span>
                                                                    <input type="text" name="mul_que[3]" class="mul_que">
																	</span>
                                                                </div>
                                                                <div class="input-options">
																	<label><input type="checkbox" value="4" name="mul_ans[]"><span></span></label>
																	<span class="input-section">
                                                                    <span class="number">4.</span>
                                                                    <input type="text" name="mul_que[4]" class="mul_que">
																	</span>
                                                                </div>
                                                            </div>
                                                            <div class="add_option">
                                                                        <input type="button" value="Add more option" onClick="addInput2('dynamicInput2');">
                                                                  
                                                            </div>

                                                        </div>

                                                      
                                                    </div>
                                                </div>
                                                <div id="queTyp4" class="queTypeOuter">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group ">
                                                                <label>Answer</label>
                                                                <textarea class="form-control" id="short_ans" name="short_ans"  ></textarea>
                                                                <div id="short_ans_validate"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="queTyp5" class="queTypeOuter">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Set Pairs</label>
                                                            <div id="dyncInpamiut4">
                                                                <div class="input-options">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <span class="number">1.</span>
                                                                            <input type="text" name="pair_que[1]" id="pair_que1" class="pair_que">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="pair_ans[1]" id="pair_ans1">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="input-options">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <span class="number">2.</span>
                                                                            <input type="text" name="pair_que[2]" id="pair_que2" class="pair_que">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="pair_ans[2]" id="pair_ans2">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="input-options">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <span class="number">3.</span>
                                                                            <input type="text" name="pair_que[3]" id="pair_que3" class="pair_que">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="pair_ans[3]" id="pair_ans3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                 <div class="input-options">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <span class="number">4.</span>
                                                                            <input type="text" name="pair_que[4]" id="pair_que4" class="pair_que">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="pair_ans[4]" id="pair_ans4">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                            
                                                            </div>
                                                            <div class="add_option multi-opt">
                                                                	<input type="button" value="Add more pair" onClick="addInput4('dyncInpamiut4');">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
	<!--<div class="col-md-6">
		<div class="row">
			<div class="col-md-6 " >
				<button type="submit" class="btn btn-primary no-bg-btn">add more questions</button>
			</div>
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary no-bg-btn">Select from Question Bank</button>
			</div>
		</div>
	</div>-->
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary no-bg-btn">Save Question</button>
			</div>
		</div>                                            
	</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</section>
	<style>
	
	.typ-warning{
		display:none;	
	}
	.upload-btn{
		cursor:pointer;
	}
	
	</style>
	<script type="text/javascript" src="<?php echo base_url('assets/front-design');?>/js/add-input.js"></script>
	<script type="text/javascript">
	/*$(document).ready(function() {
		$('.queTypeOuter').hide();
    	$("#btn-teacher").click(function(){
    		$("#teacher-slide").addClass('active-slide');
    		$(this).addClass('btn-active');
    		$("#btn-student").removeClass('btn-active');
    		$("#student-slide").removeClass('active-slide');
    	});
    	$("#btn-student").click(function(){
    		$("#student-slide").addClass('active-slide');
    		$(this).addClass('btn-active');
    		$("#btn-teacher").removeClass('btn-active');
    		$("#teacher-slide").removeClass('active-slide');
    	})
    });

   $(document).ready(function() {
	  $('.section-joinus').parallax({
		imageSrc: 'images/stefan-vladimirov-1299262-unsplash.jpg'
	  });

	  $('.section-contact').parallax({
		imageSrc: 'images/brooke-cagle-609880-unsplash.jpg'
	  });
	});*/
</script>
<script type="text/javascript">
    $('#file-upload').change(function() {
		var i = $(this).prev('label').clone();
		var file = $('#file-upload')[0].files[0].name;
		$(this).prev('label').text(file);
    });
	
	$('.upload-btn').on('click',function(){
		$('#file-upload').trigger('click');	
	});
$(function() {
    // on load show ans options acc to selected que type
    $('.queTypeOuter').hide();
    if($("#QuestionTypeSelector").val() == 3) 
    {
        $.validator.addClassRules({
            mul_que:{
                cRequired: true
            }       
        });
        $.validator.addMethod("cRequired", $.validator.methods.required,"Please enter answers");
    }
    if($("#QuestionTypeSelector").val() == 2){ 
        $('.typ-warning').css('display','block');
    }else{
        $('.typ-warning').css('display','none');
    }
    $('#queTyp' + $("#QuestionTypeSelector").val()).show();

//show ans options acc to selected que type on change of ques type
    $('#QuestionTypeSelector').change(function(){
        $('.queTypeOuter').hide();
        if($(this).val() == 3)
        {
            $.validator.addClassRules({
                mul_que:{
                    cRequired: true
                }       
            });
            $.validator.addMethod("cRequired", $.validator.methods.required,"Please enter answers");
        }
		if($(this).val() == 2){ 
			$('.typ-warning').css('display','block');
		}else{
			$('.typ-warning').css('display','none');
		}
        $('#queTyp' + $(this).val()).show();
    });
});
</script> 
<script type="text/javascript">
	$(function(){
        $('.pair_que').each(function(){
            if($(this).val()!='')
            {
                var id = $(this).attr('id');
                id = id.replace('_que','_ans');
                $("#"+id).attr("required",true);
            }
        });

        $('.pair_que').keyup(function(){
            if($(this).val()!='')
            {
                var id = $(this).attr('id');
                id = id.replace('_que','_ans');
                $("#"+id).attr("required",true);
            }
        });

		var validator = $('#add_new_test_questions').validate({
            ignore:':hidden',
			rules: {
					que_type_id: {
					   required : true,
					},
					que_name: {
						required: true,
					},
					que_points: {
						required: true,
					},
                    'fill_que[1]':{
                        required:true,
                    },
                    'fill_que[2]':{
                        required:true,
                    },
                    fill_ans:{
                        required:true,
                    },
                    'mul_ans[]':{
                        required:true,
                    },
                    short_ans:{
                        required:true,
                    },
                    'pair_que[1]':{
                        required:true,
                    },
                    'pair_que[2]':{
                        required:true,
                    },
				},
			messages: {
					 que_type_id:{
						required :"Question type field is required",
					 }, 
					 que_name:{
						required :"Question field is required",
					 },
					 que_points: {
						required: "Set Point field is required",
					},
                    'fill_que[1]':{
                        required: "Please enter answer",
                    },
                    'fill_que[2]':{
                        required: "Please enter answer",
                    },
                    fill_ans:{
                        required: "Please select correct answer option",
                    },
                    'mul_ans[]':{
                        required: "Please select correct answer option(s)",
                    },
                    short_ans:{
                        required: "Please enter answer",
                    },
                    'pair_que[1]':{
                        required: "Please enter this value",
                    },
                    'pair_que[2]':{
                        required: "Please enter this value",
                    },
				},
            errorPlacement: function(error, element)
                {
                    if(element.is(':radio') || element.is(':checkbox')) {
                        error.appendTo("#validate_errors");
                    }
                    else if(element.hasClass('fill_ans') )
                    {
                        error.appendTo( element.parents('.container') );
                    }
                    else
                    { // This is the default behavior
                        error.insertAfter( element );
                    }
                },
			submitHandler: function(form) {
					lkForms('add_new_test_questions');
					//$(window).scrollTop(0);
			  }
		 });


	 });
</script>