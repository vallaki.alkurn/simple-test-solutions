<?php //print_r($user_details); ?>
<!-- The Modal -->
    <div class="modal" id="questionModal">
        <div class="modal-dialog">
            <div class="modal-content">
            
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="qType"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <div id="queview">
				
				</div>
            </div>
            
            </div>
        </div>
    </div>

<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">QUESTION BANK</div>
						</div>
						<div class="col-lg-12">
							<div class="create-test">
								<a href="<?php echo base_url('Dashboard/Question/create-question'); ?>">Create Question</a>
							</div>
						</div>
						<div class="col-lg-12" >
								<div class="right-drop-down float-lg-right float-md-right">
										<!--<div class="create-test"> <a href="<?php echo base_url('Dashboard/Question/create_question') ?>">Create Question</a> </div>-->
										<!--<div class="drop-text"> Sort by Question Type </div>
										<div class="form-group">
										<form id="type_question_frm" name="type_question_frm" method="post" action="<?php echo base_url('Dashboard/Teacher/question-bank/'.$srno); ?>" onchange="type_question_frm.submit()">
										<select name="type_question" id="type_question">
												<option value="">Select type</option>
												<?php foreach(question_type() as $qtype) { ?>
												<option value="<?php echo $qtype->question_type_id; ?>" <?php if($type_question == $qtype->question_type_id) echo 'selected';?>><?php echo $qtype->question_type_name; ?></option>
												<?php } ?>
										</select>
										</form>
										</div>-->
								</div>
						</div>
						<div class="col-lg-12" style="display:none;">
								<div class="bank-list-outer">
										<div class="list-header">
												<div class="list-row">
														<div class="list-column text-center">Sr No</div>
														<div class="list-column text-center">Question Type</div>
														<div class="list-column ">Type Questions</div>
														<div class="list-column text-center">Set Point</div>
														<!--<div class="list-column text-center">Correct Answer</div>-->
														<!--<div class="list-column text-center">Add in Test</div>-->
														<div class="list-column text-center">Action</div>
												</div>
										</div>
										<div class="list-boby" style="display:none;">
										<?php 
								 if($results){
								 $i = $srno == 0 ? 1 : $srno+1;
								 foreach ($results as $data) { ?>
												<div class="list-row">
														<div class="list-column text-center"><?php echo $i;//$data->test_id; ?></div>
														<div class="list-column"><?php echo $data->question_type_name; ?></div>
														<div class="list-column"><?php echo str_replace_q($data->que_name); ?></div>
														<div class="list-column"><?php echo $data->que_points; ?></div>
														<!--<div class="list-column"><?php echo $data->que_option; ?></div>-->
														<!--<div class="list-column text-center tblbtn"><a href="#">Add in test</a></div>-->
														<div class="list-column text-center action"><i class="fa fa-eye" aria-hidden="true" data-toggle="modal" data-target="#questionModal" onClick="getQuestionDetails(<?php echo $data->que_id; ?>)"></i></div>
												</div>
												<?php $i++; } }else{ echo 'Question not found.';} ?>
										</div>
								</div>
						</div>
						<div class="col-lg-12">
						<?php /*if (isset($links)) { ?>
							<?php echo $links ?>
						<?php }*/ ?>
						<!--<nav aria-label="Page navigation example">
								<ul class="pagination justify-content-end">
										<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1">Previous</a> </li>
										<li class="page-item"><a class="page-link" href="#">1</a></li>
										<li class="page-item"><a class="page-link" href="#">2</a></li>
										<li class="page-item"><a class="page-link" href="#">3</a></li>
										<li class="page-item"> <a class="page-link" href="#">Next</a> </li>
								</ul>
						</nav>-->
						<table id="example" class="display table table-bordered" style="width:100%">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Question Type</th>
								<th>Questions</th>
								<th>Test</th>
								<th>Set Point</th>
								<th>Add in Test</th>
								<!--<th>Timing</th>-->
								<!--<th>Total Question</th>
								<th>Sent Invitations</th>-->
								<th>Action</th>
							</tr>
						</thead>
					</table>
						
						</div>
				</div>
		</div>
</div>

<script>
$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getQuestionBankJson');?>",
		 "columns": [
            { "data": "id" },
			{ "data": "question_type_name" },
			{ "data": "que_name" },
			{ "data": "test_name" },
			{ "data": "que_points" },
			{ "data": "add_test" },
			/*{ "data": "total_question" },
			{ "data": "sent_invitation" },*/
			{ "data": "action" }
        ]
    });
	
});
</script>


<script>
function getQuestionDetails(qID)
{
	var data = 'que_id='+qID;
	$.ajax({
	  method: 'POST',
	  url: '<?php echo base_url('Dashboard/Teacher/question_details');?>',
	  data: data,
	  dataType:'json',
	  //processData: false,
	  success: function(data){
		  var rdata = data.response;
		  if(data.status == true){
			  $('#qType').html(rdata.q_type);
			  $('#queview').html(rdata.q_option);
		  }
		
	  }
	});
	
}
</script>