<?php //echo "<pre>"; print_r($student_answer->question_id); die('here'); ?>
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
<!-- Middle section start-->
				<div class="container-fluid">
					<div class="outer-div-test">
						<div class="about-grid">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-heading" style="padding:0px;">Test Result</div>
									<hr/>
									<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>

								</div>
								<div class="col-lg-12">
									
									<div class="start-test-page">
										<div class="reque-detail-outer">
											<div class="reque-detail-sections" style="width:100%;">
												<div class="req-row" style="width: 100%">
													<div class="req-heading">Student Name</div>
													<div class="req-cont"><?php echo $student_details->first_name.'&nbsp;'.$student_details->last_name; ?></div>
												</div>
												<div class="req-row" style="width: 100%">
													<div class="req-heading">Grade</div>
													<div class="req-cont">
														<?php 
														$percentage =  (100 * $test_marks->total_mark) / $test_marks->max_mark; 

														echo $test_marks->total_mark .' / ' . $test_marks->max_mark . ' ( '.number_format($percentage, 2).'% )';
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="total-que-section" style="float:left;">
										<!--<div class="heading-hist">
											<strong>Total questions</strong>(<?php echo count($test_que);?>) 
										</div>-->
										<div class="panel-group" id="accordion">
											<?php if($test_que){ 
											$i = 1;
									foreach($test_que as $tq => $tv){ ?>
											<div class="panel panel-default" style="max-width: 100% !important; flex: 0 0 100%;">
												<div class="panel-heading">
													<dt  class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $tv->que_id; ?>">
													<a href="javascript:void(0)"><span class="counter-num"><?php echo $i;?></span><?php echo str_replace_q($tv->que_name); ?><?php //echo $tv->que_id; ?> <i class="fa fa-plus" aria-hidden="true"></i></a>
													<?php 
													$res = SelectData('tbl_question_option_1','*',array('que_id'=>$tv->que_id), '', NULL);

													$ans = user_question_anser( $student_details->user_id, $test_id, $tv->que_id); 
												
													?>
												<span style="margin-left:35px;">(<strong style="color:#a807a8;">Point : <?php echo (!empty($ans->marks)) ? $ans->marks : 0 ;  ?> /  <?php echo $tv->que_points; ?></strong>)</span></dt>
												</div>
												<div id="collapse<?php echo $tv->que_id; ?>" class="panel-collapse collapse show">
													<div class="panel-body">
														<div class="col-md-6">
													<?php 
														if($tv->que_image){
															echo '<img  width="200" src="'.base_url($tv->que_image).'" >';
														}; 
													
														$res = SelectData('tbl_question_option_1','*',array('que_id'=>$tv->que_id), '', NULL);
														
														

														if (count($res) > 1) {
															echo '<ol class="col-lg-6">';
															foreach($res as $op => $ov) {
																
																$ans = user_question_anser( $student_details->user_id, $test_id, $tv->que_id, $ov->question_option_id);

																if($tv->que_type_id == 5) {
																	$match_pair = 	'<span style="color:#fff; background:green; padding:2px 4px;">'.tbl_question_anser_pair($ov->question_option_id).'</span>';
																} else {
																	$match_pair = '';
																}

																//echo $ans->answer_id;
																//echo $ov->question_option_id;
																if($ans->answer_id == $ov->question_option_id)
																{
																	if($ans->answer_status == 'right'){ 
																		$anss ='color:green';
																		$icons ='<i class="fa fa-check-circle-o text-success" ></i>';
																	}else{
																		$anss ='color:red';
																		$icons ='<i class="fa fa-times-circle-o text-danger" ></i>';
																	}
																}else{
																	$anss ='';
																	$icons ='';
																}
																	
																echo '<li style="list-style:decimal; margin-bottom:5px; '.$anss.'">'.$ov->que_option.'&nbsp;'.$icons.' '.$match_pair.'</li>';
															}
															echo '</ol>';
														} else if (count($res) == 1) {
															$ans = user_question_anser( $student_details->user_id, $test_id, $tv->que_id, $res{0}->question_option_id);

															echo '<strong>Student answer:</strong> ' . $ans->student_short_answer . '<hr/>';
															echo '<strong class="text-success">Correct answer:</strong> ' . $res{0}->que_option;
														}
													
														if($tv->que_type_id == 5){
															$res_option_pair = que_option_pair($tv->que_id);
															//print_r($res_option_pair);
															echo '<ol class="col-lg-6">';
															foreach($res_option_pair as $v){
																echo '<li style="list-style:lower-alpha; margin-bottom:5px;">'.$v->pair_option.'</li>';
															}
															echo '</ol>';
														}
									
														?>
														</div>
														<?php if($test_marks->test_status=='Pending' && $tv->que_type_id == 4) { ?>
														<div class="col-md-4">
															<div class="_qr btn-group mb-2" id="_q<?php echo $tv->que_id; ?>" role="group">
															  <button onclick="assignMarks(<?php echo $tv->que_id ?>, <?php echo $tv->que_points ;?>)" type="button" class="btn btn-success btn-xs" style="float:left; width:35px; height: 30px;">
															  	<i class="fa fa-check"></i>
															  </button>
															  <button type="button" class="btn btn-secondary btn-xs btn-mn" style="float:left; width:35px; height: 30px;"><i class="fa fa-eraser"></i></button>
															  <button onclick="assignMarks(<?php echo $tv->que_id ?>, 0)" type="button" class="btn btn-danger btn-xs" style="float:left; width:35px; height: 30px;"><i class="fa fa-close"></i></button>
															</div>
															<div class="clearfix"></div>
																<div id="_m<?php echo $tv->que_id; ?>" class="input-group md-3" style="display:none;">
																	<input style="float:left; width:70px;" type="number" id="_mk<?php echo $tv->que_id; ?>" name="marks" class="form-control text-right" value="<?php echo $ans->marks ?>" min="0" max="<?php echo $tv->que_points; ?>"  step="0.5"/> 
																	<div class="input-group-append" style="float:left">
																		<span class="input-group-text" style="font-size: 15px;height: 34px;">/ <?php echo $tv->que_points ;?></span>
																	</div>
																	<button onclick="assignMarks(<?php echo $tv->que_id ?>, document.getElementById('_mk<?php echo $tv->que_id; ?>').value)" style="height: 34px;width:35px;" class="ml-2 btn btn-xs btn-success"><i class="fa fa-save"></i></button>
																</div>
																<div class="clearfix"></div>
														</div>
														<?php } ?>
													</div>
												</div>
											</div>
											<?php 
												$i++;
											} 
												}else{
													echo '<div class="col-lg-12"><p class="alert alert-info">Question are not added</p></div>';	
												}
												?>
										</div>
										<?php if($test_marks->test_status=='Pending') { ?>
										<button class="btn btn-success btn-complete" style="width: 200px; float:right; font-size: 18px; font-weight: bold;"><i class="fa fa-check-circle"></i> Mark as complete</button>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
    <!-- Middle section End-->
</div>
</div>
</div>
<!-- Middle section start-->
<script>
function reuseTest(testId)
{
	var data = 'test_id='+testId;
	$.ajax({
		type:'POST',
		url:'<?php echo base_url('Dashboard/Question/reuse_test');?>',
		data:data,
		dataType:'json',
		beforeSend: function(data){
		},
		success: function (data) {
			//console.log(data);
			if(data.status == true){
				if(data.url){
					window.location.assign(data.url);
				}
			}
		}
	});	
}

$(document).on('click', '.btn-mn', function(e) {
	var _id = $(this).parent().attr('id').replace('_q', '_m');
	$('#'+_id).toggle('show')	
	// console.log(_id);
});

$(document).on('click', '.btn-complete', function(e) {
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url('Dashboard/Teacher/update_test_status/'. $request_id.'/'.$test_id);?>',
		dataType: 'json',
		success: function(res) {
			// Response
			if (res.status == 'success') {
				location.reload(true);
			}
		}

	});
});

function assignMarks(qId, marks)
{
	$.ajax({
		type:'POST',
		url:'<?php echo base_url('Dashboard/Question/assign_marks/'.$student_details->user_id.'/'.$test_id);?>',
		data: {"q_id": qId, 'marks': marks, 'request_id': <?php echo $request_id; ?>},
		dataType:'json',
		success: function(data) {

			if (data.status == 'success') {
				location.reload(true);
			}
		}
	});

}
</script>