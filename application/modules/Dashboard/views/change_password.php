<!-- Middle section start-->
	<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  
							$user_details = get_user(); 
							if($user_details->user_type == 1){
								require_once(APPPATH.'views/template/sidebar-teacher.php'); 
							}else{
								require_once(APPPATH.'views/template/sidebar-student.php'); 
							}
						?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
							
                                <div class="form-heading">Change password</div>
								<div class="msg-gloabal"></div>
                                <form url="<?php echo base_url('Dashboard/Dashboard/reset_password');?>" method="post" id="reset_password" name="reset_password" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Current Password<span>*</span></label>
                                                <input type="password" class="form-control" id="current_password" value="" placeholder="" name="current_password">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>New Password<span>*</span></label>
                                                <input type="password" class="form-control" id="new_password" value="" placeholder="" name="new_password">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Confirm New Password<span>*</span></label>
                                                <input type="password" class="form-control"id="confirm_password" value="" placeholder="" name="confirm_password">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</div>
	<!-- Middle section End-->
	
	<script>
	//----------------- Validation of registration form ----------------//
$(function(){
	$("#current_password").focus();
	//Registration Form
	 $('#reset_password').validate({
		rules: {
				current_password: {
				   required : true,
				},
				new_password: {
					required: true,
					minlength: 5,
				},
				confirm_password: {
					required: true,
					equalTo: "#new_password"
				}
			},
		messages: {
				 current_password:{
					required :"Current Password field is required",
				 },
				 new_password:{
					required :"New Password field is required",
				 },
				 confirm_password:{
					required :"Confirm Password field is required",
					equalTo :"new password and confirm password do not match",
				 }
			},
		submitHandler: function(form) {
				lkForms('reset_password');
				$(window).scrollTop(0);
		  }
	 });
});
	</script>