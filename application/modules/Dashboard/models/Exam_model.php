<?php

class Exam_model extends CI_Model {

    function __construct() {
        parent::__construct();
		//$this->get_user = get_user();
		//print_r($this->db->last_query());
    }
	 
	public function getSavedAnswered($data)
  {
    $this->db->select('*');
    $this->db->from('tbl_user_question_answered');
    $this->db->where('test_id', $data['test_id']);   
    $this->db->where('user_id', $data['user_id']);   
    $this->db->where('question_id', $data['question_id']);   
    $q = $this->db->get();
   
    return $res = $q->result();
  }

	public function getQueAnswered($data)
	{
         $this->db->select('*,tbl_question_option_1.que_option');
        $this->db->from('tbl_question_anser');
		$this->db->join('tbl_question_option_1', 'tbl_question_option_1.question_option_id = tbl_question_anser.option_answer_id' );
       
        $this->db->where('question_id', $data['question_id']);   
        $q = $this->db->get();
        
        return $res = $q->row();

	}

  public function getQueMarks($que_id)
  {
        $this->db->select('*');
        $this->db->from('tbl_que_creation');
        $this->db->where('que_id', $que_id);   
        $q = $this->db->get();
        
        return $res = $q->row();
  }
	public function getShortQueAnswered($data)
	{
         $this->db->select('*');
        $this->db->from('tbl_question_option_1');
        $this->db->where('que_id', $data['question_id']);   
        $q = $this->db->get();
        
        return $res = $q->row();

	}
	public function getQuePairs($data)
	{
         $this->db->select('*');
        $this->db->from('tbl_question_pair');
        $this->db->where('question_id', $data['question_id']);   
        $q = $this->db->get();
        
        return $res = $q->result();

	}
	public function saveAnswered($data)
  {
	   return $this->db->insert('tbl_user_question_answered',$data);
  }
  
  public function updateAnswered($data)
  {
    $this->db->where('test_id', $data['test_id']);   
    $this->db->where('user_id', $data['user_id']);   
    $this->db->where('question_id', $data['question_id']); 
    return $this->db->update('tbl_user_question_answered',$data);
  }
// pallate
public function getSavedPallate($data)
{
     $this->db->select('*');
        $this->db->from('tbl_button_pallate');
        $this->db->where('test_id', $data['test_id']);   
        $this->db->where('user_id', $data['user_id']);   
        $this->db->where('que_id', $data['que_id']);   
        $q = $this->db->get();
        
        return $res = $q->result();
}
	
	public function savePallate($data)
    {
		
        return $this->db->insert('tbl_button_pallate',$data);
    }
      
        public function updatePallate($data)
    {
		
		   $q1= $this->db->where('test_id', $data['test_id']);   
       $q1= $this->db->where('user_id', $data['user_id']);   
       $q1= $this->db->where('que_id', $data['que_id']); 
       $q1= $this->db->where('subject', $data['subject']); 
        $q1= $this->db->delete('tbl_button_pallate');
		 return $this->db->insert('tbl_button_pallate',$data);
    }
	 
	  public function getAttemptedPallate($user_id,$test_id,$subject)
	{
              $this->db->select('*');
        $this->db->from('tbl_button_pallate');
        $this->db->where('test_id', $test_id);   
        $this->db->where('user_id', $user_id);   
        $this->db->where('subject', $subject);   
        $q = $this->db->get();
        
        return $res = $q->result();
	}

  public function submitTest($user_id,$test_id,$submitTime,$request_id)
		{
               
					$query = $this->db->where('test_id', $test_id);   
				$query = $this->db->where('user_id', $user_id);   
				$query = $this->db->update('tbl_user_question_answered',array('exam_status'=>'1'));
				
				$query1 = $this->db->where('test_id', $test_id);   
				$query1 = $this->db->where('user_id', $user_id);   
				$query1 = $this->db->delete('tbl_button_pallate');

             $query3 = $this->db->select('sum(tbl_user_question_answered.marks) AS total');
          $query3 =$this->db->from('tbl_user_question_answered');
       $query3 =  $this->db->join('tbl_que_creation', 'tbl_que_creation.que_id = tbl_user_question_answered.question_id' );
        $query3 = $this->db->where('tbl_user_question_answered.test_id', $test_id);   
       $query3 =  $this->db->where('tbl_user_question_answered.user_id', $user_id);   
       $query3 = $this->db->where('tbl_user_question_answered.answer_status', 'right');   
        $query3 = $this->db->get();
            $q = $query3->row();

	           if(isset($q->total)){
				    $total_right_mark=$q->total;
					 
				}
				else{
					$total_right_mark=0;
				}
      		  //echo $total_right_mark;exit;

       $query4 = $this->db->select('tbl_que_creation.*');
          $query4 =$this->db->from('tbl_test_question');
		 $query4 = $this->db->join('tbl_que_creation', 'tbl_que_creation.que_id = tbl_test_question.que_id' );
       $query4 =  $this->db->where('tbl_test_question.test_id', $test_id);   
     $query4 = $this->db->get();
            $q_data = $query4->result();
			$check_short_ans=array();
			foreach($q_data as $row)
			{
				if($row->que_type_id == '4')
				{
					$check_short_ans[] = 'short ans';
				}
			}
      if(count($check_short_ans) > 0) {
			 $test_status= 'Pending';
		 } else {
			 $test_status= 'Complete';
		 }

           $total_q=$this->db->query("select * from tbl_test_question where  test_id='".$test_id."'")->num_rows();
           $total_attempt_que=$this->db->query("select * from tbl_user_question_answered where user_id=".$user_id." and test_id='".$test_id."'")->num_rows();
		   
           $max_mark=$this->db->query("select ROUND(SUM(qc.que_points)) as sum1 from tbl_test_question as tq  join tbl_que_creation AS qc ON qc.que_id=tq.que_id where tq.test_id='".$test_id."'")->row();

 $query5 = $this->db->select('*');
          $query5 =$this->db->from('tbl_user_question_answered');
       $query5 =  $this->db->join('tbl_que_creation', 'tbl_que_creation.que_id = tbl_user_question_answered.question_id' );
        $query5 = $this->db->where('tbl_user_question_answered.test_id', $test_id);   
       $query5 =  $this->db->where('tbl_user_question_answered.user_id', $user_id);   
       $query5 = $this->db->where('tbl_user_question_answered.answer_status', 'right');   
        $query5 = $this->db->get();
 $total_right_que= $query5->num_rows();

 $query6 = $this->db->select('*');
          $query6 =$this->db->from('tbl_user_question_answered');
       $query6 =  $this->db->join('tbl_que_creation', 'tbl_que_creation.que_id = tbl_user_question_answered.question_id' );
        $query6 = $this->db->where('tbl_user_question_answered.test_id', $test_id);   
       $query6 =  $this->db->where('tbl_user_question_answered.user_id', $user_id);   
       $query6 = $this->db->where('tbl_user_question_answered.answer_status', 'wrong');   
        $query6 = $this->db->get();
 $total_wrong_que= $query6->num_rows();

	  //	return $ret;
            $not_attempt_que=$total_q- $total_attempt_que;
                  $data=array(
                            'user_id' => $user_id,
                            
                            'test_id' => $test_id,
                            'request_id' => $request_id,
                            'total_question' => $total_q,
                            'max_mark' => $max_mark->sum1,
                            'total_mark' => $total_right_mark,
                            'total_wrong_que' => $total_wrong_que,
                            'total_right_que' => $total_right_que,
                            'total_right_mark' => $total_right_mark,
                            'total_wrong_mark' => 0,
                            'time_taken' => $submitTime,
                            'attempt_question' => $total_attempt_que,
                            'not_attempt_question' => $not_attempt_que,
                            'test_status' => $test_status,
                            'created' => date('d/m/Y')
                     );
					 // print_r($data);exit;
                      $this->db->insert('tbl_completed_test',$data);
					  
                 $query2 = $this->db->where('id', $request_id);   
			 $query2 = $this->db->update('tbl_invited_exam_requests',array('request_status'=>'4'));

                   
              //  unset($_SESSION['targetdate']);
			//session_destroy();

		}
 public function getTestReport($test_id,$user_id)
{
	//echo $test_id;exit;
              $this->db->select('tbl_completed_test.*,tbl_test_creation.test_time_limit as duration,tbl_test_creation.test_name,tbl_user_registration.first_name,tbl_user_registration.last_name,tbl_standard_class.standard_class_name');
        $this->db->from('tbl_completed_test');
         $this->db->join('tbl_test_creation', 'tbl_test_creation.test_id = tbl_completed_test.test_id' );
         $this->db->join('tbl_user_registration', 'tbl_user_registration.user_id = tbl_completed_test.user_id' );
         $this->db->join('tbl_standard_class','tbl_standard_class.standard_class_id = tbl_user_registration.standard_class_id' );
        $this->db->where('tbl_completed_test.test_id', $test_id);   
        $this->db->where('tbl_completed_test.user_id', $user_id);   
		$this->db->where('tbl_completed_test.test_status', 'Complete');   
        
        $q = $this->db->get();

        return $res = $q->row();
}
public function Save_last_time($test_id,$save_time)
	{
		$query1 = $this->db->where('test_id', $test_id);   
				$query1 = $this->db->where('user_id', get_user_id());   
				$query1 = $this->db->delete('tbl_test_last_time');
				$data=array(
				 'user_id' =>get_user_id(),
				 'test_id' =>$test_id,
				 'save_time' =>$save_time,
				);
		return $this->db->insert('tbl_test_last_time',$data);
	}
	public function getTestReportById($id)
{
	  $this->db->select('tbl_completed_test.*,tbl_test_creation.test_time_limit as duration,tbl_test_creation.test_name,tbl_user_registration.first_name,tbl_user_registration.last_name,tbl_standard_class.standard_class_name');
        $this->db->from('tbl_completed_test');
         $this->db->join('tbl_test_creation', 'tbl_test_creation.test_id = tbl_completed_test.test_id' );
         $this->db->join('tbl_user_registration', 'tbl_user_registration.user_id = tbl_completed_test.user_id' );
         $this->db->join('tbl_standard_class','tbl_standard_class.standard_class_id = tbl_user_registration.standard_class_id' );
        $this->db->where('tbl_completed_test.id', $id);   
        $this->db->where('tbl_completed_test.test_status', 'Complete');   
       
        $q = $this->db->get();

        return $res = $q->row();
}
    
}
?>
