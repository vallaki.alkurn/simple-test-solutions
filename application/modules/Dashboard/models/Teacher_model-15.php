<?php

class Teacher_model extends CI_Model {

    function __construct() {
        parent::__construct();
		//$this->get_user = get_user();
		//print_r($this->db->last_query());
    }
	
	///Dashboard Count Block Start
	public function total_que_count()
	{
		$res ='';
		$this->db->select('que_id');
		$this->db->from('tbl_que_creation');
		//$this->db->join('tbl_test_creation','tbl_que_creation.test_id = tbl_test_creation.test_id');
		$this->db->where(array('tbl_que_creation.user_id' =>get_user_id()));
		$q = $this->db->get();
		$res = $q->num_rows();
		return $res;
	}
	public function total_test_count()
	{
		$res ='';
		$this->db->select('test_id');
		$this->db->from('tbl_test_creation');
		$this->db->where(array('user_id' =>get_user_id()));	
		$q = $this->db->get();
		$res = $q->num_rows();
		return $res;
	}
	public function check_completed_test_count()
    {
		$res ='';
		$this->db->select('test_id');
		$this->db->from('tbl_test_creation');
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		$this->db->where(array('user_id' =>get_user_id()));
		$q = $this->db->get();
		$res = $q->num_rows();
		return $res;
    }
	///Dashboard Count Block Ends...
	
	//Check Upcoming box
	public function check_upcoming_test()
    {
		$res ='';
		$this->db->select('*');
		$this->db->from('tbl_test_creation');
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->where(array('test_to_date >=' =>date('Y-m-d')));
		$this->db->where(array('user_id' =>get_user_id()));
		//$this->db->where(array('test_form_date  >=' => date('Y-m-d')));	
		//$this->db->where(array('test_to_date <=' => date('Y-m-d')));	
		$this->db->limit(3);
		$this->db->order_by('test_id','DESC');
		$q = $this->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res ? $res : '';
    }
	
	//Check completed test box
	public function check_latest_completed_test()
    {
		$res ='';
		$this->db->select('*');
		$this->db->from('tbl_test_creation');
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));
		$this->db->where(array('user_id' =>get_user_id()));	
		$this->db->limit(3);
		$this->db->order_by('test_id','DESC');
		$q = $this->db->get();
		$res = $q->result();
		return $res ? $res : '';
    }
	
	public function get_current_page_records_upcoming($table,$limit, $start,$order) 
    {

		//Return total Data
		$data_array = array();
		$this->db->join('test_folders', "$table.folder_id = test_folders.id");
        $this->db->limit($limit, $start);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->order_by($order,'DESC');
        $query = $this->db->get($table);
      
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            $data_array['listData'] = $data;
        }
		
		//Return total Count
		$this->db->where('user_id',get_user_id());
		$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->from($table);
        $data_array['count'] = $this->db->count_all_results();
 		
        return  $data_array;
    }
	
	public function get_current_page_records_completed($table,$limit, $start,$order) 
    {
		//Return total Data
		$data_array = array();
        $this->db->limit($limit, $start);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		$this->db->order_by($order,'DESC');
        $query = $this->db->get($table);
		//print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            $data_array['listData'] = $data;
        }
		
		//Return total Count
		$this->db->where('user_id',get_user_id());
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		$this->db->from($table);
        $data_array['count'] = $this->db->count_all_results();
 		
        return  $data_array;
    }
	
	public function get_all_questions($limit, $start,$q_type = NULL) 
    {
		//Return total Data
		$data_array = array();
		$this->db->select('tbl_que_creation.*,tbl_q_type_master.question_type_name');
		$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_q_type_master','tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id','left');
		$this->db->where(array('tbl_test_creation.user_id' =>get_user_id()));
		if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}
        $this->db->limit($limit, $start);
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		$this->db->order_by('tbl_que_creation.que_id','DESC');
        $query = $this->db->get('tbl_que_creation');
		//print_r($this->db->last_query());exit;
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            $data_array['listData'] = $data;
        }
		
		//Return total Count
		//$this->db->where('user_id',get_user_id());
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		$this->db->from('tbl_que_creation');
		$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_q_type_master','tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id','left');
		$this->db->where(array('tbl_test_creation.user_id' =>get_user_id()));
		if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}
        $data_array['count'] = $this->db->count_all_results();
 		
        return  $data_array;
    }
	
	
	public function question_details($que_id = NULL)
	{
		$this->db->select('tbl_que_creation.*,tbl_q_type_master.question_type_name');
		$this->db->from('tbl_que_creation');
		$this->db->join('tbl_q_type_master','tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id','left');
		$this->db->where(array('tbl_que_creation.que_id' =>$que_id));
		$q = $this->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res[0];
	}
	
	
	
	
	//Get Pagination Data Local flow
	public function get_current_page_records($table,$limit, $start,$order) 
    {
        $this->db->limit($limit, $start);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->order_by($order,'DESC');
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 		
        return false;
    }
	
	
	public function get_total($table = NULL) 
    {
		//echo get_user_id();
		$this->db->where('user_id',get_user_id());
		$this->db->from($table);
        return $this->db->count_all_results();
		//print_r($this->db->last_query());
    } 
	
	public function test_details_list($question_id = NULL)
	{
		//,tbl_test_question.*
		$this->db->select('tbl_test_creation.test_id,tbl_test_creation.test_name');
		$this->db->from('tbl_test_creation');
		$this->db->where('user_id',get_user_id());
		$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		//$this->db->join('tbl_test_question','tbl_test_question.test_id = tbl_test_creation.test_id','left');
		//$this->db->where_not_in('tbl_test_question.que_id','IS NULL');
		//$this->db->where('tbl_test_question.que_id !=',$question_id);
		$this->db->order_by('tbl_test_creation.test_id','desc');
		$q = $this->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res;
	}
	
	
	public function get_all_question($test_id = NULL)
	{
		$this->db->select('tbl_que_creation.*');
		$this->db->from('tbl_que_creation');
		$this->db->join('tbl_test_question','tbl_test_question.que_id = tbl_que_creation.que_id','left');
		$this->db->where('tbl_test_question.test_id',$test_id);
		$q = $this->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res;
	}
	
	public function get_all_question_marks($test_id = NULL)
	{
		$this->db->select('SUM(tbl_que_creation.que_points) as marks');
		$this->db->from('tbl_que_creation');
		$this->db->join('tbl_test_question','tbl_test_question.que_id = tbl_que_creation.que_id','left');
		$this->db->where('tbl_test_question.test_id',$test_id);
		$q = $this->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		//print_r($res[0]);
		return $res[0]->marks;
	}


    


	
    
}
?>
