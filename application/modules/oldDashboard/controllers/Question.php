<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Question extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		check_user_session() ? '' : redirect('login');
		$this->get_user = get_user();
		if($this->get_user->user_type != 1){ redirect('dashboard/logout'); }
        parent::__construct();
        $this->load->model('Dashboard_model');
		error_reporting(0);
	}
	
	public function create_question($test_unique_id = NULL)
	{
		$data = array();
		if($test_unique_id){
			$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_unique_id), 1, NULL);
		}
		$this->load->front_view('teacher/create_question',$data);
	}
	
	public function add_new_test_questions()
	{
		$data = '';
		if($_POST){			
				/* print_r($_POST['short_ans']);
				 exit;*/
				/* ------------------- Photo Upload -----------------*/
				$images = '';
				$FileNames = array();
				/* ------------------- Photo Upload -----------------*/
				if($_FILES['que_image']['name']){
					include('lomesh/lk_image.php');
					$this->lkimg = new Lk_image();
					//	print_r($_FILES);
					$path = "./media/question";
					$path_thumb = "./media/question/thumb";
					if(!is_dir($path) && !is_dir($path)) //create the folder if it's not already exists
					{
						mkdir($path,0755,TRUE);
						mkdir($path_thumb,0755,TRUE);
					} 
					/* -------------- Upload Files --------------- */
					$FileNames = array();
					$file_data = array('file'=>$_FILES,
								'fileName'=>'que_image',
								'Folder'=>$path,
								'thumFolder'=>$path_thumb,
								'width'=>'125','height'=>'');
					$FileNames = $this->lkimg->singleUploadImages($file_data);
					$images = '/media/question/'.$FileNames;
				}			
			//Create Question 
			$question_data = array('que_name'=>$_POST['que_name'],
									'que_points'=>$_POST['que_points'],
									'que_type_id'=>$_POST['que_type_id'],
									'que_image'=>$images,
									'created'=> date('Y-m-d'),
									'test_id'=>$_POST['test_id'] ? $_POST['test_id'] : NULL,
									'user_id'=>$this->get_user->user_id);
									
			$question_id =  InsertRow('tbl_que_creation', $question_data, 1);
			
			if($_POST['test_id']){
				$tbl_test_question = array('test_id'=>$_POST['test_id'],
											'que_id'=>$question_id);
				$tbl_test_question_id =  InsertRow('tbl_test_question', $tbl_test_question, 1);
			}
					
			switch($_POST['que_type_id']) {
			  case 1:
					$true_false_array = array(1=>'True',0=>'False');
					foreach($true_false_array as $t => $f){
						if(!empty($f)){
							$data_array = array('que_option'=>$f,'que_id'=>$question_id);
							
							$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
							
							if($_POST['option_que_true_false'] == $t){
								$anser_data = array('question_id' =>$question_id,'option_answer_id ' =>$option_answer_id  );
								InsertRow('tbl_question_anser', $anser_data, 1);
							}	
						}
					}					
				break;
			  case 2:
					foreach($_POST['fill_que'] as $t => $f){
						
						$data_array = array('que_option'=>$f,'que_id'=>$question_id);
						if(!empty($f)){
							$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
						}
						
						if($_POST['fill_ans'] == $t){
							$anser_data = array('question_id' =>$question_id,'option_answer_id ' =>$option_answer_id  );
							InsertRow('tbl_question_anser', $anser_data, 1);
						}	
					}
				break;
			  case 3:
			  		foreach($_POST['mul_que'] as $t => $f){
						
						$data_array = array('que_option'=>$f,'que_id'=>$question_id);
						if(!empty($f)){
							$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
						}
						if(isset($_POST['mul_ans'])){
							if(in_array($t,$_POST['mul_ans'])){
								$anser_data = array('question_id' =>$question_id,'option_answer_id ' =>$option_answer_id  );
								InsertRow('tbl_question_anser', $anser_data, 1);
							}	
						}
					}
				break;
			  case 4:
			  		if(!empty($_POST['short_ans'])){
						$data_array = array('que_option'=>$_POST['short_ans'],'que_id'=>$question_id);
						$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
						//Add anser_id
						$anser_data = array('question_id' =>$question_id,'option_answer_id ' =>$option_answer_id  );
						InsertRow('tbl_question_anser', $anser_data, 1);
					}
				break;
			  case 5:
			  		foreach($_POST['pair_que'] as $t => $f){
						$data_array = array('que_option'=>$f,'que_id'=>$question_id);
						if(!empty($f)){
							$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
						}
						$tbl_question_pair = array('question_id' =>$question_id,
											'option_id ' =>$option_answer_id,
											'pair_option' =>$_POST['pair_ans'][$t]);
						if(!empty($_POST['pair_ans'][$t])){					
							InsertRow('tbl_question_pair', $tbl_question_pair, 1);
						}
					}
				break;			  
			}
			
			$this->session->set_flashdata('msg', $this->lang->line('question_created_success'));
			$data = array('status' => true,
						  'response' => array('msg' =>''),
						  'url' => base_url('Dashboard/Question/create_question/'.$_POST['test_uniqe_code']));
								  
								   
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;		
	}
	
	public function update_question($que_id = NULL)
	{
		if($que_id){
			$data['que_details'] = SelectData('tbl_que_creation','*',
							array('que_id'=>$que_id,'user_id'=>$this->get_user->user_id), 1, NULL);
			//print_r($data);
			if($data['que_details']){
				$this->load->front_view('teacher/update_question',$data);
			}else{
				$this->load->front_view('template/404');
			}
		}else{
			$this->load->front_view('template/404');
		}
	}
	
	public function update_test_questions()
	{
		//print_r($_POST);
		//exit;
		
		$data = '';
		if($_POST){			
				/* print_r($_POST['short_ans']);
				 exit;*/
				/* ------------------- Photo Upload -----------------*/
				$images = '';
				$FileNames = array();
				/* ------------------- Photo Upload -----------------*/
				if($_FILES['que_image']['name']){
					include('lomesh/lk_image.php');
					$this->lkimg = new Lk_image();
					//	print_r($_FILES);
					$path = "./media/question";
					$path_thumb = "./media/question/thumb";
					if(!is_dir($path) && !is_dir($path)) //create the folder if it's not already exists
					{
						mkdir($path,0755,TRUE);
						mkdir($path_thumb,0755,TRUE);
					} 
					/* -------------- Upload Files --------------- */
					$FileNames = array();
					$file_data = array('file'=>$_FILES,
								'fileName'=>'que_image',
								'Folder'=>$path,
								'thumFolder'=>$path_thumb,
								'width'=>'125','height'=>'');
					$FileNames = $this->lkimg->singleUploadImages($file_data);
					$images = '/media/question/'.$FileNames;
				}else{
					$images = $_POST['check_image'];
				}			
			//Create Question 
			$question_data = array('que_name'=>$_POST['que_name'],
									'que_points'=>$_POST['que_points'],
									'que_type_id'=>$_POST['que_type_id'],
									'que_image'=>$images);
									
			$question_id =  UpdateRow('tbl_que_creation', $question_data, array('que_id' => $_POST['que_id']));			
					
			switch($_POST['que_type_id']) {
			  case 1:
					$anser_data = array('option_answer_id ' =>$_POST['option_que_true_false']);
					UpdateRow('tbl_question_anser', $anser_data, array('question_id' => $_POST['que_id']));
				break;
			  case 2:
					foreach($_POST['fill_que'] as $t => $f){
						if(!empty($f)){
							
							$check_r = array('question_option_id'=> $t,'que_id'=>$_POST['que_id']);
							$opttion_re = SelectData('tbl_question_option_1','question_option_id',$check_r, 1, NULL);
							
							if($opttion_re){
								$data_array = array('que_option'=> $f);
								UpdateRow('tbl_question_option_1',$data_array, 
										array('question_option_id' =>$opttion_re->question_option_id));
								$option_answer_id = $opttion_re->question_option_id;
							}else{
								$data_array = array('que_option'=> $f,'que_id'=>$_POST['que_id']);
								$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
							}
						}
						
						if($_POST['fill_ans'] == $t){
							//$anser_data = array('option_answer_id ' =>$option_answer_id);
							$anser_data = array('question_id' =>$_POST['que_id'],
										'option_answer_id ' =>$option_answer_id );
							$anser_data_o = array('question_id' =>$_POST['que_id']);
							$anser_data_check = SelectData('tbl_question_anser','que_ans_id',$anser_data_o, 1, NULL);
							if($anser_data_check){
								$res_o = UpdateRow('tbl_question_anser', $anser_data, 
								array('que_ans_id' =>$anser_data_check->que_ans_id));
							}else{
								$res_o = InsertRow('tbl_question_anser', $anser_data, 1);
							}
						}	
					}
				break;
			  case 3:
			  		foreach($_POST['mul_que'] as $t => $f){						
						if(!empty($f)){
							$check_r = array('question_option_id'=> $t,'que_id'=>$_POST['que_id']);
							$opttion_re = SelectData('tbl_question_option_1','question_option_id',$check_r, 1, NULL);
							if($opttion_re){
								$data_array = array('que_option'=> $f);
								UpdateRow('tbl_question_option_1',$data_array, 
										array('question_option_id' =>$opttion_re->question_option_id));
								$option_answer_id = $opttion_re->question_option_id;
							}else{
								$data_array = array('que_option'=>$f,'que_id'=>$_POST['que_id']);
								$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
							}
						}
					
						if(isset($_POST['mul_ans'])){
							if(in_array($t,$_POST['mul_ans'])){
								$anser_data = array('question_id' =>$_POST['que_id'],
											'option_answer_id ' =>$option_answer_id );
								$anser_data_check = SelectData('tbl_question_anser','que_ans_id',$anser_data, 1, NULL);
								if($anser_data_check){
									
								}else{
									//print_r($anser_data);
									$res_o = InsertRow('tbl_question_anser', $anser_data, 1);
									//print_r($res_o);
								}
							}	
						}
					}
				break;
			  case 4:
			  		if(!empty($_POST['short_ans'])){
						$anser_data = array('que_option ' =>$_POST['short_ans']);
					    UpdateRow('tbl_question_option_1', $anser_data, array('que_id' => $_POST['que_id']));
					}
				break;
			  case 5:
			  		foreach($_POST['pair_que'] as $t => $f){
						
						if(!empty($f)){
							
							$check_r = array('question_option_id'=> $t,'que_id'=>$_POST['que_id']);
							$opttion_re = SelectData('tbl_question_option_1','question_option_id',$check_r, 1, NULL);
							
							if($opttion_re){
								$data_array = array('que_option'=> $f);
								UpdateRow('tbl_question_option_1',$data_array, 
										array('question_option_id' =>$opttion_re->question_option_id));
								$option_answer_id = $opttion_re->question_option_id;
							}else{
								$data_array = array('que_option'=>$f,'que_id'=>$_POST['que_id']);
								$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
							}
							
							//$data_array = array('que_option'=>$f,'que_id'=>$_POST['que_id']);
							//$option_answer_id  =  InsertRow('tbl_question_option_1', $data_array, 1);
						}
						
						$tbl_question_pair = array('question_id' =>$_POST['que_id'],
											'option_id ' =>$option_answer_id,
											'pair_option' =>$_POST['pair_ans'][$t]);
											
						if(!empty($_POST['pair_ans'][$t])){	
						
							$anser_data = array('question_id' =>$_POST['que_id'],
											'option_id' =>$option_answer_id );
							$anser_data_check = SelectData('tbl_question_pair','pair_id',$anser_data, 1, NULL);
							
							if($anser_data_check){
								$data_array = array('pair_option' =>$_POST['pair_ans'][$t]);
								UpdateRow('tbl_question_pair',$data_array,array('pair_id' =>$anser_data_check->pair_id));
								$res_o = $opttion_re->question_option_id;
							}else{
								$res_o = InsertRow('tbl_question_pair', $tbl_question_pair, 1);
							}
							//InsertRow('tbl_question_pair', $tbl_question_pair, 1);
						}
					}
				break;			  
			}
			
			$this->session->set_flashdata('msg', $this->lang->line('question_updated_success'));
			$data = array('status' => true,
						  'response' => array('msg' =>''),
						  'url' => base_url('Dashboard/Teacher/question-bank'));
								  
								   
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;		
	
	}
	
	public function delete_option()
	{
		if($_POST['question_option_id']){
			$res = DeleteRow('tbl_question_option_1',array('question_option_id'=>$_POST['question_option_id']));
			$res = DeleteRow('tbl_question_anser',array('option_answer_id'=>$_POST['question_option_id']));	
			$data = array('status' => true,
						  'response' => array('msg' =>'','url' =>''));
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>'','url' =>''));
		}
		echo json_encode($data);
		exit;	
	}
	
	
	public function reuse_test()
	{
		/* get test */
	   $this->db->where('test_id', $_POST['test_id']); 
	   $query = $this->db->get('tbl_test_creation');
	   $result = $query->row();
	   /* get Questions */
	   $this->db->where('test_id', $_POST['test_id']); 
	   $query_q = $this->db->get('tbl_test_question');
	   $result_q = $query_q->result();
	   //Create duplicate array.
	   $test_uniqe_code = rand_string_url(20,5);
	   $data_array = array('test_name' => $result->test_name,
							'test_uniqe_code' => $test_uniqe_code,
							'test_access_code' => accessCode(5),
							'user_id' =>$this->get_user->user_id,
							'test_description' => $result->test_description,
							'test_instructions ' => $result->test_instructions,
							'test_form_date' => mysql_date(date('Y-m-d')),
							'test_to_date' => mysql_date(date('Y-m-d')),
							'test_time_limit' => $result->test_time_limit,
							'test_question_random' =>$result->test_question_random,
							'test_status' => 1,
							'is_status' => 1);
		//print_r($data_array);
		$last_id =  InsertRow('tbl_test_creation', $data_array, NULL);
	    //Create Question Rows
	    foreach($result_q as $key => $val){
			$tbl_test_question = array('test_id'=>$last_id,'que_id'=>$val->que_id);
			$tbl_test_question_id =  InsertRow('tbl_test_question', $tbl_test_question, 1);
		}
	    // print_r($result_q);
		$this->session->set_flashdata('msg', $this->lang->line('duplicat_test_success'));
		$data = array('status' => true,
					  'response' => array('msg' =>''),
					  'url' => base_url('Dashboard/Teacher/test-view/'.$test_uniqe_code));
		echo json_encode($data);
		exit;
	}

}