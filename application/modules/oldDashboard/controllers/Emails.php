<?php defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Emails extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct() {
        parent::__construct();
	    error_reporting(0);
	}
	
	public function sent_email($array = NULL)
	{
		// Load PHPMailer library
        $this->load->library('phpmailer_lib');     
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
		$mail->SMTPSecure = 'tls';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->Username = 'lomesh.creativelogi@gmail.com';
        $mail->Password = 'Lomesh@2019k';
		//or more succinctly:
		$mail->Host = 'tls://smtp.gmail.com:587';
        $mail->setFrom('Mgonzy@simpletestsolutions.com', $array['subject']);
        $mail->addReplyTo('Mgonzy@simpletestsolutions.com', $array['subject']);
        // Add a recipient
        $mail->addAddress($array['to']);
        // Add cc or bcc 
        //$mail->addCC('lomesh5387@gmail.com');
        $mail->addBCC('lomesh5387@gmail.com,ketki.alkurn@gmail.com');
		//Email content
		/*$htmlContent = email_header();
		$htmlContent .= email_body($array['html']);
		$htmlContent .= email_footer();*/
		
		$html = str_replace('{{maillogo}}',base_url('assets/front-design/email-images/logo.png'),$array['html']);
		$htmlContent = email_body($html);
		
        // Email subject
        $mail->Subject = $array['subject'];
        // Set email format to HTML
        $mail->isHTML(true);
        // Email body content
       /* $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";*/
        $mail->Body = $htmlContent;
        // Send email
		//$res = $mail->send();
		//echo $this->email->print_debugger();exit;
		//return $res ? true : false;
        if(!$mail->send()){
			return false;
           // echo 'Message could not be sent.';
           // echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            //echo 'Message has been sent';
			return true;
        }
	}
	 
	public function sent_email_old($array = NULL)
	{
		//Load email library
		//$this->load->library('email');
		
		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'lomesh.creativelogi@gmail.com',
			'smtp_pass' => 'Lomesh@2019k',
			'mailtype'  => 'html',
			//'smtp_crypto' => 'ssl',
			'charset'   => 'iso-8859-1'
		);
		
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		
		/*$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");*/
		
		//Email content
		//$htmlContent = email_header();
		
		$html = str_replace('{{maillogo}}',base_url('assets/front-design/email-images/logo.png'),$array['html']);
		
		$htmlContent = email_body($html);
		//$htmlContent .= email_footer();
		
		$this->email->to($array['to']);
		$this->email->from('simpletextsolution@gmail.com','Simple Test Solution');
		$this->email->bcc('lomesh.creativelogi@gmail.com');
		$this->email->subject($array['subject']);
		$this->email->message($htmlContent);
		
		//Send email
		$res = $this->email->send();
		return $res ? true : false;
		//$this->email->print_debugger();
	}
	
	public function send(){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');
        
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();
        
		//echo !extension_loaded('openssl')?"Not Available":"Available";
		
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = 'ssl://smtp.googlemail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'lomesh.creativelogi@gmail.com';
        $mail->Password = 'Lomesh@2019k';
        $mail->SMTPSecure = 'ssl';
        $mail->Port     = 465;
        
        $mail->setFrom('lomesh5387@gmail.com', 'test mail');
        $mail->addReplyTo('lomesh5387@gmail.com', 'test mail');
        
        // Add a recipient
        $mail->addAddress('lomesh5387@gmail.com');
        
        // Add cc or bcc 
        $mail->addCC('lomesh5387@gmail.com');
        $mail->addBCC('lomesh5387@gmail.com');
        
        // Email subject
        $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';
        
        // Set email format to HTML
        $mail->isHTML(true);
        
        // Email body content
        $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
            <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
        $mail->Body = $mailContent;
        
        // Send email
        if(!$mail->send()){
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Message has been sent';
        }
    }
}