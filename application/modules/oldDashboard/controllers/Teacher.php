<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Teacher extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -transctions
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		check_user_session() ? '' : redirect('login');
		$this->get_user = get_user();
		if($this->get_user->user_type != 1){ redirect('dashboard/logout'); }
		check_membership();
        parent::__construct();
        $this->load->model('Dashboard_model');
		$this->load->model('Teacher_model');
		$this->load->model('Json_model');
		// load Pagination library
    	$this->load->library('pagination');
		error_reporting(0);
		
	}
	
	public function index()
	{
		$data = array();
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Teacher Dashboard',
							  'keyword' => 'Teacher Dashboard',
							  'description' => 'Teacher Dashboard');
		/*------------------ SEO Content setting end ---------------------*/
		
		//Count Test And Question 
		//$total_test = CountRow('tbl_test_creation') < 10 ? '0'.CountRow('tbl_test_creation') : CountRow('tbl_test_creation');
		//$question_bank = CountRow('tbl_que_creation') < 10 ? '0'.CountRow('tbl_que_creation') : CountRow('tbl_que_creation');
		
		//Total User Created test counter
		$question_bank = $this->Teacher_model->total_que_count() < 10 ? '0'.$this->Teacher_model->total_que_count() : $this->Teacher_model->total_que_count();
		
		$total_test = $this->Teacher_model->total_test_count('tbl_test_creation') < 10 ? '0'.$this->Teacher_model->total_test_count('tbl_test_creation') : $this->Teacher_model->total_test_count('tbl_test_creation');
		
		$completed_test = $this->Teacher_model->check_completed_test_count() < 10 ? '0'.$this->Teacher_model->check_completed_test_count() : $this->Teacher_model->check_completed_test_count();
		
		//Upcoming test
		$upcoming_test = $this->Teacher_model->check_upcoming_test();
		$data['upcoming_test'] = $upcoming_test;
		
		//Latest completed test 
		$latest_completed_test = $this->Teacher_model->check_latest_completed_test();
		$data['latest_completed_test'] = $latest_completed_test;
		
		$data['question_bank'] = $question_bank;
		$data['total_test'] = $total_test;
		$data['completed_test'] = $completed_test;
		
		$this->load->front_view('teacher/teacher',$data);
	}
	
	public function profile()
	{
		//print_r($this->get_user);
		$data['user_details'] = SelectData('tbl_user_registration','*',array('user_id'=>$this->get_user->user_id), 1, NULL);
		$this->load->front_view('teacher/profile',$data);
	}
	
	public function update_profie()
	{
		$data = '';
		/*print_r($_FILES['profile_photo']);
		exit;*/
		if($_POST){
				/* ------------------- Photo Upload -----------------*/
				$images = '';
				$FileNames = array();
				/* ------------------- Photo Upload -----------------*/
				if($_FILES['profile_photo']['name']){
					include('lomesh/lk_image.php');
					$this->lkimg = new Lk_image();
					//	print_r($_FILES);
					$path = "./media/profile";
					$path_thumb = "./media/profile/thumb";
					if(!is_dir($path) && !is_dir($path)) //create the folder if it's not already exists
					{
						mkdir($path,0755,TRUE);
						mkdir($path_thumb,0755,TRUE);
					} 
					/* -------------- Upload Files --------------- */
					$FileNames = array();
					$file_data = array('file'=>$_FILES,
								'fileName'=>'profile_photo',
								'Folder'=>$path,
								'thumFolder'=>$path_thumb,
								'width'=>'125','height'=>'');
					$FileNames = $this->lkimg->singleUploadImages($file_data);
				}
				//print_r($FileNames['names']);
				if($FileNames){
					$images = '/media/profile/'.$FileNames;
				}else{
					$images = $_POST['profile_photo_edit'];
				}
			
			
			
				$data_array = array('first_name' => $_POST['first_name'],
								'last_name' => $_POST['last_name'],
								'email_address' => $_POST['email_address'],
								'subject_class_id' => $_POST['subject_class_id'],
								'organization' => $_POST['organization'],
								'organization_phone_number' => $_POST['organization_phone_number'],
								'your_phone' => $_POST['your_phone'],
								'your_title' => $_POST['your_title'],
								'profile_photo' => $images,
								'address' => $_POST['address'],);
				$res = UpdateRow('tbl_user_registration', $data_array,array('user_id'=>$this->get_user->user_id));
				if($res){
					$this->session->set_flashdata('msg', $this->lang->line('user_profile_success'));
					$data = array('status' => true,
								  'response' => array('msg' =>''),'url' => base_url('Dashboard/Teacher'));
				}
			
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;
	}
	
	public function change_password()
	{
		$this->load->front_view('change_password');
	}
	
	
	/*public function invite_student()
	{
		$this->load->front_view('teacher/teacher');
	}*/
	
	public function transctions()
	{
		$data['card_details'] = SelectData('tbl_user_card_details','*',array('user_id'=>$this->get_user->user_id), 1, NULL);
		$this->load->front_view('teacher/transactions',$data);
	}
	
	public function getTransctionsJson()
	{
		$sort = array('','');
		if($_POST['sSearch']){
			$search = "";
		}
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "*";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		$ord = array();
		$data_ = $this->Json_model->getJsonDataTransctions($get);
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['id'] = $i; //$c->test_id;
			$ord[$k]['card_id'] = $c->card_id;
			$ord[$k]['txn_id'] = $c->txn_id;
			$ord[$k]['payment_method'] = $c->payment_method;
			$ord[$k]['payment_status'] = $c->payment_status;
			$ord[$k]['amt'] = $c->amt;
			$ord[$k]['payment_date'] = $c->payment_date;
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	public function cancelled_payment()
	{
		//print_r($_POST);
		if($_POST){
			$_data = array('status ' =>3);
			UpdateRow('tbl_user_card_details', $_data, array('id' => $_POST['card_id']));
			$data = array('status' => true,
						  'response' => array('msg' =>$this->lang->line('cancelled_payment'),'url' =>''));
		}else{
			$data = array('status' => false,
			'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		}
		
		echo json_encode($data);
		exit;	
	}
	
	public function history()
	{
		$this->load->front_view('teacher/history');
	}
	
	public function reports()
	{
		$params = array();
		$this->load->front_view('teacher/reports',$params);
	}
	//Create test for teacher
	public function create_test($test_id = NULL)
	{
		//echo rand_string_url(20,5);
		//exit;
		$data['folders'] = SelectData('test_folders','*');

		$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_id), 1, NULL);

		$this->load->front_view('teacher/create_test',$data);
	}
	
	public function add_new_test_teacher()
	{
		$data = '';
		$base_url = '';
		/*print_r($_FILES['profile_photo']);
		exit; 	test_id*/
		if($_POST['test_id']){
			$test_uniqe_code = $_POST['test_uniqe_code'];
		}else{
			$test_uniqe_code = rand_string_url(20,5);
		}
		if($_POST){
			$data_array = array('test_name' => $_POST['test_name'],
							'test_uniqe_code' => $_POST['test_uniqe_code'] ? $_POST['test_uniqe_code'] : $test_uniqe_code,
							'test_access_code' =>  $_POST['test_access_code'] ? $_POST['test_access_code'] : accessCode(5),
							'user_id' =>$this->get_user->user_id,
							'test_description' => $_POST['test_description'],
							'test_instructions ' => $_POST['test_instructions'],
							'test_form_date' => mysql_date($_POST['test_form_date']),
							'test_to_date' => mysql_date($_POST['test_to_date']),
							'test_time_limit' => $_POST['test_time_limit'],
							'folder_id' => $_POST['folder_id'],
							'test_question_random' =>$_POST['test_question_random'],
							'test_status' => 1,
							'is_status' => 1);
							
			if($_POST['test_id']){
				$res = UpdateRow('tbl_test_creation', $data_array,array('test_id'=>$_POST['test_id']));
				$this->session->set_flashdata('msg', $this->lang->line('update_test_success'));
				$base_url =  base_url('Dashboard/Teacher/upcoming-test');
			}else{
				$res =  InsertRow('tbl_test_creation', $data_array, NULL);
				$this->session->set_flashdata('msg', $this->lang->line('create_test_success'));
				$base_url = base_url('Dashboard/Question/create_question/'.$test_uniqe_code);
			}
			
			if($res){
				$data = array('status' => true,
							  'response' => array('msg' =>''),'url' => $base_url);
			}
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		redirect('Dashboard/Teacher',$data);
	}
	
	public function question_bank()
	{
		//echo $_POST['type_question'];
		
		/*$params = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		if(isset($_POST['type_question'])){
			$params["type_question"] = $_POST['type_question'];
		}
		
		$res_data = $this->Teacher_model->get_all_questions($limit_per_page,$start_index,$_POST['type_question']);
		$total_records = $res_data['count'];
		
		if ($total_records > 0) 
        {
         	$params["results"] = $res_data['listData'];
			$params["srno"] = $start_index;
			$page_array = array('base_url' =>base_url() . 'Dashboard/Teacher/question-bank',
								'total_rows' =>$total_records,
								'per_page' =>$limit_per_page,
								'uri_segment' =>4);
            // build paging links
            $params["links"] = $this->pageConfig($page_array);
        }*/
		//print_r($res_data);
		
		$this->load->front_view('teacher/question_bank',$params);
	}
	
	public function question_details()
	{
		$res_data = $this->Teacher_model->question_details($_POST['que_id']);
		//Get Options
		$html = '';
		$res_option = que_option($res_data->que_id);
		
		$img = $res_data->que_image ? '<br><img src="'.base_url($res_data->que_image).'" width="200"><br>'  : '';
		
        switch ($res_data->que_type_id) {
			case 5:
				$res_option_pair = que_option_pair($res_data->que_id);
				$html .= '<label>'.str_replace_q($res_data->que_name).'</label><br>'.$img .'<ol class="col-md-6">';
				foreach($res_option as $v){
					if($res_data->que_type_id == 5){
					$match_pair = '<span style="color:#fff; background:green; padding:2px 4px;">'.tbl_question_anser_pair($v->question_option_id).'</span>';
							}else{
								$match_pair = '';
							}
					$html .= '<li style="list-style:decimal; margin-bottom:5px;">'.$v->que_option.''.$match_pair.'</li>';
				}
				$html .= '</ol>';
				$html .= '<ol class="col-md-6">';
				foreach($res_option_pair as $v){
					$html .= '<li style="list-style:lower-alpha; margin-bottom:5px;">'.$v->pair_option.'</li>';
				}
				$html .= '</ol>';
				break;
			case 4:
				$html .= '<label>'.str_replace_q($res_data->que_name).'</label><br>'.$img;
				$html .= '<br><strong>Correct Answer :</strong> <span style="color:green">'.$res_option[0]->que_option.'</span>';
				//$html .= '<span style="height:100px; width:100px; border:1px solid #000;">&nbsp;</span>';
				//.'<ol class="col-md-6">'
				/*foreach($res_option as $v){
					$html .= '<li style="list-style:decimal; margin-bottom:5px;">'.$v->que_option.'</li>';
				}*/
				//$html .= '</ol>';
				break;
			default:
				if($res_option){
				
					$html .= '<label>'.str_replace_q($res_data->que_name).'</label><br>'.$img .'<ol class="col-md-6">';
					foreach($res_option as $v){
						//if($v->)
						$ans = tbl_question_anser($v->question_option_id) ; 
						if($ans->option_answer_id == $v->question_option_id)
						{
							$anss ='color:green';
							$icons ='<i class="fa fa-check-circle-o text-success" ></i>';
						}else{
							$anss ='';
							$icons ='';
						}
						$html .= '<li style="list-style:decimal; margin-bottom:5px;'.$anss.'"> '.$v->que_option.'&nbsp;'.$icons.'</li>';
						
					}
					$html .= '</ol>';	
				}
		}
		$data = array('status' => true,
					  'response' => array('q_type' => $res_data->question_type_name,
					  					  'q_option' => $html));
		echo json_encode($data);
		exit;	
		//print_r($res_data);	
		//exit;
	}
	
	
	public function upcoming_test()
	{
		//$this->Teacher_model->get_total('tbl_test_creation');
		// init params
        $params = array();
        $limit_per_page = 5;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$res_data = $this->Teacher_model->get_current_page_records_upcoming('tbl_test_creation',$limit_per_page, 
								$start_index,'test_id');
		
       	$total_records = $res_data['count']; //$this->Teacher_model->get_total('tbl_test_creation');
        if ($total_records > 0) 
        {
            // get current page records
			/*$res_data = $this->Teacher_model->get_current_page_records_upcoming('tbl_test_creation',$limit_per_page, 
								$start_index,'test_id');
			print_r($res_data['listData']);
			exit;*/
         	$params["results"] = $res_data['listData'];
			$params["srno"] = $start_index;
			$page_array = array('base_url' =>base_url() . 'Dashboard/Teacher/upcoming-test',
								'total_rows' =>$total_records,
								'per_page' =>$limit_per_page,
								'uri_segment' =>4);
            // build paging links
            $params["links"] = $this->pageConfig($page_array);
        }
		$this->load->front_view('teacher/upcoming_test',$params);
	}
	
	public function completed_test()
	{
        $params = array();
		$this->load->front_view('teacher/completed_test',$params);
	}
	
	
	public function pageConfig($page_array = NULL)
	{
			$config['base_url'] = $page_array['base_url'];
            $config['total_rows'] = $page_array['total_rows'];
            $config['per_page'] = $page_array['per_page'];
            $config["uri_segment"] = $page_array['uri_segment'];
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<liclass="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="page-link">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';
			//Create Pagination and return page Links
            $this->pagination->initialize($config);
			return $this->pagination->create_links();
	}
	
	
	
	public function getUpcomingTestJson()
	{
		$sort = array('','');
		
		if($_POST['sSearch']){
			$search = "";
		}
		
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "*";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonDataUpcomingTest($get);
		/*print_r($blogs);
		exit;*/
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['test_id'] = $i; //$c->test_id;
			$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['folder_name'] = ($c->name) ? $c->name : '-';
			$ord[$k]['date'] = display_date($c->test_form_date).' to <br>'.display_date($c->test_to_date);
			$ord[$k]['test_question_random'] = random_que($c->test_question_random);
			$ord[$k]['test_time_limit'] = $c->test_time_limit.'Minutes';
			$ord[$k]['total_question'] = total_question($c->test_id);//totalQues('tbl_que_creation',array('test_id'=>$c->test_id));
			$ord[$k]['sent_invitation'] = sent_invitation($c->test_id);
			$ord[$k]['action'] = '<a href="'.base_url('Dashboard/Teacher/create-test/'.$c->test_uniqe_code).'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			
			<a href="'.base_url('Dashboard/Teacher/test-view/'.$c->test_uniqe_code).'"><i class="fa fa-eye" aria-hidden="true"></i></a><br>
											<a href="'.base_url('Dashboard/Question/create-question/'.$c->test_uniqe_code).'">Add Question</a>
			';
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	
	public function getCompletedTestJson()
	{
		$sort = array('','');
		
		if($_POST['sSearch']){
			$search = "";
		}
		
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "*";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonDataCompletedTest($get);
		/*print_r($blogs);
		exit;<a href="'.base_url('Dashboard/Teacher/create-test/'.$c->test_uniqe_code).'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="'.base_url('Dashboard/Question/create-question/'.$c->test_uniqe_code).'">Add Question</a>*/
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['test_id'] = $i; //$c->test_id;
			$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['date'] = display_date($c->test_form_date).' to <br>'.display_date($c->test_to_date);
			$ord[$k]['test_question_random'] = random_que($c->test_question_random);
			$ord[$k]['test_time_limit'] = $c->test_time_limit.'Minutes';
			$ord[$k]['total_question'] = total_question($c->test_id);//totalQues('tbl_que_creation',array('test_id'=>$c->test_id));
			$ord[$k]['sent_invitation'] = sent_invitation($c->test_id);
			$ord[$k]['student_attempt'] = student_attempt($c->test_id);
			$ord[$k]['action'] = '<a target="_blank" href="'.base_url('Dashboard/Teacher/test-reports-view/'.$c->test_uniqe_code).'" class="btn btn-sm btn-info">View Student</a>';
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	public function getReportsTestJson()
	{
		$sort = array('','');
		
		if($_POST['sSearch']){
			$search = "";
		}
		
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "*";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = 'DESC';//$_POST['sSortDir_0'];
		$get['title'] = 'test_id';//$sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonDataReportsTest($get);
		/*print_r($blogs);
		exit;<a href="'.base_url('Dashboard/Teacher/create-test/'.$c->test_uniqe_code).'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="'.base_url('Dashboard/Question/create-question/'.$c->test_uniqe_code).'">Add Question</a>*/
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['test_id'] = $i; //$c->test_id;
			$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['date'] = display_date($c->test_form_date).' to <br>'.display_date($c->test_to_date);
			$ord[$k]['test_question_random'] = random_que($c->test_question_random);
			$ord[$k]['test_time_limit'] = $c->test_time_limit.'Minutes';
			$ord[$k]['total_question'] = total_question($c->test_id);//totalQues('tbl_que_creation',array('test_id'=>$c->test_id));
			$ord[$k]['sent_invitation'] = sent_invitation($c->test_id);
			$ord[$k]['student_attempt'] = student_attempt($c->test_id);
			$ord[$k]['action'] = '<a target="_blank" href="'.base_url('Dashboard/Teacher/test-reports-view/'.$c->test_uniqe_code).'" class="btn btn-sm btn-info">View Student</a>';
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	
	
	public function getStudentResultJson()
	{
		$sort = array('','');
		//print_r($_GET['test_d']);
		
		
		if($_POST['sSearch']){
			$search = "";
		}
		
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "tbl_completed_test.*,tbl_user_registration.first_name,tbl_user_registration.last_name,tbl_user_registration.user_id";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$get['test_id'] = $_GET['test_id'];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonDataStudentResult($get);
		/*print_r($blogs);
		exit;<a href="'.base_url('Dashboard/Teacher/create-test/'.$c->test_uniqe_code).'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="'.base_url('Dashboard/Question/create-question/'.$c->test_uniqe_code).'">Add Question</a>*/
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['id_test'] = $i; //$c->test_id;
			$ord[$k]['user_id'] = $c->first_name.'&nbsp;'.$c->last_name;
			$ord[$k]['total_question'] = $c->total_question;
			$ord[$k]['attempt_question'] = $c->attempt_question;
			$ord[$k]['total_mark'] = $c->total_mark;
			$ord[$k]['time_taken'] = $c->time_taken;
			$ord[$k]['test_status'] = $c->test_status;
			$ord[$k]['action'] = '<a href="'.base_url('Dashboard/Teacher/view-test-result/'.$c->id.'/'.$_GET['test_id'].'/'.$c->user_id).'" title="View Test" target="_blank" class="btn btn-sm btn-info">View Test</a>';
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	
	public function view_test_result($com_id,$test_id,$user_id)
	{
		$data['test_que'] = $this->Teacher_model->get_all_question($test_id);
		$data['test_id'] = $test_id;
		$data['student_details'] = SelectData('tbl_user_registration','*',array('user_id'=>$user_id), 1, NULL);
		$this->load->front_view('teacher/view_test_result',$data);
		//$test_id = $data['test_details']->test_id;
	}
	
	
	
	public function test_reports_view($test_uniqe_code = NULL)
	{
		$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_uniqe_code), 1, NULL);
		$test_id = $data['test_details']->test_id;
		$data['test_que'] = $this->Teacher_model->get_all_question($test_id);
		$data['total_marks'] = $this->Teacher_model->get_all_question_marks($test_id);
		$this->load->front_view('teacher/test_reports_view',$data);
	}
	
	
	public function getQuestionBankJson()
	{
		$sort = array('','');
		
		if($_POST['sSearch']){
			$search = "";
		}
		//,tbl_test_creation.test_name
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "tbl_que_creation.*,tbl_q_type_master.question_type_name";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonQuestionBank($get);
		/*print_r($blogs);
		exit;*/
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['id'] = $i; //$c->que_id; //$c->test_id;
			$ord[$k]['question_type_name'] = $c->question_type_name;
			//$ord[$k]['date'] = date('d-m-Y',strtotime($c->test_form_date)).' - '.date('d-m-Y',strtotime($c->test_to_date));
			$ord[$k]['que_name'] = str_replace_q($c->que_name);
			//$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['que_points'] = $c->que_points;
			$ord[$k]['add_test'] = '<a href="#" class="tblbtn" aria-hidden="true" data-toggle="modal" data-target="#testModal" onClick="getTestList('.$c->que_id.')">Add in test</a>';
			//$ord[$k]['sent_invitation'] = 0;
			$ord[$k]['action'] = '<i class="fa fa-eye" aria-hidden="true" data-toggle="modal" data-target="#questionModal" onClick="getQuestionDetails('.$c->que_id.')"></i>&nbsp;&nbsp;<a href="'.base_url('Dashboard/Question/update-question/'.$c->que_id).'" class="tblbtn"><i class="fa fa-pencil-square-o " ></i></a>&nbsp;&nbsp;<a href="#" class="tblbtn" aria-hidden="true" data-toggle="modal" data-target="#testModalDelete" onClick="getTestListDelete('.$c->que_id.')"><i class="fa fa-trash " ></i></a>';
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	
	
	
	public function test_details_list()
	{
		$res_data = $this->Teacher_model->test_details_list($_POST['que_id']);
		//print_r($res_data);
		//Get Options
		//<form  url="'.base_url('Dashboard/Teacher/set_question_for_test').'" method="post" id="set_question_for_test" name="set_question_for_test"><button type="button" id="setTests"  class="btn btn-primary">Submit</button>
		if($res_data){
			$html = '';
			$html .= '<table class="table table-bordered">';
			foreach($res_data as $ki => $vi){
				$html .= '<tr><input type="hidden" name="que_id" value="'.$_POST['que_id'].'" >'; 
				$html .= '<td><input type="checkbox" id="checkbox_'.$vi->test_id.'" name="test_id[]" value="'.$vi->test_id.'"></td>'; 
				$html .= '<td><label for="checkbox_'.$vi->test_id.'">'.$vi->test_name.'</label></tr>'; 
				$html .= '</tr>';
			}
			$html .= '</table></form>'; 
			
			$data = array('status' => true,
						  'response' => array('form_detail' => $html));
		}else{
			//'.$this->lang->line('set_test_not_error').'
			$html = '<div class="col-lg-12">
						<div class="create-test" style="float:left;">
							<a href="'.base_url('Dashboard/Teacher/create-test').'">Create test</a>
						</div>
					</div>';
			$data = array('status' => false,
						  'response' => array('form_detail' => $html));
		}
					  
					  
		echo json_encode($data);
		exit;
	}
	
	public function test_details_list_delete()
	{
		$res_data = $this->Teacher_model->test_details_list_delete($_POST['que_id']);
		if($res_data){
			$html = '';
			$html .= '<table class="table table-bordered">';
			foreach($res_data as $ki => $vi){
				$html .= '<tr><input type="hidden" name="que_id" value="'.$_POST['que_id'].'" >'; 
				$html .= '<td><input type="checkbox" id="checkbox_'.$vi->test_question_id.'" name="test_question_id[]" value="'.$vi->test_question_id.'"></td>'; 
				$html .= '<td><label for="checkbox_'.$vi->test_question_id.'">'.$vi->test_name.'</label></tr>'; 
				$html .= '</tr>';
			}
			$html .= '</table></form>'; 
			$data = array('status' => true,
						  'response' => array('form_detail' => $html));
		}else{
			//'.$this->lang->line('set_test_not_error').'
			$html = '<div class="col-lg-12">
						<div class="create-test" style="float:left;">
							No test found
						</div>
					</div>';
			$data = array('status' => false,
						  'response' => array('form_detail' => $html));
		}
		echo json_encode($data);
		exit;
	}
	
	public function set_question_for_test()
	{
		//print_r($_POST);
		foreach($_POST['test_id'] as $k => $v){
			if($v){
				$check_arr = array('que_id' => $_POST['que_id'],'test_id' => $v);
				$data_a = SelectData('tbl_test_question','*',$check_arr, 1, NULL);
				if(!$data_a){
					$last_id = InsertRow('tbl_test_question',$check_arr,1);
				}
			}
		}
		if($last_id){
			$this->session->set_flashdata('msg', $this->lang->line('set_test_q_success'));
			$data = array('status' => true,
						  'response' => array('msg' =>$this->lang->line('set_test_q_success')),
						  'url' => base_url('Dashboard/Teacher/question-bank'));
		}else{
			$this->session->set_flashdata('msg', $this->lang->line('set_test_qu_error'));
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('set_test_qu_error')),
						  'url' => base_url('Dashboard/Teacher/question-bank'));
		}
		echo json_encode($data);
		exit;
	}
	
	public function delete_question_on_test()
	{
		//print_r($_POST);
		foreach($_POST['test_question_id'] as $k => $v){
			if($v){
				$last_id = DeleteRow('tbl_test_question',array('test_question_id'=>$v));
			}
		}
		if($last_id){
			$this->session->set_flashdata('msg', $this->lang->line('delete_test_q_success'));
			$data = array('status' => true,
						  'response' => array('msg' =>$this->lang->line('delete_test_q_success')),
						  'url' => base_url('Dashboard/Teacher/question-bank'));
		}
		echo json_encode($data);
		exit;
	}
	
	
	
	public function test_view($test_uniqe_code = NULL)
	{
		//echo $test_uniqe_code;
		$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_uniqe_code), 1, NULL);
		$test_id = $data['test_details']->test_id;
		$data['test_que'] = $this->Teacher_model->get_all_question($test_id);
		$data['total_marks'] = $this->Teacher_model->get_all_question_marks($test_id);
		//SelectData('tbl_que_creation','*',array('test_id'=>$test_id ), '', NULL);
		
		/*$total_marks =0;
		foreach($data['test_que'] as $value)
		{
			$total_marks += $value->que_points;
		}
		$data['total_marks'] =$total_marks;*/
	
		$this->load->front_view('teacher/test_view',$data);
		//exit;
	}
	
	public function test_completed_view($test_uniqe_code = NULL)
	{
		$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_uniqe_code), 1, NULL);
		$test_id = $data['test_details']->test_id;
		$data['test_que'] = $this->Teacher_model->get_all_question($test_id);
		$data['total_marks'] = $this->Teacher_model->get_all_question_marks($test_id);
		$this->load->front_view('teacher/test_completed_view',$data);
	}

	public function folders($folderId = null)
	{
		$data['folders'] = SelectData('test_folders','*');

		if (isset($folderId) && !empty($folderId)) {
			$data['folder'] = SelectData('test_folders','*', ['id' => $folderId], 1);
			$this->load->front_view('teacher/edit_folders', $data);
		}
		
		$this->load->front_view('teacher/folders', $data);
	}
	
	public function store_folder($folderId = null)
	{

		if (isset($folderId) && !empty($folderId)) {
			$data['folder'] = SelectData('test_folders','*', ['id' => $folderId], 1);

			redirect('/Dashboard/Teacher/folders/'. $folderId);
		}

		$this->load->helper('url'); 
         
        $folderName = $this->input->post('folder_name');
        $folderId 	= $this->input->post('folder_id');

		$this->form_validation->set_rules('folder_name', 'Folder Name', 'required');

		if($this->form_validation->run() === false) {
			$this->session->set_flashdata('msg', 'Please enter folder name');
			redirect('/Dashboard/Teacher/folders');

		} else {
			
			if (isset($folderId) && !empty($folderId)) {
				$data_array = array(
				'name' 		 => $folderName,
				'created_at' => date('Y-m-d H:i:s')
				
				);
				$res = UpdateRow('test_folders', $data_array, array('id' => $folderId));

				$this->session->set_flashdata('info', 'Folder renamed');
				redirect('/Dashboard/Teacher/folders'); 

			} else {
				$data_array = array(
				'name' 		 => $folderName,
				'created_at' => date('Y-m-d H:i:s')
				
				);

				$res =  InsertRow('test_folders', $data_array, NULL);

				if($res){
					$this->session->set_flashdata('info', 'Folder created');
					redirect('/Dashboard/Teacher/folders'); 
				}
			}
		} 
	}

	public function delete_folder($folderId)
	{
		$res = DeleteRow('test_folders',array('id' => $folderId));
		$updateFolderId = UpdateRow('tbl_test_creation', ['folder_id' => null ], array('folder_id' => $folderId));

		$this->session->set_flashdata('msg', 'Folder deleted');

		redirect('/Dashboard/Teacher/folders');
	}
}	