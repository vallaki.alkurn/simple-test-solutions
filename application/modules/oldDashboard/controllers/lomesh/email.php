<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		//session_start();
		$this->load->library('session');
		$this->load->library('email');
		$this->load->helper('url');
		//$this->load->model('user_reg_model');
		$this->load->helper('form');
		$this->load->helper('array');
		$this->load->helper('email');
		////$this->load->model('dashbord_model');
		//$this->load->model('login_model');
		//$this->load->model('jobsdetails_model');
		//$this->load->model('home_vertical_model');
		//$this->load->model('postjob_model');
		//$this->load->library('upload');
		$this->load->library('form_validation'); 
		//$this->_init();
	}
	 
	/*private function _init()
	{
		$this->output->set_template('gips/default');
	} */
	
	public function SendMail($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:6px; background:#ee3130;"><a href="http://insurancetimesea.com/" style="padding:10px 5px;"><img src="'.base_url('media/logo.png').'" /></a><span style="float:right; color:#fff; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#d61a19; color:#fff;"><a href="http://insurancetimesea.com/" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by Insurance Times '.date('Y').'</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		//echo $html;
		//exit;
		/////////////// MAIL Function /////////////////////////////
		$this->email->from($from, 'insurancetimesea.com');
		$this->email->to($to);
		$this->email->cc('lomesh@mirackle.com');
		$this->email->bcc('lomesh@mirackle.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	
	public function SendorderMail($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFFFFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:5px; background:#FFF;"><h2 style="border-bottom:1px solid #ccc; padding:5px; color:#fff; font-weight:bold; margin-left:10px;"><a href="'.base_url().'"><img src="'.base_url('skins/caterazzi/images/mail-logo.jpg').'" /></a><span style="float:right; color:#F00; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></h2> </td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#333333; color:#fff;"><a href="'.base_url().'" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by caterazzi 2015</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		//echo $html;
		//exit;
		/////////////// MAIL Function /////////////////////////////
		$this->email->from('no-reply@caterazzi.com', 'Caterazzi');
		$this->email->to($to);
		$this->email->cc($cc);
		$this->email->bcc('lomesh@mirackle.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	
	public function JobSendMail($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFFFFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:5px; background:#FFF;"><h2 style="border-bottom:1px solid #ccc; padding:5px; color:#fff; font-weight:bold; margin-left:10px;"><a href="'.base_url().'"><img src="'.base_url('skins/caterazzi/images/mail-logo.jpg').'" /></a><span style="float:right; color:#F00; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></h2> </td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#333333; color:#fff;"><a href="'.base_url().'" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by caterazzi 2015</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		/////////////// MAIL Function /////////////////////////////
		$this->email->from('mail-noreply@caterazzi.com', 'Caterazzi');
		$this->email->to($to);
		$this->email->cc('support@caterazzi.com');
		$this->email->bcc('lomesh@mirackle.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	public function PasswordMail($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFFFFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:5px; background:#FFF;"><h2 style="border-bottom:1px solid #ccc; padding:5px; color:#fff; font-weight:bold; margin-left:10px;"><a href="'.base_url().'"><img src="'.base_url('skins/caterazzi/images/mail-logo.jpg').'" /></a><span style="float:right; color:#F00; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></h2> </td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p><br><p>Regards <br>Team Caterazzi</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#333333; color:#fff;"><a href="'.base_url().'" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by caterazzi 2015</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		//echo $html;
		//exit;
		/////////////// MAIL Function /////////////////////////////
		$this->email->from('mail-noreply@caterazzi.com', 'Caterazzi');
		$this->email->to($to);
		//$this->email->cc('support@caterazzi.com');
		//$this->email->bcc('info.lomesh.web.developer@gmail.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	public function SendForms($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFFFFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:5px; background:#FFF;"><h2 style="border-bottom:1px solid #ccc; padding:5px; color:#fff; font-weight:bold; margin-left:10px;"><a href="'.base_url().'"><img src="'.base_url('skins/caterazzi/images/mail-logo.jpg').'" /></a><span style="float:right; color:#F00; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></h2> </td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#333333; color:#fff;"><a href="'.base_url().'" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by caterazzi 2015</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		/////////////// MAIL Function /////////////////////////////
		$this->email->from('no-reply@caterazzi.com', 'Caterazzi');
		$this->email->to($to);
		//$this->email->cc('support@caterazzi.com');
		//$this->email->bcc('support@caterazzi.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	
	public function InboxMail($from = NULL,$to= NULL,$cc= NULL,$bcc= NULL,$subject= NULL,$message= NULL)
	{
		/********************** Email head ************************************/
		$html = '<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc; background:#FFFFFF; font-family:Arial; font-size:12px;"><thead><tr style=""><td style="padding:5px; background:#FFF;"><h2 style="border-bottom:1px solid #ccc; padding:5px; color:#fff; font-weight:bold; margin-left:10px;"><a href="'.base_url().'"><img src="'.base_url('skins/caterazzi/images/mail-logo.jpg').'" /></a><span style="float:right; color:#F00; font-size:12px; margin-top:3px;">'.date('d-M-Y').'</span></h2> </td></tr></thead>';
		/********************** Email Body ************************************/
		$html .= '<tbody><tr><td style="padding:25px;"><p>'.$message.'</p></td></tr></tbody>';
		/********************** Email Footer ************************************/
		$html .=' <tfoot style=" "><tr><td align="center" style="padding:10px; background:#333333; color:#fff;"><a href="'.base_url().'" style="color:#fff; text-decoration:none; cursor:pointer;"> All rights reserved by caterazzi 2015</a> </td></tr></tfoot></table>';
		/******************************************************************************/
		//echo $html;
		//exit;
		/////////////// MAIL Function /////////////////////////////
		$this->email->from('no-reply@caterazzi.com', 'Caterazzi');
		$this->email->to($to);
		$this->email->cc('support@caterazzi.com');
		$this->email->bcc('lomesh@mirackle.com');
		$this->email->set_mailtype('html');
		$this->email->subject($subject);
		$this->email->message($html);
		
		$mail = $this->email->send();
		return $mail;
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */