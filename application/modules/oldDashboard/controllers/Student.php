<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Student extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		check_user_session() ? '' : redirect('login');
		$this->get_user = get_user();
		if($this->get_user->user_type != 2){ redirect('dashboard/logout'); }
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->model('Student_model');
         $this->load->library('pagination');
		error_reporting(0);
	}
	
	public function index()
	{
		$data = '';
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Student Dashboard',
							  'keyword' => 'Student Dashboard',
							  'description' => 'Student Dashboard');
		$header_array['total_no_upcommig_test'] =CountRow('tbl_invited_exam_requests',array('student_id'=>$this->get_user->user_id,'request_status'=>'0'));		
		$total_invite = $this->Student_model->getStudentInviteData();
		//print_r($total_invite);exit;
		$header_array['total_invite'] =count($total_invite);		
		$header_array['total_no_completed_test']=CountRow('tbl_completed_test',array('user_id'=>$this->get_user->user_id));		
		$header_array['test_history'] = SelectAll('tbl_completed_test','*',array('user_id'=>$this->get_user->user_id), 3, array('col'=>'id','order'=>'desc'));
		$header_array['invite_test'] = $total_invite;

		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('student/student',$header_array);
	}
	
	public function profile()
	{
		$data['user_details'] = SelectData('tbl_user_registration','*',array('user_id'=>$this->get_user->user_id), 1, NULL);
		$this->load->front_view('student/profile',$data);
	}
	
	public function update_profie()
	{
		$data = '';
		/*print_r($_FILES);
		exit;*/
		if($_POST){
			
				/* ------------------- Photo Upload -----------------*/
				$images = '';
				$FileNames = array();
				/* ------------------- Photo Upload -----------------*/
					
				if($_FILES['profile_photo']['name']){
					if($_FILES['profile_photo']['size'] > 10485760){
						$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('file_size')),'url' =>'');
						echo json_encode($data);
						exit;
					}else{
						include('lomesh/lk_image.php');
						$this->lkimg = new Lk_image();
						$path = "./media/profile";
						$path_thumb = "./media/profile/thumb";
						if(!is_dir($path) && !is_dir($path)) //create the folder if it's not already exists
						{
							mkdir($path,0755,TRUE);
							mkdir($path_thumb,0755,TRUE);
						} 
						/* -------------- Upload Files --------------- */
						$FileNames = array();
						$file_data = array('file'=>$_FILES,
									'fileName'=>'profile_photo',
									'Folder'=>$path,
									'thumFolder'=>$path_thumb,
									'width'=>'125','height'=>'');
						$FileNames = $this->lkimg->singleUploadImages($file_data);
					}
				}
				//print_r($FileNames['names']);
				if($FileNames){
					$images = '/media/profile/'.$FileNames;
				}else{
					$images = $_POST['profile_photo_edit'];
				}
			
			
			
				$data_array = array('first_name' => $_POST['first_name'],
								'last_name' => $_POST['last_name'],
								'email_address' => $_POST['email_address'],
								'standard_class_id' => $_POST['standard_class_id'],
								'school_collage' => $_POST['school_collage'],
								'profile_photo' => $images);
				$res = UpdateRow('tbl_user_registration', $data_array,array('user_id'=>$this->get_user->user_id));
				if($res){
					$this->session->set_flashdata('msg', $this->lang->line('user_profile_success'));
					$data = array('status' => true,
								  'response' => array('msg' =>''),'url' => base_url('Dashboard/Student'));
				}
			
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;
	}
	
	public function change_password()
	{
		$this->load->front_view('change_password');
	}
	
	public function invited_test()
	{	
		$params = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = getInvitedExamList(1);
        if ($total_records > 0) 
        {
            // get current page records
            $params["results"] = getInvitedExamList(NULL, $limit_per_page, $start_index);
            $config['base_url'] = base_url() . 'Dashboard/Student/invited-test';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;
			  
			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<liclass="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="page-link">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';
			
            $this->pagination->initialize($config);
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }
			

		$this->load->front_view('student/invited_test',$params);
	}
	
	
	public function results()
	{
		$this->load->front_view('student/results');
	}

	public function updateExamStatus()
	{
		$request_status = $this->input->post('request_status');
		$request_id = $this->input->post('request_id');
		$updateStatus = $this->Student_model->updateStatus($request_status, $request_id);
		if($updateStatus)
		{	
			
			//Set notification by user---------
			$get_user_d = SelectData('tbl_invited_exam_requests','teacher_id',array('id'=>$request_id),1);
			if($this->input->post('request_status') == 1 ){
				$notification_msg = str_replace('{{username}}',
				$this->get_user->first_name.'&nbsp;'.$this->get_user->last_name,
				$this->lang->line('set_notification_accept'));
			}else{
				$notification_msg = str_replace('{{username}}',
				$this->get_user->first_name.'&nbsp;'.$this->get_user->last_name,
				$this->lang->line('set_notification_decline'));			
			}
			$notification_data = array('sender_id' => $this->get_user->user_id,
										'reciver_id' =>$get_user_d->teacher_id,
										'notification_msg' => $notification_msg,
										'status' => 0);
			$res_not =  InsertRow('tbl_notification', $notification_data, NULL);
			/*------------------------------------------------*/	
			
			$message = ($this->input->post('request_status') == 1) ? $this->lang->line('request_accept') : $this->lang->line('request_decline');
			$arr = array('code'=>200,'message'=>$message);
		} else {
			$arr = array('code'=>100,'message'=>'error');
		}
		echo json_encode($arr);
		exit;
	}

	public function start_exam($request_id)
	{
		if($request_id) 
		{	
			$data['exam'] = getInvitedExamDetails($request_id);
			$this->load->front_view('student/start_exam',$data);	
		}
	}

	public function exam($exam_id, $request_id)
	{	
		//Set notification by user---------
		if(CountRow('tbl_completed_test',array('user_id'=>$this->get_user->user_id,'test_id'=>$exam_id)) > 0)
		{
			redirect('Dashboard/Student/invited-test');
		}else{

			$get_user_d = SelectData('tbl_invited_exam_requests','teacher_id',array('id'=>$request_id),1);
				$notification_msg = str_replace('{{username}}',
				$this->get_user->first_name.'&nbsp;'.$this->get_user->last_name,
				$this->lang->line('set_notification_start'));
			$notification_data = array('sender_id' => $this->get_user->user_id,
										'reciver_id' =>$get_user_d->teacher_id,
										'notification_msg' => $notification_msg,
										'status' => 0);
			$res_not =  InsertRow('tbl_notification', $notification_data, NULL);
			/*------------------------------------------------*/
			$data['exam'] =$this->Student_model->getTestDetailsById($exam_id);
			$data['que_list'] = $this->examquestionlist($exam_id, $request_id);
			$data['request_id'] = $request_id;
			$data['last_time_save'] = SelectAll('tbl_test_last_time','*',array('user_id'=>$this->get_user->user_id,'test_id'=>$exam_id), 1, NULL);
			$last_que_save = SelectAll('tbl_button_pallate','*',array('user_id'=>$this->get_user->user_id,'test_id'=>$exam_id), 1, array('col'=>'id','order'=>'desc'));
			$data['last_que_save'] = $last_que_save;
			//print_r($last_que_save);exit;
			$total_marks =0;

			foreach($data['que_list'] as $row)
			{
				   $total_marks +=$row->que_points;;
			}
			$data['total_marks'] = $total_marks;
			 $data['controller']=$this; 
			//$this->load->front_view('student/exam',$data);
		  $this->load->view('exam1',$data);
		}
	}

  public function examquestionlist($exam_id, $request_id)
	{

		$getQuestion = $this->Student_model->getExamquestion($exam_id);
        $optionarray = array();

		foreach($getQuestion as $key=>$que)
		{
			$getQuestion[$key]->options = $this->Student_model->getExamoption($que->que_id);
		
			$alpha = range( 'A', 'G' );
			foreach($que->options as $key1=>$val)
			{
                $getQuestion[$key]->options[$key1]->alpha =  $alpha[$key1];    
;			}
		}
 //print_r($getQuestion);exit;
      return $getQuestion;
	}
	
	public function test_history()
	{
		$data['results'] = $this->Student_model->getCompletedTest();
		//print_r($data['results']);exit;
		$this->load->front_view('student/test_history',$data);	
	}
	public function test_history_details($test_uniqe_code = NULL)
	{
		$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_uniqe_code), 1, NULL);
		$test_id = $data['test_details']->test_id;
		$data['test_que'] = $this->Student_model->getExamquestion($test_id);
		$this->invited_by = $this->Student_model->getInvitedBy($test_id);
		
		//$data['test_que'] = SelectData('tbl_que_creation','*',array('test_id'=>$test_id ), '', NULL);
		$total_marks =0;
		foreach($data['test_que'] as $value)
		{
			$total_marks += $value->que_points;
		}	
		$data['total_marks'] =$total_marks;
	
			//print_r($data['results']);exit;
			$this->load->front_view('student/test_history_details',$data);	
	}
		function shuffle_me($sorted_array) { 
			$shuffled_array = array();

				$keys = array_keys($sorted_array);
				shuffle($keys);

				foreach ($keys as $key)
				{
					$shuffled_array[$key] = $sorted_array[$key];
				}
				return $shuffled_array;
	} 
	public function getTestResultJson()
	{
		$sort = array('','');
		
		if($_POST['sSearch']){
			$search = "";
		}
		//,tbl_test_creation.test_name
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "tbl_completed_test.*,tbl_test_creation.test_name,tbl_test_creation.test_time_limit";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Student_model->getJsonTestResultData($get);
		
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['id'] = $i; //$c->test_id;
			$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['test_time_limit'] = $c->test_time_limit;
			$ord[$k]['created'] = $c->created;
			//$ord[$k]['date'] = date('d-m-Y',strtotime($c->test_form_date)).' - '.date('d-m-Y',strtotime($c->test_to_date));
			$ord[$k]['total_question'] = $c->total_question;
			//$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['grade'] = $c->total_mark . ' / ' . $c->max_mark .' pts';
			$ord[$k]['percentage'] = ($c->total_mark * 100) / $c->max_mark .'%';
			$ord[$k]['action'] = "<a href='".base_url()."Home/Home_page/generate_pdf/".$c->id."'><i class='fa fa-download'></i></a>";
			//$ord[$k]['sent_invitation'] = 0;
			
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
}