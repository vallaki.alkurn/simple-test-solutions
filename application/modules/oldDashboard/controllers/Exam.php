<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Exam extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		check_user_session() ? '' : redirect('login');
		$this->get_user = get_user();
		if($this->get_user->user_type != 2){ redirect('dashboard/logout'); }
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->model('Student_model');
        $this->load->model('Exam_model');
        $this->load->library('pagination');
        //$this->load->library('pdf');
	}
	
	public function index()
	{
		$data = '';
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Student Dashboard',
							  'keyword' => 'Student Dashboard',
							  'description' => 'Student Dashboard');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('student/student',$header_array);
	}
	
	public function saveQAns()
	{
		error_reporting(0);
       if(isset($_POST))
      {
				 $data=array(
						 'user_id' =>$_POST['user_id1'],
						 'test_id' =>$_POST['testCode'],
						 'question_id' =>$_POST['q_id2'],
						 'answer_id' =>$_POST['rdopt'],
						 'save_time' =>$_POST['savetime'],
						 'exam_status' =>'0',
						
				 );
			 $res =  $this->Exam_model->getQueAnswered($data);
           if($res->option_answer_id == $_POST['rdopt'])
			{
				  $data['answer_status'] ='right';
			}else{
                 $data['answer_status'] ='wrong';
			}
			if(count($this->Exam_model->getSavedAnswered($data)) > 0){
                
                 $this->Exam_model->updateAnswered($data);
               $msg="data updated";
			}else{
                $this->Exam_model->saveAnswered($data);
                 $msg="data saved";
			}
		

		 }else{
					   $msg="data not saved";
		}
	
          echo json_encode($msg);
	}
	
	public function save_pallate()
	{
       if(isset($_POST))
      {
				 $data=array(
						 'user_id' =>$_POST['user_id'],
						 'test_id' =>$_POST['test_id'],
						 'className' =>$_POST['className'],
						 'save_time' =>$_POST['save_time'],
						 'ans_time' =>$_POST['ans_time'],
						 'que_id' =>$_POST['que_id'],
						  'subject' =>$_POST['subject'],
						 'fragment' =>$_POST['fragment'],
						 'que_no' =>$_POST['que_no'],
				 );
			   
			if(count($this->Exam_model->getSavedPallate($data)) > 0){
                 $this->Exam_model->updatePallate($data);
               $msg="data updated";
			}else{
                $this->Exam_model->savePallate($data);
                 $msg="data saved";
			}
			if($_POST['que_type'] == 'Fill in the blank'){
				$select_data = $_POST['sel_arr'];
				$this->save_fill_in_blank_ans($_POST['user_id'],$_POST['test_id'],$_POST['que_id'],$select_data,$_POST['save_time']);
				
			}else if($_POST['que_type'] == 'Matching')
			{
				if(isset($_POST['matching_arr'])){
				$matching_data = $_POST['matching_arr'];
				$this->save_matching_ans($_POST['user_id'],$_POST['test_id'],$_POST['que_id'],$matching_data,$_POST['save_time']);
				}
			}
			else if($_POST['que_type'] == 'Short Answer'){
				//echo "hi12";exit;
				if(isset($_POST['short_ans'])){
			  $this->save_short_ans($_POST['user_id'],$_POST['test_id'],$_POST['que_id'],$_POST['short_ans'],$_POST['save_time']);
				}
				
			}
			
			
			
		 }else{
					   $msg="data not saved";
		}
	
          echo json_encode($msg);
	}
public function save_short_ans($user_id,$test_id,$que_id,$answer,$save_time)
{
	 $data=array(
								 'user_id' =>$user_id,
								 'test_id' =>$test_id,
								 'question_id' =>$que_id,
								 'answer_id' =>$answer,
								 'save_time' =>$save_time,
								 'exam_status' =>'0',
								
						 );
					 $res =  $this->Exam_model->getQueAnswered($data);
					//print_r($res);exit;
				 //  if($res->que_option == $answer)
					//{
				$data_arr =array();
				$arr =explode(',',$res->que_option);
			foreach($arr as $key=>$row)
			{
				if(strpos($answer, $row) == true){}
				//if (preg_match('/\$row\b/', $answer)){}
			     else
				{
					$data_arr[]='not_match'.$key;
				}
			}		
		
				//if(preg_match("/{$answer}/i", $res->que_option)) {
				if(count($data_arr) == 0) {
						  $data['answer_status'] ='right';
					}else{
						 $data['answer_status'] ='wrong';
					}
					if(count($this->Exam_model->getSavedAnswered($data)) > 0){
						
						 $this->Exam_model->updateAnswered($data);
					   $msg="data updated";
					}else{
						//echo "hi";exit;
						$this->Exam_model->saveAnswered($data);
						 $msg="data saved";
					}
					return $msg;
}
public function save_matching_ans($user_id,$test_id,$que_id,$matching_data,$save_time)
{
	                   $data=array(
								 'user_id' =>$user_id,
								 'test_id' =>$test_id,
								 'question_id' =>$que_id,
								 'answer_id' =>serialize($matching_data),
								 'save_time' =>$save_time,
								 'exam_status' =>'0',
								
						 );
					// $res =  $this->Exam_model->getQueAnswered($data);
					  $res =  $this->Exam_model->getQuePairs($data);
					 // print_r($res);exit;
					  //print_r($matching_data);exit;
					 $chk_arr=array();
					 $avl_ans =array();
					 $send_ans =array();
					 foreach($res as $row)
					 {
						 $avl_ans[] = $row->option_id.'_'.$row->pair_id;
					 }
					 foreach($matching_data as $row1)
						 {
							 $send_ans[] = $row1['option_id'].'_'.$row1['pair_id'];
						 }
					// print_r($matching_data);exit;
					 foreach($send_ans as $row2)
					 {
						 if(!in_array($row2,$avl_ans))
						 {
							 $chk_arr[]='wrong';
						 }
					 }
					if(count($chk_arr) == 0)
					{
						  $data['answer_status'] ='right';
					}else{
						 $data['answer_status'] ='wrong';
					}
					if(count($this->Exam_model->getSavedAnswered($data)) > 0){
						
						 $this->Exam_model->updateAnswered($data);
					   $msg="data updated";
					}else{
						$this->Exam_model->saveAnswered($data);
						 $msg="data saved";
					}
					return $msg;
}
public function save_fill_in_blank_ans($user_id,$test_id,$que_id,$answer,$save_time)
{
	 $data=array(
								 'user_id' =>$user_id,
								 'test_id' =>$test_id,
								 'question_id' =>$que_id,
								 'answer_id' =>serialize($answer),
								 'save_time' =>$save_time,
								 'exam_status' =>'0',
								
						 );
					 $res =  $this->Exam_model->getQueAnswered($data);
				   if($res->option_answer_id == $answer[0])
					{
						  $data['answer_status'] ='right';
					}else{
						 $data['answer_status'] ='wrong';
					}
					if(count($this->Exam_model->getSavedAnswered($data)) > 0){
						
						 $this->Exam_model->updateAnswered($data);
					   $msg="data updated";
					}else{
						$this->Exam_model->saveAnswered($data);
						 $msg="data saved";
					}
					return $msg;
}
public function attempted()
{
   
  $data =$this->Exam_model->getAttemptedPallate($_POST['user_id1'],$_POST['testCode'],$_POST['subject']);
    echo count($data);exit;
}
public function test_submit()
{
   if(isset($_POST))
  {
           $user_id = $_POST['user_id'];
        $test_id = $_POST['test_id'];
      
        $submitTime = $_POST['submitime'];
        $request_id = $_POST['request_id'];
		if(CountRow('tbl_completed_test',array('test_id'=>$test_id,'request_id'=>$request_id,'user_id'=>$this->get_user->user_id)) == 0){
            $data1 = $this->Exam_model->submitTest($user_id, $test_id, $submitTime,$request_id); 
		}
		
			$notification_msg = str_replace('{{username}}',
				$this->get_user->first_name.'&nbsp;'.$this->get_user->last_name,
				$this->lang->line('set_notification_submit'));
	$notification_data = array('sender_id' => $this->get_user->user_id,
								'reciver_id' => $request_id,
								'notification_msg' => $notification_msg,
								'status' => 0);
	$res_not =  InsertRow('tbl_notification', $notification_data, NULL);
        
        $msg="Thank you for appearing in the test. Your test has been submitted. Click below to proceed further.";
              
        }else{
     $msg="not saved";
  }
     echo json_encode($msg);
               
}
	public function save_last_time() {
		if($_POST){
		$this->Exam_model->Save_last_time($_POST['test_id'],$_POST['save_time']);
		 echo json_encode('Saved');
		}
	}
	public function test_Summery($exam_id) {
        $test_id = $exam_id;
        $data['test_id'] = $test_id;
       $data['results'] = $this->Exam_model->getTestReport($exam_id,$this->get_user->user_id);
	  
          $this->load->front_view('student/certificate',$data);	
       
    }
	public function generate_pdf($id)
	{
		 $data = [];
		 // $data['results'] = $this->Exam_model->getTestReportById($id);
		//load the view and saved it into $html variable
		$html=$this->load->view('certificate1', $data, true);

        //this the the PDF filename that user will get to download
		$pdfFilePath = "Certificate.pdf";

        //load mPDF library
		$this->load->library('m_pdf');
$mpdf = new mPDF('c', 'A4-L'); 
       //generate the PDF from the given html
		$mpdf->WriteHTML($html);

        //download it.
		$mpdf->Output($pdfFilePath, "D");	
	}
		
}