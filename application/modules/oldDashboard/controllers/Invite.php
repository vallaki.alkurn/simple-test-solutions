<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Invite extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		check_user_session() ? '' : redirect('login');
		$this->get_user = get_user();
		if($this->get_user->user_type != 1){ redirect('dashboard/logout'); }
        parent::__construct();
        $this->load->model('Dashboard_model');
		$this->load->model('Teacher_model');
		$this->load->model('Json_model');
		// load Pagination library
    	$this->load->library('pagination');
		
		include('Emails.php');
	    $this->myEmail = new Emails();
		error_reporting(0);
	}
	
	public function index()
	{
		
		
	}
	
	public function invite_student()
	{
		/*$params = array();
        $limit_per_page = 5;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$res_data = $this->Teacher_model->get_current_page_records_upcoming('tbl_test_creation',$limit_per_page, 
								$start_index,'test_id');
		
       	$total_records = $res_data['count']; //$this->Teacher_model->get_total('tbl_test_creation');
        if ($total_records > 0) 
        {
            // get current page records
			/*$res_data = $this->Teacher_model->get_current_page_records_upcoming('tbl_test_creation',$limit_per_page, 
								$start_index,'test_id');
         	$params["results"] = $res_data['listData'];
			$params["srno"] = $start_index;
			$page_array = array('base_url' =>base_url() . 'Dashboard/Teacher/upcoming-test',
								'total_rows' =>$total_records,
								'per_page' =>$limit_per_page,
								'uri_segment' =>4);
            // build paging links
            $params["links"] = $this->pageConfig($page_array);
        }*/
		$this->load->front_view('teacher/invite_test',$data);
	}
	
	
	public function getUpcomingTestJson()
	{
		$sort = array('','');
		if($_POST['sSearch']){
			$search = "";
		}
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "*";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getJsonDataUpcomingTest($get);
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			$ord[$k]['test_id'] = $i; //$c->test_id;
			$ord[$k]['test_name'] = $c->test_name;
			$ord[$k]['date'] = display_date($c->test_form_date).' to<br> '.display_date($c->test_to_date);
			$ord[$k]['test_question_random'] = random_que($c->test_question_random);
			$ord[$k]['test_time_limit'] = $c->test_time_limit.'&nbsp;Minutes';
			$ord[$k]['total_question'] = total_question($c->test_id);//totalQues('tbl_que_creation',array('test_id'=>$c->test_id));
			$ord[$k]['sent_invitation'] = sent_invitation($c->test_id); //totalInvite(array('exam_id'=>$c->test_id));
			$ord[$k]['action'] = '<div class="btn-sent"><a href="'.base_url('Dashboard/Invite/student-list/'.$c->test_uniqe_code).'">Send Invitation</a></div>';	
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	public function student_list($test_uniqe_code = NULL)
	{
		if($test_uniqe_code){
			$data['test_details'] = SelectData('tbl_test_creation','*',array('test_uniqe_code'=>$test_uniqe_code), 1, NULL);
		}else{
			redirect(base_url('Dashboard/Invite/invite-student'),'refresh');	
		}
		//print_r($data);
		$this->load->front_view('teacher/student_list',$data);
	}
	
	public function getStudentListJson()
	{
		$sort = array('','');
		if($_POST['sSearch']){
			$search = "";
		}
		$get['limit'] = $_POST['iDisplayLength'];
		$get['fields'] = "tbl_user_registration.user_id,
						tbl_user_registration.first_name,
						tbl_user_registration.last_name,
						tbl_user_registration.email_address,
						tbl_standard_class.standard_class_name";
		$get['offset'] = $_POST['iDisplayStart'];
		$get['search'] = $search;
		$get['order'] = $_POST['sSortDir_0'];
		$get['title'] = $sort[$_POST['iSortCol_0']];
		$sr = $get['offset'];
		
		$ord = array();
		
		$data_ = $this->Json_model->getStudentListJson($get);
		$total_rec = array_pop($data_);
		$i = 1;
		foreach($data_ as $k => $c){
			//$ord[$k]['id'] = $c->user_id;
			$ord[$k]['id'] = $c->user_id;//$c->user_id; //$c->test_id;
			$ord[$k]['srno'] = $i;
			$ord[$k]['name'] = $c->first_name.'&nbsp;'.$c->last_name ;
			$ord[$k]['email_address'] = $c->email_address;
			$ord[$k]['standard_class'] = $c->standard_class_name;
			$i++;
		}
		$res['recordsTotal'] = count($ord);
		$res['recordsFiltered'] = $total_rec;
		$res['data'] = $ord;
		echo json_encode($res);
		exit;
	}
	
	public function sent_invitation()
	{
		$data = '';
		$base_url = '';
		if($_POST){
			foreach($_POST['user_id'] as $k => $v){
				if($v){
					
					$cond = array('student_id' =>$v,'exam_id' =>$_POST['exam_id']);
				    $check_d = SelectData('tbl_invited_exam_requests', 'student_id',$cond, 1,'');
					//print_r($check_d);exit;
					if(empty($check_d)){
						
						$data_array = array('student_id' => $v,
										'teacher_id' => $this->get_user->user_id,
										'exam_id' =>   $_POST['exam_id'],
										'request_date_time' => mysql_date(date('Y-m-d')),
										'request_status' =>0);
						$res =  InsertRow('tbl_invited_exam_requests', $data_array, NULL);
						
						$cond_de = array('user_id' =>  $v);
						$check_d = SelectData('tbl_user_registration', 
									'first_name,last_name,email_address', 
									$cond_de, 1,'');
									
						//Set notification by user---------
						$notification_msg = str_replace('{{username}}',
									$this->get_user->first_name.'&nbsp;'.$this->get_user->last_name,
									$this->lang->line('set_notification_test'));
						$notification_data = array('sender_id' => $this->get_user->user_id,
													'reciver_id' => $v,
													'notification_msg' => $notification_msg,
													'status' => 0);
						$res_not =  InsertRow('tbl_notification', $notification_data, NULL);
						/*------------------------------------------------*/			
						
						$get_temp =  file_get_contents(APPPATH.'views/mailer/send_invitation_for_exam.html');
						$rep_str = str_replace('{{username}}',$check_d->first_name.'&nbsp;'.$check_d->last_name,$get_temp);
						$rep_str = str_replace('{{link}}',base_url('Dashboard/Student/invited-test'),$rep_str);
						
						//sent mail after register by user
						$email_sent = array('to' => $check_d->email_address,
								  'bcc' => '',
								  'html' => $rep_str,
								  'subject' => 'Invited for the test on Simple Test Solution. ');
						//sent_mail($email_sent);
						$this->myEmail->sent_email($email_sent);
						
						//echo $rep_str;
					}else{
						$data = array('status' => false,
								  'response' => array('msg' =>$this->lang->line('sent_invitation_error')),'url' => '');
					}
					
				}
			}
			if($res){
				$this->session->set_flashdata('msg', $this->lang->line('create_test_success'));
				$data = array('status' => true,
							  'response' => array('msg' =>$this->lang->line('sent_invitation_success')),'url' => '');
			}
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;	
	}
	
	
	public function sent_invitation_new()
	{
		$data = '';
		$base_url = '';
		/*print_r($_FILES['profile_photo']);
		exit; 	test_id*/
		if($_POST){
				$cond = array('student_email' => $_POST['student_email'],'exam_id' =>$_POST['exam_id']);
				$check_d = SelectData('tbl_invite_new_student', 'student_email', $cond, 1,'');
				//print_r($check_d);
				if($check_d){
					$data = array('status' => false,
								  'response' => array('msg' =>$this->lang->line('sent_invitation_error')),'url' => '');
				}else{
					$data_array = array('student_name' => $_POST['student_name'],
										'student_email' => $_POST['student_email'],
										'teacher_id' => $this->get_user->user_id,
										'exam_id' =>   $_POST['exam_id'],
										'invite_date' => mysql_date(date('Y-m-d')));
					$res =  InsertRow('tbl_invite_new_student', $data_array, NULL);
					
					$get_temp =  file_get_contents(APPPATH.'views/mailer/send_registration_registration.html');
					$rep_str = str_replace('{{username}}',$_POST['student_name'],$get_temp);
					$rep_str_m = str_replace('{{link}}',base_url('student'),$rep_str);
					
				//	echo $rep_str;
					//exit;
					//sent mail after register by user
					$email_sent = array('to' => $_POST['student_email'],
							  'bcc' => '',
							  'html' => $rep_str_m,
							  'subject' => 'Invited to register on Simple Test Solution website.');
					//sent_mail($email_sent);
					$this->myEmail->sent_email($email_sent);
					
					if($res){
						$this->session->set_flashdata('msg', $this->lang->line('create_test_success'));
						//$base_url = base_url('Dashboard/Question/create_question/'.$test_uniqe_code);
						$data = array('status' => true,
									  'response' => array('msg' =>$this->lang->line('sent_invitation_success')),'url' => '');
					}
				}
		}else{
				$data = array('status' => false,
							  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
		};
		echo json_encode($data);
		exit;		
	}
	
	
	public function pageConfig($page_array = NULL)
	{
			$config['base_url'] = $page_array['base_url'];
            $config['total_rows'] = $page_array['total_rows'];
            $config['per_page'] = $page_array['per_page'];
            $config["uri_segment"] = $page_array['uri_segment'];
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<liclass="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="page-link">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';
			//Create Pagination and return page Links
            $this->pagination->initialize($config);
			return $this->pagination->create_links();
	}
	public function get_registered_user()
     {
     	$users =$this->Teacher_model->get_registered_users('tbl_user_registration',null, 
								null,'user_id');
  		 echo  json_encode($users);exit;
     }
	
	
	
	
	
  
}