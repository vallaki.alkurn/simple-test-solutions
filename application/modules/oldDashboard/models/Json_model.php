<?php

class Json_model extends CI_Model {

    function __construct() {
        parent::__construct();
		//$this->get_user = get_user();
		//print_r($this->db->last_query());
    }
	
	function getJsonDataUpcomingTest($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');
		$this->db->join('test_folders', "tbl_test_creation.folder_id = test_folders.id", 'left');
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where(array('test_to_date >=' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where(array('test_to_date >=' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	
	function getJsonDataCompletedTest($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');	
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	function getJsonDataReportsTest($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');	
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_test_creation');
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	function getJsonDataStudentResult($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_completed_test');	
		$this->db->join('tbl_user_registration','tbl_user_registration.user_id = tbl_completed_test.user_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('test_id' =>$data['test_id']));
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_completed_test');
		$this->db->join('tbl_user_registration','tbl_user_registration.user_id = tbl_completed_test.user_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('test_id' =>$data['test_id']));
		//$this->db->where(array('test_to_date <' =>date('Y-m-d')));	
		//$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	
	function getJsonQuestionBank($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_que_creation');	
	//	$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_q_type_master','tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id','left');
		if($data['search'])
		$this->db->where($data['search']);
		
		$this->db->where(array('tbl_que_creation.user_id' =>get_user_id()));
		/*if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}*/
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_que_creation');
	//	$this->db->join('tbl_test_creation','tbl_test_creation.test_id = tbl_que_creation.test_id','left');
		$this->db->join('tbl_q_type_master','tbl_q_type_master.question_type_id = tbl_que_creation.que_type_id','left');
		if($data['search'])
		$this->db->where($data['search']);
		
		$this->db->where(array('tbl_que_creation.user_id' =>get_user_id()));
		/*if($q_type){
			$this->db->where(array('tbl_q_type_master.question_type_id' =>$q_type));
		}*/
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	
	function getStudentListJson($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_user_registration');	
		$this->db->join('tbl_standard_class','tbl_standard_class.standard_class_id = tbl_user_registration.standard_class_id','left');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_type' =>2));
		/*$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');*/
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_user_registration');
		$this->db->join('tbl_standard_class','tbl_standard_class.standard_class_id = tbl_user_registration.standard_class_id','left');
		//$this->db->join('lk_blogs_category','lk_blogs_category.blog_category_id = blogs.cat_id');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_type' =>2));
		/*$this->db->where(array('user_id' =>get_user_id()));
		$this->db->where('DATE("'.date('Y-m-d').'") between test_form_date and test_to_date');*/
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }
	
	
	function getJsonDataTransctions($data)
    {
		//Select Query
		$this->db->select($data['fields']);
		$this->db->from('tbl_transaction');	
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$this->db->order_by($data['title'], $data['order']);
		$this->db->limit($data['limit'],$data['offset']);
        $query = $this->db->get();
        $row = $query->result();
		//print_r($this->db->last_query());
		
		//For total number of records
		$this->db->select($data['fields']);
		$this->db->from('tbl_transaction');
		if($data['search'])
		$this->db->where($data['search']);
		$this->db->where(array('user_id' =>get_user_id()));
		$count = $this->db->count_all_results();
		
		$row['count'] = $count;
		return $row;
    }

    


	
    
}
?>
