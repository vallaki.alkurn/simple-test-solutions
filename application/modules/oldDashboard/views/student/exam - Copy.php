stylesheet" href="<?php echo base_url(); ?>assets/exampanel/tabs.css" type="text/css" />

<section class="middle_section inner-page">
        <div class="container">
            <div class="outer-div-test">
                <div class="about-grid">
                    <div class="row">
                        <div class="col-lg-12">
							<div class="start-test-page">
								<div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="que-section">
                                            <div class="que-number">
                                                <span class="que-number-txt">Question <span id="sr_no">1 </span> of <?php echo count($que_list); ?></span>
                                                <span class="que-time">Remaining Time: <?php echo $exam->test_time_limit; ?></span>
                                            </div>
                                        
                                                <div id="wizard">
                                           <?php foreach($que_list as $key=>$q_row){ ?>

                                                    <h4 class="que-number"><?php echo $key+1 ?></h4>
                                                    <section class="question-options">
                                                        <h5 class="question-text"><?php echo $q_row->que_name; ?></h5>
                                                        <div class="radio">
                                                             <?php 
                                                             
                                                             foreach($q_row->options as $o_row){ ?>
                                                            <div class="ops">
                                                                <label><input type="radio" name="optradio" checked><span><span class="opt-number"><?php echo $o_row->alpha ?></span><span><?php echo $o_row->que_option ?></span></span></label>
                                                            </div>
                                                        <?php } ?>
                                                           
                                                        </div>
                                                    </section>


                                                <?php } ?>

                                               <div class="actions clearfix"><ul role="menu" aria-label="Pagination"><li class="disabled" aria-disabled="true"><a href="#previous" role="menuitem">Previous</a></li><li aria-hidden="false" aria-disabled="false"><a href="#next" role="menuitem">Next</a></li><li aria-hidden="true" style="display: none;"><a href="#finish" role="menuitem">Submit Test</a></li></ul></div>
                                                   
                                                </div>
                                           
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
	</section>
<script>
    
</script>