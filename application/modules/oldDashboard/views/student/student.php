<!-- Middle section start-->
<!--<section class="middle_section inner-page">
	<div class="container">
		<div class="" style="padding:150px 10px;" align="center">
		<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
		<h1>Welcome Student...</h1>
	</div>
	</div>
</section>-->
<div class="co">
        <div class="row">
            <div class="col-lg-3 col-md-3 p-0">
                <!-- Sidebar start-->
                 <?php  require_once(APPPATH.'views/template/sidebar-student.php'); ?>
                <!--sidebar end-->
            </div>
            <div class="col-lg-9 col-md-9 p-0">
                <div class="container-fluid">
                    <!--middle section start-->
                    <section class="dashboard-section">
                        <div class="p-10">
                            <div class="row">
                                <div class="col-lg-12">
									<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
                                    <div class="dash-heading heading-first">
                                        Dashboard
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard1.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Upcoming test</div>
                                                    <div class="dash-icon-content-count"><?php echo $total_no_upcommig_test; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard2.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Total Invites</div>
                                                    <div class="dash-icon-content-count"><?php echo $total_invite; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard3.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Completed test</div>
                                                    <div class="dash-icon-content-count"><?php echo $total_no_completed_test; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            


							<?php if($test_history){?>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="dash-heading">
                                        Test History
                                        <div class="view-all-section">
                                            <a href="<?php echo base_url('Dashboard/Student/results'); ?>">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
							<?php foreach($test_history as $row){ ?>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard4.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title"></div>
                                                    <div class="dash-test-dtdym">
													<?php 
													$tests= test_details($row->test_id);
													echo $tests->test_name
													?>
													</div>
													   <div class="dash-test-dtdym">Performed on: <?php echo $row->created; ?></div>
                                                    <div class="dash-test-dtdym">Total Marks: <?php echo $row->max_mark; ?></div>
                                                    <div class="dash-test-dtdym">Marks Obtained: <?php echo $row->total_mark; ?></div>
                                                    <div class="view-btn">
                                                        <a href="<?php echo base_url('Dashboard/Student/results'); ?>">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                              <!--  <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard4.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title">12<sup>th</sup> Standard math Test</div>
                                                    <div class="dash-test-dtdym">Performed on: 6<sup>th</sup> April 2019</div>
                                                    <div class="dash-test-dtdym">Total Marks: 100</div>
                                                    <div class="dash-test-dtdym">Marks Obtained: 80</div>
                                                    <div class="view-btn">
                                                        <a href="#">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard4.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title">12<sup>th</sup> Standard math Test</div>
                                                    <div class="dash-test-dtdym">Performed on: 6<sup>th</sup> April 2019</div>
                                                    <div class="dash-test-dtdym">Total Marks: 100</div>
                                                    <div class="dash-test-dtdym">Marks Obtained: 80</div>
                                                    <div class="view-btn">
                                                        <a href="#">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </div>

						<?php } ?>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="dash-heading">
                                        invited test
                                        <div class="view-all-section">
                                            <a href="<?php echo base_url(); ?>Dashboard/Student/invited-test">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
							<?php 
							if(count($invite_test) == 0){ ?>
							<div class="msg-gloabal"><div class="alert alert-success" role="alert"><strong>Your have not invited for any test.</div></div>
							<?php }else{
							foreach($invite_test as $row1){ ?>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard5.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title">
													<?php 
													$tests= test_details($row1->exam_id);
													echo $tests->test_name;
													?>
													</div>
                                                    <div class="dash-test-dtdym">Date <?php echo DateFormat($tests->test_form_date); ?> to <?php echo DateFormat($tests->test_to_date); ?></div>
                                                    <div class="dash-test-dtdym">Duration: <?php echo $tests->test_time_limit;?> min</div>
                                                    <div class="dash-icon-sylbs">Description: <?php echo $tests->test_description; ?></div>
                                                    <div class="view-btn">
                                                        <a href="<?php echo base_url(); ?>Dashboard/Student/invited-test">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php } } ?>
                             <!--   <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard5.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title">12<sup>th</sup> Standard math Test</div>
                                                    <div class="dash-test-dtdym">Date 6<sup>th</sup> April 2019 to 8<sup>th</sup> April 2019</div>
                                                    <div class="dash-test-dtdym">Duration: 30 min</div>
                                                    <div class="dash-icon-sylbs">Description: Concept, notation, order, equality, types of matrices, zero and identity matrix, transpose of a matrix, symmetric and skew symmentric mateices.</div>
                                                    <div class="view-btn">
                                                        <a href="#">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard5.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title">12<sup>th</sup> Standard math Test</div>
                                                    <div class="dash-test-dtdym">Date 6<sup>th</sup> April 2019 to 8<sup>th</sup> April 2019</div>
                                                    <div class="dash-test-dtdym">Duration: 30 min</div>
                                                    <div class="dash-icon-sylbs">Syllabus: Concept, notation, order, equality, types of matrices, zero and identity matrix, transpose of a matrix, symmetric and skew symmentric mateices.</div>
                                                    <div class="view-btn">
                                                        <a href="#">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                            </div>


                        </div>
                    </section>
                    <!--middle section end-->
                </div>
            </div>
        </div>
    </div>

<!-- Middle section End-->