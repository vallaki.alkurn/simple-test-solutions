<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-student.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Completed Test</div>
						</div>
						<div class="col-lg-12" >
						<div class="start-test-page">
								
                                <div class="reque-detail-outer">
                                    <div class="reque-detail-sections">
                                       <div class="req-row">
                                            <div class="req-heading">Test Title</div>
                                            <div class="req-cont"><?php echo $test_details->test_name; ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">randomize questions</div>
                                            <div class="req-cont"><?php echo random_que($test_details->test_question_random); ?></div>
                                        </div>
                                       
                                        <div class="req-row">
                                            <div class="req-heading">date</div>
                                            <div class="req-cont">	<?php echo date('d/m/Y',strtotime($test_details->test_form_date)).' - '.date('d/m/Y',strtotime($test_details->test_to_date)); ?></div>
                                        </div>
                                        <div class="req-row">
                                            <div class="req-heading">Test timing</div>
                                            <div class="req-cont"><?php echo $test_details->test_time_limit; ?> minutes</div>
                                        </div>
                                    </div>
                                    <div class="reque-detail-sections2">
                                        <div class="req-row">
                                            <div class="req-heading">invited by</div>
                                            <div class="req-cont"><?php echo totalQues('tbl_que_creation',array('test_id'=>$data->test_id)); ?></div>
                                        </div>
                                    </div>
                                </div>
								<div class="test-sub">
									<div class="heading-test">
										Test Description
									</div>
									<div class="description-test full">
										<?php echo $test_details->test_description; ?>
                                    </div>
                                    <div class="heading-test">
                                        Test Instruction
                                    </div>
                                    <div class="description-test full">
                                       <?php echo $test_details->test_instructions; ?>
                                    </div>
								</div>
                            </div>
                            <div class="total-que-section">
                                <div class="heading-hist">
                                    Totle questions(<?php echo count($test_que);?>)
                                </div>
                                <div class="questions-section">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div id="accordion">
											<?php if($test_que){ 
																foreach($test_que as $tq => $tv){
															?>
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $tv->que_id; ?>" aria-expanded="true" aria-controls="collapseOne">
                                                                <?php echo str_replace_q($tv->que_name); ?>
                                                            </button>
                                                        </h5>
                                                    </div>

                                                    <div id="collapse<?php echo $tv->que_id; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">
                                                           <?php 
																		if($tv->que_image){
																			echo '<img  width="200" src="'.base_url($tv->que_image).'" >';
																		}; 
																	?>
																	
																	
																		<?php 
																		
									$res = SelectData('tbl_question_option_1','*',array('que_id'=>$tv->que_id), '', NULL);
									if($res){
										echo '<ol class="col-lg-6">';
										foreach($res as $op => $ov){
											echo '<li style="list-style:decimal; margin-bottom:5px;">'.$ov->que_option.'</li>';
										}
										echo '</ol>';
									}
									
									if($tv->que_type_id == 5){
										$res_option_pair = que_option_pair($tv->que_id);
										echo '<ol class="col-lg-6">';
										foreach($res_option_pair as $v){
											echo '<li style="list-style:lower-alpha; margin-bottom:5px;">'.$v->pair_option.'</li>';
										}
										echo '</ol>';
									}
									
																		?>
                                                        </div>
                                                    </div>
                                                </div>
												<?php } 
															} ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
				</div>
		</div>
</div>
<script>
/*$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "desc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php //echo base_url('Dashboard/Teacher/getUpcomingTestJson');?>",
		 "columns": [
            { "data": "test_id" },
			{ "data": "test_name" },
			{ "data": "date" },
			{ "data": "test_question_random" },
			{ "data": "test_time_limit" },
			{ "data": "total_question" },
			{ "data": "sent_invitation" },
			{ "data": "action" }
        ]
    });
	
});*/
</script>