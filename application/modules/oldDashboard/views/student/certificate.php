 <?php
                                     $total_per =  ($results->total_mark * 100)/ $results->max_mark;
            if($results->test_status == 'Complete'){                      
                     ?>
					 <div>
<section class="middle_section inner-page" >

        <div class="container">
            <div class="outer-div-certificet">
                <div class="about-grid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-div-certificet">
                                <div class="cert-logo"> <a href="<?php echo $base_url; ?>Dashboard/Student"><img src="<?php echo base_url(); ?>assets/front-design/images/logo.jpg" ></a></div>
                                <div class="cert-content">
                                    <div class="cert-title">Certificate</div>
                                    <div class="cert-sub-title">of Achievement</div>
                                    <div class="cert-name"><?php echo $results->first_name.' '.$results->last_name; ?></div>
                                    <div class="cert-score">Got a Score of <span class="cert-score-txt"><?php echo $results->total_mark; ?>/<?php echo $results->max_mark; ?></span></div>
                                    <div class="cert-percent"><span class="cert-percent-tr"><?php echo round($total_per,2); ?>%</span> on</div>
                                    <div class="cert-normal"><?php echo $results->standard_class_name; ?></div>
                                    <div class="cert-normal"><?php echo date('d F, Y',strtotime($results->created_at)); ?></div>
                                   
                                </div>
                            </div>
						</div>
                    </div>
                </div>
            </div>
            <div class="download-link">
                    <a href="<?php echo base_url() ?>Home/Home_page/generate_pdf/<?php $results->id ?>" ><i class="fa fa-download" aria-hidden="true"></i> Download test result</a>
            </div>
        </div>
	</section>
	</div>
			<?php }else{ ?>
			<div class="download-link" style="margin-left:-200px">
                    <a href="javascript:void(0)" > Your test report is pending for approval.</a>
            </div>
			<?php } ?>

	
