<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-student.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Test History</div>
						</div>
						<!--<div class="col-lg-12">
								<div class="right-drop-down float-lg-right float-md-right">
									<div class="create-test"> <a href="<?php echo base_url('Dashboard/Question/create_question') ?>">Create Question</a> </div>
										<div class="drop-text"> Sort by Question Type </div>
										<div class="form-group">
										<select>
												<?php /*foreach(question_type() as $qtype) { ?>
												<option value="<?php echo $qtype->question_type_id; ?>"><?php echo $qtype->question_type_name; ?></option>
												<?php }*/ ?>
										</select>
										</div>
								</div>
						</div>-->
						<div class="col-lg-12" style="display:none;" >
						<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							<div class="bank-list-outer">
								<div class="list-header">
									<div class="list-row">
										<div class="list-column text-center">Sr No</div>
										<div class="list-column text-center">Test Title</div>
										<div class="list-column ">Date</div>
										<div class="list-column text-center">Randomize Questions</div>
										<div class="list-column text-center">Timing</div>
										<div class="list-column text-center">Total Question</div>
										<div class="list-column text-center">Sent Invitations</div>                                
										<div class="list-column text-center">Action</div>
									</div>
								</div>
								<div class="list-boby">
								 <?php 
								 if($results){
								 $i = $srno == 0 ? 1 : $srno+1;
								 foreach ($results as $data) { ?>
									<div class="list-row">
											<div class="list-column text-center"><?php echo $i;//$data->test_id; ?></div>
											<div class="list-column text-center"><?php echo $data->test_name; ?></div>
											<div class="list-column text-center"><?php echo date('d-m-Y',strtotime($data->test_form_date)).' to '.date('d-m-Y',strtotime($data->test_to_date)); ?></div>
											<div class="list-column text-center"><?php echo random_que($data->test_question_random); ?></div>
											<div class="list-column text-center"><?php echo $data->test_time_limit; ?> Minutes </div>
											<div class="list-column text-center">
											<?php echo totalQues('tbl_que_creation',array('test_id'=>$data->test_id)); ?>
											</div>
											<div class="list-column text-center"><?php echo 0; ?></div>
										<div class="list-column text-center action">
											<a href="<?php echo base_url('Dashboard/Teacher/create-test/'.$data->test_uniqe_code);?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<a href="<?php echo base_url('Dashboard/Teacher/test-view/'.$data->test_uniqe_code);?>"><i class="fa fa-eye" aria-hidden="true"></i></a><br>
											<a href="<?php echo base_url('Dashboard/Question/create-question/'.$data->test_uniqe_code);?>">Add Question</a>
										</div>
									</div>
									 <?php $i++; } }else{ echo 'Test not found.';} ?>
								</div>
							</div>
                </div>
						<div class="col-lg-12" >
						<?php /*if (isset($links)) { ?>
                <?php echo $links ?>
            <?php }*/ ?>
						<!--<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
							<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1">Previous</a> </li>
							<li class="page-item"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"> <a class="page-link" href="#">Next</a> </li>
							</ul>
						</nav>-->
						<?php i/*f(isset($links)) { ?>
                <?php echo $links ?>
            <? }*/ ?>
			
	<table id="example" class="display table table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Sr.No</th>
              	<th>Test Title</th>
				<th>Total Question</th>
				<th>Duration</th>
				<th>Date</th>
              	<th>Total Marks</th>
				<th>Obtained Marks</th>
				<th>Action</th>
             
              
            </tr>
        </thead>
    </table>
						</div>
				</div>
		</div>
</div>
<script>
$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Student/getTestResultJson');?>",
		 "columns": [
            { "data": "id" },
			{ "data": "test_name" },
			{ "data": "total_question" },
				{ "data": "test_time_limit" },
				{ "data": "created" },
			{ "data": "max_mark" },
			{ "data": "total_mark" },
			{ "data": "action" },
			
		
		
		
        ]
    });
	
});
</script>