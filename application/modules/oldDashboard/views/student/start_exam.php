<section class="middle_section inner-page">
        <div class="container">
            <div class="outer-div-test">
                <div class="about-grid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="start-test-page">
                                <div class="rtart-test-btn">
								<?php $curr_date=date('Y-m-d h:i:s'); 
								if($curr_date >= $exam->test_form_date && $curr_date <= $exam->test_to_date)
								{
								?>
                                    <a href="<?php echo base_url('Dashboard/Student/exam/'.$exam->exam_id.'/'.$exam->request_id.'') ?>">Start test</a>
								<?php } ?>
                                </div>
                                <div class="test-sub">
                                    <div class="heading-test">
                                        Subjact
                                    </div>
                                    <div class="description-test">
                                       <?php echo ($exam->subject_class_name) ? $exam->subject_class_name : ''; ?>
                                    </div>
                                </div>
                                <div class="test-sub">
                                    <div class="heading-test">
                                        Test Timing
                                    </div>
                                    <div class="description-test">
                                      <?php echo ($exam->test_time_limit) ? $exam->test_time_limit : ''; ?> minutes
                                    </div>
                                </div>
								 <div class="test-sub">
                                    <div class="heading-test">
                                        Date
                                    </div>
                                    <div class="description-test">
                                      <?php echo DateFormat($exam->test_form_date); ?> - <?php echo DateFormat($exam->test_to_date); ?>
                                    </div>
                                </div>
                                <div class="test-sub">
                                    <div class="heading-test">
                                        Questions
                                    </div>
                                    <div class="description-test">
                                       <?php echo ($exam->totalquestion) ? $exam->totalquestion : ''; ?>
                                    </div>
                                </div>
                                <div class="test-sub">
                                    <div class="heading-test">
                                        Test Description
                                    </div>
                                    <div class="description-test full">
                                          <?php echo ($exam->test_description) ? $exam->test_description : ''; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>