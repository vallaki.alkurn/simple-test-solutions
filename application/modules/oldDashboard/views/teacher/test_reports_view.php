<?php //print_r($test_details); ?>
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
<!-- Middle section start-->
				<div class="container">
					<div class="outer-div-test">
						<div class="about-grid">
							<div class="row">
								<div class="col-lg-12">
								<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
									<div class="start-test-page">
										<div class="reque-detail-outer">
											<div class="reque-detail-sections" style="width:100%;">
												<div class="req-row">
													<div class="req-heading">Subject</div>
													<div class="req-cont">Maths</div>
												</div>
												<div class="req-row">
													<div class="req-heading">randomize questions</div>
													<div class="req-cont"><?php echo random_que($test_details->test_question_random); ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">Test Title</div>
													<div class="req-cont"><?php echo $test_details->test_name; ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">date</div>
													<div class="req-cont">
	<?php echo date('d/m/Y',strtotime($test_details->test_form_date)).' - '.date('d/m/Y',strtotime($test_details->test_to_date)); ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">Test timing</div>
													<div class="req-cont"><?php echo $test_details->test_time_limit; ?> minutes</div>
												</div>
												<div class="req-row">
													<div class="req-heading">Total questions</div>
													<div class="req-cont"><?php echo count($test_que);?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">Total marks</div>
													<div class="req-cont"><?php echo $total_marks;?></div>
												</div>
											</div>
											
										</div>
										<div class="test-sub">
											<div class="heading-test">
												Test Description
											</div>
											<div class="description-test full">
												<?php echo $test_details->test_description; ?>
											</div>
											<div class="heading-test">
												Test Instruction
											</div>
											<div class="description-test full">
												<?php echo $test_details->test_instructions; ?>
											</div>
										</div>
									</div>
								</div>
								
								
								<div class="col-lg-12" >
								
									<table id="example" class="display table table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Sr.No</th>
												<th>Student Name</th>
												<th>Total Question</th>
												<th>Total Attempt</th>
												<th>Total Mark</th>
												<th>Total Time</th>
												<th>Status </th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
								
							</div>
						</div>
					</div>
				</div>
    <!-- Middle section End-->
</div>
</div>
</div>
<!-- Middle section start-->
<script>
$(document).ready(function(){
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"bSearch": <?php echo $test_details->test_id; ?>,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getStudentResultJson?test_id='.$test_details->test_id);?>",
		 "columns": [
            { "data": "id_test" },
			{ "data": "user_id" },
			{ "data": "total_question" },
			{ "data": "attempt_question" },
			{ "data": "total_mark" },
			{ "data": "time_taken" },
			{ "data": "test_status" },
			{ "data": "action" }
        ]
    });
});
</script>
<script>
function reuseTest(testId)
{
	var data = 'test_id='+testId;
	$.ajax({
		type:'POST',
		url:'<?php echo base_url('Dashboard/Question/reuse_test');?>',
		data:data,
		dataType:'json',
		beforeSend: function(data){
		},
		success: function (data) {
			//console.log(data);
			if(data.status == true){
				if(data.url){
					window.location.assign(data.url);
				}
			}
		}
	});	
}
</script>