<?php //print_r($user_details); ?>
<!-- Middle section start-->
<section class="middle_section inner-page update-profile loginpage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
                                <div class="form-heading">Create New Test</div>
                                <form  url="<?php echo base_url('Dashboard/Teacher/add_new_test_teacher');?>" method="post" id="add_new_test_teacher" name="add_new_test_teacher" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Test Title</label>
                                                <input type="text" class="form-control" id="first_name" value="Washington, D.C. is the capital of usa?" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Date</label>
                                                <div class="datediv">
                                                    <input type="date" class="form-control" id="first_name" value="Washington, D.C. is the capital of usa?" placeholder="First Name*"><span>To</span><input type="date" class="form-control" id="first_name" value="Washington, D.C. is the capital of usa?" placeholder="First Name*">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Test Description</label>
                                                <textarea>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Test Instructions</label>
                                                <textarea>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Randomize Questions</label>
                                                <select>
                                                    <optgroup>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Test Timing</label>
                                                <input type="text" class="form-control" id="first_name" value="Washington, D.C. is the capital of usa?" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                                <div class="add-que-heading">Add Questions</div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Enter Question</label>
                                                <input type="text" class="form-control" id="first_name" value="Washington, D.C. is the capital of usa?" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>True or False</label>
                                                <select>
                                                    <optgroup>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Add Option</label>
                                                <input type="text" class="form-control" id="first_name" value="Yes, No" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Correct Answer</label>
                                                <input type="text" class="form-control" id="first_name" value="Yes" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Set Point</label>
                                                <input type="text" class="form-control" id="first_name" value="2" placeholder="First Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Upload Files</label>
                                                    <div class="upload-file create">
                                                        <label for="file-upload">No file select</label>
                                                        <input id="file-upload" name='upload_cont_img' type="file" style="display:block;"> 
                                                        <span class="upload-btn"><i class="fa fa-folder-open" aria-hidden="true"></i> Browse</span>
                                                    </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6 " >
                                                        <button type="submit" class="btn btn-primary no-bg-btn">add more questions</button>
                                                </div>
                                                <div class="col-md-6">
                                                        <button type="submit" class="btn btn-primary no-bg-btn">Select from Question Bank</button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                        <button type="submit" class="btn btn-primary no-bg-btn">Save Question</button>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</section>
	