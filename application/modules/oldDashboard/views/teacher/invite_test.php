<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Invite Students for Test</div>
						</div>
						<div class="col-lg-12" >
						<table id="example" class="display table table-bordered" style="width:100%">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>Test Title</th>
									<th>Date</th>
									<th>Randomize Questions</th>
									<th>Timing</th>
									<th>Total Question</th>
									<th>Sent Invitations</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
						</div>
				</div>
		</div>
</div>
<script>
$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Invite/getUpcomingTestJson');?>",
		 "columns": [
            { "data": "test_id" },
			{ "data": "test_name" },
			{ "data": "date" },
			{ "data": "test_question_random" },
			{ "data": "test_time_limit" },
			{ "data": "total_question" },
			{ "data": "sent_invitation" },
			{ "data": "action" }
        ]
    });
	
});
</script>

  <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
            
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">join the website</h4>
                <button type="button" class="close" data-dismiss="modal" id="close1">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
            	  <form  url="<?php echo base_url('Dashboard/Teacher/send_new_request');?>" method="post" id="send_new_request" name="send_new_request" enctype="multipart/form-data">
                 <input type="hidden"  name="text_id" id="test_id" />
                    <div class="form-group" id="div1">
                        <select type="name" class="form-control" name=multi_user id="multi_user" placeholder="Name"  >
                        	<option value=""></option>
                        </select>
                    </div>
                  
                    <button type="submit"  class="btn btn-primary">Send</button>
                </form>
            </div>
            
            
            </div>
        </div>
    </div>