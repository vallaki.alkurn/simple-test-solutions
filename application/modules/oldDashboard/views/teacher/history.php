<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
						<!-- Sidebar start-->
						<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
						<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
				<div class="col-lg-12">
					<div class="form-heading">Payment history</div>
				</div>
                <div class="col-lg-12">
                    <div class="bank-list-outer">
                        <div class="list-header">
                            <div class="list-row">
                                <div class="list-column text-center sn">Sr No</div>
                                <div class="list-column dt">Date</div>
                                <div class="list-column ">Test Title</div>
                                <div class="list-column text-center">Amount</div>                                
                                <div class="list-column text-center">Action</div>
                            </div>
                        </div>
                        <div class="list-boby">
                            <div class="list-row">
                                    <div class="list-column text-center sn">1</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">2</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">3</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">4</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">5</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">6</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">7</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">8</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                    <div class="list-column text-center sn">9</div>
                                    <div class="list-column dt"> 3/20/2019</div>
                                    <div class="list-column ">Maths Test 2019 </div>
                                    <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                            <div class="list-row">
                                <div class="list-column text-center sn">10</div>
                                <div class="list-column dt"> 3/20/2019</div>
                                <div class="list-column ">Maths Test 2019 </div>
                                <div class="list-column text-center">150</div>
                                <div class="list-column text-center action"><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
					<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-end">
									<li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1">Previous</a> </li>
									<li class="page-item"><a class="page-link" href="#">1</a></li>
									<li class="page-item"><a class="page-link" href="#">2</a></li>
									<li class="page-item"><a class="page-link" href="#">3</a></li>
									<li class="page-item"> <a class="page-link" href="#">Next</a> </li>
							</ul>
					</nav>
					</div>
            </div>
        </div>
	</div>
	<!-- Middle section End-->