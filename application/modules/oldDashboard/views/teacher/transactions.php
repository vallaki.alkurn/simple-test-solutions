<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
							<div class="form-heading">Payment Transaction</div>
						</div>
						<div class="col-lg-12">
							<div class="msg-gloabal"></div>
							<div class="col-lg-6">
								<div class="req-heading">Credit/Debit card number</div>
								<div class="req-cont"><?php echo $card_details->card_number; ?></div>
							</div>
							<div class="col-lg-6">
								<div class="req-heading">Customer name</div>
								<div class="req-cont"><?php echo $card_details->cardholder_name; ?></div>
							</div>
							<div class="col-lg-6">
								<div class="req-heading">CVV</div>
								<div class="req-cont"><?php echo $card_details->cvv_number; ?></div>
							</div>
							<div class="col-lg-6">
								<div class="req-heading">Expiry date</div>
								<div class="req-cont"><?php echo $card_details->expiry_date; ?></div>
							</div>
							<div class="col-lg-12" id="calP">
								<?php if($card_details->status != 3){ ?>
								 <div class="btn-section">
									<a href="javascript:void(0)"  id="<?php echo $card_details->id; ?>" onClick="cancelledPayment(this.id)">Cancelled</a>
								</div>
							
							<?php }else{
									?>
										<p class="alert alert-warning" id="calP">Cancelled Payment Transaction</p>
									<?php
								} ?>
							</div>
						</div>
						<div class="col-lg-12" >
							<table id="example" class="display table table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Sr. No</th>
										<th>Transaction id</th>
										<th>Payment Date</th>
										<th>Amount</th>
										<th>Payment status</th>
										<th>Payment Method</th>
									</tr>
								</thead>
							</table>
						</div>
				</div>
		</div>
</div>
<script>

$(document).ready(function(){
	
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getTransctionsJson');?>",
		 "columns": [
            { "data": "id" },
			{ "data": "txn_id" },
			{ "data": "payment_date" },
			{ "data": "amt" },
			{ "data": "payment_status" },
			{ "data": "payment_method" },
        ]
    });
	
});
function cancelledPayment(card_id)
{
	var data = 'card_id='+card_id;
	$.ajax({
		type:'POST',
		url:'<?php echo base_url('Dashboard/Teacher/cancelled_payment');?>',
		data:data,
		dataType:'json',
		beforeSend: function(data){
		},
		success: function (data) {
			//console.log(data);
			if(data.status == true){
				var response = data.response;
				$('.msg-gloabal').html(response.msg);
				$('#calP').hide();
			//	$('#calP').html('<span class="alert alert-warning" id="calP">Cancelled Payment Transaction</span>');
				if(data.url){
					window.location.assign(data.url);
				}
			}
		}
	});	
}
</script>