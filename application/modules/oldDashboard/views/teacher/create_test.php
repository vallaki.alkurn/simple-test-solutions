<?php //echo $test_details->name ?>
<!-- Middle section start-->
<section class="middle_section inner-page update-profile loginpage">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="login-container">
					<div class="form-div">   
						<div class="">
							<div class="form-heading">Create New Test</div>
							<form action="<?php echo base_url('Dashboard/Teacher/add_new_test_teacher');?>" method="post" id="add_new_test_teacher" name="add_new_test_teacher" enctype="multipart/form-data">
							<input type="hidden" name="test_id" id="test_id" value="<?php echo $test_details->test_id;?>" >
							<input type="hidden" name="test_uniqe_code" id="test_uniqe_code" value="<?php echo $test_details->test_uniqe_code;?>" >
							<input type="hidden" name="test_access_code" id="test_access_code" value="<?php echo $test_details->test_access_code;?>" >
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Test Title</label>
											<input type="text" class="form-control" id="test_name" name="test_name" value="<?php echo $test_details->test_name;?>" placeholder="Test Title">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group ">
											<label>Date</label>
											<div class="datediv">
												<input type="text" class="form-control datepicker" id="test_form_date" name="test_form_date" value="<?php echo html_date($test_details->test_form_date);?>" placeholder="From Date"><span>To</span>
												<input type="text" class="form-control datepicker" id="test_to_date" name="test_to_date" value="<?php echo  html_date($test_details->test_to_date);?>" placeholder="To Date">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Test Description</label>
											<textarea style="color:#2c2c2c;" id="test_description" name="test_description"><?php echo $test_details->test_description;?></textarea>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Test Instructions</label>
											<textarea style="color:#2c2c2c;" id="test_instructions" name="test_instructions"><?php echo $test_details->test_instructions;?></textarea>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label> Randomize Questions</label>
											<select id="test_question_random" name="test_question_random">
												<option value="0" <?php echo $test_details->test_question_random == 0 ? 'selected' : ''  ;?>>No</option>
												<option value="1" <?php echo $test_details->test_question_random == 1 ? 'selected' : ''  ;?>>Yes</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group ">
											<label>Test Timing</label>
											<select id="test_time_limit" name="test_time_limit" class="">
												<?php for($i=20; $i<=180;){ ?>
												<option <?php echo $test_details->test_time_limit == $i ? 'selected' : ''  ;?> value="<?php echo  $i; ?>"><?php echo $i.'&nbsp;Minutes'; ?></option>
												<?php  $i= $i+10;} ?>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group ">
										<label>Select Folder</label>
											<select id="folder_id" name="folder_id" class="">
												<option value="">Select Folder</option>

												<?php foreach($folders as $folder) : ?>
												<option value="<?php echo $folder->id; ?>" <?php echo ($folder->id == $test_details->folder_id) ? 'selected' : ''; ?>>
													<?php echo $folder->name; ?>
													</option>
												<?php endforeach;  ?>
											</select>
										</div>
									</div>
									
									 <div class="col-md-6">
									</div>
									 <div class="col-md-4">
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
	
<script type="text/javascript">
	$(function(){
	//Update Profile
	 $('#add_new_test_teacher').validate({
		rules: {
				test_name: {
				   required : true,
				},
				test_description: {
					required: true,
				},
				test_instructions: {
					required: true,
				},
				test_time_limit: {
					required: true,
				}
			},
		messages: {
				 test_name:{
					required :"Test Title field is required",
				 }, 
				 test_description:{
					required :"Description field is required",
				 },
				 test_instructions:{
					required :"Instructions field is required",
				 },
				 test_time_limit:{
					required :"Test timing field is required",
				 }
			},
		/*submitHandler: function(form) {
				lkForms('add_new_test_teacher');
				$(window).scrollTop(0);
		  }
		 });*/
	 });
</script>