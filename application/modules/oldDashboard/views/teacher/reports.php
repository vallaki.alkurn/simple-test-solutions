<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
							<div class="form-heading">Test Reports</div>
						</div>
						<div class="col-lg-12">
						<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
		                </div>
						<div class="col-lg-12" >
							<table id="example" class="display table table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Sr.No</th>
										<th>Test Title</th>
										<th>Date</th>
										<th>Randomize Questions</th>
										<th>Timing</th>
										<th>Total Question</th>
										<th>Sent Invitations</th>
										<th>Student performed test</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
				</div>
		</div>
</div>

<!--<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js "></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js "></script>-->

<script>
$(document).ready(function(){
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		/*dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],*/
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getReportsTestJson');?>",
		 "columns": [
            { "data": "test_id" },
			{ "data": "test_name" },
			{ "data": "date" },
			{ "data": "test_question_random" },
			{ "data": "test_time_limit" },
			{ "data": "total_question" },
			{ "data": "sent_invitation" },
			{ "data": "student_attempt" },
			{ "data": "action" }
        ]
    });
});
</script>