<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Students List</div>
						</div>
						<div class="col-lg-12">
							<div class="msg-gloabal">
							<?php //if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							</div>
						</div>
						
								
						<div class="col-lg-12" >
							<div class="btn-section" style="position:relative; float:right; clear:both; top:0px;">
								<a href="javascript:void(0)" id="inviteSend">Send invitation</a>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Send Request</a>
							</div>
							<div class="start-test-page">
								<div class="reque-detail-outer">
									<div class="reque-detail-sections">
									  <div class="req-row">
											<div class="req-heading">Test Title</div>
											<div class="req-cont"><?php echo $test_details->test_name; ?></div>
										</div>
										<div class="req-row">
											<div class="req-heading">Test timing</div>
											<div class="req-cont"><?php echo $test_details->test_time_limit; ?> minutes</div>
										</div>
										<div class="req-row">
											<div class="req-heading">date</div>
											<div class="req-cont">
	<?php echo display_date($test_details->test_form_date).' - '.display_date($test_details->test_to_date); ?></div>
										</div>
										<div class="req-row">
											<div class="req-heading">randomize questions</div>
											<div class="req-cont"><?php echo random_que($test_details->test_question_random); ?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12" >
						<form name="student_sent_invite" id="student_sent_invite" method="post">
							<input type="hidden" name="exam_id" id="exam_id_f" value="<?php echo $test_details->test_id;?>" >
							<table id="example" class="display table table-bordered" style="width:100%">
								<thead>
									<tr>
										<th><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
										<th>Sr.No</th>
										<th>Name</th>
										<th>Email</th>
										<th>Class</th>
									</tr>
								</thead>
							</table>
						</form>
						</div>
				</div>
		</div>
</div>


<!-- The Modal -->
 <div class="modal" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
			
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">join the website</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
			<div class="msg-gloabal-n"></div>
				<form name="student_sent_invite_new" id="student_sent_invite_new" >
					<input type="hidden" name="exam_id" id="exam_id" value="<?php echo $test_details->test_id;?>" >
					<div class="form-group">
						<input type="name" name="student_name" class="form-control" id="student_name" placeholder="Name">
					</div>
					<div class="form-group lesspadding">
						<input type="email" name="student_email" class="form-control" id="student_email" placeholder="Email Address">
					</div>
					<button type="submit" class="btn btn-primary">Send</button>
				</form>
			</div>
			
			
			</div>
		</div>
	</div>

<script>
$(document).ready(function(){
	
   var table = $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Invite/getStudentListJson');?>",
		 "columns": [
		 	{ "data": "id" },
            { "data": "srno" },
			{ "data": "name" },
			{ "data": "email_address" },
			{ "data": "standard_class" },
        ],
		'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
			 //console.log(full);
             return '<input type="checkbox" name="user_id[]" value="'+full.id+'">';
         }
      }],
    });
	
	// Handle click on checkbox to set state of "Select all" control
   $('#example tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });
   
     // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      var rows = table.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });
   
   //Invite Send to user
   $('#inviteSend').on('click',function(){
		//var url = $('#student_sent_invite').attr('url');
		if($('[type="checkbox"]').is(":checked")){
			var data = new FormData($('#student_sent_invite')[0]);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('Dashboard/Invite/sent_invitation');?>',
				data:data,
				dataType:'json',
				async:false,
				processData: false,
				contentType: false,
				beforeSend: function(data){
					//loaderIn();
				},
				success: function (data) {
					console.log(data);
					if(data.status == false){
						$('.msg-gloabal').html(data.response.msg);
						//loaderOut({status:0,msg:data.response.msg,url:data.url});
					}else{
						$('.msg-gloabal').html(data.response.msg);
						/*$('#'+ID)[0].reset();
						if(data.url){
							$(":submit").attr("disabled", true);
							window.location.assign(data.url);
						}*/
					}
				}
			});	
			return false;
        }else{
			alert('please check at least one user.');
			return false;
		}
	});
	
	//Update Profile
	 $('#student_sent_invite_new').validate({
		rules: {
				student_name: {
				   required : true,
		    	   minlength   : 3,
				},
				student_email: {
					required: true,
					email: true
				}
			},
		messages: {
				 student_name:{
					required :"Name field is required",
				 }, 
				 student_email:{
					required :"Email Address field is required",
				 },
			},
		submitHandler: function(form) {
			var data = new FormData($('#student_sent_invite_new')[0]);
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('Dashboard/Invite/sent_invitation_new');?>',
				data:data,
				dataType:'json',
				async:false,
				processData: false,
				contentType: false,
				beforeSend: function(data){
				},
				success: function (data) {
					console.log(data);
					if(data.status == false){
						$('.msg-gloabal-n').html(data.response.msg);
					}else{
						$('.msg-gloabal-n').html(data.response.msg);
						$('#student_name').val('');
						$('#student_email').val('');
					}
				}
			});	
		  }
	 });	
});
</script>

  