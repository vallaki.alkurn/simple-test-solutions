<!-- Middle section start-->
<!--<section class="middle_section inner-page">
	<div class="container">
		<div class="" style="padding:150px 10px;" align="center">
		<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
		<h1>Welcome Teacher....</h1>
	</div>
	</div>
</section>
-->
<div class="co">

            <div class="p-0 dash1 sidebar"  id="mySidebar">
                 <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                <!-- Sidebar start-->
                <?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
                <!--sidebar end-->
            </div>
           <div class="row">
            <div class="col-lg-12 col-md-12 p-0 dash2" id="dash2">
                <div class="container-fluid">
                    <!--middle section start-->
                    <section class="dashboard-section">
                        <div class="p-10">
                            <div class="row">
                                <div class="col-lg-12">
								<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
                                    <div class="dash-heading heading-first">
                                        Dashboard
                                        <div class="create-test">
                                            <a href="<?php echo base_url('Dashboard/Teacher/create-test'); ?>">Create test</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard1.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Question Bank</div>
                                                    <div class="dash-icon-content-count"><?php echo $question_bank; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard2.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Total test</div>
                                                    <div class="dash-icon-content-count"><?php echo $total_test; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard3.png">
                                                </div>
                                            </div>
                                            <div class="col-sm-8 p-0">
                                                <div class="dash-icon-content">
                                                    <div class="dash-icon-content-mane">Completed test</div>
                                                    <div class="dash-icon-content-count"><?php echo $completed_test; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php if($upcoming_test){ ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="dash-heading">
                                        Upcoming test
                                        <div class="view-all-section">
                                            <a href="<?php echo base_url('Dashboard/Teacher/upcoming-test'); ?>">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
							<?php 
								//print_r($upcoming_test); 
								foreach($upcoming_test as $up => $uv){
							?>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard1.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title"><?php echo $uv->test_name ;?></div>
                                                    <div class="dash-test-dtdym">
													Date <?php echo html_date_s($uv->test_form_date); ?><sup>th</sup> <?php echo html_month($uv->test_form_date); ?> <?php echo html_year($uv->test_form_date); ?> to <?php echo html_date_s($uv->test_to_date); ?><sup>th</sup> <?php echo html_month($uv->test_to_date); ?> <?php echo html_year($uv->test_to_date); ?></div>
                                                    <div class="dash-test-dtdym">Duration: <?php echo $uv->test_time_limit;?> min</div>
                                                    <div class="dash-icon-sylbs">Description: 
													<?php
													if(strlen($uv->test_description) > 100){
													//echo $uv->test_description.'<hr>'; 
													echo substr($uv->test_description,0,100).'<br><a href="javascript:void(0)" id="loadmore_'.$uv->test_id.'" class="loadlink">Load more..</a>';
													
													echo '<span id="loadcontent_'.$uv->test_id.'" class="loadcontent">'.substr($uv->test_description,101,500).'<br><a href="javascript:void(0)" id="less_'.$uv->test_id.'" class="lessink">Less..</a></span>';
													}else{
														echo $uv->test_description;
													}
													//echo $uv->test_description;
													//echo strlen($uv->test_description);
													?></div>
                                                    <div class="view-btn">
                       <a href="<?php echo base_url('Dashboard/Teacher/test-view/'.$uv->test_uniqe_code);?>">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<?php } ?>
                            </div>

							<?php } ?>
							
							
							<?php if($latest_completed_test){ ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="dash-heading">
                                        Latest completed test
                                        <div class="view-all-section">
                                            <a href="<?php echo base_url('Dashboard/Teacher/completed-test'); ?>">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
							<?php 
								//print_r($upcoming_test); 
								foreach($latest_completed_test as $up => $uv){
							?>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="dash-icon sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="dash-icon-img">
                                                    <img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboard4.png">
                                                </div>
                                            
                                                <div class="dash-test-content">
                                                    <div class="dash-test-title"><?php echo $uv->test_name ;?></div>
                                                    <div class="dash-test-dtdym">Date: <?php echo html_date_s($uv->test_to_date); ?><sup>th</sup> <?php echo html_month($uv->test_to_date); ?> <?php echo html_year($uv->test_to_date); ?></div>
                                                    <div class="dash-test-dtdym">Invite sent: <?php echo sent_invitation($uv->test_id);?></div>
                                                    <div class="dash-test-dtdym">Student performed test: <?php echo student_attempt($uv->test_id);?></div>
                                                    <div class="view-btn">
                                                        <a href="<?php echo base_url('Dashboard/Teacher/test-completed-view/'.$uv->test_uniqe_code);?>">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php } ?>
                            </div>
							<?php } ?>
                        </div>
                    </section>
                    <!--middle section end-->
                </div>
            </div>
        </div>
    </div>
<!-- Middle section End-->
<style>
.loadcontent{
	display:none;	
}
</style>
<script>
$('.loadlink').on('click',function(){
	var getId = $(this).attr('id');
	var splitID = getId.split('_');
	$('#loadcontent_'+splitID[1]).show();
	$('#'+getId).hide();
});
$('.lessink').on('click',function(){
	var getId = $(this).attr('id');
	var splitID = getId.split('_');
	$('#loadcontent_'+splitID[1]).hide();
	$('#loadmore_'+splitID[1]).show();
});
</script>