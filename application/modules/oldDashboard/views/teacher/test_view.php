<?php //print_r($test_details); ?>
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<!-- Sidebar start-->
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
					<!--sidebar end--> 
				</div>
				<div class="col-lg-9 col-md-9 p-0">
<!-- Middle section start-->
				<div class="container">
					<div class="outer-div-test">
						<div class="about-grid">
							<div class="row">
								<div class="col-lg-12">
								<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
									<div class="start-test-page">
										<div class="reque-detail-outer">
											<div class="reque-detail-sections">
												<div class="req-row">
													<div class="req-heading">Subject</div>
													<div class="req-cont">Maths</div>
												</div>
												<div class="req-row">
													<div class="req-heading">randomize questions</div>
													<div class="req-cont"><?php echo random_que($test_details->test_question_random); ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">Test Title</div>
													<div class="req-cont"><?php echo $test_details->test_name; ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">date</div>
													<div class="req-cont">
	<?php echo date('d/m/Y',strtotime($test_details->test_form_date)).' - '.date('d/m/Y',strtotime($test_details->test_to_date)); ?></div>
												</div>
												<div class="req-row">
													<div class="req-heading">Test timing</div>
													<div class="req-cont"><?php echo $test_details->test_time_limit; ?> minutes</div>
												</div>
												<div class="req-row">
													<div class="req-heading">Total marks</div>
													<div class="req-cont"><?php echo $total_marks;?></div>
												</div>
											</div>
											
										</div>
										<div class="test-sub">
											<div class="heading-test">
												Test Description
											</div>
											<div class="description-test full">
												<?php echo $test_details->test_description; ?>
											</div>
											<div class="heading-test">
												Test Instruction
											</div>
											<div class="description-test full">
												<?php echo $test_details->test_instructions; ?>
											</div>
											<div class="description-test full">
												<div class="btn-section">
											<a href="<?php echo base_url('Dashboard/Question/create-question/'.$test_details->test_uniqe_code); ?>" id="">Add Question</a>
												</div>
												 <div class="btn-section">
													<!--<a href="javascript:void(0)">Download</a>-->
													<a href="javascript:void(0)" 
													id="<?php echo $test_details->test_id; ?>" onClick="reuseTest(this.id)">Re-Use test</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="total-que-section">
										<div class="heading-hist">
											<strong>Total questions</strong>(<?php echo count($test_que);?>) 
										</div>
										<div class="panel-group" id="accordion">
											
											<?php if($test_que){ 
											$i = 1;
												foreach($test_que as $tq => $tv){
											?>
										
											<div class="panel panel-default">
												<div class="panel-heading">
													<dt  class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $tv->que_id; ?>">
													<a href="javascript:void(0)"><span class="counter-num"><?php echo $i;?></span><?php echo str_replace_q($tv->que_name); ?> <i class="fa fa-plus" aria-hidden="true"></i></a></dt>
												</div>
												<div id="collapse<?php echo $tv->que_id; ?>" class="panel-collapse collapse">
													<div class="panel-body">
													<?php 
														if($tv->que_image){
															echo '<img  width="200" src="'.base_url($tv->que_image).'" >';
														}; 
													?>
													<?php 
																		
														$res = SelectData('tbl_question_option_1','*',array('que_id'=>$tv->que_id), '', NULL);
														if($res){
															
															echo '<ol class="col-lg-6">';
															foreach($res as $op => $ov){
															$ans = tbl_question_anser($ov->question_option_id) ; 
																//print_r($ans);
															if($tv->que_type_id == 5){
													$match_pair = 	'<span style="color:#fff; background:green; padding:2px 4px;">'.tbl_question_anser_pair($ov->question_option_id).'</span>';
															}else{
																$match_pair = '';
															}
															
															if($ans->option_answer_id == $ov->question_option_id)
															{
																$anss ='color:green';
																$icons ='<i class="fa fa-check-circle-o text-success" ></i>';
															}else{
																$anss ='';
																$icons ='';
															}
																
																echo '<li style="list-style:decimal; margin-bottom:5px; '.$anss.'">'.$ov->que_option.'&nbsp;'.$icons.' '.$match_pair.'</li>';
															}
															echo '</ol>';
														}
														
														if($tv->que_type_id == 5){
															$res_option_pair = que_option_pair($tv->que_id);
															//print_r($res_option_pair);
															echo '<ol class="col-lg-6">';
															foreach($res_option_pair as $v){
																echo '<li style="list-style:lower-alpha; margin-bottom:5px;">'.$v->pair_option.'</li>';
															}
															echo '</ol>';
														}
									
														?>
													</div>
												</div>
											</div>
											<?php 
												$i++;
											} 
												}else{
													echo '<div class="col-lg-12"><p class="alert alert-info">Question are not added</p></div>';	
												}
												?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
    <!-- Middle section End-->
	
</div>
</div>
</div>
<!-- Middle section start-->
<script>
function reuseTest(testId)
{
	var data = 'test_id='+testId;
	$.ajax({
		type:'POST',
		url:'<?php echo base_url('Dashboard/Question/reuse_test');?>',
		data:data,
		dataType:'json',
		beforeSend: function(data){
		},
		success: function (data) {
			//console.log(data);
			if(data.status == true){
				if(data.url){
					window.location.assign(data.url);
				}
			}
		}
	});	
}
</script>