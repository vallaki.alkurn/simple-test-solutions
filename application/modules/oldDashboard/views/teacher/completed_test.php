<?php //print_r($user_details); ?>
<!-- Middle section start-->
<div class="co">
		<div class="row">
				<div class="col-lg-3 col-md-3 p-0"> 
					<?php  require_once(APPPATH.'views/template/sidebar-teacher.php'); ?>
				</div>
				<div class="col-lg-9 col-md-9 p-0">
						<div class="col-lg-12">
								<div class="form-heading">Completed Test</div>
						</div>
						<div class="col-lg-12">
						<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
		                </div>
						<div class="col-lg-12" >
							<table id="example" class="display table table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>Sr.No</th>
										<th>Test Title</th>
										<th>Date</th>
										<th>Randomize Questions</th>
										<th>Timing</th>
										<th>Total Question</th>
										<th>Sent Invitations</th>
										<th>Student performed test</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
				</div>
		</div>
</div>
<script>
$(document).ready(function(){
   $('#example').DataTable({
        "bServerSide": false,
        "bDeferRender": false,
        "bProcessing": false,
		"iDisplayLength": 10,
        "order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "ajax": "<?php echo base_url('Dashboard/Teacher/getCompletedTestJson');?>",
		 "columns": [
            { "data": "test_id" },
			{ "data": "test_name" },
			{ "data": "date" },
			{ "data": "test_question_random" },
			{ "data": "test_time_limit" },
			{ "data": "total_question" },
			{ "data": "sent_invitation" },
			{ "data": "student_attempt" },
			{ "data": "action" }
        ]
    });
});
</script>