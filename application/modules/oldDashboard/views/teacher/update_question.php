<section class="middle_section inner-page update-profile loginpage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="">
							<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
      <form url="<?php echo base_url('Dashboard/Question/update_test_questions');?>" method="post" id="update_test_questions" name="update_test_questions" enctype="multipart/form-data">
								<input type="hidden" name="que_id" id="que_id" value="<?php echo $que_details->que_id ;?>" >
                                    <div class="row">
                                        <div class="col-lg-12">
                                                <div class="add-que-heading">Update Questions</div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Select Question type</label>
                                                <div class="button dropdown"> 
                                                    <select class="select-color" id="QuestionTypeSelector" name="que_type_id" id="que_type_id">
														<?php foreach(question_type() as $qtype) { 
														if($qtype->question_type_id == $que_details->que_type_id){
														?>
                                                        <option value="<?php echo $qtype->question_type_id; ?>" selected><?php echo $qtype->question_type_name; ?></option>
                                                        <?php 
														}
														} ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Enter Question</label>
                                                <input type="text" class="form-control" id="que_name" value="<?php echo $que_details->que_name; ?>" placeholder="Enter Question*" name="que_name">
                                                <span  class="typ-warning" <?php if($que_details->que_type_id == 2){ echo 'style="display:block;"';} ?>>Mention blank space in []. ex. []</span>
                                            </div>
                                        </div>
										
										  <div class="col-md-6">
												<div class="form-group ">
													<label>Upload Files</label>
													<div class="upload-file create">
														<label for="file-upload">No file select</label>
														<input id="file-upload" name='que_image' type="file" style="display:block;"> 
														<span class="upload-btn"><i class="fa fa-folder-open" aria-hidden="true"></i> Browse</span><input type="hidden" name="check_image" value="<?php echo $que_details->que_image; ?>">
													</div>
													<?php 
													if($que_details->que_image){
													 	echo '<img src="'.base_url($que_details->que_image).'" height="100">';	
													}
												?>
												</div>
												
											</div>
											<div class="col-md-6">
												<div class="form-group ">
													<label>Set Point</label>
													<input type="text" class="form-control" id="que_points" value="<?php echo $que_details->que_points; ?>" placeholder="Set Point*" name="que_points">
												</div>
											</div>
										
										<?php 
										//Get all option of question.
										$option = SelectData('tbl_question_option_1','*',
															array('que_id'=>$que_details->que_id), '', NULL);
										?>
											
                                        <div class="col-md-12">
                                            <div class="output">
											<?php if($que_details->que_type_id == 1){ ?>
                                                <div id="queTyp1" class="">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>True or False</label>
                                                            <div class="options-section">
															<?php 
																foreach($option as $k => $op){
																$ans = tbl_question_anser($op->question_option_id) ; 
																if($ans->option_answer_id == $op->question_option_id){
																	$check = 'checked';
																}else{
																	$check = '';
																}
															?>
									<div class="option">
										<input type="radio" name="option_que_true_false" 
										value="<?php echo $op->question_option_id;?>" <?php echo $check; ?>>
										<span><?php echo $op->que_option;?>
									</span>
									</div>
                                                                
																<?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
													<br>
                                                </div>
											<?php } ?>
											
											
											<?php if($que_details->que_type_id == 2){ ?>
                                                <div id="queTyp2" >
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Add options <span style="font-size:10px;">(Select options required for correct answer)</span></label>
                                                            <div id="dynamicInput">
															<?php 
																$i = 1;
																foreach($option as $k => $op){
																$ans = tbl_question_anser($op->question_option_id) ; 
																if($ans->option_answer_id == $op->question_option_id){
																	$check = 'checked';
																}else{
																	$check = '';
																}
															?>
                                                                <div class="input-options" id="option_list_<?php echo $op->question_option_id;?>">
																	<label><input type="radio" value="<?php echo $op->question_option_id;?>" name="fill_ans" <?php echo $check;?> ><span></span></label>
																	<span class="input-section">
                                                                    <!--<span class="number"><?php echo $i;?>.</span>-->
                                                                    <input type="text" name="fill_que[<?php echo $op->question_option_id;?>]" value="<?php echo $op->que_option;?>">
																	</span>
																		<!--<a href="javascript:void(0)" onClick="deleteOption(<?php echo $op->question_option_id;?>)">
																		<i class="fa fa-trash" aria-hidden="true"></i>
																		</a>-->
																	
                                                                </div>
																
																<?php $i++; 
																$list_option_id = $op->question_option_id;
																} ?>
                                                            </div>
                                                            <div class="add_option">
                                           <input type="button" value="Add more option"  onClick="addInputUp('dynamicInput');">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
												<input type="hidden" name="conter_r" id="conter_r" value="<?php echo $list_option_id+1; ?>">
												<script>
												//<span class='number'>"+(counter)+".</span>
													function addInputUp(divName){
														var counter = document.getElementById('conter_r').value;
														console.log(counter);	
														var newdiv=document.createElement('div');
														newdiv.innerHTML="<div class='input-options'><label><input type='radio' value='"+counter+"' name='fill_ans' ><span></span></label><span class='input-section'><input type='text' name='fill_que["+counter+"]'></span></div>";
														console.log(counter++);
														document.getElementById('conter_r').value = counter++;
														document.getElementById(divName).appendChild(newdiv);
													}
												</script>
												
											<?php } ?>
											<?php if($que_details->que_type_id == 3){ ?>
                                                <div id="queTyp3" >
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Add options <span style="font-size:10px;">(Select options required for correct answer)</span></label>
                                                            <div id="dynamicInput2">
															
															<?php 
																$i = 1;
																foreach($option as $k => $op){
																$ans = tbl_question_anser($op->question_option_id) ; 
																//echo $ans->option_answer_id;
																if($ans->option_answer_id == $op->question_option_id){
																	$check = 'checked';
																}else{
																	$check = '';
																}
															?>
															<div class="input-options">
																<label><input type="checkbox" value="<?php echo $op->question_option_id;?>" name="mul_ans[<?php echo $op->question_option_id;?>]" <?php echo $check;?> ><span></span></label>
																<span class="input-section">
																<!--<span class="number">1.</span>-->
																<input type="text" name="mul_que[<?php echo $op->question_option_id;?>]" value="<?php echo $op->que_option;?>">
																</span>
															</div>
															
															<?php $i++; 
															$list_option_id = $op->question_option_id;
															} ?>
                                                            </div>
                                                            <div class="add_option">
                                                                        <input type="button" value="Add more option" onClick="addInput2Up('dynamicInput2');">
                                                                  
                                                            </div>
                                                        </div>

                                                      
                                                    </div>
                                                </div>
												
												<input type="hidden" name="conter_r" id="conter_r" value="<?php echo $list_option_id+1; ?>">
												<script>
												//<span class='number'>"+(counter)+".</span>
												//<span class='number'>"+(counter)+".</span>
													function addInput2Up(divName){
														var counter = document.getElementById('conter_r').value;
														console.log(counter);	
														var newdiv=document.createElement('div');newdiv.innerHTML="<div class='input-options'><label><input type='checkbox' value='"+counter+"' name='mul_ans["+counter+"]' ><span></span></label><span class='input-section'><input type='text' name='mul_que["+counter+"]'></span></div>";
        document.getElementById(divName).appendChild(newdiv);
														console.log(counter++);
														document.getElementById('conter_r').value = counter++;
														document.getElementById(divName).appendChild(newdiv);
													}
												</script>
												
												<?php } ?>
												<?php if($que_details->que_type_id == 4){ ?>
                                                <div id="queTyp4">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group ">
                                                                <label>Answer</label>
																<?php //print_r($option[0]->que_option); ?>
                                                                <textarea class="form-control" id="short_ans" name="short_ans"  ><?php echo $option[0]->que_option;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											<?php } ?>
											
											<?php if($que_details->que_type_id == 5){ ?>
                                                <div id="queTyp5">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Set Pairs</label>
                                                            <div id="dyncInpamiut4">
															
															<?php 
																$res_option_pair = que_option_pair($tv->que_id);
																$i = 1;
																foreach($option as $k => $op){
																	$pair_d = que_option_pair_c($op->question_option_id);
																	//print_r($pair_d);
															?>
															
                                                                <div class="input-options">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                           <!-- <span class="number">1.</span>-->
                                                                            <input type="text" name="pair_que[<?php echo $op->question_option_id;?>]" value="<?php echo $op->que_option;?>">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="pair_ans[<?php echo $pair_d->option_id;?>]" value="<?php echo $pair_d->pair_option;?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
																
															<?php $i++; 
															$list_option_id = $op->question_option_id;
															} ?>
                                                                           
                                                            </div>
                                                            <div class="add_option multi-opt">
                                                                	<input type="button" value="Add more pair" onClick="addInput4Up('dyncInpamiut4');">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
												
												<input type="hidden" name="conter_r" id="conter_r" value="<?php echo $list_option_id+1; ?>">
												<script>
												//<span class='number'>"+(counter)+".</span>
												//<span class='number'>"+(counter)+".</span>
												//<span class='number'>"+(counter)+".</span>
													function addInput4Up(divName){
														var counter = document.getElementById('conter_r').value;
														console.log(counter);	
														var newdiv=document.createElement('div');newdiv.innerHTML="<div class='input-options'><div class='row'><div class='col-sm-6'><input type='text' name='pair_que["+counter+"]'></div><div class='col-sm-6'><input type='text' name='pair_ans["+counter+"]'></div></div></div>";
        document.getElementById(divName).appendChild(newdiv);
														console.log(counter++);
														document.getElementById('conter_r').value = counter++;
														document.getElementById(divName).appendChild(newdiv);
													}
												</script>
												
												<?php } ?>
                                            </div>
                                        </div>
	<!--<div class="col-md-6">
		<div class="row">
			<div class="col-md-6 " >
				<button type="submit" class="btn btn-primary no-bg-btn">add more questions</button>
			</div>
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary no-bg-btn">Select from Question Bank</button>
			</div>
		</div>
	</div>-->
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary no-bg-btn">Update Question</button>
			</div>
		</div>                                            
	</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</section>
	<style>
	
	.typ-warning{
		display:none;	
	}
	.upload-btn{
		cursor:pointer;
	}
	
	</style>
	<script type="text/javascript" src="<?php echo base_url('assets/front-design');?>/js/add-input.js"></script>
	<script type="text/javascript">
	/*$(document).ready(function() {
		$('.queTypeOuter').hide();
    	$("#btn-teacher").click(function(){
    		$("#teacher-slide").addClass('active-slide');
    		$(this).addClass('btn-active');
    		$("#btn-student").removeClass('btn-active');
    		$("#student-slide").removeClass('active-slide');
    	});
    	$("#btn-student").click(function(){
    		$("#student-slide").addClass('active-slide');
    		$(this).addClass('btn-active');
    		$("#btn-teacher").removeClass('btn-active');
    		$("#teacher-slide").removeClass('active-slide');
    	})
    });

   $(document).ready(function() {
	  $('.section-joinus').parallax({
		imageSrc: 'images/stefan-vladimirov-1299262-unsplash.jpg'
	  });

	  $('.section-contact').parallax({
		imageSrc: 'images/brooke-cagle-609880-unsplash.jpg'
	  });
	});*/
</script>
<script type="text/javascript">
    $('#file-upload').change(function() {
		var i = $(this).prev('label').clone();
		var file = $('#file-upload')[0].files[0].name;
		$(this).prev('label').text(file);
    });
	
	$('.upload-btn').on('click',function(){
		$('#file-upload').trigger('click');	
	});
$(function() {
  $('#QuestionTypeSelector').change(function(){
    $('.queTypeOuter').hide();
		if($(this).val() == 2){ 
			$('.typ-warning').css('display','block');
		}else{
			$('.typ-warning').css('display','none');
		}
        $('#queTyp' + $(this).val()).show();
    });
});
</script> 
<script type="text/javascript">
	$(function(){
		//Update Profile
		 $('#update_test_questions').validate({
			rules: {
					que_type_id: {
					   required : true,
					},
					que_name: {
						required: true,
					},
					que_points: {
						required: true,
					},
				},
			messages: {
					 que_type_id:{
						required :"Question type field is required",
					 }, 
					 que_name:{
						required :"Question field is required",
					 },
					 que_points: {
						required: "Set Point field is required",
					}
				},
			submitHandler: function(form) {
					lkForms('update_test_questions');
					//$(window).scrollTop(0);
			  }
		 });
	 });
	 
function deleteOption(optionID)
{
	var result = confirm("Are you want to delete?");
	if (result) {
	   var data = 'question_option_id='+optionID;
		$.ajax({
		  method: 'POST',
		  url: '<?php echo base_url('Dashboard/Question/delete_option');?>',
		  data: data,
		  dataType:'json',
		  //processData: false,
		  success: function(data){
			 
			$('#option_list_'+optionID).hide();
		  }
		});
	}
	
	
}
</script>