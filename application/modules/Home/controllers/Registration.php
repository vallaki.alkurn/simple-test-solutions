<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Registration extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	const  moduled='Masteradmin';
	public function __construct() {
        parent::__construct();
       $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_home_model');
	   $this->load->model('Global_model');
       $this->load->library('form_validation');
	   $this->lang->load('standard_controller_lang', 'english');
       $this->lang->load('global_controller_lang', 'english');
	   $this->load->library('user_agent');

	   include('Emails.php');
	   $this->myEmail = new Emails();
	   //error_reporting(E_ALL);
	   error_reporting(0);
	}
	
	
	//Start Teacher Functionality with registration email.
	public function Teacher()
	{
		redirect_user();
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Teacher registration',
							  'keyword' => 'Teacher registration',
							  'description' => 'Teacher registration');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('teacher',$header_array);
	}
	
	public function save_data_teacher()
	{
	
		$data = '';
		if($_POST){
			//Check Email id
			$check_email = array('table'=>'tbl_user_registration',
								'field'=>'email_address',
								'where'=>array('email_address' => $_POST['email_address'],'is_deleted !=' => 1));
								
			$check_mobile = array('table'=>'tbl_user_registration',
								'field'=>'your_phone',
								'where'=>array('your_phone' => $_POST['your_phone'],'is_deleted !=' => 1));
								
			if(check_duplicate($check_email) == true){
				$data = array('status' => false,
						      'response' => array('msg' =>$this->lang->line('duplicat_email_error')),
							  'url' => '');
							  //print_r($data);
							  //echo 1;exit;
				
			}
			/*elseif(check_duplicate($check_mobile) == true){
				
				$data = array('status' => false,
						      'response' => array('msg' =>$this->lang->line('duplicat_phone_error')),'url' => '');
				//echo 2;exit;
			}*/
			else{
				$data_array = array('first_name' => $_POST['first_name'],
								'last_name' => $_POST['last_name'],
								'email_address' => $_POST['email_address'],
								'subject_class_id' => $_POST['subject_class_id'],
								'user_password' => get_md5($_POST['user_password']),
								'organization' => $_POST['organization'],
								'organization_phone_number' => $_POST['organization_phone_number'],
								'your_phone' => $_POST['your_phone'],
								'your_title' => $_POST['your_title'],
								'address' => $_POST['address'],
								'registered_date' => date('Y-m-d H:i:s'),
								'created' =>date('Y-m-d H:i:s'),
								'user_type' =>1,
								'tearm_conditions' => $_POST['tearm_conditions'] ? $_POST['tearm_conditions'] : false);
				$res = InsertRow('tbl_user_registration', $data_array, NULL);
				if($res){
					
					//sent mail after register by user
					$user_name = $_POST['first_name'].'&nbsp;'.$_POST['last_name'];
					$html = str_replace("{{USER_NAME}}",$user_name,NEW_REGISTRATION_TEACHER);
					//$html = str_replace("{{USER_LINK}}","http://simpletestsolutions/reg-done?id=328",$html);
					$email_sent = array('to' => $_POST['email_address'],
									  'bcc' => '',
									  'html' => $html,
									  'subject' => 'Registration');
					//sent_mail($email_sent);
					$this->myEmail->sent_email($email_sent);
					
					
					$this->session->set_flashdata('msg', $this->lang->line('reg_success_teacher'));
					//$this->lang->line('reg_success_teacher')
					$data = array('status' => true,
								  'response' => array('msg' =>''),'url' => base_url('Home/Registration/plan/'.$res));
								  //echo 3;exit;
				}
			}
			
		}else{
			$data = array('status' => false,
						  'response' => array('msg' =>$this->lang->line('server_error')),'url' => base_url());
			//echo 4;exit;	
		};
		echo json_encode($data);
		exit;
	}
	
	public function check_email_format()
	{
		
		$html = str_replace("{{USER_NAME}}","Lomesh Kelwadkar",NEW_REGISTRATION_TEACHER);
		$html = str_replace("{{USER_LINK}}","http://simpletestsolutions/reg-done?id=328",$html);
		$email_s = array('to' => 'lomesh5387@gmail.com',
		  'bcc' => 'sonekar.nilesh0@gmail.com',
		  'html' => $html,
		  'subject' => 'test');
		//echo sent_mail($email_s);
		$this->myEmail->sent_email($email_s);
		//echo $this->email->print_debugger();
		//$this->myEmail->send();
		
		////Load email library
//		$this->load->library('email');
//		
//		//SMTP & mail configuration
//		$config = array(
//			'protocol'  => 'smtp',
//			'smtp_host' => 'ssl://smtp.googlemail.com',
//			'smtp_port' => 465,
//			'smtp_user' => 'lomesh.creativelogi@gmail.com',
//			'smtp_pass' => 'Lomesh@2019',
//			'mailtype'  => 'html',
//			'charset'   => 'utf-8'
//		);
//		$this->email->initialize($config);
//		$this->email->set_mailtype("html");
//		$this->email->set_newline("\r\n");
//		
//		//Email content
//		$htmlContent = '<h1>Sending email via SMTP server</h1>';
//		$htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';
//		
//		$this->email->to('lomesh5387@gmail.com');
//		$this->email->from('sender@example.com','MyWebsite');
//		$this->email->subject('How to send email via SMTP server in CodeIgniter');
//		$this->email->message($htmlContent);
//		
//		//Send email
//		$this->email->send();
//		$this->email->print_debugger();
		
		
		/*$config = array();
        $config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "mail";
        $config['smtp_host']           = "localhost";
        $config['smtp_port']           = "25";
        $config['mailtype'] = 'html';
        $config['charset']  = 'utf-8';
        $config['newline']  = "\r\n";
        $config['wordwrap'] = TRUE;*/
		
		/*$this->load->library('email');

		$this->email->from('your@example.com', 'Your Name');
		$this->email->to('lomesh5387@gmail.com');
		$this->email->cc('lomesh5387@gmail.com');
		$this->email->bcc('lomesh5387@gmail.com');
		
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		
		echo $this->email->send();
		echo $this->email->print_debugger();*/
	}
	
	public function Student()
	{
		redirect_user();
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Student registration',
							  'keyword' => 'Student registration',
							  'description' => 'Student registration');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('student',$header_array);
	}
	
	public function save_data_student()
	{
		$data = '';
		if($_POST){
			//Check Email id
			$check_email = array('table'=>'tbl_user_registration',
								'field'=>'email_address',
								'where'=>array('email_address' => $_POST['email_address'],'is_deleted !=' => 1));
								
			if(check_duplicate($check_email) == true){
				$data = array('status' => false,
						      'response' => array('msg' =>$this->lang->line('duplicat_email_error')),
							  'url' => '');
			}else{
				$data_array = array('first_name' => $_POST['first_name'],
								'last_name' => $_POST['last_name'],
								'email_address' => $_POST['email_address'],
								'standard_class_id' => $_POST['standard_class_id'],
								'school_collage' => $_POST['school_collage'],
								'user_password' => get_md5($_POST['user_password']),
								'verification_by_email' => get_md5($_POST['email_address']),
								'registered_date' => date('Y-m-d H:i:s'),
								'created' =>date('Y-m-d H:i:s'),
								'user_type' =>2,
								'tearm_conditions' => $_POST['tearm_conditions'] ? $_POST['tearm_conditions'] : true);
				$res = InsertRow('tbl_user_registration', $data_array, NULL);
				
				if($res){
					
					$verification_by_email_url = base_url('verification-by-email/?verificationCode='.get_md5($_POST['email_address']));
					
					//sent mail after register by user
					$user_name = $_POST['first_name'].'&nbsp;'.$_POST['last_name'];
					$html = str_replace("{{USER_NAME}}",$user_name,NEW_REGISTRATION_STUDENT);
					$html = str_replace("{{USER_LINK}}",$verification_by_email_url,$html);
					$email_sent = array('to' => $_POST['email_address'],
									  'bcc' => '',
									  'html' => $html,
									  'subject' => 'Registration');
					//sent_mail($email_sent);
					$this->myEmail->sent_email($email_sent);
					$this->session->set_flashdata('msg', $this->lang->line('reg_success_student'));
					$data = array('status' => true,
								  'response' => array('msg' =>$this->lang->line('reg_success_student')),'url' => base_url('login'));
				}
			}
			
		}else{
			$data = array('status' => false,
						  'response' => '','url' => '');	
		};
		echo json_encode($data);
		exit;
	}
	
	
	public function verification_by_email($verification_by_email = NULL)
	{
		//echo $_GET['verificationCode'];
		if($_GET['verificationCode']){
			$access_code = array('verification_by_email' => $_GET['verificationCode'],
								'verification_by_email_status'=>false);
			$res_data = SelectData('tbl_user_registration', 'user_id', $access_code, 1);
			//print_r($res_data);
			if($res_data){
				$arra_data = array('verification_by_email_status'=>true,
								  'is_profile_status'=>1);
				UpdateRow('tbl_user_registration',$arra_data, array('user_id'=>$res_data->user_id));
				//$data['msg'] =$this->lang->line('user_verify_account_success');
				$this->session->set_flashdata('msg', $this->lang->line('user_verify_account_success'));
				redirect('login');
	                     	///exit;
				//$this->load->front_view('login');
			}else{
				$this->load->front_view('template/404');
			}
			//print_r($res_data->user_id);
		}else{
			$this->load->front_view('template/404');
		}
		
	}
	
	  public function plan($id)
	{
		
		$this->id =$id;
			$this->load->front_view('plan');
	}
	 public function payment($id)
	{
		$this->id =$id;
		$this->load->front_view('payment');
	}
	public function save_card_details()
	 {
		 if($_POST)
		 {
			
$user_id =$_POST['user_id'];
			 
			 $data_array=array(
			   "user_id" =>$user_id,
			   "card_number" =>$_POST['card_number'],
			   "cardholder_name" =>$_POST['cardholder_name'],
			   "cvv_number" =>$_POST['cvv_number'],
			   "expiry_date" =>$_POST['expiry_date'],
			 
			 );
			 	 $res = InsertRow('tbl_user_card_details', $data_array, NULL);
				 
		
			 if($res){
				 $arra_data = array('paid_membership'=>'yes','is_profile_status'=>1);
				UpdateRow('tbl_user_registration',$arra_data, array('user_id'=>$user_id));
				//$this->user_subscription($_POST['user_id']);
				$this->session->set_flashdata('msg', $this->lang->line('reg_success_teacher'));
				 $data = array('status' => true,
								'response' => array('msg' =>$this->lang->line('reg_success_teacher')),
								'url' => base_url('login'));
				
				//After save card details change membership status.
				 $user_session_data = get_user();
		         $user_session_data->paid_membership = 'yes';
										
			 }else{
			 
		    $data = array('status' => false,
		 				  'response' => '','url' => '');	
		    }
			$user_data = SelectAll('tbl_user_registration','*',array('user_id'=>$user_id), 1, NULL);
			// print_r($user_data);exit;
			 if($user_data[0]->is_profile_status == '0')
			 {
				 UpdateRow('tbl_user_login_status', array('logOut' => date('Y-m-d H:i:s')), 
					array('session_id'=>$this->session->userdata('last_session_id')));
				$this->session->sess_destroy();
			 }
		 }else{
			 $data = array('status' => false,
		 				  'response' => '','url' => '');	
		 }
		 
		
		echo json_encode($data);
		exit;
	 }
	 public function user_subscription($user_id)
	 {
		 
		  $req1['itemName'] = 'Member Subscriptions';
			$req1['itemNumber'] = 'MS'.$user_id;

			//subscription price for one month
			$req1['itemPrice'] = 50.00;
			 $req1['paypalURL']     = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	$req1['paypalID']     = 'mangesh.alkurn-facilitator@gmail.com';
	$req1['successURL'] = '';
	$req1['cancelURL']    = '';
	$req1['notifyURL']     = '';
	//$req1['cmd']     = '_xclick-subscriptions';
	$req1['currency_code']     = 'USD';
	$req1['business']     = 'mangesh.alkurn-facilitator@gmail.com';
	$req1['custom']     = $user_id;
		 
		/*$myPost = array();
foreach ($raw_post_array as $keyval) {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $myPost[$keyval[0]] = urldecode($keyval[1]);
}*/

// Read the post from PayPal system and add 'cmd'
$req = '_xclick-subscriptions';
if(function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}
foreach ($req1 as $key => $value) {
    if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}

/*
 * Post IPN data back to PayPal to validate the IPN data is genuine
 * Without this step anyone can fake IPN data
 */
$paypalURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
$ch = curl_init($paypalURL);
if ($ch == FALSE) {
    return FALSE;
}
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSLVERSION, 6);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: company-name'));
$res = curl_exec($ch); 
		 
		 
						
				/*
				 * Inspect IPN validation result and act accordingly
				 * Split response headers and payload, a better way for strcmp
				 */ 
				$tokens = explode("\r\n\r\n", trim($res));
				$res = trim(end($tokens));
				if (strcmp($res, "VERIFIED") == 0 || strcasecmp($res, "VERIFIED") == 0) {
					//Include DB configuration file
					include 'dbConfig.php';
					
					$unitPrice = 25;
					
					//Payment data
					$subscr_id = $_POST['subscr_id'];
					$payer_email = $_POST['payer_email'];
					$item_number = $_POST['item_number'];
					$txn_id = $_POST['txn_id'];
					$payment_gross = $_POST['mc_gross'];
					$currency_code = $_POST['mc_currency'];
					$payment_status = $_POST['payment_status'];
					$custom = $_POST['custom'];
					$subscr_month = ($payment_gross/$unitPrice);
					$subscr_days = ($subscr_month*30);
					$subscr_date_from = date("Y-m-d H:i:s");
					$subscr_date_to = date("Y-m-d H:i:s", strtotime($subscr_date_from. ' + '.$subscr_days.' days'));
					
					if(!empty($txn_id)){
						//Check if subscription data exists with the same TXN ID.
						$prevPayment = $this->db->query("SELECT id FROM user_subscriptions WHERE txn_id = '".$txn_id."'");
						if($prevPayment->num_rows > 0){
							exit();
						}else{
							//Insert tansaction data into the database
							$insert = $this->db->query("INSERT INTO user_subscriptions(user_id,validity,valid_from,valid_to,item_number,txn_id,payment_gross,currency_code,subscr_id,payment_status,payer_email) VALUES('".$custom."','".$subscr_month."','".$subscr_date_from."','".$subscr_date_to."','".$item_number."','".$txn_id."','".$payment_gross."','".$currency_code."','".$subscr_id."','".$payment_status."','".$payer_email."')");
							
							//Update subscription id in users table
							if($insert){
								$subscription_id = $db->insert_id;
								$update = $db->query("UPDATE tbl_card_details SET subscription_id = {$subscription_id} WHERE user_id = {$custom}");
							}
						}
					}
				}
				die;
	 }
  
}