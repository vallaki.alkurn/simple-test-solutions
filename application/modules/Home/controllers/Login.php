<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Login extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	const  moduled='Masteradmin';
	public function __construct() {
        parent::__construct();
      // $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_home_model');
	   $this->load->model('Global_model');
       $this->load->library('form_validation');
	   $this->lang->load('standard_controller_lang', 'english');
       $this->lang->load('global_controller_lang', 'english');
	   $this->load->library('user_agent');
	   error_reporting(E_ALL);
	}
	
	
	public function login()
	{
		$this->load->front_view('login');
	}
	 
	 
  
}