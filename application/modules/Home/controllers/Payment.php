<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Payment extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	const  moduled='Masteradmin';
	public function __construct() {
        parent::__construct();
       $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_home_model');
	   $this->load->model('Global_model');
	   $this->load->model('Payment_model');
       $this->load->library('form_validation');
	   $this->lang->load('standard_controller_lang', 'english');
       $this->lang->load('global_controller_lang', 'english');
	   $this->load->library('user_agent');

	   include('Emails.php');
	   $this->myEmail = new Emails();
	   //error_reporting(E_ALL);
	   error_reporting(0);
	   
	   $this->load->helper('url');
		
		// Load PayPal library
		$this->config->load('paypal');
		
		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
		);
		
		// Show Errors
		if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}
		
		$this->load->library('paypal/Paypal_pro', $config);	
	}
	
	
	//Start Teacher Functionality with registration email.
	public function pay()
	{
		redirect_user();
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Teacher registration',
							  'keyword' => 'Teacher registration',
							  'description' => 'Teacher registration');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('teacher',$header_array);
	}
	 public function generate_payment()
	 {
		 error_reporting(0);
		 $res_data = SelectData('tbl_user_card_details', NULL,'status=1', NULL);
		//print_r($res_data);exit;
		 if($res_data){
			 foreach($res_data as $row)
			 {
				 $user_data = $this->Global_model->getUserData($row->user_id);
				  $registered_date =$user_data->registered_date;
				 $card_type ='MasterCard'; 
				 $date_after_trial= date('Y-m-d', strtotime($registered_date. ' + 14 day'));
				 $current_date =date('Y-m-d');
				 if($current_date == $date_after_trial){
					 // paypal link add here
					//echo $row->expiry_date;exit;
					$res=$this->Do_direct_payment($card_type,$row->card_number,$row->expiry_date,$row->cvv_number);
				//	print_r($res);exit;
					if($res['PayPalResult']['ACK'] == 'Success' )
					{
						
							$user_data = $this->Payment_model->UpdatePaymentData($row->user_id,$res['PayPalResult']['TRANSACTIONID'],$res['PayPalResult']['AMT']);
					}else{
					
					}
					// print_r($res);exit;
				 }
				 
			 }
		 }
		 echo "cron updated";
	 }
	/* function payment($id){ 
        $data = array(); 
         
        // Get product data from the database 
      
            // Buyer information 
            $name = 'nilesh sonekar'; 
            $nameArr = explode(' ', $name); 
            $firstName = !empty($nameArr[0])?$nameArr[0]:''; 
            $lastName = !empty($nameArr[1])?$nameArr[1]:''; 
            $city = 'Charleston'; 
            $zipcode = '25301'; 
            $countryCode = 'US'; 
             
            // Card details 
            $creditCardNumber = trim(str_replace(" ","",'1234123412341234')); 
            $creditCardType ='MasterCard';
            $expMonth = 02; 
            $expYear = 2023; 
            $cvv = '235'; 
             
            // Load PaypalPro library 
            $this->load->library('paypalpro'); 
             
            // Payment details 
            $paypalParams = array( 
                'paymentAction' => 'Sale', 
                'itemName' => 'products', 
                'itemNumber' => '001', 
                'amount' => '50', 
                'currencyCode' => 'USA', 
                'creditCardType' => $creditCardType,
                'creditCardNumber' => $creditCardNumber, 
                'expMonth' => $expMonth, 
                'expYear' => $expYear, 
                'cvv' => $cvv, 
                'firstName' => $firstName, 
                'lastName' => $lastName, 
                'city' => $city, 
                'zip'    => $zipcode, 
                'countryCode' => $countryCode, 
            ); 
            $response = $this->paypalpro->paypalCall($paypalParams); 
            $paymentStatus = strtoupper($response["ACK"]); 
            if($paymentStatus == "SUCCESS"){ 
                // Transaction info 
                $transactionID = $response['TRANSACTIONID']; 
                $paidAmount = $response['AMT']; 
                $currency = $response['CURRENCYCODE']; 
                 
                // Insert the transaction data in the database 
                $txnData['product_id'] = $id; 
                $txnData['buyer_name'] = $name; 
                $txnData['buyer_email']    = ''; 
                $txnData['card_num'] = $creditCardNumber; 
                $txnData['card_cvc'] = $cvv; 
                $txnData['card_exp_month'] = $expMonth; 
                $txnData['card_exp_year'] = $expYear; 
                $txnData['paid_amount'] = $paidAmount; 
                $txnData['paid_amount_currency'] = $currency; 
                $txnData['payment_txn_id'] = $transactionID; 
                $txnData['payment_status'] = $paymentStatus; 
 
                $insert = $this->product->insertOrder($txnData); 
                 
                $data['status'] = 1; 
                $data['orderID'] = $transactionID; 
            }else{ 
                 $data['status'] = 0; 
            } 
       
         
        // Transaction status 
        echo json_encode($data); 
    } */
	function Do_direct_payment($card_type,$card_number,$exp_date,$cvv)
	{
		
		$DPFields = array(
							'paymentaction' => 'Sale', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => $_SERVER['REMOTE_ADDR'], 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '1' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $card_type, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $card_number, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $exp_date, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
						);
				/*		$CCDetails = array(
							'creditcardtype' => 'MasterCard', 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => '5424180818927383', 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => '102020', 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => '123', 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
						);*/
						
						
		$PayerInfo = array(
							'email' => 'test@domain.com', 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => '', 						// Status of payer.  Values are verified or unverified
							'business' => 'Testers, LLC' 							// Payer's business name.
						);
						
		$PayerName = array(
							'salutation' => 'Mr.', 						// Payer's salutation.  20 char max.
							'firstname' => 'Tester', 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => 'Testerson', 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
						
		$BillingAddress = array(
								'street' => '123 Test Ave.', 						// Required.  First street address.
								'street2' => '', 						// Second street address.
								'city' => 'Kansas City', 							// Required.  Name of City.
								'state' => 'MO', 							// Required. Name of State or Province.
								'countrycode' => 'US', 					// Required.  Country code.
								'zip' => '64111', 							// Required.  Postal code of payer.
								'phonenum' => '555-555-5555' 						// Phone Number of payer.  20 char max.
							);
							
		$ShippingAddress = array(
								'shiptoname' => 'Tester Testerson', 					// Required if shipping is included.  Person's name associated with this address.  32 char max.
								'shiptostreet' => '123 Test Ave.', 					// Required if shipping is included.  First street address.  100 char max.
								'shiptostreet2' => '', 					// Second street address.  100 char max.
								'shiptocity' => 'Kansas City', 					// Required if shipping is included.  Name of city.  40 char max.
								'shiptostate' => 'MO', 					// Required if shipping is included.  Name of state or province.  40 char max.
								'shiptozip' => '64111', 						// Required if shipping is included.  Postal code of shipping address.  20 char max.
								'shiptocountry' => 'US', 					// Required if shipping is included.  Country code of shipping address.  2 char max.
								'shiptophonenum' => '555-555-5555'					// Phone number for shipping address.  20 char max.
								);
							
		$PaymentDetails = array(
								'amt' => '50.00', 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => 'USD', 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '50.00', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.  
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => 'Web Order', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'notifyurl' => ''						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
							);	
				
		$OrderItems = array();
		$Item	 = array(
							'l_name' => 'Membership', 						// Item Name.  127 char max.
							'l_desc' => 'The best test widget on the planet!', 						// Item description.  127 char max.
							'l_amt' => '50.00', 							// Cost of individual item.
							'l_number' => '123', 						// Item Number.  127 char max.
							'l_qty' => '1', 							// Item quantity.  Must be any positive integer.  
							'l_taxamt' => '', 						// Item's sales tax amount.
							'l_ebayitemnumber' => '', 				// eBay auction number of item.
							'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
							'l_ebayitemorderid' => '' 				// eBay order ID for the item.
					);
		array_push($OrderItems, $Item);
		
		$Secure3D = array(
						  'authstatus3d' => '', 
						  'mpivendor3ds' => '', 
						  'cavv' => '', 
						  'eci3ds' => '', 
						  'xid' => ''
						  );
						  
		$PayPalRequestData = array(
								'DPFields' => $DPFields, 
								'CCDetails' => $CCDetails, 
								'PayerInfo' => $PayerInfo, 
								'PayerName' => $PayerName, 
								'BillingAddress' => $BillingAddress, 
								'ShippingAddress' => $ShippingAddress, 
								'PaymentDetails' => $PaymentDetails, 
								'OrderItems' => $OrderItems, 
								'Secure3D' => $Secure3D
							);
							
		$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
		
		if(!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK']))
		{
			$data = array('Errors'=>$PayPalResult['ERRORS']);
			//$this->load->view('paypal/samples/error',$errors);
		}
		else
		{
			// Successful call.  Load view or whatever you need to do here.
			$data = array('PayPalResult'=>$PayPalResult);
		 //$this->load->view('paypal/samples/do_direct_payment',$data);
		}
		return $data;
		//return $this->paypal_pro->APICallSuccessful($PayPalResult['ACK']);
	}

  
}