<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Users extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	const  moduled='Masteradmin';
	 public function __construct() {
        parent::__construct();
       $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_home_model');
	   $this->load->model('Global_model');
       $this->load->library('form_validation');
	}
	 
	 public function UserRegistration()
	 {
		 	if(isset($_POST))
			{
			/*******Server side validation for all feilds***********/
      	    $configValidate = array(
                   array(
                         'field'   => 'name',
                         'label'   => 'Name',
                         'rules'   => 'required'
                      ),
                   array(
                         'field'   => 'email_reg',
                         'label'   => 'Email',
                         'rules'   => 'required'
                      ),
                   array(
                         'field'   => 'mobile',
                         'Mobile'   => 'Mobile',
                         'rules'   => 'required'
                      ),
                   array(
                         'field'   => 'password',
                         'label'   => 'Password',
                         'rules'   => 'required'
                      )
                 );
            serverValidation($configValidate);
		 	$checkEmail = DuplicationCheck('tbl_users','email',array('email'=>$_POST['email_reg']));
			$checkMobile = DuplicationCheck('tbl_users','mobile',array('mobile'=>$_POST['mobile']));
			if($checkEmail)
			{
				  echo '{"code":"100","message":"Email id already Exist"}';
			}
			else if($checkMobile)
			{
				  echo '{"code":"100","message":"Mobile No already exist"}';
			}
				$data = array('name'=>$_POST['name'],
							  'email'=>strtolower($_POST['email_reg']),
							  'mobile'=>$_POST['mobile'],
							  'password'=>md5($_POST['password']),
							  'usertype'=>1);
				$email = strtolower($_POST['email_reg']);		  
				$header_body = mail_header();
				$footer_body = mail_footer();
				/*---------- Attach All body Parts-------------*/
				$html = '';
				$html .= $header_body;
			    $html .= '<tbody>
						   <tr>
                           <td style="padding:20px;">
							Hello '.$_POST['name'].',<br>
							Welcome to ShopperDeals<br><br>
							You have just entered into the world of offers, deals and discounts. Please find below your login details. Download the app and start finding great deals.
							<br><br>
							Login User Name: '.$email.'<br>
							Password : '.$_POST['password'].'<br><br>
							Download the App  Now
							<a href="https://play.google.com/store/apps/details?id=com.lambros.shopper">Click Here</a>
							<br><br>Find Great deals on www.shopperdeals.in
							<br><br>Regards<br>
							ShopperDeals Team<br>
							contact@shopperdeals.in
							</td>
							</tr>
							</tbody>';	
				$html .= $footer_body;	
				$textMsg = 'Welcome to ShopperDeals Your registration was successful. Please check your registered mail for more details and offers.';
				$SmsSent =  SmsSent($_POST['mobile'],$textMsg);
				require_once('Email.php');
				$SendEmail = new Email();
				$mail = $SendEmail->SendMail('',$email,'','','Welcome to ShopperDeals Your registration was successful',$html);				  			
				$UserData = $this->Global_model->InsertRecord('tbl_users',$data);
				/*******get user type for store in session**********/
				$getUserType = $this->Global_model->GetsingleData('tbl_users','usertype',array('_id'=>$UserData));
				$_SESSION['userId']  = $UserData;
				$_SESSION['userType']  = $getUserType->usertype;
				
				echo '{"code":"200","message":"Your account has been created successfully","url":"'.$_POST['redirect_url'].'"}';
				exit;		
			}
			else
			{
				 echo '{"code":"100","message":"Invalid Record"}';
			}
			exit;	 
	 }
	 
	 
	 public function UserLoginFirst()
	 {
		if($_POST)
		{
		/*******Server side validation for all feilds***********/
		$configValidate = array(
							   array(
									 'field'   => 'email',
									 'label'   => 'Email',
									 'rules'   => 'required'
								  	)
							   		);
		serverValidation($configValidate);
		$checkCond = "email = '".$_POST['email']."' || tracking_id = '".strtolower($_POST['email'])."'";
		$checkEmail = $this->Global_model->GetsingleData('tbl_users','_id,name,usertype',$checkCond);
		if($checkEmail)
		{
			//$_SESSION['userId'] = $checkEmail->_id;
			echo '{"code":"200","message":"success","user_id":"'.$checkEmail->_id.'","usertype":"'.$checkEmail->usertype.'"}';
		}
		else
		{
			echo '{"code":"100","message":"Email id Not Exist"}';
		}
		}
		else
		{
			echo '{"code":"100","message":"Invalid Record"}';
		}
		exit;	
	 }
	 
	
	 
	 /*************Second Login*************/
	 public function UserLoginSecond()
	 {
		if($_POST)
		{
		/*******Server side validation for all feilds***********/
		$configValidate = array(
							   array(
									 'field'   => 'login_password',
									 'label'   => 'Password',
									 'rules'   => 'required'
								  )
							    );
		serverValidation($configValidate);
		$checkEmail = $this->Global_model->GetsingleData('tbl_users','_id,name,usertype',array('_id'=>$_POST['login_id'],'usertype'=>$_POST['usertype'],'password'=>md5($_POST['login_password'])));
		$_SESSION['userId'] = $checkEmail->_id;
		$_SESSION['userType'] = $checkEmail->usertype;
		if($checkEmail)
		{
			  $_SESSION['userId'] = $checkEmail->_id;
			  $_SESSION['userType'] = $checkEmail->usertype;
			  echo '{"code":"200","message":"success","user_id":"'.$checkEmail->_id.'","url":"'.$_POST['redirect_url'].'"}';
		}
		else
		{
			 echo '{"code":"100","message":"Incorrect Password"}';
		}
		}
		else
		{
			 echo '{"code":"100","message":"Invalid Record"}';
		}
		exit;	
	 }
	 
	 
	 	 
	 public function forgotPassword()
	 {
		date_default_timezone_set('Asia/Calcutta'); 
		$condOr = "(tracking_id = '".$_POST['email_forgot']."' AND usertype = 2) OR (email = '".strtolower($_POST['email_forgot'])."' AND usertype = 1)";
		$getPassword = $this->Global_model->GetsingleData('tbl_users','_id,name,email,mobile,token',$condOr);
		if($getPassword)
		{
		$token = session_id();
		$updateToken = $this->Global_model->UpdateRecord('tbl_users',array('token'=>$token,'modified'=>date('Y-m-d H:i:s')),array('_id'=>$getPassword->_id));
		$header_body = mail_header();
		$footer_body = mail_footer();
		/*---------- Attach All body Parts-------------*/
		$html = '';
		$html .= $header_body;
		$html .= '<tbody>
				  <tr>
				  <td style="padding:20px;">
				  Dear '.$getPassword->name.',<br><br> 
				  To reset your password click the link below.
				  <a href="'.base_url('Home/users/reset_password/'.$getPassword->_id.'/'.$token.'').'">Click Here</a>
				  This link will expire in two hours.
				  <br><br>Regards <br>
				  Shopeerdeals
				  </td>
				  </tr>
				  </tbody>';	
		$html .= $footer_body;	
		$textMsg = 'To reset your password click the link below '.base_url('Home/Users/reset_password/'.$getPassword->_id.'/'.$token);
		$SmsSent =  SmsSent($getPassword->mobile,$textMsg);
		require_once('Email.php');
		$SendEmail = new Email();
		$mail = $SendEmail->SendMail('','amolkharate.wwg@gmail.com','','','Shopperdeals - Reset your password',$html);
		if($mail)
		{
			echo '{"code":"200","message":"Password reset link send has been your Email id and Mobile"}';
		}
		else
		{
			echo  '{"code":"100","message":"Email not sent successfully"}';
		}
		}
		else
		{
		 	echo  '{"code":"100","message":"Invalid email id"}';
		}
		exit;
	 }
	 
	 public function reset_password()
	 {
		 $userId = $this->uri->segment(4);
		 $tokenId = $this->uri->segment(5);
		 $getPassword = $this->Global_model->GetsingleData('tbl_users','_id,modified',array('_id'=> $userId,'token'=>$tokenId));
		 //echo '<pre>';
		 //print_r($getPassword);
		 $PasswordLinktime = date('Y-m-d H:i:s',strtotime("+2 hours", strtotime($getPassword->modified)));
		 $currentTime = date('Y-m-d H:i:s');
		 if($currentTime < $PasswordLinktime)
		 {
			$data['resetpassword'] = array('expire_link'=>0,
						  				   'user_id'=>$userId,
										   'token'=>$tokenId);
		 }else
		 {
			$data['resetpassword'] = array('expire_link'=>1);
		 }
		 $this->load->front_view('reset_password',$data);
	}
	 
	 public function update_password()
	 {
		 $passwordReset = md5($_POST['re_password']);
		 $updatepasswrd = $this->Global_model->UpdateRecord('tbl_users',array('password'=>$passwordReset),array('_id'=>$_POST['id']));
		 if($updatepasswrd)
		 {
			$this->session->set_flashdata('success', 'Password Updated Successfully');
			redirect('Home/Users/reset_password/'.$_POST['id'].'/'.$_POST['token'].'');
		 }
		 exit;
	 }


	/*********************MY Account design**************/
	public function myaccount()
	{
		if($_SESSION['userId'])
		{
		  $wallet_data = $this->Global_model->GetsingleData('tbl_wallet','wallet_amount', array('userId' =>$_SESSION['userId']));
		  $data['PendingOrders'] = $this->Global_model->GetsingleData('tbl_cashback_users_by_panel','count(*) as pendingOrder', array('user_id' =>$_SESSION['userId'],'cashback_status' =>0,'is_deleted'=>0));
		  
		  $data['openTicket'] = $this->Global_model->GetsingleData('tbl_raise_ticket','count(*) as openTicket', array('user_id' =>$_SESSION['userId'],'is_status' =>0,'is_deleted'=>0));
		  
		   // $data['cashbackToCustomer'] = $this->Global_model->GetsingleData('tbl_cashback_users_by_panel','sum(cashback_to_customer_amount) as earningCustomer', array('user_id' =>$_SESSION['userId'],'cashback_status' =>1,'is_deleted'=>0));

		  $data['cashbackToCustomer'] = $this->Global_model->GetsingleData('tbl_imway_transerd','sum(transfer_ammount) as earningCustomer', array('user_id' =>$_SESSION['userId'],'transfer_status'=>1));

		  if(count($wallet_data)>0)
		  {
		  	$data['wallet_data'] = $wallet_data->wallet_amount;
		  }
		  else
		  {
		  	$data['wallet_data'] = 0;
		  }
		}
		$this->load->front_view('Home/user_section/my_dashboard',$data);	
	}
	
	public function myorders()
	{
		$this->load->front_view('Home/user_section/my_orders');	
	}
	
	public function redirectUrl()
	{
		$this->load->view('Home/redirect_page');	
	}
	
	
	public function Logout()
	{
		unset($_SESSION['userId']);
		unset($_SESSION['userType']);
		//session_destroy();
		redirect('/');
	}
	
	/*******my wallet**********/
	public function my_passbook()
	{
		$this->load->front_view('Home/user_section/my_passbook');
	}
	
	/***********Bank details**********/
	public function myBankDetails()
	{
		$data['getBank'] = $this->Global_model->GetsingleData('tbl_users_bank_details','*',array('userId'=>$_SESSION['userId']));
		$this->load->front_view('Home/user_section/my_bank_details',$data);	
	} 
	
	public function AddBankDetails()
	{
		 if(isset($_POST))
		 {
			/*******Server side validation for all feilds***********/
			$configValidate = array(
							   array(
									 'field'   => 'name_on_account',
									 'label'   => 'Name Of Account Holder',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'ifsc_code',
									 'label'   => 'Bank Name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'bank_name',
									 'Mobile'   => 'Bank Name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'branch_name',
									 'label'   => 'Branch Name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'address',
									 'label'   => 'Address',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'city',
									 'label'   => 'City',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'district',
									 'label'   => 'District',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'state',
									 'label'   => 'State',
									 'rules'   => 'required'
								  )
							   ,
							   array(
									 'field'   => 'account_type',
									 'label'   => 'Account Type',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'account_no',
									 'label'   => 'Account No',
									 'rules'   => 'required'
								  )
								);
			serverValidation($configValidate);
			$data = array('name_on_account'=>$_POST['name_on_account'],
						  'userId'=>$_SESSION['userId'],
						  'bank_name'=>$_POST['bank_name'],
						  'branch_name'=>$_POST['branch_name'],
						  'address'=>$_POST['address'],
						  'city'=>$_POST['city'],
						  'district'=>$_POST['district'],
						  'state'=>$_POST['state'],
						  'account_type'=>$_POST['account_type'],
						  'account_no'=>$_POST['account_no'],
						  'ifsc_code'=>$_POST['ifsc_code'],
						  'micr_code'=>$_POST['micr_code'],
						  'modified'=>date('Y-m-d H:i:s'),
						  );
		  
		    $getUserData = $this->Global_model->GetsingleData('tbl_users_bank_details','*',array('userId'=>$_SESSION['userId']));
			if($getUserData)
			{
				$UserData = $this->Global_model->UpdateRecord('tbl_users_bank_details',$data,array('userId'=>$_SESSION['userId']));
				$dataPriviousData =      array('name_on_account'=>$getUserData->name_on_account,
											   'userId'=>$_SESSION['userId'],
											   'bank_name'=>$getUserData->bank_name,
											   'branch_name'=>$getUserData->branch_name,
											   'address'=>$getUserData->address,
											   'city'=>$getUserData->city,
											   'district'=>$getUserData->district,
											   'state'=>$getUserData->state,
											   'account_type'=>$getUserData->account_type,
											   'account_no'=>$getUserData->account_no,
											   'ifsc_code'=>$getUserData->ifsc_code,
											   'micr_code'=>$getUserData->micr_code,
											   'modified'=>date('Y-m-d H:i:s'),
											   );
				$PriviousData = $this->Global_model->InsertRecord('tbl_users_bank_details_histroy',$dataPriviousData);
					
			}
			else
			{
				$PriviousData = $this->Global_model->InsertRecord('tbl_users_bank_details_histroy',$data);
				$UserData = $this->Global_model->InsertRecord('tbl_users_bank_details',$data);
			}
				
			echo '{"code":"200","message":"Your account has been created successfully","url":"'.$_POST['redirect_url'].'"}';
			}
			else
			{
				 echo '{"code":"100","message":"Invalid Record"}';
			}
			exit;	
	 }
	 
	 
	 /*******************mypasswordsetting******************/
	 public function MypasswordSetting()
	 {
		$data['getUser'] = getUser($_SESSION['userId']);
		//print_r($getUser->password);
		//exit;
		$this->load->front_view('Home/user_section/my_passwordSetting',$data);	
	 }
	 
	public function UpdatePassword()
	 {
		 //echo md5('amol1234');
		 if(isset($_POST))
		 {
			/*******Server side validation for all feilds***********/
			$configValidate = 	 array(
								  array(
										 'field'   => 'old_password',
										 'label'   => 'Old Password',
										 'rules'   => 'required'
									   ),
								  array(
										 'field'   => 'password',
										 'label'   => 'Password',
										 'rules'   => 'required'
									  )
								  );
			serverValidation($configValidate);
			$getUserData = $this->Global_model->GetsingleData('tbl_users','*',array('_id'=>$_SESSION['userId'],'password'=>md5($_POST['old_password'])));	
			if($getUserData)
			{
			$data = array(
						 'password'=>md5($_POST['password'])
						 );
			$UserData = $this->Global_model->UpdateRecord('tbl_users',$data,array('_id'=>$_SESSION['userId']));
			echo '{"code":"200","message":"Your Password has been updated successfully"}';
			}
			else
			{
			echo '{"code":"100","message":"Old password enter is incorrect"}';	
			}
			}
			else
			{
				 echo '{"code":"100","message":"Invalid Record"}';
			}
			exit;
	 }
	 
	 
	  /*******************mypasswordsetting******************/
	 public function My_personalDetails()
	 {
		$data['cityList'] =  $this->Global_model->GetmultiData('tbl_city');
		$data['getUser'] = getUser($_SESSION['userId']);
		$this->load->front_view('Home/user_section/My_personalDetails',$data);	
	 }
	 
	 public function UpdatePersonalDetails()
	 {
		 //echo md5('amol1234');
		 if(isset($_POST))
		 {
			/*******Server side validation for all feilds***********/
			$configValidate = array(
							   array(
									 'field'   => 'name',
									 'label'   => 'name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'email',
									 'label'   => 'Email',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'mobile',
									 'label'   => 'Mobile',
									 'rules'   => 'required'
								  )
								);
			serverValidation($configValidate);
			$getUserData = $this->Global_model->GetsingleData('tbl_users','*',array('_id'=>$_SESSION['userId']));	
			//print_r($getUserData);
			if($getUserData)
			{
			$data = array(
						   'name'=>$_POST['name'],
						   'mobile'=>$_POST['mobile'],
						   'pincode'=>$_POST['pincode'],
						   'city_id'=>$_POST['city_id']
						 );
			$UserData = $this->Global_model->UpdateRecord('tbl_users',$data,array('_id'=>$_SESSION['userId']));
			echo '{"code":"200","message":"Personal details has been updated successfully"}';
			}
			}
			else
			{
				 echo '{"code":"100","message":"Invalid Record"}';
			}
			exit;
	 }
	 
	 /**********My Orders************/
	 public function activeOrder()
	 {
		$this->load->front_view('Home/user_section/active_orders');	
	 }
	 
	 public function cancelledOrder()
	 {
		$this->load->front_view('Home/user_section/cancelled_order');	
	 }
	 
	 public function orderHistroy()
	 {
		$this->load->front_view('Home/user_section/order_histroy');	
	 }
	 
	  
	 /*******************Raise Ticket******************/
	 public function raiseTicket()
	 {
		$data['getStoreDetails'] = $this->Global_model->getStore_groupBy();
		$this->load->front_view('Home/user_section/raiseTicket',$data);	
	 }
	 
	 public function AddraiseTicket()
	{
		 if(isset($_POST))
		 {
			/*******Server side validation for all feilds***********/
			$configValidate = array(
							   array(
									 'field'   => 'date_of_transaction',
									 'label'   => 'Date of Transaction',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'product_name',
									 'label'   => 'Product Name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'store_name',
									 'Mobile'  => 'Store Name',
									 'rules'   => 'required'
								  ),
							   array(
									 'field'   => 'transaction_amount',
									 'label'   => 'Transaction Amount',
									 'rules'   => 'required'
								  )
								);
			serverValidation($configValidate);
			
			$SeoCatName = seoUrl($_POST['store_name'].'-'.$_POST['date_of_transaction'].'-'.$_SESSION['userId']);
            $LoginUser =  $this->Global_model->getCreatedByName($_SESSION['user_id']);
            if(!empty($_FILES["ticket_screenshot"]["name"]))
            {
                $extension= pathinfo($_FILES["ticket_screenshot"]["name"], PATHINFO_EXTENSION);
                $newfilename=  $SeoCatName.".".$extension;
                $category_images = UploadImage($newfilename,50,50,'images/raise-ticket/','images/raise-ticket/thumb/','ticket_screenshot');
                $category_image = $category_images['picture'];
                $thumb_image = $category_images['thumbImg'];
            }
            else
            {
                $category_image = $_POST['actual_image'];
                $thumb_image = $_POST['actual_thumb_image'];
            }
			
			$datetransaction = date('Y-m-d',strtotime($_POST['date_of_transaction']));
			$data = array('user_id'=>$_SESSION['userId'],
						  'date_of_transaction'=>$datetransaction,
						  'product_name'=>$_POST['product_name'],
						  'store_name'=>$_POST['store_name'],
						  'transaction_amount'=>$_POST['transaction_amount'],
						  'transaction_id'=>$_POST['transaction_id'],
						  'order_id'=>$_POST['order_id'],
						  'ticket_screenshot'=> $category_image,
						  'created'=>date('Y-m-d')
						  );
				$RaiseTicket = $this->Global_model->InsertRecord('tbl_raise_ticket',$data);
				echo '{"code":"200","message":"Your Query Sent Successfully. Your ticket no is : '.$RaiseTicket.'"}';
			}
			else
			{
				 echo '{"code":"100","message":"Invalid Record"}';
			}
			exit;	
	 }
	 
	 
	public function ticket_status()
	{  
		$this->load->front_view('Home/user_section/my_ticket_status', $data);
	}
	
	public function my_ticket_status_history()
    {
        $sort = array('product_name','store_name');
        $search="user_id ='".$_SESSION['userId']."'";  
        $get['fields'] = array('*');
		if(isset($search)){
			$get['search']=$search;
			}
			$get['myll']=$_POST['start'];
			$get['offset'] = $_POST['length'];
			if(isset($_POST['order'][0])){
			  $orrd= $_POST['order'][0];
		 }
        $get['title']='_id';
        $get['order']='desc';
        $list = $this->Global_model->getSingleListAjax('tbl_raise_ticket',$get);
        $cc=$list['count'];
        $data = array();
        $no = $_POST['start'];
        $total_rec = array_pop($list);
        foreach ($list as $missing) {
			//print_r($missing);
            $no++;
            $row = array();
			$status = ($missing->is_status == 0) ? 'Not Answered' : 'Answered';
			$class = ($missing->is_status == 0) ? 'danger' : 'success';
			$row[] = $missing->_id;
            $row[] = DateFormat($missing->date_of_transaction);
            $row[] = $missing->product_name;
			$row[] = $missing->store_name;
			$row[] = $missing->transaction_amount;
			$row[] = '<a href="javascript:void()" id='.$missing->_id.' data-toggle="modal" data-target="#myModalRaiseScreenShot" onclick="getRaiseTicket(this.id);"><img src="'.$missing->ticket_screenshot.'" width="50"  height="50"></a>';
			$row[] = $missing->remark;
			$row[] = '<button class="btn btn-'.$class.' btn-xs">'.$status.'</button>';
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" =>$cc,
                        "recordsFiltered" => $total_rec,
                        "data" => $data,
                );
        echo json_encode($output);
  }
  
  public function getRaiseScreenShot()
  {
		$getStatus = $this->Global_model->GetsingleData('tbl_raise_ticket','ticket_screenshot',array('_id'=>$_POST['id']));
		echo $html = '<img src="'.$getStatus->ticket_screenshot.'" width="100%">';
		exit;
  }
	
  /***************user panel faq***************/
  public function Faq_Myaccount()
  {
	$data['faqList'] = $this->Global_model->GetMultiData_order_by('tbl_myaccount_faq','faq_question,faq_answer',array('is_status'=>1,'is_deleted'=>0),'sort_sequence','asc'); 
	$this->load->front_view('Home/user_section/faq_myaccount', $data);  
  }
  
  /*****************Users****************/
  public function checkTransactionValid()
  { 
  	  $transactionDate = date('Y-m-d',strtotime($_POST['transactionDate']));
	  $getUserData = $this->Global_model->GetMultiData_group_by('tbl_all_url','*',array('user_id'=>$_SESSION['userId'],'click_date'=>$transactionDate),'store_name');
	  $html = '';
	  if($getUserData)
	  {
		  foreach($getUserData as $userData) 
		  {
			  $html .= '<option value="'.$userData->store_name.'">'.$userData->store_name.'</option>';
		  }
		//echo '{"code":"200","message":""}';
	  }
	  else
	  {
		$html = 0;
	  }
	  echo $html;
	  //echo json_encode(array('result'=>$data));
	  exit;
	  //echo '{"code":"100","message":"Sorry | On this date not any record found in your transaction histroy"}';
	  //echo json_encode(array('result'=>$data));
  }
  
  
  
}