<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Blog extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	const  moduled='Masteradmin';
	 public function __construct() {
        parent::__construct();
       $this->config->set_item('base_url', $this->config->config['secure_url']);
	   $this->load->model('Global_home_model');
	   $this->load->model('Global_model');
       $this->load->library('form_validation');
        $this->load->library('pagination');
	}
	 
	 
  	public function index()
	{
			
		$params = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $blogkeywords = (isset($_GET['blogkeywords'])) ? $_GET['blogkeywords'] : '';
        $total_records = getBLogs(1,$blogkeywords);
        if ($total_records > 0) 
        {
            // get current page records
            $params["results"] = getBLogs(NULL, $blogkeywords, $limit_per_page, $start_index);
            $config['base_url'] = base_url() . 'blog';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;
			  
			$config['full_tag_open'] = '<div class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['first_link'] = false;
			$config['last_link'] = false;
			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="page-item prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="page-item next">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<liclass="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="page-link">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';
			
            $this->pagination->initialize($config);
            // build paging links
            $params["links"] = $this->pagination->create_links();
        }
		$this->load->front_view('blog',$params);
	}

	public function blogdetails()
	{
		$blog_id = $this->uri->segment(2);
		$data['blog_details'] = $this->Global_model->GetsingleData('posts','*',array('id'=>$blog_id));
		$this->load->front_view('blogdetails',$data);
	}
}