    <section class="middle_section inner-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 blog_p">
                    <div class="form-heading">Explore & Learn Simple Test Solutions</div>
                    <div class="blog-search">
                        <form action="<?php echo base_url('blog') ?>" method="GET">
                            <div class="search-outer">
                                <input type="text" name="blogkeywords" value="<?php echo (isset($_GET['blogkeywords'])) ? $_GET['blogkeywords'] : '' ?>" placeholder="Search by keywords">
                                <input type="submit" value="Search">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="blog-list-outer">
                        <div class="row">

                            <?php  if(isset($_GET['blogkeywords'])) {
                                if(count($results) == 0) {
                            ?>
                             <div class="alert alert-info" style="width: 100%;text-align: center;">
                              <strong>No record found.</strong> 
                            </div>   
                            <?php }  }  ?>
                            
                            <?php foreach($results as $r) { ?>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="blog-list-div">
                                    <div class="blog-list-image">
                                        <img src="<?php echo $r->featured_image; ?>">
                                    </div>
                                    <div class="blog-list-content">
                                        <div class="blog-list-content-title"><?php echo $r->title; ?></div>
                                        <div class="blog-list-content-date">Poster on: <?php echo date('d M Y',strtotime($r->published_at)); ?></div>
                                        <div class="blog-list-content-read"><a href="<?php echo base_url('blogdetails/'.$r->id.'/'.$r->slug.'') ?>">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                           <?php } ?>
                            


                            <div class="col-sm-12">
                                <div class="pagination">
                                     <?php echo (isset($links)) ? $links : ''; ?>
                                    <!-- <ul>
                                        <li><a href="#">Previous</a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#">6</a></li>
                                        <li><a href="#">next</a></li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>