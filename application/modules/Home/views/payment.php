<?php if(empty(check_user_session())){ ?>
	<script>
	$(window).load(function() {
     $("#header").hide();
});
</script>
	<?php } ?>
<section class="middle_section inner-page Membership-planpage payment">
        <div class="container">
            <div class="login-container">
                <div class="form-heading">Payment</div>
					<div class="msg-gloabal"></div>
                <div class="payment-form">
                    <form url="<?php echo base_url('Home/Registration/save_card_details');?>" method="post" id="card_details" name="card_details" enctype="multipart/form-data" >
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $this->id; ?>" />
                        <div class="form-group">
                            <label>Credit/Debit card number</label>
                            <input type="text" class="form-control" id="card_number" name="card_number" placeholder="Credit/Debit card number">
                        </div>
                        <div class="form-group">
                            <label>Customer name</label>
                            <input type="text" class="form-control" id="cardholder_name" name="cardholder_name" placeholder="Customer name">
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>CVV</label>
                                    <input type="password" class="form-control" id="cvv_number" name="cvv_number" placeholder="CVV">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Expiry date</label>
                                    <input type="text" class="form-control" id="expiry_date" name="expiry_date" placeholder="MM/YY">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
						 <button type="submit" class="btn btn-primary">Payment</button>
                          
                        </div>
                    </form>
                </div>
            </div>
        </div>
	</section>