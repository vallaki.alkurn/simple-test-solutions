<section class="middle_section inner-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-detail-outer">
                        <div class="row">
                            <div class="offset-lg-3 offset-md-3 col-lg-6 col-md-6 col-sm-12">
                                <div class="blog-list-div">
                                    <div class="form-heading"><?php echo $blog_details->title; ?></div>
                                    <div class="blog-list-content-date">Poster on: <?php echo date('d M Y',strtotime($blog_details->published_at)); ?></div>
                                    <div class="blog-list-image">
                                       <img src="<?php echo $blog_details->featured_image; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="content-outer">
                                    <?php echo $blog_details->body; ?>
                                </div>
                            </div>
                            
                        </div><br><br><br>
                    </div>
                </div>
                
            </div>
        </div>
    </section>