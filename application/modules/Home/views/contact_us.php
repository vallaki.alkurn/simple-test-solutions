<!-- Middle section start-->
<section class="middle_section inner-page contact">
        <div class="page-heading">
            <div class="title">
                How can we help you?
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!--<div class="col-lg-1 col-md-1 col-sm-12">-->
                <!--</div>-->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="login-container contactpage">
                        <div class="form-div">
                            <div class="form-upper-div contact-container">
                                <div class="form-heading">Get in touch</div>
								 <div class="msg-gloabal">
								<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
								</div>
                                <form action="#"  id="contactUs" name="contact_us" url="<?php echo base_url('Home/Home_page/submit_contact');?>" method="post"   enctype="multipart/form-data">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <textarea placeholder="Message" name="message" class="select-color"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary bg-pink">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="content-div">
                        <div class="milticolor-heading">
                                Simple <span class="pinktext">Test</span> Solutions
                        </div>
                        <div class="mail-div">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> supportsimpletestsolutions@gmail.com                                 
                        </div>
                        <div class="mail-div">
                            <i class="fa fa-envelope" aria-hidden="true"></i> simpletestsolutions@gmail.com
                        </div>
                        <div class="para-div">
                            Feel free to ask a front desk representative for more information. Save<br />
                            money with pre-registration at the front desk also. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
<!-- Middle section End-->

<script type="text/javascript">
	$(function(){
		//Update Profile
		 $('#contactUs').validate({
			rules: {
					name: {
					   required : true,
					},
					email: {
						required: true,
					},
					subject: {
						required: true,
					},
					message: {
						required: true,
					},
				},
			messages: {
					 que_type_id:{
						required :"Name field is required",
					 }, 
					 email:{
						required :"Email field is required",
					 },
					 subject: {
						required: "Subject field is required",
					},
					 message: {
						required: "Message field is required",
					}
				},
			submitHandler: function(form) {
					lkForms('contactUs');
					//$(window).scrollTop(0);
			  }
		 });
	 });
</script>