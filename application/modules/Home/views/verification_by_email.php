<section class="middle_section inner-page registration-student loginpage">
        <div class="container">
            <div class="login-container">
                
                <div class="form-div">
                        
                    <div class="form-upper-div" style="padding-bottom: 40px;">
                            <div class="form-heading">Email Verification Successful</div>
                                <div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your email has been confirmed and your account is now verified. 
                                </div>                     
                    </div>
                </div>
            </div>
        </div>
	</section>