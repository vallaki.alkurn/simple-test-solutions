<section class="middle_section inner-page registration-student loginpage">
        <div class="container">
            <div class="login-container">
                
                <div class="form-div">
                        
                    <div class="form-upper-div">
                            <div class="form-heading">register as a Student</div>
							<div class="msg-gloabal"></div>
                        <form  url="<?php echo base_url('Home/Registration/save_data_student');?>" method="post" id="registration_student" name="registration_student" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name*" autofocus>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name*">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email_address" id="email_address" placeholder="Email Address*">
                            </div>
							 <div class="form-group">
							 	<select name="standard_class_id" id="standard_class_id" class="select-color">
									<option value="">Standard/Class*</option>
									<?php foreach(get_standard_class() as $key => $val){
										?>
											 <option value="<?php echo $val->standard_class_id; ?>">
												<?php echo $val->standard_class_name; ?>
											 </option>
										<?php
									}?>
								  </select>
							 </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="user_password" 
												id="user_password" placeholder="Password*">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="user_cpassword" 
												id="user_cpassword" placeholder="Confirm Password*">
                            </div>
                            <div class="form-group lesspadding">
                                <input type="text" class="form-control" name="school_collage" 
												id="school_collage" placeholder="School/College*">
                            </div>
                            <div class="form-group form-check">
                            <label class="form-check-label remember-div term">
                                <input class="form-check-input" type="checkbox" id="tearm_conditions" name="tearm_conditions"><span><i class="fa fa-check" aria-hidden="true"></i></span> I agree to your <a target="page" href="<?php echo base_url();?>term-condition">Terms & Conditions</a>
                            </label>
							<label class="error" id="tearm" style="float:left;"></label>
                            </div>
                            <button type="submit" class="btn btn-primary">Register</button>
                        </form>
		
                        <div class="dont-have-account">
                            Already have an Account? <a href="<?php echo base_url('login'); ?>">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<style>
	#tearm_conditions-error{
		position:absolute;
		top:22px;
		left:0px;
	}
	</style>