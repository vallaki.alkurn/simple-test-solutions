<!-- Middle section start-->
	<section class="middle_section inner-page registration-teacher loginpage">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="login-container">
                        <div class="form-div">   
                            <div class="form-upper-div">
                                <div class="form-heading">register as a teacher</div>
								<div class="msg-gloabal"></div>
                                <form url="<?php echo base_url('Home/Registration/save_data_teacher');?>" method="post" id="registration_teacher" name="registration_teacher" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name*" autofocus>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control"  name="last_name" id="last_name" placeholder="Last Name*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email_address" id="email_address" placeholder="Email Address*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
											 <select name="subject_class_id" class="select-color" id="subject_class_id">
											 	<option value="">Subject*</option>
												<?php foreach(get_subject() as $key => $val){
													?>
														 <option value="<?php echo $val->subject_class_id; ?>">
															<?php echo $val->subject_class_name; ?>
														 </option>
													<?php
												}?>
                                              </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="user_password" 
												id="user_password" placeholder="Password*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="user_cpassword" 
												id="user_cpassword" placeholder="Confirm Password*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="organization" placeholder="Organization*" name="organization">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="organization_phone_number" placeholder="Organization Phone Number" name="organization_phone_number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="your_title" class="form-control" id="your_title" placeholder="Your Title">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="your_phone" class="form-control number" id="your_phone" placeholder="Your Phone*">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="address" class="form-control" id="address" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="dont-have-account">
                                                Already have an Account? <a href="<?php echo base_url('login'); ?>">Login</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary">Register</button>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-check">
                                                <label class="form-check-label remember-div term" style="float:left; float:left;" >
                                                    <input class="form-check-input" type="checkbox" id="tearm_conditions" name="tearm_conditions" value="1">
													<span><i class="fa fa-check" aria-hidden="true"></i></span> I agree to your <a target="page" href="<?php echo base_url();?>term-condition">Terms & Conditions</a>
                                                </label>
												<label class="error" id="tearm" style="float:left; float:left;"></label>
												
                                            </div>
                                        </div>
                                    </div>
                                </form>
								
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
	</section>
	<style>
	#tearm_conditions-error{
		position:absolute;
		top:22px;
		left:0px;
	}
	</style>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2oRAljHGZArBeQc5OXY0MI5BBoQproWY&libraries=places"></script>
	<script type="text/javascript">
		function initialize() {
			var input = document.getElementById('address');
			var options = '';
			new google.maps.places.Autocomplete(input, options);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<!-- Middle section End-->