<!DOCTYPE html>

<html>

<head>

  <meta charset="UTF-8">

  <title>Teacher student portal</title>

  <!-- <link href="<?php //echo get_bloginfo('template_url') ?>/css/custom.css" rel="stylesheet"> -->

  <link href="<?php echo base_url(); ?>assets/front-design/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/front-design/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/front-design/css/responsive.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/front-design/css/custom.css" rel="stylesheet">

  

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/jquery.min.js"></script>

  <link href="<?php echo base_url(); ?>assets/front-design/css/ninja-slider.css" rel="stylesheet" type="text/css" />

    <script src="<?php echo base_url(); ?>assets/front-designjs/ninja-slider.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-design/engine1/style.css" />

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/engine1/jquery.js"></script>

 

</head>



<body>



 <!----------------- Loader And Messages ---------------> 

<div class="loader" style="display:none;">

<div class="loader-inner">

	<div class="loader-img">

		<!--<div class="mdl-spinner mdl-js-spinner is-active"></div>-->

		<img src="<?php echo base_url('assets/front-design/images/loaderilk.gif');?>" title="" alt=""></div>	

		<div class="loader-text">Please wait...</div>

</div>

</div>

<div class="globel-msg" id="gmsg"></div>



  <!-- Header Start-->

  <header>

    <div class="container">

      <div class="row">

        <div class="col-lg-12">

          <div class="logo">

            <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/logo.jpg"></a>

          </div>

          <div class="main-menu">

            <nav class="navigation-menu">

              <ul>

                <li><a href="<?php echo base_url('teacher'); ?>" 

				class="<?php echo $this->router->fetch_method() == 'Teacher' ? 'active' : ''; ?>">Teacher</a></li>

                <li><a class="<?php echo $this->router->fetch_method() == 'Student' ? 'active' : ''; ?>" href="<?php echo base_url('student'); ?>">Student</a></li>

              </ul>

            </nav>

          </div>

          <div class="login-menu">

            <span><a href="<?php echo base_url('login'); ?>" class="<?php echo $this->router->fetch_method() == 'login' ? 'active' : ''; ?>">Login</a></span>

          </div>

        </div>

      </div>

    </div>

  </header>

  <!-- Header End-->

  

 