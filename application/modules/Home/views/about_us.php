<!-- Middle section start-->
<section class="middle_section inner-page contact">
	<div class="page-heading abt">
		<div class="title">
			Why Choose Simple Test Solutions?
			<span class="small-title">Create Online Test Easily  For Your Students</span>
		</div>
	</div>
	<div class="container">
		<div class="outer-div">
			<div class="about-grid">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12">
						<div class="abt-sections">
							<div class="abt-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/abt-icon1.png"></div>
							<div class="abt-title">Easy to use</div>
							<div class="abt-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever </div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<div class="abt-sections">
							<div class="abt-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/abt-icon2.png"></div>
							<div class="abt-title">Looks great on all devices</div>
							<div class="abt-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever </div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<div class="abt-sections">
							<div class="abt-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/abt-icon3.png"></div>
							<div class="abt-title">Awesome support</div>
							<div class="abt-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever </div>
						</div>
					</div>
				</div>
			</div>
			<div class="abt-text-section">
				<div class="text-heading">Our Mission</div>
				<div class="text-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 
					1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, 
					but also the leap into electronic typesetting, remaining essentially unchanged.</div>
			</div>
		</div>
	</div>
</section>
<!-- Middle section End-->