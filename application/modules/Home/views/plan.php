<section class="middle_section inner-page Membership-planpage loginpage">
        <div class="container">
            <div class="login-container">
                <div class="form-heading">Membership Subscription</div>
                <p class="my-text">14 Days Free Trial</strong></p>
                <p class="my-text1">Premium Plan (Annual Subscription) - <span>$50.00</span></p>
                <p> Your card on file will be billed <strong>$50.00</strong> monthly after the first <strong>14 days</strong>.</p>
                <div class="btn-section">
                    <button onclick="redirect()" id="payment" class="btn btn-lg btn-success">Membership Plan @$50.00</button>
                    
                </div>
                    
            </div>
        </div>
	</section>
	<script>
	function redirect()
	{
		window.location.href ="<?php echo base_url() ?>Home/Registration/payment/<?php echo $this->id; ?>";
	}
	</script>