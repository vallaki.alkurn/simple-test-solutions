

  <!--Slider section start-->

  

  <section class="slider-section">

    <div class="row">

      <div class="col-lg-6 col-md-6 col-sm-12">

        <div class="banner-section">

          <div class="banner-inner">

            <div class="banner-heading">Create Online Test Easily  For Your Students</div>

            <div class="banner-para">In your own Brand | All Question Types | Test Analytics | Question Bank</div>

            <div class="banner-button"><a href="<?php echo base_url('student'); ?>">Get Started</a></div>

            <div class="banner-smalltent">With 14-day free trial.</div>

          </div>

        </div>

      </div>

      <div class="col-lg-6 col-md-6 col-sm-12 pl-0">

        

          

<div id="wowslider-container1">

  <div class="ws_images"><ul>

    <li><img src="<?php echo base_url(); ?>assets/front-design/data1/images/slider1.jpg" alt="slider1" title="slider1" id="wows1_0"/></li>

    <li><img src="<?php echo base_url(); ?>assets/front-design/data1/images/slider2.jpg" alt="slider2" title="slider2" id="wows1_1"/></li>

    <li><img src="<?php echo base_url(); ?>assets/front-design/data1/images/slider3.jpg" alt="css image slider" title="slider3" id="wows1_2"/></li>

    <li><img src="<?php echo base_url(); ?>assets/front-design/data1/images/slider4.jpg" alt="slider4" title="slider4" id="wows1_3"/></li>

  </ul></div>

  <div class="ws_bullets"><div>

    <a href="#" title="slider1"><span>1</span></a>

    <a href="#" title="slider2"><span>2</span></a>

    <a href="#" title="slider3"><span>3</span></a>

    <a href="#" title="slider4"><span>4</span></a>

  </div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="javascript:void(0)">css image slider</a> by WOWSlider.com v8.8</div>

  <div class="ws_shadow"></div>

  </div>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/engine1/wowslider.js"></script>

  <script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/engine1/script.js"></script>

        <!--div class="slider-outer">

          <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto;  height: 622px; overflow: hidden;">

            

                  <!-- Slides Container ->

                  <div data-u="slides" style="position: absolute; left: 0px; top: 0px; width: 1140px; height: 622px;

                  overflow: hidden;">

                      <div>

                          <img data-u="image" src="images/slider1.jpg" />

                      </div>

                      <div>

                          <img data-u="image" src="images/slider2.jpg" />

                      </div>

                      <div>

                          <img data-u="image" src="images/slider3.jpg" />

                      </div>

                      <div>

                          <img data-u="image" src="images/slider4.jpg" />

                      </div>

                  </div>

                  

                  <!--#region Bullet Navigator Skin Begin -->

                  <!-- Help: https://www.jssor.com/development/slider-with-bullet-navigator.html ->

                  <style>

                      .jssorb031 {position:absolute;}

                      .jssorb031 .i {position:absolute;cursor:pointer;}

                      .jssorb031 .i .b {fill:#fff;fill-opacity:1;stroke-width:1200;stroke-miterlimit:10;stroke-opacity:0.3;}

                      .jssorb031 .i:hover .b {fill:#fff;fill-opacity:.7;stroke:#000;stroke-opacity:.5;}

                      .jssorb031 .iav .b {fill: #a807a8;fill-opacity:1;}

                      .jssorb031 .i.idn {opacity:.3;}

                  </style>

                  <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">

                      <div data-u="prototype" class="i" style="width:19px;height:19px;">

                          <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">

                              <circle class="b" cx="8000" cy="8000" r="5800"></circle>

                          </svg>

                      </div>

                  </div>

                  <!--#endregion Bullet Navigator Skin End ->

              

                  <!--#region Arrow Navigator Skin Begin ->

                  <!-- Help: https://www.jssor.com/development/slider-with-arrow-navigator.html ->

                  <style>

                      .jssora051 {display:block;position:absolute;cursor:pointer;}

                      .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}

                      .jssora051:hover {opacity:.8;}

                      .jssora051.jssora051dn {opacity:.5;}

                      .jssora051.jssora051ds {opacity:.3;pointer-events:none;}

                  </style>

                  <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">

                      <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">

                          <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>

                      </svg>

                  </div>

                  <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">

                      <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">

                          <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>

                      </svg>

                  </div>

                  <!--#endregion Arrow Navigator Skin End >

              </div>

              <!-- Jssor Slider End >

          </div-->

          </div>

    </div>

  </section>

  <!--Slider section end-->

  <!-- Middle section start-->

  <section class="middle_section">

    <div class="section-works">

      <div class="container">

        <div class="row">

          <div class="col-sm-12">

            <div class="section-heading">

              How it works?

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-sm-12">

            <div class="work-slider-section">

              <div class="slide-section active-slide" id="teacher-slide">

                <div class="slide-smallheading">For Teacher</div>

                <div class="row">

                  <div class="col-lg-3 col-md-3 col-sm-12 teac_blogs">

                    <div class="info-block-section">

                      <div class="block-heading">Register as Teacher</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work1.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12 teac_blogs">

                    <div class="info-block-section">

                      <div class="block-heading">Create Free Test</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work2.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12 teac_blogs">

                    <div class="info-block-section">

                      <div class="block-heading">Set Test Date & Time</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work3.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12 teac_blogs">

                    <div class="info-block-section">

                      <div class="block-heading">Send Invitation To Student</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work4.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="slide-section" id="student-slide">

                <div class="slide-smallheading">For Student</div>

                <div class="row">

                  <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="info-block-section">

                      <div class="block-heading">)register as student</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work5.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="info-block-section">

                      <div class="block-heading">get invites from teachers </div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work6.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="info-block-section">

                      <div class="block-heading">perform a test</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work7.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                  <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="info-block-section">

                      <div class="block-heading">get immediate results</div>

                      <div class="block-image"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-work8.png"></div>

                      <div class="block-text">Lorem Ipsum is simply dummy text of the printing and typeng industry.</div>

                    </div>

                  </div>

                </div>

              </div>

              <div class="work-slider-btns">

                <div class="btn-inner">

                  <button type="button" id="btn-teacher" class="btn btn-primary click-slide btn-active"></button>

                  <button type="button" id="btn-student" class="btn btn-primary click-slide"></button>

                </div>

              </div>

            </div>

          </div>

        </div>

            

      </div>

    </div>

    <div class="section-providing">

      <div class="container">

        <div class="row">

          <div class="col-sm-12">

            <div class="section-heading">

              We are Providing

            </div>

          </div>

        </div>

        <div class="row">

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="providing-list">

              <ul>

                <li>

                  <img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Affordable online test generating platform</li>

                <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Tests and quizzes automatically graded</li>

              </ul>

            </div>

          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="providing-list">

              <ul>



                <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Publish online tests and quizzes</li>

                <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Immediate feedback to students</li>

              </ul>

            </div>

          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="providing-list">

              <ul>

                <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Customize your own tests</li>

                <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk1.png"> Tests saved and stored for future use</li>

              </ul>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="section-joinus">

      <div class="container">

        <div class="row">

          <div class="col-sm-12">

            <div class="section-heading">

              Join us today!

            </div>

            <div class="small-heading">Join our community of thousands of students and Teachers</div>

          </div>

        </div>

        <div class="row">

          <div class="col-lg-2  col-sm-12"></div>

          <div class="col-lg-4 col-md-6 col-sm-12 pr-0">

            <div class="join-box" id="join-teacher">

              <div class="join-heading-section">

                <div class="join-heading-small">Become a</div>

                <div class="join-heading-big">Teacher</div>

              </div>

              <div class="join-content">

                <ul>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2blk.png"> Register as Teacher</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2blk.png"> Question Bank Creation</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2blk.png"> Create Free Test</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2blk.png"> Invite a Student for Test</li>

                </ul>

              </div>

              <div class="register-btn-section">

                <a href="<?php echo base_url('teacher'); ?>">register as teacher</a>

              </div>

            </div>

          </div>

          <div class="col-lg-4 col-md-6 col-sm-12 pl-0">

            <div class="join-box" id="join-student">

              <div class="join-heading-section">

                <div class="join-heading-small">Become a</div>

                <div class="join-heading-big">Student</div>

              </div>

              <div class="join-content">

                <ul>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2wht.png"> Free Register</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2wht.png"> Get Invitation for Test</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2wht.png"> Download the Result in a PDF format</li>

                  <li><img src="<?php echo base_url(); ?>assets/front-design/images/chk2wht.png"> See Taken Tests in History</li>

                </ul>

              </div>

              <div class="register-btn-section">

                <a href="<?php echo base_url('student'); ?>">register as student</a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="section-count">

      <div class="container">

        <div class="row">

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="count-div">

              <div class="count-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-count1.png"></div>

              <div class="count-text"><span class="count-number">1000+</span> Happy Students</div>

            </div>

          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="count-div">

              <div class="count-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-count2.png"></div>

              <div class="count-text"><span class="count-number">200+</span> Teachers</div>

            </div>

          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">

            <div class="count-div">

              <div class="count-icon"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-count3.png"></div>

              <div class="count-text"><span class="count-number">5000+</span> Tests Taken</div>

            </div>

          </div>

          <!--div class="col-lg-3 col-md-3 col-sm-12">

            <div class="count-div">

              <div class="count-icon"><img src="images/icon-count4.png"></div>

              <div class="count-text"><span class="count-number">4900+</span> Available Questions</div>

            </div>

          </div-->

        </div>

      </div>

    </div>

    <div class="section-contact">

      <div class="container">

        <div class="row">

          <div class="col-sm-12">

            <div class="contact-heading"><p>All Tests.</p><p>One Platform.</p><p>Complete Preparation.</p></div>

            <div class="contact-btn"><a href="<?php echo base_url('student'); ?>">Get Started</a></div>

            <div class="contact-smalltext">With 14-day free trial.</div>

          </div>

        </div>

      </div>

    </div>

    <div class="section-help">

      <div class="container">

        <div class="row">

          <div class="col-sm-12">

            <div class="help-inner">

              <div class="help-heading">Helping Students Succeed!</div>

              <div class="help-text">Affordable, user-friendly online tool that make implementing best practices easier.</div>

              <div class="help-btn"><a href="<?php echo base_url('student'); ?>">Join Now</a></div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </section>

  <!-- Middle section End-->