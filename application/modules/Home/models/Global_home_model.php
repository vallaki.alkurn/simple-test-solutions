<?php

class Global_home_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    /*********start comman function for all*******************/
    public function GetmultiData($table, $feilds, $cond = NULL, $orderby = NULL,$limit = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
		if(isset($orderby)){
            $this->db->order_by($orderby['col'], $orderby['sort_type']);
        }
		if(isset($limit)){
            $this->db->limit($limit['end'], $limit['start']);
        }
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }


    public function getCashbackDataStages($table,$data)
	{
		$this->db->select($data['fields']);
		$this->db->from($table);
		$this->db->where('is_deleted',0);
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		$this->db->order_by('unique_click_id', 'desc');
		$this->db->order_by('retailer_status', 'asc');
		$this->db->limit($data['offset'],$data['myll']);
		$query = $this->db->get();
		//print_r($this->db->last_query());
		$row = $query->result();
		$this->db->select($data['fields']);
		$this->db->from($table);
		$this->db->where('is_deleted',0);
		if(isset($data['search'])){
		$this->db->where($data['search']);
		}
		$count = $this->db->count_all_results();
		$row['count'] = $count;
		return $row;
    }
	
	/*static public function abcd()
	{
		$res = self::xyz();
	}
	
	static public function xyz()
	{
		echo 'xyz';
	}*/

    public function GetsingleData($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
        $query = $this->db->get();
        $rowCount= $query->row();
		//print_r($this->db->last_query());
        return $rowCount;
    }

    public function UpdateRecord($table, $data, $cond = NULL)
    {
    $this->db->where($cond);
    $res = $this->db->update($table,  $data);
    //print_r($this->db->last_query());
    return $res;
    }

    public function InsertRecord($table, $data)
    {
    $res = $this->db->insert($table,$data);
    //print_r($this->db->last_query());
    return $res;
    }

    public function InsertUpdate($table, $data, $tbid = NULL)
    {
        if(!empty($_POST['id']))
        {
            $this->db->where($tbid,$_POST['id']);
            $res = $this->db->update($table,  $data);
            return $_POST['id'];
        }
        {
            $res = $this->db->insert($table,$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
		//print_r($this->db->last_query());
       // print_r($_POST);
    }


	
    public function DeleteRecord($table,$cond = NULL)
    {
    $this->db->where($cond);
    $res = $this->db->delete($table);
    return $res;
    }

    public function GetSingleDataRecord()
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by('a._id', 'asc');
        }
        $this->db->limit($data['offset'],$data['myll']);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();

        //For total number of records
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function getCreatedByName($id)
    {
        $this->db->select('name');
        $this->db->where('user_id',$id);
        $this->db->from('users');
        $query = $this->db->get();
        $rowCount = $query->row();
        return $rowCount;
    }

    public function getSingleListAjax($table,$data)
	{
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by('_id', 'asc');

        }
        $this->db->limit($data['offset'],$data['myll']);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
    /*********end comman function for all*******************/
	
	public function getMenuwithUrl($cond = NULL)
	{
		$this->db->select('*');
        $this->db->from('tbl_menu_structure');
		$this->db->join('url_rewrite','tbl_menu_structure._id = url_rewrite.rowid','left');
		$this->db->where('urltype',2);
		$this->db->where('is_deleted',0);
		if($cond)
		{
		$this->db->where($cond);
		}
        $query = $this->db->get();
        $rowCount = $query->result();
		//print_r($this->db->last_query());
        return $rowCount;
	}
	
	
	
	/********get offer category wise**********/
	public function getOfferDetails($cond = NULL)
	{
		$this->db->select('*');
        $this->db->from('tbl_offers');
		$this->db->join('tbl_all_offer_details','tbl_offers.offer_id = tbl_all_offer_details.offer_id','left');
		$this->db->where($cond);
		$this->db->where('tbl_offers.status',1);
		if($cond)
		{
		$this->db->where($cond);
		}
        $query = $this->db->get();
        $rowCount = $query->result();
        return $rowCount;
	}
	
	/********get offer category wise**********/
	public function getOffers($cond = NULL)
	{
		$this->db->select('to.coupon_code,to.store_image as store_img,to.name,to.terms_and_conditions,to.offer_line,to.json_id,toi.url');
        $this->db->from('tbl_all_offer_details to');
		$this->db->join('tbl_all_offer_images toi','to.offer_id = toi.offer_id','left');
		$this->db->where($cond);
        $query = $this->db->get();
        $rowCount = $query->row();
        return $rowCount;
	}
	
	/********get rewards offer**********/
	public function getrewardsOffer($keywords,$cond = NULL)
	{
		$this->db->select('name,offer_line,json_id,to.offer_id,expiration_date,td._id as offid');
        $this->db->from('tbl_all_offer_details to');
		if($cond)
		{
		$this->db->where($cond);
		}
		$this->db->join('tbl_offers td','td.offer_id = to._id','left');
		$this->db->like('name',$keywords);
        $query = $this->db->get();
        $rowCount = $query->result();
        return $rowCount;
	}
	
	
	/*********get recent search***********/
	public function getRecentSearch()
    {
		$browser = $this->agent->browser();
        $this->db->select('product_id');
        $this->db->from('recently_view');
		$this->db->join('tbl_product','tbl_product._id = recently_view.product_id','left');
		$this->db->where('tbl_product.is_status',0);
		$this->db->where('tbl_product.is_deleted',0);
		$this->db->where('recently_view.ip',$_SERVER['SERVER_ADDR']);
		//$this->db->where('browser',$browser);
		$this->db->limit('6','0');
		$this->db->group_by('recently_view.product_id');
		$this->db->order_by('recently_view._id','desc');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
        return $rowCount;
    }
	
	public function getSimilarProduct($id)
    {
        $this->db->select('similar_product_id');
		$this->db->from('similar_product');
		$this->db->where('product_id',$id);
		$this->db->limit('6','0');
        $query = $this->db->get();
        $rowCount = $query->result();
        return $rowCount;
    }
	
	/********get offer details**********/
	public function getOfferDescription($cond = NULL)
	{
		$this->db->select('taod.terms_and_conditions,comission_chart');
		$this->db->from('tbl_offers to');
		$this->db->join('tbl_all_offer_details taod','to.offer_id = taod._id','left');
		$this->db->where($cond);
		$this->db->group_by('to.offer_id');
		$query = $this->db->get();
		//print_r($this->db->last_query());
		$rowCount = $query->row();
		return $rowCount;
	}
	
	
	/*************recent searching************/
	public function getHomeProduct($id)
    {
        $this->db->select('_id');
        $this->db->from('tbl_product');
		$this->db->where('tbl_product.is_status',0);
		$this->db->where('tbl_product.is_deleted',0);
		$this->db->where('tbl_product.is_home',1);
		$this->db->limit('6','0');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
        return $rowCount;
    }
	
	/*************recent searching************/
	public function getHomeProductStorePage($id)
    {
        $this->db->select('tbl_product._id');
        $this->db->from('tbl_product');
		$this->db->from('tbl_product_price','tbl_product_price.product_id = tbl_product._id','left');
		$this->db->where('tbl_product.is_status',0);
		$this->db->where('tbl_product.is_deleted',0);
		$this->db->where('tbl_product.is_home',1);
		$this->db->where('tbl_product_price.store_id',$id);
		$this->db->group_by('tbl_product._id');
		$this->db->limit('6','0');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
        return $rowCount;
    }


    public function getThirdpartyStoreData($soreId=NULL)
    {
        $this->db->select('a.category_title, b.thirdpartyStoreName');
        $this->db->from('tbl_thirdpartycategory as a');
        $this->db->join('tbl_thirdpartystore as b','a.thirdpartystore_id = b._id','left');
        $this->db->where(array('a._id'=>$soreId));
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount = $query->row();
        return  $dataReturn=$rowCount->category_title.":  ".$rowCount->thirdpartyStoreName;
    }
	
	/**********get offer details********/
	public function getOffersDetails($id)
	{
		$condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
	    $this->db->select('tod._id,name,url,tod.json_id,offer_line, to._id as offid, tod.store_image');
        $this->db->from('tbl_offers to');
		$this->db->join('tbl_all_offer_details tod','to.offer_id = tod._id','left');
		$this->db->join('tbl_all_offer_images toi','tod.offer_id = toi.offer_id','left');
		$this->db->where('to.status',1);
		$this->db->where('to.category_id',$id);
		$this->db->where($condEnd);
		if($cond)
		{
		$this->db->where($cond);
		}
		//$this->db->limit(10,0);
		$this->db->order_by('to._id','desc');
		$this->db->group_by('tod.offer_id');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
		return $rowCount;
	}
	
	public function getRecentProductDetailsSearch()
    {
		$browser = $this->agent->browser();
        $this->db->select('product_id');
        $this->db->from('recently_view');
		$this->db->join('tbl_product','tbl_product._id = recently_view.product_id','left');
		$this->db->where('tbl_product.is_status',0);
		$this->db->where('tbl_product.is_deleted',0);
		$this->db->where('recently_view.ip',$_SERVER['SERVER_ADDR']);
		$this->db->where('browser',$browser);
		$this->db->limit('6','0');
		$this->db->group_by('recently_view.product_id');
		$this->db->order_by('recently_view._id','desc');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
        return $rowCount;
    }
	
	
	/****************get Offer Title Strip**********/
	public function getOfferStrip($limit,$offset)
    {
		//$condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
        $this->db->select('name, toi.url,category_name,category_image,store_name');
        $this->db->from('tbl_offer_coupon_home_category toch');
		$this->db->join('tbl_all_offer_details to','to.name = toch.category_name','left');
		$this->db->join('tbl_all_offer_images toi','to.offer_id = toi.offer_id','left');
		$this->db->where('to.is_deleted',0);
		$this->db->where('toch.is_status',1);
		//$this->db->where($condEnd);
		//$this->db->where('to.store_name IS NOT NULL');
		$this->db->group_by('name');
		$this->db->order_by('toch.sort_sequence','asc');
		if($limit)
		{
		$this->db->limit($limit,$offset);
		}
        $query = $this->db->get();
        $rowCount = $query->result();
		//print_r($this->db->last_query());
        return $rowCount;
    }
	
	/**********get third party offer details********/
	public function getThirdPartyOfferDetails($cond)
	{
		$condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
	    $this->db->select('to.name,toi.url as image,to.store_link,SUBSTRING(to.offer_line,1,20)  as offer_line,SUBSTRING(to.offer_heading,1,22) as offer_heading,to._id', FALSE);
        $this->db->from('tbl_all_offer_details to');
		$this->db->join('tbl_all_offer_images toi','to.offer_id = toi.offer_id','left');
		$this->db->where($cond);
		$this->db->where('copy_offer',0);
		$this->db->where('is_deleted',0);
		//$this->db->order_by('to._id','desc');
		$this->db->group_by('to.offer_id');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
		return $rowCount;
	}
	
	public function getThirdPartyOfferDetails1($cond)
	{
		$condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
	    $this->db->select('to.name,to.copy_image as image,to.store_link,to.offer_line,to._id,to.offer_heading,SUBSTRING(to.offer_line,1,22)  as offer_line,SUBSTRING(to.offer_heading,1,20) as offer_heading', FALSE);
        $this->db->from('tbl_all_offer_details to');
		//$this->db->join('tbl_all_offer_images toi','to.offer_id = toi.offer_id','left');
		$this->db->where($cond);
		$this->db->where('is_deleted',0);
		$this->db->where('copy_offer',1);
		$this->db->where('is_status',1);
		$this->db->order_by('to.sort_ordering','asc');
		$this->db->limit(7);
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
		return $rowCount;
	}
	
	/*********get multiple data**********/
	public function getStoreProduct($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
		$this->db->from($table);
        if($cond) 
		{
        $this->db->where($cond);
        }
		$this->db->where($cond);
		$this->db->where('product_id !=',0);
		//$this->db->order_by('rand()');
		$this->db->limit(10);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
}
?>
