<?php

class Global_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
	
    /*********start comman function for all*******************/
    public function GetmultiData($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }


    public function GetsingleData($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount= $query->row();
        return $rowCount;
    }

    public function GetMultiData_group_by($table, $feilds, $cond = NULL, $groupBy=NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        if($groupBy){
        $this->db->group_by($groupBy); 
        }
        $this->db->from($table);
        $query = $this->db->get();

        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function GetMultiData_order_by($table, $feilds, $cond = NULL, $orderFeild=NULL,$orderAttr=NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        if($orderFeild){
        $this->db->order_by($orderFeild,$orderAttr); 
        }
        $this->db->from($table);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }


    public function UpdateRecord($table, $data, $cond = NULL)
    {
    $this->db->where($cond);
    $res = $this->db->update($table,  $data);
    //print_r($this->db->last_query());
    return $res;
    }

    public function InsertRecord($table, $data)
    {
    $res = $this->db->insert($table,$data);
	$insert_id = $this->db->insert_id();
    //print_r($this->db->last_query());
    return $insert_id;
    }

    public function InsertUpdate($table, $data, $tbid = NULL)
    {
        if(!empty($_POST['id']))
        {
            $this->db->where($tbid,$_POST['id']);
            $res = $this->db->update($table,  $data);
            return $_POST['id'];
        }
        {
            $res = $this->db->insert($table,$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
		//print_r($this->db->last_query());
       // print_r($_POST);
    }


	
    public function DeleteRecord($table,$cond = NULL)
    {
    $this->db->where($cond);
    $res = $this->db->delete($table);
    return $res;
    }

    public function GetSingleDataRecord()
    {
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
            $this->db->order_by($data['title'], $data['order']);
        }
        else{
            $this->db->order_by('a._id', 'asc');
        }
        $this->db->limit($data['offset'],$data['myll']);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();

        //For total number of records
        $this->db->select($data['fields']);
        $this->db->from('tbl_category a');
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }

    public function getCreatedByName($id)
    {
        $this->db->select('name');
        $this->db->where('user_id',$id);
        $this->db->from('users');
        $query = $this->db->get();
        $rowCount = $query->row();
        return $rowCount;
    }

    public function getSingleListAjax($table,$data){
       $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        if(isset($data['order'])){
      	  	$this->db->order_by($data['title'], $data['order']);
        }
        else{
       		$this->db->order_by('_id', 'desc');

        }
        $this->db->limit($data['offset'],$data['myll']);
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row = $query->result();
        $this->db->select($data['fields']);
        $this->db->from($table);
        $this->db->where('is_deleted',0);
        if(isset($data['search'])){
        $this->db->where($data['search']);
        }
        $count = $this->db->count_all_results();
        $row['count'] = $count;
        return $row;
    }
    /*********end comman function for all*******************/
     public function GetmultiData_time($table, $feilds, $cond = NULL, $column=NULL,$time_compare=NULL, $limit = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        if($time_compare){
        $this->db->where($column,$time_compare);
        }
        $this->db->from($table);
		if($limit) 
		{
		$this->db->limit($limit);	
		}
		$query = $this->db->get();
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function getOfferCategory($table, $feilds, $cond = NULL)
    {
        $this->db->select($feilds);
        if($cond) {
        $this->db->where($cond);
        }
        $this->db->from($table);
		$this->db->order_by('set_ordering','ASC');
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function SelectData($table = NULL, $field = NULL, $cond = NULL, $offset=NULL, $limit = NULL, $groupby = NULL, $order = NULL)
    {
        $this->db->select($field);
        $this->db->from($table);
        if($cond){
        $this->db->where($cond);
        }
        if($groupby){
             $this->db->group_by($groupby);
        }
        if($order){
        $this->db->order_by($order['col'],$order['order']);
        }
       
        if($limit){
        $this->db->limit($limit, $offset);
        }

        $q = $this->db->get();
        //echo '<pre>';
        //print_r($this->db->last_query());
        return $res = $q->result();
    }
	
	public function SelectDataShuffle($table = NULL, $field = NULL, $cond = NULL, $offset=NULL, $limit = NULL, $groupby = NULL)
    {
        $this->db->select($field);
        $this->db->from($table);
        if($cond){
        $this->db->where($cond);
        }
        if($groupby){
             $this->db->group_by($groupby);
        }
        $this->db->order_by('offerCount','desc');
        if($limit){
        $this->db->limit($limit, $offset);
        }

        $q = $this->db->get();
        //echo '<pre>';
        //print_r($this->db->last_query());
        return $res = $q->result();
    }
	
	public function getLastuserLogin($cond = NULL)
    {
        $this->db->select('*');
        $this->db->where($cond);
        $this->db->from('user_session');
		$this->db->order_by('_id','DESC');
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->row();
        return $rowCount;
    }
	
	public function getStore_groupBy($cond = NULL)
    {
        $this->db->select('store_name');
        $this->db->from('tbl_all_offer_details');
		$this->db->where('store_name !=','');
		$this->db->group_by('store_name','DESC');
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
		public function getUserData($id)
	{
		$this->db->select('*');
        $this->db->from('tbl_user_registration');
		$this->db->where('user_id',$id);
	
        $query = $this->db->get();
        //print_r($this->db->last_query());
        $row= $query->row();
        return $row;
	}
	public function getTestReportById($id)
    {
    	  $this->db->select('tbl_completed_test.*,tbl_test_creation.test_time_limit as duration,tbl_test_creation.test_name,tbl_user_registration.first_name,tbl_user_registration.last_name,tbl_standard_class.standard_class_name');
            $this->db->from('tbl_completed_test');
             $this->db->join('tbl_test_creation', 'tbl_test_creation.test_id = tbl_completed_test.test_id' );
             $this->db->join('tbl_user_registration', 'tbl_user_registration.user_id = tbl_completed_test.user_id' );
             $this->db->join('tbl_standard_class','tbl_standard_class.standard_class_id = tbl_user_registration.standard_class_id' );
            $this->db->where('tbl_completed_test.id', $id);   
            $this->db->where('tbl_completed_test.test_status', 'Complete');   
           
            $q = $this->db->get();
    
            return $res = $q->row();
    }
}
?>
