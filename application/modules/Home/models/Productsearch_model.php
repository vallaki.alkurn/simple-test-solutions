<?php
class ProductSearch_model extends CI_Model {

    function __construct() 
	{
        parent::__construct();
    }

    public function getProductSearch($likequery = NULL)
    {
  		$this->db->select('tbl_product.product_name,tbl_product._id as productID,category_name');
        $this->db->from('tbl_product');
		$this->db->join('tbl_product_price','tbl_product._id = tbl_product_price.product_id','left');
		//$this->db->join('tbl_store','tbl_store._id = tbl_product_price.store_id','left');
		$this->db->join('tbl_category','tbl_category._id = tbl_product_price.category_id','left');
		$query = $this->db->where($likequery);
		$query = $this->db->group_by('tbl_product_price.product_id');
		$this->db->limit(9, 0);
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function getStoreSearch($likequery = NULL)
    {
  		$this->db->select('*');
        $this->db->from('tbl_store');
		$query = $this->db->where($likequery);
		$query = $this->db->where('is_deleted',0);
        $query = $this->db->get();
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function getOfferDetailsNew($likeOffer = NULL)
    {
		$condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
  		$this->db->select('tod._id,name,url,tod.json_id,offer_line, to._id as offid');
        $this->db->from('tbl_offers to');
		$this->db->join('tbl_all_offer_details tod','to.offer_id = tod._id','left');
		$this->db->join('tbl_all_offer_images toi','tod.offer_id = toi.offer_id','left');
		$this->db->where('to.status',1);
		$this->db->group_by('to.offer_id');
		$query = $this->db->where($likeOffer);
		$query = $this->db->where($condEnd);
		//$query = $this->db->where('is_deleted',0);
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount= $query->result();
        return $rowCount;
    }
	
	public function OfferSearchProduct($likequery)
	{
		/******check expiry date******/
	    $condEnd = "DATE(expiration_date) >= '".date('Y-m-d')."'";
	    $this->db->select('tod._id,name,url,tod.json_id,offer_line, to._id as offid');
        $this->db->from('tbl_offers to');
		$this->db->join('tbl_all_offer_details tod','to.offer_id = tod._id','left');
		$this->db->join('tbl_all_offer_images toi','tod.offer_id = toi.offer_id','left');
		$this->db->where('to.status',1);
		$this->db->where($likequery);
		$this->db->where($condEnd);
		$this->db->group_by('to.offer_id');
        $query = $this->db->get();
		//print_r($this->db->last_query());
        $rowCount = $query->result();
        return $rowCount;
	}

	public function offerSearchBy_Store($searchKey=NULL)
	{
		if($searchKey!='')
		{
			$this->db->select('store_name');
        	$this->db->from('tbl_all_offer_details');
        	$this->db->like('store_name', $searchKey);
			$this->db->where('is_status',1);
			$this->db->group_by('store_name');
			$this->db->limit(3);
        	$query = $this->db->get();
			//print_r($this->db->last_query());
        	$rowCount = $query->result();
        	return $rowCount;
		}
		

	}	
}
?>
