<?php
class Cart_model extends CI_Model {

    function __construct() 
	{
        parent::__construct();
    }


	public function getVendorCashback($cond)
    {
  		$this->db->select('*,cd._id as transId');
        $this->db->from('cart_details cd');
		$this->db->join('tbl_product_attributes_new tpa','tpa._id = cd.product_price_vendor_id','left');
		$query = $this->db->where($cond);
        $query = $this->db->get();
        $rowCount= $query->result();
        return $rowCount;
    }
	
	
	public function getPeymentDetails($orderId)
	{
		$this->db->select('*, dd._id as orderId');
        $this->db->from('delivery_details dd');
		$this->db->join('tbl_users tu','tu._id = dd.customer_id','left');
		$this->db->join('cart c','c._id = dd.cart_id','left');
		$query = $this->db->where('dd._id',$orderId);
        $query = $this->db->get();
		///print_r($this->db->last_query());
        $rowCount= $query->row();
        return $rowCount;
	}
}
?>
