<!-- Middle section start-->
	<section class="middle_section inner-page loginpage">
        <div class="container">
            <div class="login-container">
                
                <div class="form-div">
                        
                    <div class="form-upper-div">
                            <div class="form-heading">Login</div>
                            <div class="msg-gloabal">
							<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							</div>
                        <form  url="<?php echo base_url('Login/auth_with_login');?>" method="post" id="auth_with_login" name="auth_with_login">
                            <div class="form-group">
                                <input type="email" name="user_email" value="" class="form-control" id="email" placeholder="Email Address" autofocus> 
                            </div>
                            <div class="form-group lesspadding">
                                <input type="password" name="user_password" class="form-control" id="pwd" placeholder="Password">
                            </div>
                            <div class="form-group form-check">
                            <label class="form-check-label remember-div">
                                <input name="user_remember" value="1" id="user_remember" class="form-check-input" type="checkbox"><span><i class="fa fa-check" aria-hidden="true"></i></span> Remember me
                            </label>
                            <div class="forgot-link"><a href="<?php echo base_url('forgot-password');?>">Forgot Password?</a></div>
                            </div>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                        <div class="dont-have-account">
                            Don’t have Account? Please Register
                        </div>
                    </div>
                    <div class="become-div">
                        <ul>
                            <li><a href="<?php echo base_url('student');?>">Become a Student <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo base_url('teacher');?>">Become a Teacher <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<!-- Middle section End-->