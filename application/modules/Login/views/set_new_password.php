<!-- Middle section start-->
	<section class="middle_section inner-page loginpage">
        <div class="container">
            <div class="login-container">
                
                <div class="form-div">
                        <?php //print_r($user_details); ?>
                    <div class="form-upper-div">
                            <div class="form-heading">Change your password</div>
                            <div class="msg-gloabal"></div>
                        <form  action="<?php echo base_url('Login/change_password');?>" method="post" id="change_password" name="change_password">
						<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_details->user_id; ?>">
						<input type="hidden" name="email_address" id="email_address" value="<?php echo $user_details->email_address; ?>">
						<input type="hidden" name="first_name" id="first_name" value="<?php echo $user_details->first_name; ?>">
						<input type="hidden" name="last_name" id="last_name" value="<?php echo $user_details->last_name; ?>">
                            <div class="form-group">
                                 <input type="password" name="new_user_password" class="form-control" id="new_user_password" placeholder="New password" autofocus />
                            </div>
                            <div class="form-group lesspadding">
                                <input type="password" name="new_user_password_confirm" class="form-control" id="new_user_password_confirm" placeholder="Confirm new password">
                            </div>
                            <button type="submit" class="btn btn-primary">Change your password</button>
                        </form>
                        <div class="dont-have-account">
                            &nbsp;
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
	</section>
	<!-- Middle section End-->