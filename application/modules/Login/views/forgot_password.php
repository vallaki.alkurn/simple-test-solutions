<!-- Middle section start-->
	<section class="middle_section inner-page loginpage">
        <div class="container">
            <div class="login-container">
                
                <div class="form-div">
                        
                    <div class="form-upper-div">
                            <div class="form-heading">Reset Password</div>
                            <div class="msg-gloabal">
						<?php if($this->session->flashdata('msg')){ echo $this->session->flashdata('msg'); } ?>
							</div>
                        <form  action="<?php echo base_url('Login/reset_password');?>" method="post" id="reset_password" name="reset_password">
                            <div class="form-group">
                                <input type="email" name="user_email" value="" class="form-control" id="user_email" placeholder="Email Address" autofocus> 
                            </div>
                            <button type="submit" class="btn btn-primary">Reset Password</button>
                        </form>
                        <div class="dont-have-account">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</section>
	<!-- Middle section End-->
	<!--If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.
	
	Your password has been changed successfully.
	-->