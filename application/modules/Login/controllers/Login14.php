<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Login extends MX_Controller {
	/*
		 Lomesh Kelwdkar 
		 Login module
		 version 1.0
	*/
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	//const  moduled='Masteradmin';
	public function __construct() {
		//check_user_session();
		//check_user_session() ? '' : redirect(base_url());
        parent::__construct();
        $this->load->model('login_model');
		//$this->load->helper('cookie');
		include('Emails.php');
	    $this->myEmail = new Emails();
	}
	
	public function index()
	{
		redirect_user();
		$data = '';
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Users Login',
							  'keyword' => 'Users Login',
							  'description' => 'Users Login');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('login');
	}


	public function auth_with_login()
	{
		if($_POST){
			$data ='';
			//Check User Details 
			$array_user = array('table' => 'tbl_user_registration',
								'fields' => '*',
								'where' => array('email_address' => $_POST['user_email'],
												'user_password' => get_md5($_POST['user_password']),
												'is_deleted !=' => 1));
			$check_data = $this->login_model->check_user($array_user);
				
			if(!$check_data){
				$data = array('status' => false,
						      'response' => array('msg' =>$this->lang->line('user_login_error')),'url' => '');	
			}else{
				// print_r($_POST);
				 //Check User Status
				 switch ($check_data->is_profile_status) {
					 case 0 :
						  $data = array('status' => false,
								  'response' => array('msg' =>$this->lang->line('user_login_inactive_error')),'url' => '');
					 break;
					 case 1 :
					 	  if($check_data->is_deleted == 0){	
						  
							 $check_session = set_user(array('key'=>'user_session','value'=>$check_data));
							 
							 $user_login_status =  array('usermaster_id' => $check_data->user_id,
														'ipAddress' => $_SERVER['REMOTE_ADDR'],
														'loginIn' => date('Y-m-d H:i:s'),
														'logOut' => date('Y-m-d H:i:s'));
														
							 $insert_session = array('table' => 'tbl_user_login_status',
							 						'data' => $user_login_status);
													
							$last_session_id = $this->login_model->InsertRecord($insert_session);
							//$this->session->set_userdata('last_session_id',$last_session_id);
							set_user(array('key'=>'last_session_id','value'=>$last_session_id));
							//print_r($this->session->userdata('last_session_id'));
					 	      //Check User type
							  switch ($check_data->user_type) {
								  case 1 :
								  	  $this->session->set_flashdata('msg', $this->lang->line('user_login_success'));
									  $data = array('status' => true,
											  'response' => array('msg' =>''),'url' => base_url('Dashboard/teacher'));
								  break;
								  case 2 :
								  	$this->session->set_flashdata('msg', $this->lang->line('user_login_success'));
								  	$data = array('status' => true,
										  'response' => array('msg' =>''),'url' => base_url('Dashboard/student'));
								  break;
							  }
						  }else{
							   $data = array('status' => false,
								  'response' => array('msg' =>$this->lang->line('user_login_deleted_error')),'url' => '');
						  }
					 break;
					 case 2 :
						  $data = array('status' => false,
								  'response' => array('msg' =>$this->lang->line('user_login_disapprove_error')),'url' => '');
						  break;
				 }
			}
			//Jsone Return Response
			/*$data = array('status' => false,
						  'response' => '','url' => '');*/
			echo json_encode($data);
			exit;
				
		}else{
			redirect(base_url('login'));
		}
	}
	
	
	public function forgot_password()
	{
		redirect_user();
		$data = '';
		//echo rand_string(25);
		/* ------------------ SEO  Content setting start--------------*/
		$header_array = array('title' => 'Forgot Password',
							  'keyword' => 'Forgot Password',
							  'description' => 'Forgot Password');
		/*------------------ SEO Content setting end ---------------------*/
		$this->load->front_view('forgot_password',$header_array);
	}
	
	public function reset_password()
	{
		//print_r($_POST);
		if($_POST['user_email']){
			$array_user = array('table' => 'tbl_user_registration',
								'fields' => 'user_id,first_name,last_name,email_address',
								'where' => array('email_address' => $_POST['user_email']));
			$check_data = $this->login_model->check_user($array_user);
			if($check_data->user_id){
				
				$user_rest_token = rand_string(25);
				
				UpdateRow('tbl_user_registration', array('reset_password_str' => $user_rest_token), 
							array('user_id' => $check_data->user_id));
							
				//sent mail after register by user
				$user_name = $check_data->first_name.'&nbsp;'.$check_data->last_name;
				$html = str_replace("{{USER_NAME}}",$user_name,FORGOT_PASSWORD);
				$html = str_replace("{{USER_LINK}}",base_url('set-new-password?resetPasswordToken='.$user_rest_token),$html);
				$email_sent = array('to' => $check_data->email_address,
								  'bcc' => '',
								  'html' => $html,
								  'subject' => 'Reset Password');
				//sent_mail($email_sent);
				$this->myEmail->sent_email($email_sent);
							
				$this->session->set_flashdata('msg', $this->lang->line('user_email_reset_success'));
				redirect(base_url('login'));
				
			}else{
				$this->session->set_flashdata('msg', $this->lang->line('user_email_reset_error'));
				redirect(base_url('forgot-password'));
			}
			//print_r($check_data->user_id);

		}else{
			$this->session->set_flashdata('msg', $this->lang->line('user_email_empty_error'));
			redirect(base_url('forgot-password'));
		}
	}
	
	
	public function set_new_password()
	{
		date_default_timezone_set('America/Chicago'); // CDT
		redirect_user();
		$data = array();
		//echo $_GET['resetPasswordToken'];
		$array_user = array('table' => 'tbl_user_registration',
								'fields' => 'user_id,first_name,last_name,email_address,modified',
								'where' => array('reset_password_str' => $_GET['resetPasswordToken']));
		$check_data = $this->login_model->check_user($array_user);
		//print_r($check_data);		
		if($check_data){
			
			//Check time of link
			$start_date = new DateTime($check_data->modified);
			$since_start = $start_date->diff(new DateTime(date('Y-d-m H:i:s')));
			$minu = $since_start->i;
			
			/*echo $minu;
			exit;*/
			if($minu > 30){
				$this->session->set_flashdata('msg', $this->lang->line('user_token_error'));
				redirect(base_url('login'));
			}else{
				 // $data['user_details'] = array();
				$data['user_details'] = $check_data;
				 
				/* ------------------ SEO  Content setting start--------------*/
				$header_array = array('title' => 'Set new password',
									  'keyword' => 'Set new password',
									  'description' => 'Set new password');
									  
									 
				/*------------------ SEO Content setting end ---------------------*/
				$this->load->front_view('set_new_password',$data);
			}
		}else{
			redirect(base_url('login'));
		}
	}
	
	
	public function change_password()
	{
		$user_array = array('user_password' => get_md5($_POST['new_user_password']));
		
		UpdateRow('tbl_user_registration', $user_array, 
				array('user_id' =>$_POST['user_id']));
		//sent mail after register by user
		$user_name = $_POST['first_name'].'&nbsp;'.$_POST['last_name'];
		$html = str_replace("{{USER_NAME}}",$user_name,CHANGED_PASSWORD);
		$html = str_replace("{{USER_LINK}}",base_url(''),$html);
		$email_sent = array('to' => $_POST['email_address'],
						  'bcc' => '',
						  'html' => $html,
						  'subject' => 'Password Changed');
		//sent_mail($email_sent);
		$this->myEmail->sent_email($email_sent);
					
		$this->session->set_flashdata('msg', $this->lang->line('user_email_set_pass_success'));
		redirect(base_url('login'));
				
		
	}
	 
	 
  
}