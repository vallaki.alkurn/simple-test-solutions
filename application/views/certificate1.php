<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Teacher student portal</title>

</head>

<body>
<style>

@font-face {
	font-family: 'bwmodelica-boldcondensed';
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-boldcondensed.eot');
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-boldcondensed.woff') format('woff'), url('fonts/bwmodelica-boldcondensed.ttf') format('truetype'), url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-boldcondensed.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'bwmodelica-extraboldcondensed';
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-extraboldcondensed.eot');
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-extraboldcondensed.woff') format('woff'), url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-extraboldcondensed.ttf') format('truetype'), url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-extraboldcondensed.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'BrushScriptStdMedium';
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/BrushScriptStdMedium.eot');
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/BrushScriptStdMedium.woff') format('woff'), url('fonts/BrushScriptStdMedium.ttf') format('truetype'), url('<?php echo base_url(); ?>assets/front-design/fonts/BrushScriptStdMedium.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'bwmodelica-mediumcondensed';
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-mediumcondensed.eot');
	src: url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-mediumcondensed.woff') format('woff'), url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-mediumcondensed.ttf') format('truetype'), url('<?php echo base_url(); ?>assets/front-design/fonts/bwmodelica-mediumcondensed.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
body {
    font-family: 'bwmodelica-mediumcondensed';
    font-size: 16px;
}
.outer-div-certificet {
	width: 932px;
	height: 502px;
	margin: 147px auto 0;
	background: url(<?php echo base_url(); ?>assets/front-design/images/certificet-bg.jpg);
		background-size: auto;
	position: relative;
	padding: 30px;
	background-size: 100%;
}
.outer-div-certificet::before {
    position: absolute;
    content: "";
    right: 0;
    bottom: 0;
    border-style: solid;
    border-color: transparent #a807a8 transparent transparent;
    border-width: 300px 693px 0 0;
}
.outer-div-certificet .about-grid {
    height: 100%;
}
.inner-div-certificet {
    background: rgba(240,239,239,1);
    background: -moz-linear-gradient(left, rgba(240,239,239,1) 0%, rgba(203,203,203,1) 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(240,239,239,1)), color-stop(100%, rgba(203,203,203,1)));
    background: -webkit-linear-gradient(left, rgba(240,239,239,1) 0%, rgba(203,203,203,1) 100%);
    background: -o-linear-gradient(left, rgba(240,239,239,1) 0%, rgba(203,203,203,1) 100%);
    background: -ms-linear-gradient(left, rgba(240,239,239,1) 0%, rgba(203,203,203,1) 100%);
    background: linear-gradient(to right, rgba(240,239,239,1) 0%, rgba(203,203,203,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f0efef', endColorstr='#cbcbcb', GradientType=1 );
    height: 100%;
    display: inline-block;
    width: 100%;
    position: relative;
}
.inner-div-certificet::before {
    content: "";
    position: absolute;
    left: 28px;
    top: -29px;
    width: 82px;
    height: 142px;
    background: url(<?php echo base_url(); ?>assets/front-design/images/cert-ribin.png);
}
.cert-logo {
    padding: 9px 0 0 121px;
}
.cert-logo img {
    width: 140px;
}
.cert-content {
    text-align: center;
}
.cert-title {
    font-size: 50px;
    text-transform: uppercase;
    font-family: 'bwmodelica-extraboldcondensed';
    line-height: normal;
    padding: 30px 0 0;
}
.cert-sub-title {
    font-family: 'bwmodelica-extraboldcondensed';
    color: #a807a8;
    font-size: 24px;
    text-transform: uppercase;
}
.cert-name {
    color: #a807a8;
    font-size: 50px;
    text-transform: capitalize;
    font-family: 'BrushScriptStdMedium';
    border-bottom: solid 2px #363636;
    min-width: 515px;
    width: auto;
    display: inline-block;
    padding: 30px 0 0;
    margin: 0 0 55px;
}
.cert-score {
    font-size: 20px;
    padding: 0 0 5px;
}
.cert-score .cert-score-txt {
    font-family: 'bwmodelica-extraboldcondensed';
}
.cert-percent {
    font-size: 20px;
    padding: 0 0 5px;
}
.cert-percent-tr {
    color: #a807a8;
    font-size: 30px;
    font-family: 'bwmodelica-extraboldcondensed';
}
.cert-normal {
    font-size: 20px;
    text-transform: capitalize;
    padding: 0 0 5px;
}
.cert-normal {
    font-size: 20px;
    text-transform: capitalize;
    padding: 0 0 5px;
}
.download-link {
    width: 932px;
    margin: 30px auto 147px;
    text-align: right;
}
.download-link a {
    color: #a807a8;
    text-transform: uppercase;
    font-family: 'bwmodelica-boldcondensed';
    font-size: 13px;
}


</style>
	<!-- Middle section start-->
	<?php    $total_per =  ($results->total_mark * 100)/ $results->max_mark; ?>
	<section class="middle_section inner-page">
		<div class="outer-div-certificet" style="">
			<div class="about-grid">
				<div class="inner-div-certificet">
					<div class="cert-logo"><img src="<?php echo base_url(); ?>assets/front-design/images/logo.jpg"></div>
					<div class="cert-content">
						<div class="cert-title">Certificate</div>
						<div class="cert-sub-title">of Achievement</div>
						<div class="cert-name"><?php echo $results->first_name.' '.$results->last_name; ?></div>
						<div class="cert-score">Got a Score of <span class="cert-score-txt"><?php echo $results->total_mark; ?>/<?php echo $results->max_mark; ?></span></div>
						<div class="cert-percent"><span class="cert-percent-tr"><?php echo round($total_per,2); ?>%</span> on</div>
						<div class="cert-normal"><?php echo $results->standard_class_name; ?></div>
						<div class="cert-normal"><?php echo date('F d, Y',strtotime($results->created_at)); ?></div>
					</div>
				</div>
			</div>
		</div>
		
	</section>
</body>
	
    

</html>