<?php error_reporting(0); ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8">

<!-- Viewport Metatag -->
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<!-- Plugin Stylesheets first to ease overrides -->
<!--<link rel="stylesheet" type="text/css" href="plugins/colorpicker/colorpicker.css" media="screen">    -->

<!-- Required Stylesheets -->
<link rel="stylesheet" type="text/css" href="test_summery/bootstrap/css/bootstrap.min.css" media="screen">
<!--<link rel="stylesheet" type="text/css" href="css/fonts/ptsans/stylesheet.css" media="screen">-->
<!--<link rel="stylesheet" type="text/css" href="css/fonts/icomoon/style.css" media="screen">  -->

<link rel="stylesheet" type="text/css" href="test_summery/css/mws-style.css" media="screen">
 <link rel="stylesheet" type="text/css" href="test_summery/css/icons/icol16.css" media="screen">
 <link rel="stylesheet" type="text/css" href="test_summery/css/icons/icol32.css" media="screen">


<!-- Demo Stylesheet -->
<!--<link rel="stylesheet" type="text/css" href="css/demo.css" media="screen">  -->

<!-- jQuery-UI Stylesheet -->
<!--<link rel="stylesheet" type="text/css" href="jui/css/jquery.ui.all.css" media="screen">  -->
<!--<link rel="stylesheet" type="text/css" href="jui/jquery-ui.custom.css" media="screen"> -->

<!-- Theme Stylesheet -->
<link rel="stylesheet" type="text/css" href="test_summery/css/mws-theme.css" media="screen">
<link rel="stylesheet" type="text/css" href="test_summery/css/themer.css" media="screen">
 <link rel="stylesheet" type="text/css" href="test_summery/css/responsive_summery.css" media="screen">


 <link href="assets/css/customstyle.css" rel="stylesheet">
  
 
<title> | Test Summery</title>
   <style type="text/css">
    .floatr
    {
      float:right;
    }

   </style>
   </head>

<body>

    <div id="mws-wrapper" style="overflow-y:auto;  ">
        <!-- Main Container Start -->
        <div id="mws-container" class="clearfix" style="margin-left: 0px; " >


            <div class="container test-summery" >





            	<div class="mws-panel grid_10">
                	<div class="mws-panel-header" style="text-align:center;">
                    	<span style="color:#fff;verticle-align:middle;"> Test Summery</span><!--<i class="icon-table"></i>-->
                    </div>

                    <div class="container" >
                    <div>
                    <h3 class="testsummery-font">Your Test Submitted Successfully</h3>
                    <h3 class="testsummery-font" style="color: #686868;">Thank you for submitting your test.The summery of the test given below.</h3>

                    <h4 class="testnameset" style="color: #686868;"><span class="test-summery-testname">Test Name:</span> <?php echo $results->test_name; ?></h4>
                     <!--<h4  style="color: #686868;"><span style="font-size: 15px; font-weight: bold; color: #3076A0;">Subject Name:</span> SSC   Maths  </h4>  -->
                    </div>
                    <div class="mws-panel-body no-padding" style="overflow-x: auto">
                        <table class="mws-table" style="text-align: center; border: 1px solid #BDBDBD;">
                            <thead>
                                <tr>
                                    <th style="width: 100px;">Total Questions</th>
                                    <th style="width: 100px;">Maximum Marks</th>
                                    <th style="width: 100px;">Total Attempted</th>
                                    <th style="width: 100px;"> Left Questions</th>
                                      <th style="width: 100px;">Correct Ques.</th>
                                        <th style="width: 100px;">InCorrect Ques.</th>
                                        <th style="width: 100px;">Total Time(in min.)</th>
                                          <th style="width: 100px;">Total Taken(in min.)</th>
                                            <th style="width: 100px;">Right Marks</th>
                                              <th style="width: 100px;"> Negative Marks</th>
                                              <th style="width: 100px;"> Total Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                     $total_per =  ($results->total_mark * 100)/ $results->max_mark;
                                  
                     ?>
                                <tr>
                                    <td><?php echo $results->total_question;?><?php //echo "select ans_time  from tbl_button_pallate where test_id=".$testCode." and test_type='".$testType."' and user_id='".$this->id."'"?></td>
                                    <td><?php echo $results->max_mark; ?></td>
                                    <td><?php echo $results->attempt_question;  ?></td>
                                    <td><?php echo $results->not_attempt_question; ?></td>
                                    <td><?php echo $results->total_right_que; ?></td>
                                    <td><?php echo $results->total_wrong_que; ?></td>
                                    <td><?php echo $results->duration; ?></td>
									 <td><?php echo $time_taken ?></td>
									 <td><?php echo $results->total_right_mark; ?></td>
                                    <td><?php echo $results->total_wrong_mark; ?></td>
                                    <td><?php echo $results->total_mark; ?></td>
                                </tr>


                            </tbody>
                        </table>
                    </div>


                   <div style="padding-top: 2%;" >
                   <h3 class="testsummery-score"><span style="color: #1C1C1C; font-weight:  bold;">  <i class="icol-emoticon-smile" style=" padding-top: 0%;  "></i> &nbsp;&nbsp;Your Score is <?php echo round($total_per,2); ?>% </span></h3>


                 </div>

                  </div><!-- container closed here-->


                         
                 




                </div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->

        </div>
        <!-- Main Container End -->
      
    </div>
	

	
	
	
     <script type="text/javascript">
            function close1()
            {
              window.close();
            }


     </script>
    <!-- JavaScript Plugins -->
    <script src="test_summery/js/libs/jquery-1.8.3.min.js"></script>
    <script src="test_summery/js/libs/jquery.mousewheel.min.js"></script>
    <script src="js/libs/jquery.placeholder.min.js"></script>
    <script src="test_summery/custom-plugins/fileinput.js"></script>

    <!-- jQuery-UI Dependent Scripts -->
    <script src="test_summery/jui/js/jquery-ui-1.9.2.min.js"></script>
    <script src="test_summery/jui/jquery-ui.custom.min.js"></script>
    <script src="test_summery/jui/js/jquery.ui.touch-punch.js"></script>

    <!-- Plugin Scripts -->
    
    <!-- Core Script -->
    <script src="test_summery/bootstrap/js/bootstrap.min.js"></script>
    <script src="css/mymodal.css"></script>
   
	
	

	
	
	
	
	<script>
// Get the modal
var modal = document.getElementById('myModal');
$("#forgetModal").show(modal)

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

</script>
</body>
</html>
