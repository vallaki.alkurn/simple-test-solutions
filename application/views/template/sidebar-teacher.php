<?php 
$user_details = get_user(); 
//print_r($user_details->user_id);
$user_details  = SelectData('tbl_user_registration','*',array('user_id'=>$user_details->user_id), 1, NULL);
$profileImg = $user_details->profile_photo  ?   base_url($user_details->profile_photo) : base_url('assets/front-design/images/icon-teacher.jpg');
?>
<section class="sidebar" id="side_bar">
                    <div class="dash-profile">
                        <div class="dash-profile-picouter">
                            <div class="dash-profile-picouter">
                                <img src="<?php echo $profileImg; ?>" width="70">
                            </div>
                        </div>
                        <div class="profile-name">
                            <div class="pro-big-name">
                                <?php echo $user_details->first_name.'&nbsp;'.$user_details->last_name; ?>
								<br><span style="font-size:14px; color:#b4aeae;">Teacher</span>
								<!--<span style="font-size:12px;">Teacher</span>-->
                            </div>
                            <!--<div class="pro-nm-dropdown">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        Teacher
                                    </button>
								<!--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="<?php echo base_url('Dashboard/Teacher/profile');?>">My Profile</a>
									<a class="dropdown-item" href="<?php echo base_url('Dashboard/Teacher/change-password');?>">Change Password</a>
									<a class="dropdown-item" href="<?php echo base_url('Dashboard/logout');?>">Logout</a>
								</div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <div class="dash-menu-section">
                        <div class="dash-menu">
                            <ul>
                                <li class="<?php echo $this->router->fetch_method() == 'index' ? 'active' : ''; ?>">
									<a href="<?php echo base_url('Dashboard/Teacher'); ?>">
										<img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu1.png"><span>Dashboard</span></a></li>
                                <li class="<?php echo $this->router->fetch_method() == 'question_bank' ? 'active' : ''; ?>"><a href="<?php echo base_url('Dashboard/Teacher/question-bank'); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu2.png"><span>Question bank</span></a></li>
								
                                <li class="<?php echo $this->router->fetch_method() == 'invite_student' ? 'active' : ''; ?>"><a href="<?php echo base_url('Dashboard/Invite/invite-student'); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu3.png"><span>Invite students</span></a></li>
								
                                <li class="<?php echo $this->router->fetch_method() == 'transctions' ? 'active' : ''; ?>"><a href="<?php echo base_url('Dashboard/Teacher/transctions'); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu4.png"><span>Payment Transaction</span></a></li>
								
                         <!--       <li class="<?php echo $this->router->fetch_method() == 'history' ? 'active' : ''; ?>"><a href="<?php echo base_url('Dashboard/Teacher/history'); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu5.png"><span>History</span></a></li>-->
								
                                <li class="<?php echo $this->router->fetch_method() == 'reports' ? 'active' : ''; ?>"><a href="<?php echo base_url('Dashboard/Teacher/reports'); ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/icon-dashboardmenu6.png"><span>Reports</span></a></li>
                            </ul>
                        </div>
                    </div>
                	</section>