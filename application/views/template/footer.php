 <!-- Footer start-->
 <?php 
 $user_details = get_user(); 
 ?>
  <footer id="footer">
    <div class="container">
      <div class="footer-inner">
        <div class="row">
          <div class="col-lg-2 col-md-2 col-sm-12"></div>
          <div class="col-lg-2 col-md-2 col-sm-12">
            <div class="footer-menu">
              <div class="footer-menu-heading">Quick Links</div>
              <div class="footer-nevigation">
                <ul>
				<?php 
					if(isset($user_details)){
						if($user_details->user_type == 1){
						?>
<li><a href="<?php echo base_url('Dashboard/Teacher'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Teacher</a></li>
						<?php
						}else{
						?>
<li><a href="<?php echo base_url('Dashboard/Student'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Student</a></li>
						<?php	
						}
				?>
				<?php 
				}else{
				?>
<li><a href="<?php echo base_url('teacher'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Teacher</a></li>
<li><a href="<?php echo base_url('student'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Student</a></li>
				<?php 	
				}
				?>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-12"></div>
          <div class="col-lg-2 col-md-2 col-sm-12">
            <div class="footer-menu">
              <div class="footer-menu-heading">Menu</div>
              <div class="footer-nevigation">
                <?php $pages = pages();  ?>

                <ul>
                  <li><a href="<?php echo base_url('contact-us'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Contact us</a></li>
                  <li><a href="<?php echo base_url('about-us'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> About us</a></li>
                  <li><a href="<?php echo base_url('blog') ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Blog</a></li>

                  <?php if(!empty($pages)) { ?>
                    <?php foreach($pages as $p) { ?>
                      <li><a href="<?php echo base_url('pages/'.$p->id.'/'.$p->slug.'') ?>"><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $p->title; ?></a></li>

                  <?php } } ?>


                </ul>
              </div>
            </div>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-12"></div>
          <div class="col-lg-2 col-md-2 col-sm-12">
            <div class="footer-menu">
              <div class="footer-menu-heading">Contact with us</div>
              <div class="footer-nevigation">
                <ul>
                  <li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                  <li><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                  <li><a href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
        <p>Copyright © <?php echo date('Y');?> Simple Test Solutions. Designed and Developed by Alkurn Technologies.</p>
      </div>
    </div>

  </footer>
  <!-- Footer End-->
</body>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   <!-- <div class="container">-->
        <!-- Jssor Slider Begin -->
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax.js/1.3.1/parallax.min.js"></script>

<!-- Data Tables-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/datatables.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css
"/>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->

 <!-- Validation js-->
<script src="<?php echo base_url(); ?>assets/jquery-validation-1.17.0/dist/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/jquery.steps.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/lk-validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/countdown.js"></script>


  <script>
    $('.cont').addClass('hide');

    $('#question_splitter_1').removeClass('hide');
     
    $(document).on('click','.next',function(){
      //alert('hi')
        last=parseInt($(this).attr('id'));  console.log( last );   
        nex=last+1;
        $('#question_splitter_'+last).addClass('hide');
        
        $('#question_splitter_'+nex).removeClass('hide');
    });
    
    $(document).on('click','.previous',function(){
        last=parseInt($(this).attr('id'));     
        pre=last-1;
        $('#question_splitter_'+last).addClass('hide');
        
        $('#question_splitter_'+pre).removeClass('hide');
    });
            
       /*  setTimeout(function() {
             $("form").submit();
          }, 60000);*/
    </script>


<script>
$( function() {
  //$( ".datepicker" ).datepicker();

/*$("#test_form_date").datepicker({
        numberOfMonths: 1,
		minDate: 0,
		dateFormat: 'dd-mm-yy',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#test_to_date").datepicker("option", "minDate", dt);
			//$("#test_form_date").datepicker("setDate",  new Date());
        }
    });
	
    $("#test_to_date").datepicker({
        numberOfMonths: 1,
		minDate: 0,
		dateFormat: 'dd-mm-yy',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
			//$("#test_form_date").datepicker("option", "maxDate", dt);
        }
    });*/
	
	$("#test_form_date").datepicker({
        minDate: 0,
        maxDate: "+365D",
		dateFormat: 'mm/dd/yy',
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#test_to_date").datepicker("option","minDate", selected)
        }
    });
    $("#test_to_date").datepicker({ 
        minDate: 0,
        maxDate:"+365D",
		dateFormat: 'mm/dd/yy',
        numberOfMonths: 1,
        onSelect: function(selected) {
           $("#test_form_date").datepicker("option","maxDate", selected)
        }
    });  


});
</script>
<script type="text/javascript">
  $(document).ready(function() {
      $("#btn-teacher").click(function(){
        $("#teacher-slide").addClass('active-slide');
        $(this).addClass('btn-active');
        $("#btn-student").removeClass('btn-active');
        $("#student-slide").removeClass('active-slide');
      });
      $("#btn-student").click(function(){
        $("#student-slide").addClass('active-slide');
        $(this).addClass('btn-active');
        $("#btn-teacher").removeClass('btn-active');
        $("#teacher-slide").removeClass('active-slide');
      })
    });

    $(document).ready(function() {
  $('.section-joinus').parallax({
    imageSrc: '<?php echo base_url(); ?>assets/front-design/images/stefan-vladimirov-1299262-unsplash.jpg'
  });

  $('.section-contact').parallax({
    imageSrc: '<?php echo base_url(); ?>assets/front-design/images/brooke-cagle-609880-unsplash.jpg'
  });
});
</script>
<script>
        /*$("#wizard").steps({
            headerTag: "h4",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true
        });
        $( "#wizard-t-1" ).addClass( "completed" );*/
    </script>
</html>