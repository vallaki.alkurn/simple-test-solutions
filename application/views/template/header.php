<?php error_reporting(0); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Teacher student portal<?php //echo $title;?></title>
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/front-design/images/fevicon.png" />
	<meta name="description" content="Teacher student portal<?php //echo $description;?>" />
	<meta name="keywords" content="Teacher student portal<?php //echo $keyword;?>" />
	<!-- <link href="<?php //echo get_bloginfo('template_url') ?>/css/custom.css" rel="stylesheet"> -->
	<link href="<?php echo base_url(); ?>assets/front-design/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front-design/css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front-design/css/responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/front-design/css/custom.css" rel="stylesheet">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/js/jquery.min.js"></script>
	<!--<link href="<?php echo base_url(); ?>assets/front-design/css/ninja-slider.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>assets/front-designjs/ninja-slider.js" type="text/javascript"></script>-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/front-design/engine1/style.css" />
	<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/front-design/engine1/jquery.js"></script>-->
	
	<style>
/*	    .sidebar {*/
/*  height: 100%;*/
/*  width: 0;*/
/*  position: fixed;*/
/*  z-index: 1;*/
/*  top: 0;*/
/*  left: 0;*/
/*  background-color: #23313e;*/
/*  overflow: hidden;*/
/*  transition: 0.5s;*/
/*  padding-top: 60px;*/
/*}*/
.mystyle{
      overflow: visible;
}
.sidebar a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidebar a:hover {
  color: #f1f1f1;
}

.sidebar .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

.openbtn {
  font-size: 20px;
  cursor: pointer;
  background-color: #a806a7;
  color: white;
  padding: 10px 15px;
  border: none;
  width:20%;
  display:none;
}

.openbtn:hover {
  background-color: #444;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

	    
	</style>
	
	 <script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("dash2").style.marginLeft = "25%";
  document.getElementById("side_bar").className = "mystyle";
  document.getElementById("desktop-site").style.marginLeft = "25%";
  document.getElementById("footer").style.marginLeft = "24%";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";

  
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("dash2").style.marginLeft= "0";
  document.getElementById("desktop-site").style.marginLeft = "0";
  document.getElementById("footer").style.marginLeft = "0";
  document.body.style.backgroundColor = "white";
}
</script>
</head>
<body>
 <!----------------- Loader And Messages ---------------> 
<div class="loader" style="display:none;">
	<div class="loader-inner">
		<div class="loader-img">
			<!--<div class="mdl-spinner mdl-js-spinner is-active"></div>-->
			<img src="<?php echo base_url('assets/front-design/images/loaderilk.gif');?>" title="" alt=""></div>	
			<div class="loader-text"><!--Please wait...--></div>
	</div>
</div>
<div class="globel-msg" id="gmsg"></div>
  <!-- Header Start-->
  <header class="desktop-site" id="desktop-site">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="logo">
		  <?php 
		  $user_details = get_user();
		  $user_details  = SelectData('tbl_user_registration','*',array('user_id'=>$user_details->user_id), 1, NULL);
$profileImg = $user_details->profile_photo  ?   base_url($user_details->profile_photo) : base_url('assets/front-design/images/icon-teacher.jpg');
		  $profileImg = $user_details->profile_photo  ?   base_url($user_details->profile_photo) : base_url('assets/front-design/images/icon-teacher.jpg');
		  $base_url = '';
		  if(isset($user_details->user_type)){
			  switch ($user_details->user_type) {
				case 1:
					$base_url =  base_url('Dashboard/Teacher');
					
					break;
				case 2:
					$base_url =  base_url('Dashboard/Student');
					break;
				default:
					$base_url =  base_url();
					break;
			 }
		  }else{
			  $base_url =  base_url();
		  }
		//  	print_r($user_details->user_type);
		
		/*---- Notification ------------*/
		$notification = tbl_notification($user_details->user_id);
		$notification_count = count($notification);
		/*---------------------------------------------*/
		  ?>
		  
		  <div id="main">
		      <button class="openbtn" onclick="openNav()">☰ </button>  
		      <a href="<?php echo $base_url; ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/logo.jpg"></a>
		  </div>
          
          </div>
		  <?php if(check_user_session()){ ?>
			<!-- <div class="login-menu">
				<span><a href="<?php echo base_url('Dashboard/logout'); ?>" class="<?php echo $this->router->fetch_class() == 'login' ? 'active' : ''; ?>">Logout</a></span>
			  </div>-->
			  
			  <div class="login-menu">
                        <div class="dropdown profile-menu">
                            <div class="img-nm">
                                <span class="img-profile"><img src="<?php echo $profileImg; ?>"></span>
                                
                            </div> 
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_details->first_name.'&nbsp;'.$user_details->last_name; ?></button>  
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
<?php if($user_details->user_type == 2){ ?>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Student');?>">Dashboard</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Student/profile');?>">My Profile</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Student/change-password');?>">Change Password</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/logout');?>">Logout</a>
<?php }else{ ?>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Teacher');?>">Dashboard</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Teacher/profile');?>">My Profile</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/Teacher/change-password');?>">Change Password</a>
<a class="dropdown-item" href="<?php echo base_url('Dashboard/logout');?>">Logout</a>
<?php } ?>
                            </div>
                        </div>
					</div>
			  
			  <div class="notification">
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle notificationClick" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-bell" aria-hidden="true"></i>
							<span><?php echo $notification_count; ?></span>
							<?php //echo $user_details->first_name.'&nbsp;'.$user_details->last_name; ?>
						</button>
						<?php 
							if($notification_count > 0){
						?>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<?php 
								foreach($notification as $k => $v){
							?>
							<a class="dropdown-item" href="javascript:void(0)">
								<?php echo $v->notification_msg; ?>
							</a>
							<?php } ?>
						</div>
						<?php
							}
						?>
					</div>
				</div>
			  
			<?php }else{ ?>
			  <div class="main-menu">
				
				<nav class="navigation-menu">
				  <ul>
					<li><a href="<?php echo base_url('teacher'); ?>" 
					class="<?php echo $this->router->fetch_method() == 'Teacher' ? 'active' : ''; ?>">Teacher</a></li>
					<li><a class="<?php echo $this->router->fetch_method() == 'Student' ? 'active' : ''; ?>" href="<?php echo base_url('student'); ?>">Student</a></li>
				  </ul>
				</nav>
			  </div>
			  <div class="login-menu">
				<span><a href="<?php echo base_url('login'); ?>" class="<?php echo $this->router->fetch_class() == 'login' ? 'active' : ''; ?>">Login</a></span>
			  </div>
		   <?php } ?>
        </div>
      </div>
    </div>
  </header>

    <header class="mobile-site">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">

          <div class="logo col-xs-5">
          	<a href="<?php echo $base_url; ?>"><img src="<?php echo base_url(); ?>assets/front-design/images/logo.jpg"></a>
          </div>

 		  <div class="login-menu col-xs-3">
 		  	<img src="" >
 		  </div>

          <div class="col-xs-4 main-menu">
	        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
			                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			                  <span class="navbar-toggler-icon"></span>
			                </button>
			                <div class="collapse navbar-collapse" id="collapsibleNavbar">
			                  <ul class="navbar-nav">
			                    <li class="nav-item">
			                      <a class="nav-link" href="#">Student</a>
			                    </li>
			                    <li class="nav-item">
			                      <a class="nav-link" href="#">Teacher</a>
			                    </li>    
			                  </ul>
			                </div>  
	        </nav>
	      </div>
</div>
</div>
</div>
</header>



  <!-- Header End-->
<?php 
	if($notification_count > 0){
?>
 <script>
 $(document).ready(function(e) {
	$('.notificationClick').on('click',function(){
		var data = 'user_id='+<?php echo $user_details->user_id;?>;
		$.ajax({
				type:'POST',
				url:'<?php echo base_url('Dashboard/update_notification');?>',
				data:data,
	  			dataType:'json',
				beforeSend: function(data){
					//loaderIn();
				},
				success: function (data) {
					console.log(data);
				}
			});	
	});
});
 </script>

 <?php } ?>
  
 