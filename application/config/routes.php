<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/



error_reporting(0);

$route['default_controller'] = 'Home/home_page';
$route['teacher'] = 'Home/Registration/Teacher';
$route['student'] = 'Home/Registration/Student';

$route['payment'] = 'Home/Registration/payment';
$route['login'] = 'Login';
$route['verification-by-email'] = 'Home/Registration/verification_by_email';
$route['forgot-password'] = 'Login/forgot_password';
$route['set-new-password'] = 'Login/set_new_password';



//CMS Pages
$route['about-us'] = 'Home/home_page/about_us';
$route['term-condition'] = 'Home/home_page/term_condition';
$route['contact-us'] = 'Home/home_page/contact_us';
$route['blog/:any'] = 'Home/blog/index';
$route['blog'] = 'Home/blog/index';

$route['blogdetails/:any/:any'] = 'Home/blog/blogdetails';

$route['Masteradmin'] = 'Masteradmin/Auth';

$route['404_override'] = 'Home/home_page/custom404';
$route['pages/:any/:any'] = 'Home/home_page/pages';

$route['translate_uri_dashes'] = true;
//$route['stock/upload-stock'] = 'stock/upload_stock';
