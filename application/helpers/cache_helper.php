<?php 
error_reporting(0);


function img_cache($path,$size=NULL)
{
	/********Image cache store*******/
	if(!empty($size)){
		list($w,$h) = $size;
        $extracted_path = array_pop(explode('/',$path));
		$cache_img = 'var/cache/media/'.$w.'x'.$h.'/'.substr($extracted_path,0,1).'/'.substr($extracted_path,1,1).'/'.$extracted_path;
		if (file_exists($cache_img)) {
			return base_url($cache_img);
		}else{
		$cache_path = 'var/cache/media/'.$w.'x'.$h.'/'.substr($extracted_path,0,1).'/'.substr($extracted_path,1,1);	
		if (!file_exists($cache_path)) {
			mkdir($cache_path, 0777, true);
			}
			$img = file_get_contents($path);
			$im = imagecreatefromstring($img);			
			$width = imagesx($im);			
			$height = imagesy($im);			
			$newwidth = $w;			
			$newheight = $h;			
			$thumb = imagecreatetruecolor($newwidth, $newheight);			
			imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);			
			imagejpeg($thumb,$cache_img); //save image as jpg			
			imagedestroy($thumb);			
			imagedestroy($im);
			return base_url($cache_img);
		}
	}
	else
	{
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$data = file_get_contents($path);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			return $base64;
	}
}
?>