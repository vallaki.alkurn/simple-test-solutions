<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 function get_standard_class()
 {
	$ci=& get_instance();
    $ci->load->database();
	//$sql = "select model_action from moderators_per "; 
	$ci->db->select('standard_class_id,standard_class_name');
	$ci->db->from('tbl_standard_class');
	$ci->db->where(array('is_status' => 1,'is_deleted !=' => 1));
	$ci->db->order_by('standard_class_id','DESC');
	$q = $ci->db->get();
	$res = $q->result();
	return $res; 
 }
 
  function get_subject()
 {
	$ci=& get_instance();
    $ci->load->database();
	//$sql = "select model_action from moderators_per "; 
	$ci->db->select('subject_class_id,subject_class_name');
	$ci->db->from('tbl_subject_class');
	$ci->db->where(array('is_status' => 1,'is_deleted !=' => 1));
	$ci->db->order_by('subject_class_id','DESC');
	$q = $ci->db->get();
	$res = $q->result();
	return $res; 
 }
 
 function get_md5($password){
	return md5($password);
 }
 
 function rand_string($length = NULL)
 {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
 }
 
 //Check check_duplicate Email and mobile no. Id
 function check_duplicate($array)
 {
	$ci=& get_instance();
    $ci->load->database();
	//$sql = "select model_action from moderators_per "; 
	$ci->db->select($array['field']);
	$ci->db->from($array['table']);
	$ci->db->where($array['where']);
	$q = $ci->db->get();
	$res = $q->row();
	return $res ? true : false; 
 }
 
 function random_que($value)
 {
	return $value == 1 ? 'Yes' : 'No'; 	
 }
 
 function display_date($date)
 {
	return date('m/d/Y',strtotime($date)); 
 }
 

 function pages()
 {
	$ci=& get_instance();
    $ci->load->database();
	//$sql = "select model_action from moderators_per "; 
	$ci->db->select('*');
	$ci->db->from('pages');
	$ci->db->where('status',1);
	$q = $ci->db->get();
	$res = $q->result();
	return $res; 
 }
 