<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   /* --- Auth Helper To check User Session ---
   	  --- Lomesh Kelwadkar --------------------
   */
   
   //Set User data after login 
   function set_user($user_details = NULL)
   {
	   $ci =& get_instance();
	   $ci->session->set_userdata($user_details['key'],$user_details['value']);
	   return  $ci->session->userdata($user_details['key']) ? true : false; 
   }
   
   //Get User data after login and check  
   function get_user()
   {
	   $ci =& get_instance();
	   return $ci->session->userdata('user_session');
   }
   
   //Get User data after login and check  
   function check_user_session($url = NULL)
   {
	   $ci =& get_instance();
	   return $ci->session->userdata('user_session') ? true : false;
   }
   
   //Clear User Session data of user
   function redirect_user()
   {
	   if(check_user_session()){
		   $user_session_data = get_user();
		   //print_r($user_session_data->user_type);
		   if($user_session_data->user_type == 1){
			   redirect(base_url('Dashboard/Teacher')); 
		   }else{
			   redirect(base_url('Dashboard/Student')); 
		   }
		}
   }
   
   function check_membership()
   {
	   if(check_user_session()){
		   $user_session_data = get_user();
		   //print_r($user_session_data->user_type);
		   if($user_session_data->paid_membership == 'no'){
			   redirect(base_url('Home/Registration/plan/'.$user_session_data->user_id)); 
		   }
		}
   }
   
   function get_user_id()
   {
	    $user_session_data = get_user();
		//print_r($user_session_data->user_id);
		//exit;
		return $user_session_data->user_id;
   }