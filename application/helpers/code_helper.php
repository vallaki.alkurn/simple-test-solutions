<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function crypto_rand_secure($min, $max)
{
	$range = $max - $min;
	if ($range < 1) return $min; // not so random...
	$log = ceil(log($range, 2));
	$bytes = (int) ($log / 8) + 1; // length in bytes
	$bits = (int) $log + 1; // length in bits
	$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	do {
		$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
		$rnd = $rnd & $filter; // discard irrelevant bits
	} while ($rnd > $range);
	return $min + $rnd;
}

function accessCode($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   // $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
    }

    return $token;
}

function rand_string_url( $length ) {
    $length += 3;
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  
    $str= "";
    $size = strlen( $chars );
    for( $i = 0; $i < $length; $i++ ) {
            if($i == 5 || $i == 11 || $i == 17 )
                $str .= '-';
            else
                $str .= $chars[ rand( 0, $size - 1 ) ];
    }

    return $str;
}


function mysql_date($date)
{
	$res =  date("Y-m-d", strtotime($date));
	return $res;
}

function html_date($date)
{
	if($date){
		$res =  date("m/d/Y", strtotime($date));
	}else{
		$res =  date("m/d/Y");
	}
	return $res;
}

function html_date_s($date)
{
	$res =  date("d", strtotime($date));
	return $res;
}

function html_month($date)
{
	$res =  date("F", strtotime($date));
	return $res;
}

function html_year($date)
{
	$res =  date("Y", strtotime($date));
	return $res;
}

	
 
   
