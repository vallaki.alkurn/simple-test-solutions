<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  /*********Function Get DateTime Format**********/
  function DateTimeFormat($dateTime)
  {
     $dateTimeformat = date('d F Y H:i:s',strtotime($dateTime));
     return  $dateTimeformat;
  }

  /*********Function Get Date Format**********/
  function DateFormat($Date)
  {
     $dateformat = date('m/d/Y',strtotime($Date));
     return  $dateformat;
  }

  /*********Function Get Time Format**********/
  function TimeFormat($Time)
  {
     $Timeformat = date('H:i:s',strtotime($Time));
     return  $Timeformat;
  }
  
  /*********Function Get Time Format**********/
  function NumberOfDay($fromDate, $toDate)
  {
    $date1=date_create($fromDate);
	$date2=date_create($toDate);
	return $diff=date_diff($date1,$date2)->format("%a");
  }
  
  /**********valid 10 number for send sms************/
  function getsmsValidNumber($mobileNo)
  {
	  Preg_match("/\d*(\d{10})/", $mobileNo, $match);
	  return $match[1];
	  exit;
  }


  /********function mysql real escape string*************/
  function mysqlRealEscape($MysqRealEscape)
  {
        $RealEscape = mysql_real_escape_string($MysqRealEscape);
        return $RealEscape;
  }

  /*********Code Start SMS Integration API***********/
  function get_data($url)
  {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
  }


  function SmsSent($mob,$message)
  {
    $ci =& get_instance();
		/*$url = "http://websmspanel.com/API/WebSMS/Http/v1.0a/index.php?username=imway&password=imway&sender=IMWSDS&to=".$mob."&message=".urlencode($message);*/
		/*$url = "http://live.websmspanel.net/API/send?username=imway&password=dFxesH6n&from=IMWAYS&to=".$mob."&content=".$message."&dlr=yes&dlr-level=3&dlr-method=GET&dlr-url=http://localhost:2000/receivedlr";*/

    echo $url = 'https://sms.wwwsmsservicesnagpur.in/api/api_http.php?username=imway&password=zu64nZMX&senderid=IMWSDS&to='.$mob.'&text='.$message.'&route=Informative&type=text';
		//$url ='http://www.websmspanel.com/API/WebSMS/Http/v1.0a/index.php?username=imway&password=zu64nZMX&sender=IMWSDS&to='.$mob.'&message='.$message.'';
    $mystring =  get_data($url);
		return $mystring;
  }
  

  function getsystemConfig($system_config_key)
  {
    $ci =& get_instance();
    $ci->db->select('system_config_value');
    $ci->db->from('tbl_system_config');
    $ci->db->where('system_config_key',$system_config_key);
    $query = $ci->db->get();
    //print_r($ci->db->last_query());
    $rowCount= $query->row();
    if($rowCount) {
       return $rowCount->system_config_value;
    }
    else
    {
       return '';
    }
   }

  function getTeacherStatus($status)
  {   
      $a = 'inactive';
      $b = 'active';
      if (strpos($a, strtolower($status)) !== false) {
          $res = 0;
      }
      if(strpos($b, strtolower($status)) !== false) {
          $res = 1;
      }
      if(strpos($a, strtolower($status)) !== 0 && strpos($b, strtolower($status)) !== 0) {
          $res = 'c';
      }
      return $res;
      exit;
  }
   

   function getStudentProfile($user_id)
   {    
        $ci =& get_instance();
        $ci->db->select('*');
        $ci->db->where('user_id',$user_id);
        $ci->db->from('tbl_user_registration');
        $query = $ci->db->get();
        //print_r($ci->db->last_query());
        $row= $query->row();
        return $row;
   }

   function getstandard()
   {    
        $ci =& get_instance();
        $ci->db->select('*');
        $ci->db->where('is_deleted',0);
        $ci->db->where('is_status',0);
        $ci->db->from('tbl_standard_class');
        $query = $ci->db->get();
        //print_r($ci->db->last_query());
        $row= $query->result();
        return $row;
   }
   
   function question_type()
   {    
        $ci =& get_instance();
        $ci->db->select('*');
        $ci->db->where('is_deleted',0);
        $ci->db->from('tbl_q_type_master');
        $query = $ci->db->get();
        $row= $query->result();
        return $row;
   }
 
 
 	function str_replace_q($q = NULL)
	{
		return str_replace('[]','_________________',$q);
	}
   

  function getBLogs($count = NULL,$keyword = NULL, $limit = NULL, $start  = NULL)
  {    
    //= 1 and ttc.is_deleted = 0 and request_status != 4 and tier.is_deleted = 0
    $ci =& get_instance();
    $ci->db->select('*');
    $ci->db->from('posts');
    $ci->db->where('status',1);
    if(!empty($keyword)) {
    $ci->db->where("title LIKE '%$keyword%'");
    }
    if($count) {  
       $row= $ci->db->count_all_results();  
    } else {
       $ci->db->limit($limit, $start);
       $ci->db->order_by('id','desc');
       $query = $ci->db->get();
       $row= $query->result();  
    }
   /* print_r($ci->db->last_query());
  exit;*/
    return $row;
  }
