<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  

   function getInvitedExamList($count = NULL, $limit = NULL, $start  = NULL)
   {    

        $condMatch = array('ttc.is_status'=>1,'ttc.is_deleted'=>0,'tier.is_deleted'=>0);
        //= 1 and ttc.is_deleted = 0 and request_status != 4 and tier.is_deleted = 0
        $ci =& get_instance();
        $ci->db->select('*, tier.id as request_id,  (SELECT subject_class_name FROM tbl_subject_class where tbl_subject_class.subject_class_id = tur.subject_class_id) as subject_class_name');
        $ci->db->from('tbl_invited_exam_requests tier');
        $ci->db->join('tbl_test_creation ttc','tier.exam_id  = ttc.test_id');
        $ci->db->join('tbl_user_registration tur','tur.user_id  = tier.teacher_id');
      //  $ci->db->where("('".date('Y-m-d')."' BETWEEN test_form_date AND test_to_date)");
        $ci->db->where("student_id",$ci->session->userdata('user_session')->user_id);
        //$ci->db->where("student_id",$ci->session->userdata('user_session')->user_id);
        $ci->db->where($condMatch);
        $ci->db->where('request_status !=',4);
        $ci->db->where('request_status !=',2);
        if($count) { 
           $row= $ci->db->count_all_results();  
        } else {
           $ci->db->limit($limit, $start);
           $ci->db->order_by('tier.id', 'desc');
           $query = $ci->db->get();
           $row= $query->result();  
        }
       /* print_r($ci->db->last_query());
		exit;*/
        return $row;
   }


   function getInvitedExamDetails($request_id)
   {    

        $condMatch = array('ttc.is_status'=>1,'ttc.is_deleted'=>0,'tier.is_deleted'=>0);
        //= 1 and ttc.is_deleted = 0 and request_status != 4 and tier.is_deleted = 0
        $ci =& get_instance();
        $ci->db->select('*, tier.id as request_id,  (SELECT subject_class_name FROM tbl_subject_class where tbl_subject_class.subject_class_id = tur.subject_class_id) as subject_class_name, (SELECT count(*) FROM tbl_test_question tqc where test_id = tier.exam_id ) as totalquestion');
        $ci->db->from('tbl_invited_exam_requests tier');
        $ci->db->join('tbl_test_creation ttc','tier.exam_id  = ttc.test_id');
        $ci->db->join('tbl_user_registration tur','tur.user_id  = tier.teacher_id');
      //  $ci->db->where("('".date('Y-m-d')."' BETWEEN test_form_date AND test_to_date)");
        $ci->db->where("student_id",$ci->session->userdata('user_session')->user_id);
        $ci->db->where("student_id",$ci->session->userdata('user_session')->user_id);
        $ci->db->where($condMatch);
        $ci->db->where('request_status !=',4);
        $ci->db->where('request_status !=',2);
        $ci->db->where('tier.id',$request_id);
        $query = $ci->db->get();
        $row= $query->row();  

        $ci->db->select('SUM(tbl_que_creation.que_points) as marks');
        $ci->db->from('tbl_que_creation');
        $ci->db->join('tbl_test_question','tbl_test_question.que_id = tbl_que_creation.que_id','left');
        $ci->db->where('tbl_test_question.test_id',$row->test_id);
        $q = $ci->db->get();
        $res = $q->row();


        $row->marks = $res->marks;
        // echo "<pre>";
        // print_r($row);
        // echo "</pre>";exit;
        //print_r($ci->db->last_query());
        return $row;
   }


   // public function totalExamQ($exam_id)
   // {
   //    $condMatch = array('ttc.is_status'=>1,'ttc.is_deleted'=>0,'tier.is_deleted'=>0);
   //      //= 1 and ttc.is_deleted = 0 and request_status != 4 and tier.is_deleted = 0
   //      $ci =& get_instance();
   //      $ci->db->select('*');
   //      $ci->db->from('tbl_que_creation tqc');
   //      $ci->db->where("('".date('Y-m-d')."' BETWEEN test_form_date AND test_to_date)");
   //      $ci->db->where($condMatch);
   //      $ci->db->where('is_status',1);
   //      $query = $ci->db->get();
   //      $row= $query->row();  
   //      //print_r($ci->db->last_query());
   //      return $row;
   // }

