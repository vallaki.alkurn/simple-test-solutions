<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   /********************* Data-Base Functions *****************************/
   /******************** Select Data ***********************/
	function SelectData($table = NULL, $field = NULL, $cond = NULL, $limit = NULL, $order = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select($field);
		$ci->db->from($table);
		if($cond){
			$ci->db->where($cond);	
		}
		if($order){
			$ci->db->order_by($order['col'],$order['order']);	
		}
		$q = $ci->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		if($res){
			if($limit){
				return $res[0];	
			}else{
				return $res;
			}
		}else{
			return $res;
		}
		
	
	}
	/***************************** Insert-Data ******************************/
	function InsertRow($table = NULL, $data = NULL, $last_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$res = $ci->db->insert($table,$data);
		$insert_id = $ci->db->insert_id();
		if($insert_id){
			return $insert_id;
		}else{
		return false; }
	}
	/***************************** Update-Data ******************************/
	function UpdateRow($table = NULL, $data = NULL, $cond = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->update($table, $data, $cond);
		
		return true;
	}
	/***************************** Delete-Data ******************************/
	function DeleteRow($table = NULL, $cond = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->delete($table,$cond);
		return true;
	}
	/************************************************************************/
	/***************************** Delete-Data ******************************/
	function CountRow($table = NULL, $cond = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		if($cond){
			$ci->db->where($cond);	
		}
		//$ci->db->where(array('user_id'=>get_user_id()));
		return $ci->db->count_all_results($table);
	}
	/************************************************************************/
	
	/***************************** Delete-Data ******************************/
	function totalQues($table = NULL, $cond = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->where($cond);	
		return $ci->db->count_all_results($table);
	}
	
	function total_question($test_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->where('tbl_test_question.test_id',$test_id);	
		return $ci->db->count_all_results('tbl_test_question');
	}
	
	function sent_invitation($test_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->where('tbl_invited_exam_requests.exam_id',$test_id);	
		return $ci->db->count_all_results('tbl_invited_exam_requests');
	}
	
	function student_attempt($test_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->where('tbl_completed_test.test_id',$test_id);	
		return $ci->db->count_all_results('tbl_completed_test');
	}
	
	/***************************** Delete-Data ******************************/
	function totalInvite($cond = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();
		$ci->db->where($cond);	
		return $ci->db->count_all_results('tbl_invited_exam_requests');
	}
	/************************************************************************/
	
	function que_option($que_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select('question_option_id,que_id,que_option');
		$ci->db->from('tbl_question_option_1');
		$ci->db->where('que_id',$que_id);	
		$q = $ci->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res ? $res : false;
	}
	function que_option_pair($que_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select('*');
		$ci->db->from('tbl_question_pair');
		$ci->db->where('question_id',$que_id);	
		$ci->db->order_by('pair_id','RANDOM');
		$q = $ci->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res ? $res : false;
	}
	
	function que_option_pair_c($option_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select('*');
		$ci->db->from('tbl_question_pair');
		$ci->db->where('option_id',$option_id);	
		//$ci->db->order_by('pair_id','RANDOM');
		$q = $ci->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		return $res[0] ? $res[0] : false;
	}
	
	function tbl_question_anser($que_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		
		$ci->db->select('*');
		$ci->db->from('tbl_question_anser');
		$ci->db->where(array('option_answer_id' => $que_id));	
		$q = $ci->db->get();
		$res = $q->result();
		
		return $res ? $res[0] : false;
		
	}
	
	
	function user_question_anser($user_id = NULL,$test_id = NULL,$que_id = NULL,$answer_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		
		$ci->db->select('*');
		$ci->db->from('tbl_user_question_answered');
		$ci->db->where(array('user_id' => $user_id,
							'test_id' => $test_id,
							'question_id' => $que_id,
							'answer_id' => $answer_id));	
		$q = $ci->db->get();
		$res = $q->result();
		
		return $res ? $res[0] : false;
		
	}
	
	
	function tbl_question_anser_pair($que_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		
		$ci->db->select('pair_option');
		$ci->db->from('tbl_question_pair');
		//$ci->db->join('tbl_question_option_1','tbl_question_option_1.question_option_id = tbl_question_pair.option_id');
		$ci->db->where(array('option_id' => $que_id));	
		$q = $ci->db->get();
		$res = $q->row();
		//print_r($res);
		return $res->pair_option ?  $res->pair_option : false;
		//return $res ? $res[0] : false;
		
	}
	
	function tbl_notification($user_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select('*');
		$ci->db->from('tbl_notification');
		$ci->db->where(array('reciver_id' => $user_id,'status' => 0));	
		$q = $ci->db->get();
		$res = $q->result();
		return $res;
	}
	
	function tbl_notification_update($reciver_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->update('tbl_notification', array('status'=>1), array('reciver_id' => $reciver_id));
		return true;
	}
	function SelectAll($table = NULL, $field = NULL, $cond = NULL, $limit = NULL, $order = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select($field);
		$ci->db->from($table);
		
		if($cond){
			$ci->db->where($cond);	
		}
		if($order){
			$ci->db->order_by($order['col'],$order['order']);	
		}
		if($limit){
			$ci->db->limit($limit);	
		}
		$q = $ci->db->get();
		$res = $q->result();
		//print_r($this->db->last_query());
		
			return $res;
	}
	function test_details($test_id = NULL)
	{
		$ci=& get_instance();
        $ci->load->database();	
		$ci->db->select('*');
		$ci->db->from('tbl_test_creation');
		$ci->db->where(array('test_id' =>$test_id ));	
		$q = $ci->db->get();
		$res = $q->row();
		return $res;
	}