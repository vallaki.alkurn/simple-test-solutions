<?php 
/* -------------------------------------*/
/* -------- Lomesh Kelwadadkar -------- */ 
/* -------- Versoin 1.1 	   ---------*/
/* -------------------------------------*/

/*

Define massages for email templation.
Like all changes add defing special 
Characters.

str_replace("{{USER_NAME}}","user_name","{{USER_NAME}}");
{{USER_NAME}} - Replace with on time user registration.
{{USER_LINK}} - Replace with on user provide some Links.

//Example to use global emails.
$html = str_replace("{{USER_NAME}}","Lomesh Kelwadkar",NEW_REGISTRATION_STUDENT);
$html = str_replace("{{USER_LINK}}","http://simpletestsolutions/reg-done?id=328",$html);

$email_s = array('to' => '',
'bcc' => '',
'html' => $html,
'subject' => '',);
sent_mail($email_s);

*/

//New user @registration msg
define('NEW_REGISTRATION_TEACHER','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">Thanks for registering! Your registration request is under admin approval, We will soon notify you regarding approval/disapproval.</td>
						  </tr>');
						  
//New user registration of @Student msg					  
define('NEW_REGISTRATION_STUDENT','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">Thanks for registering! We are super-excited to have you on board. Before you get started, please verify your email address below.</td>
						  </tr>
						  <tr>
                        <td style="text-align: center">
                            <a href="{{USER_LINK}}" style="color:#2870f6;text-decoration:underline;font-size: 14px;">{{USER_LINK}}</a>
                        </td>
                    </tr>');
					
//New user registration of @approve msg							
define('USER_APPROVE','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">Thanks for registering! Admin has approve your registration request, Now you can login on Simple Test Solution website with your login credential.</td>
						  </tr>');
						  
//New user registration of @not approved msg						  
define('USER_NOT_APPROVE','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">Your account has not approved from web admin, kindly contact though <a href="{{USER_LINK}}" style="color:#a707a7;text-decoration:underline">contact form</a> for more details.</td>
						  </tr>');
			
						  
//New user registration of @send msg					  
define('NEW_SEND_REGISTRATION','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">You are invited to register on Simple Test Solution website. You can register through following link.</td>
						  </tr>
						  <tr>
                        <td style="text-align: center">
                            <a href="{{USER_LINK}}" style="color:#2870f6;text-decoration:underline;font-size: 14px;">{{USER_LINK}}</a>
                        </td>
                    </tr>');
					
//New user registration of @invited msg					  
define('INVITED_EXAM','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">You are invited for the test on Simple Test Solution. For more details click on below link.</td>
						  </tr>
						  <tr>
                        <td style="text-align: center">
                            <a href="{{USER_LINK}}" style="color:#2870f6;text-decoration:underline;font-size: 14px;">{{USER_LINK}}</a>
                        </td>
                    </tr>');
					

//New user registration of @payment msg					  
define('PAYMENT','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">You have Successfully completed your payment and subscribed to our annual premium plan on simple text solution.</td>
						  </tr>');
						  
//New user registration of @payment msg					  
define('FORGOT_PASSWORD','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">It seems like you have forgot your password for Simple Test Solution, Click the link below and you&acute;ll redirected to a secure site from which you can set a new password.</td>
                    </tr>
                    <tr>
                        <td style="text-align: center;padding-bottom: 28px;">
                            <a href="{{USER_LINK}}" style="color: #fff;background: #a707a7;font-size: 12px;text-decoration: none;padding: 14px 24px;display: inline-block;border-radius: 5px;">RESET MY PASSWORD</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;text-align: center">If you did not forgot your password you can safely ignore this email.</td>
                    </tr>');
					

//New user registration of @payment msg					  
define('CHANGED_PASSWORD','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">The password for your Simple Test Solution account on <a href="{{USER_LINK}}" >'.base_url().'</a> has successfully been changed.</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;text-align: center">If you did not initiate this change, please contact your administrator immediately.</td>
                    </tr>');
	
	
	//New user registration of @approve msg							
define('CONTACT_US','<tr>
							<td style="border-top:solid 1px #e9d1e7; padding:43px 37px;border-bottom:solid 10px #a707a7;">
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">Hello {{USER_NAME}},</td>
									</tr>
									<tr>
										<td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">Thanks for Get in touch. <br> Admin contact as soon as possible.</td>
						  </tr>');
					
					
/*
	Email Array
	array('to' => '',
		  'bcc' => '',
		  'html' => '',
		  'subject' => '',)
*/
function sent_mail($array = NULL)
{
	$html  = email_header();
	$html .= email_body($array['html']);
	$html .= email_footer();
	
	$ci=& get_instance();
	$ci->load->library('email');
	
	//SMTP & mail configuration
	$config = array(
		'protocol'  => 'smtp',
		'smtp_host' => 'ssl://smtp.googlemail.com',
		'smtp_port' => 465,
		'smtp_user' => 'lomesh.creativelogi@gmail.com',
		'smtp_pass' => 'Lomesh@2019',
		'mailtype'  => 'html',
		'charset'   => 'utf-8'
	);
	$ci->email->initialize($config);
	$ci->email->set_mailtype("html");
	$ci->email->set_newline("\r\n");

	/////////////// MAIL Function /////////////////////////////
	$ci->email->from('simpletextsolution@gmail.com', 'Simple Test Solution');
	$ci->email->to($array['to']);
	$ci->email->cc('lomesh.creativelogi@gmail.com');
	if($array['bcc'] != 'reset'){
		$ci->email->bcc('lomesh.creativelogi@gmail.com');
	}
	$ci->email->set_mailtype('html');
	$ci->email->subject($array['subject']);
	$ci->email->message($html);
	$mail = $ci->email->send();
	//$ci->email->print_debugger();
	//print_r($mail);
	//echo $html;
	//exit;
	return $mail ? true : false;
}

function email_header()
{
	$imgae_logo = base_url('media/logo.png');
	$imgae_mob = base_url('assets/front-design/email-images/logo.png');	
	/********************** Email head ************************************/
	$html = '<table border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;border:0;width:700px;color:#616161; font-family: Arial, Helvetica, sans-serif;">
    <tbody>
        <tr>
            <td style="border-top:solid 10px #a707a7; background:#fde7fc;padding:28px 0;text-align: center;"><a href="#"><img src="'.$imgae_mob.'"></a></td>
        </tr>';
	
	return $html;
}
function email_body($body  = NULL){
	/********************** Email Body ************************************/
	$html = $body;
	return $html;
}
function email_footer()
{
	$html = '<tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;line-height: 26px;text-align: center;padding: 17px 0 12px;">Stay connected with us!</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;color:#616161; font-size: 14px;line-height: 26px;text-align: center;">We wish you a great experience!</td>
                    </tr>
                    <tr>
                        <td style="color:#616161; font-size: 14px;padding: 39px 0 11px;">Cheers,</td>
                    </tr>
                    <tr>
                        <td style="color:#616161; font-size: 14px;">Simple Test Solution Team!</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;color:#616161; font-size: 14px;padding: 36px 0 0;"><a href="#" style="color:#616161; font-size: 14px;text-decoration: none;padding: 0 7px;">Simple Test Solution</a>|<a href="#" style="color:#616161; font-size: 14px;text-decoration: none;padding: 0 7px;">Facebook</a>|<a href="#" style="color:#616161; font-size: 14px;text-decoration: none;padding: 0 7px;">Twitter</a>|<a href="#" style="color:#616161; font-size: 14px;text-decoration: none;padding: 0 7px;">LinkedIn</a></td>
                    </tr>
                </table>
            </td>
        </tr>
            
    </tbody>
</table>';
	return $html;
}


function play_sms($sms_array = NULL)
{
	//request parameters array
	$requestParams = array(
		'APIKEY' => 'XhkEkR7V5US1ubrQ3NOXEw',
		'senderid' => 'EASYKP',
		'channel' => 'Trans',//'Trans',
		'DCS' => '0',
		'flashsms' => '0',
		'number' => $sms_array['mobile_no'],
		'text' => $sms_array['sms_msg'],
		'route' => '6',
	);

	//merge API url and parameters
	$apiUrl = "http://smppsmshub.in/api/mt/SendSMS?";
	foreach($requestParams as $key => $val){
		$apiUrl .= $key.'='.urlencode($val).'&';
	}
	$apiUrl = rtrim($apiUrl, "&");
	//echo $apiUrl;
	//echo '<br>';
	//API call
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $apiUrl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($ch);
	//print($data);
	curl_close($ch);	
}