<?php
$lang['system_config_page_title'] = 'System Config';
$lang['system_config_form_title'] = 'Update System Config';

$lang['system_company_name_label'] = 'Company Name';
$lang['system_company_address_label'] = 'Company Address';
$lang['system_company_website_label'] = 'Website';
$lang['system_company_email_label'] = 'Email';
$lang['system_company_phone_label'] = 'Company Phone';
$lang['system_company_fax_label'] = 'Fax';

$lang['system_dateformat_label'] = 'Date Format';
$lang['system_timeformat_label'] = 'Time Format';


$lang['system_protocol_label'] = 'protocol';
$lang['system_path_to_sendmail_label'] = 'Path to Sendmail';
$lang['system_smtp_server_label'] = 'SMTP Server';
$lang['system_smtp_port_label'] = 'SMTP Port';
$lang['system_smtp_encryption_label'] = 'SMTP Encryption';
$lang['system_smtp_timeout_label'] = 'SMTP Timeout (s)';
$lang['system_smtp_username_label'] = 'SMTP Username';
$lang['system_smtp_password_label'] = 'SMTP Password';

?>