<?php
$lang['save_button'] = 'Save';
$lang['save_and_continue_button'] = 'Save & Continue';
$lang['reset_button'] = 'Reset';

//Validation Message
$lang['standard_class_name_error'] =  'standard_class_name';
$lang['standard_class_name_erro_name'] = 'standard / class field is required.';
$lang['action'] = 'Action';

//validation message
$lang['subject_class_name_error'] =  'subject_class_name';
$lang['subject_class_name_erro_name'] = 'subject field is required.';

//Validation Message Student
$lang['studetn_class_first_name_error'] =  'first_name';
$lang['standard_class_first_name_erro_name'] = 'student first name field is required.';

$lang['studetn_class_last_name_error'] =  'last_name';
$lang['standard_class_last_name_erro_name'] = 'student last name field is required.';

$lang['studetn_class_email_address_error'] =  'email_address';
$lang['standard_class_email_address_erro_name'] = 'email ID field is required.';
//$lang['standard_class_email_valid_address_erro_name'] = 'please enter valid email address.';

$lang['studetn_class_school_collage_error'] =  'school_collage';
$lang['standard_class_school_collageerro_name'] = 'school/college field is required.';

$lang['studetn_class_standard_error'] =  'standard_class_id';

//Validation Message Teachers
$lang['duplicat_email_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>email address already exists</div>';

$lang['duplicat_phone_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>your phone already exists</div>';

$lang['reg_success_teacher'] =   '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Thanks for registering!</strong> You can login now.</div>';

$lang['reg_success_student'] =   '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Thanks for registering!</strong>  We are super-excited to have you on board. Before you get started, please verify your email address as per provide link on email.</div>';


$lang['server_error'] =   '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>error_</div>';


//Teacher validation
$lang['teacher_organization_name_error'] =  'organization';
$lang['teacher_organization_name_erro_name'] = 'organization field is required.';

$lang['teacher_organization_name_error'] =  'organization';
$lang['teacher_organization_name_erro_name'] = 'organization field is required.';

$lang['teacher_your_phone_name_error'] =  'your_phone';
$lang['teacher_your_phone_name_erro_name'] = 'your phone field is required.';

$lang['teacher_subject_name_error'] =  'subject_class_id';
$lang['teacher_subject_name_erro_name'] = 'subject field is required.';

$lang['teacher_organization_phone_name_error'] =  'organization_phone_number';
$lang['teacher_organization_phone_name_erro_name'] = 'please enter only digits..';


//User Login validation msg
$lang['user_login_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Wrong credential</div>';

$lang['user_login_inactive_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>your account is under admin verification</div>';

$lang['user_login_disapprove_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your account is disapproval by admin.</div>';

$lang['user_login_deleted_error'] =  '<div class="alert alert-danger" role="alert">Your account is deleted by admin.</div>';

$lang['user_email_reset_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>your email address not exists in our database</div>';

$lang['user_email_empty_error'] = '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>email address field is required.</div>';

$lang['user_email_reset_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You will receive a password recovery link at your email address in a few minutes.</div>';
$lang['user_verify_account_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Your email has been confirmed and your account is now verified.</div>';

$lang['user_email_set_pass_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your password has been changed successfully.</div>';

$lang['user_token_error'] = '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your password reset token has expired.</div>';

$lang['user_login_success'] = '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully logged in.</div>';

$lang['user_profile_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully updated the profile .</div>';

$lang['user_change_pass_error'] = '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All field is required.</div>';

$lang['user_change_pass_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully updated your password</div>';

$lang['user_current_pass_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Wrong Current Password.</div>';

$lang['file_size'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>File is over 10Mb in size!</div>';

$lang['create_test_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully created the test, now you can add questions.</div>';


$lang['update_test_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully updated test</div>';

$lang['question_created_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully created Question</div>';

$lang['duplicat_test_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have successfully created Duplicate Test.</div>';

$lang['question_updated_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have updated Question successfully.</div>';

$lang['sent_invitation_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Invitation Sent successfully.</div>';

$lang['sent_invitation_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You have already sent request for test.</div>';

$lang['set_test_q_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>please check at least one test.</div>';

$lang['set_test_q_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Added successfully.</div>';

$lang['delete_test_q_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Deleted successfully.</div>';

$lang['set_test_qu_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>already in exist on test.</div>';

$lang['set_test_not_error'] =  '<div class="alert alert-danger" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Test not exist.</div>';

$lang['invited_test_info'] =  '<div class="alert alert-info" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>You dont have any invitation request.</div>';

$lang['contact_success'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Thanks for Get in touch. Admin contact as soon as possible..</div>';


$lang['set_notification_test'] =  'Invite test by {{username}}';

$lang['set_notification_accept'] =  'Accept your test request by {{username}}';

$lang['set_notification_decline'] =  'Decline your test request by {{username}}';

$lang['set_notification_start'] =  'Start test by {{username}}';

$lang['set_notification_submit'] =  'Submit test by {{username}}';
$lang['request_accept'] =  '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Request accepted successfully.</div> ';

$lang['request_decline'] =   '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Request declined successfully.</div> ';

$lang['cancelled_payment'] =   '<div class="alert alert-success" role="alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Your payment request cancelled successfully.</div> ';

?>