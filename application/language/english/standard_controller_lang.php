<?php
$lang['standard_list_main_title'] = 'Standard';
$lang['standard_list_title'] = 'Standard List';
$lang['standard_add_title'] = 'Add Standard';
$lang['add_standard_input_title'] = 'Standard / Class';
$lang['add_standard_input_status'] = 'Status';
$lang['dropdown_status_label_1'] = 'Active';
$lang['dropdown_status_label_0'] = 'Inactive';
$lang['select_standard'] = 'Select Standard';


$lang['add_subject_input_title'] = 'Subject';
$lang['select_subject'] = 'Select Subject';
$lang['subject_list_main_title'] = 'Subject';
$lang['subject_list_title'] = 'Subject List';
$lang['add_subject_input_title'] = 'Subject';
$lang['add_subject_input_status'] = 'Status';
$lang['subject_add_title'] = 'Add Subject';

//Student
$lang['student_profile_main_title'] = 'Student Profile';
$lang['student_list_main_title'] = 'Student';
$lang['add_standard_sr_no'] = 'Sr No';
$lang['add_standard_name_title'] = 'First Name';
$lang['add_standard_last_name_title'] = 'Last Name';
$lang['add_standard_email_title'] = 'Email';
$lang['add_standard_school_title'] = 'School/College';
$lang['add_registration_date_title'] = 'Registration Date';

$lang['student_list_all_main_title'] = 'All Student';
$lang['student_list_approved_main_title'] = 'Approved Student';
$lang['student_list_non_approved_main_title'] = 'Non Approved Student';
$lang['student_list_dis_approved_student_main_title'] = 'Dis Approved Student';

$lang['teacher_list_all_main_title'] = 'All Teachers';
$lang['teacher_list_approved_main_title'] = 'Approved Teachers';
$lang['teacher_list_non_approved_main_title'] = 'Non Approved Teachers';
$lang['teacher_list_dis_approved_student_main_title'] = 'Dis Approved Teachers';
$lang['teacher_list_main_title'] = 'Teachers';

$lang['add_standard_subject_title'] = 'Subject/Class';


$lang['student_update_profile_main_title'] = 'Student';
$lang['student_update_title'] = 'Update Student Profile';
$lang['student_add_title'] = 'Add Student Profile';


$lang['teacher_update_profile_main_title'] = 'Teacher';
$lang['teacher_update_title'] = 'Update Teacher Profile';
$lang['teacher_add_title'] = 'Add Teacher Profile';

$lang['add_organization_teacher_title'] = 'Organization';
$lang['add_organization_phone_teacher_title'] = 'Organization Phone Number';
$lang['add_your_title_teacher_title'] = 'Your Title';
$lang['add_your_phone_teacher_title'] = 'Your Phone';
$lang['add_address_teacher_title'] = 'Address';
$lang['add_subject_teacher_title'] = 'Subject';
$lang['teacher_profile_main_title'] = 'Teacher Profile';
?>